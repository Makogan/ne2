# Contributing
[To Home Page](../README.md)

## Copyright of the contributed code

NeverEngine is first and foremost a passion project, and as such, it is very important to
the main author to retain control over the project. All contributors will be asked to
donate the copyright to the code they submit to the project. This is not out of a desire
to exploit anyone, but many projects in the pass have had important decissions blocked,
for example a license change, change of finance model or similar, because some users
are no longer reachable.

The project will remain FOSS, even if a fork is made for private profit (should the need
arise to feed the main developer :) ) a FOSS fork will always be available for the
community.

Once more, I do not wish to take advantage of anyone and if the above is not reasonable to
you, I understand, I just have spent too much time and effort making the project and I am
not ready to let it go.

## Style guide

This is a work in progress and more items need to be added, if in doubt, ask the main
maintainer Makogan, as it probably should be added to this document.

* Functions, classes and structs should be camel cased, e.g. `MyFancyClass` or `MyCoolFunction`.
* Variables should be snake cased, e.g. `local_variable`.
* Names should be descriptive and abreviations are to be avoided unless
they are ubiquitous, e.g. `url` is fine but `FunImpl` is not, prefer `FunctionImplementation`.
* Comments matter, a function can always benefit of 2 or 3 comments describing the overall structure of the body, for example:

```cpp
void CreateBigBang()
{
    // In the beginning there was darkness, before the appearance of matter.
    Matter matter = CreateAllMatter().
    ...
    // Then the big expansion started.
    universe.BeginExmapnsion().
    ...
    // Millions of years pass and then life apperas in a small planet.
    earth.CreateLife().
    ...
}
```

* Copy the structure of the code you see around the place where you are doing your edits.

* Whitespace is your friend use it to group or separate code according to sensible relationships. For example a series of variable initializations in a function probably should be in the same block.

* Use clang-format with your editor. The `.clan-format` configuration file is on the root of the project.

* Test your assumptions judiciously, use `Assert` to test as many things as you think are relevant, like for example that a pointer isn't null or that a numeric variable is in the correct range.

* NeverEngine provides an `Assert` method that should be used over the standard `assert`. `Assert` provides more logging capabilities, for example you can format error messages. For example
`Assert("I like {0}, and {1}.", candy.ToString(), to_string(M_PI));`
Would print an error message containing the string `I like candy and 3.14`.

* Excercise empathy, when coding always think "If I, or someone else has to work with this code one year from now, how easy will it be for them to understand it and work with it". Code is meant to be read by others, and it is everybody's responsibility that it is easy to read and use.

## Git and commits

NeverEngine uses git for version control. There are certain rules to ensure the project remains sane.

* DO NOT REWRITE HISTORY, do not use commands that can alter the git history, for example `git rebase`.

* Do not create commits that do not compile. At the bare minimum the demo you are currently working on should compile before making a commit.

* There are 2 kinds of commits, work in progress (feature is not complete), should be prefaced with WIP (work in progress). These commits don't need to compile all demos. Finished commits should have no prefix. These commits should compile for all demos before asking for a merge.

* Commits should have a mandatory short header(i.e up to ~60 characters) summarizing the changes and an optional body adding more details.

* All commits should be in the past tense and describe the changes made in that specific commit, for example:
    - `WIP Developed 3 helper methods to complete the DoomsDayRay feature.`
    - `Fully Implemented the DoomsDayRay feature.`
    - ```
        Implemented the WaterMagnetInator.

        As suggested by Dr. D, we implemented a function to attract water to a given point, like a water magnet.

* As always, excercise empathy, think what it will be like for a future reader to see your commit message. It should be straighforward for them to understand what happened in that commit.