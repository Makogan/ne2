## NeverEngine Introduction

NeverEngine is a low level rendering engine designed with the purpose of making
the task of testing rendering techniques easier. It tries to provide a healthy balance of
abstraction and exposure, to make it easier to use existing functionality without much
work, but also being versatile enough to be easily extensible.

![WalkingFox](Docs/images/WalkingFox.gif)

## Motivation

Why another rendering engine? Unreal and Unity already cover a large amount of the use
cases for game development. However, I find them unsuitable for graphics development.
Both tools are big, heavy and bury the graphics APIs under layers and layers of
abstractions. Both tools are also very old, Unreal now has 20 years and Unity has 15.
They are both oriented towards non-programmers, with the goal of helping game designers
achieve their goals as simply as possible.

However, this makes them very hard to modify from a programming perspective. Unreal alone
is 2 million lines of code, without adding application logic. Customizing, or worse,
replacing their rendering pipeline is something only the most seasoned developers have
a shot at doing. The tools simply have not been built with the idea of being easily
manipulated by the programmer.
> Note: I am talking about the graphics pipeline specifically. Unreal has excellent
> customisability for game logic.

A simple example to prove my point. Go and try to roll your own GPU driven cloth
simulation in either tool. You will probably find that rolling your own from scratch
is probably easier.

NeverEngine aims at alleviating the problem. It tries to be small and to expose the
Vulkan API as much as possible. It does not provide a visual material system, nor a
does it try to be GUI centric. Its goal is not to provide abstractions, but rather
an expressive system to make it as easy as possible to make those abstractions yourself.
However, I will also provide plugins to hook into the engine, to provide functionality
for common use cases.

Useful docs:
- [Vulkan information](Docs/VulkanInfo.md)
- [Half edge data structure](Docs/HalfEdge.md)
- [glTF Skinning](Docs/glTFSkinning.md)
- [Voxel Tracing](Docs/VoxelTracing.md)
- [Dual Contouring](Docs/DualContouring.md)
- [Shader add-ons](Docs/Shaders.md)

## Building

In all cases, make sure pip and vulkan capable drivers are installed in the system. For vulkan, make sure the commands `vulkaninfo` and `vkcube` run without errors.

In all cases we rely on conan and meson to build.

### Ubuntu Linux
Switch into the root directory (i.e. do `cd neverengine`). And in an ideal scenario all
you need to do is `source setup.sh` to install al dependencies and build. From then on,
from the build directory, to compile all you need to do is `meson compile <Target>` and `./<Target>` to run. Note
`setup.sh` will ask you for sudo permissions when you run it.

Make sure you have installed the latest graphics drivers and *reboot afterwards*, e.g `sudo apt install nvidia-driver-460` followed by `reboot` for nvidia based gpus. If you have a pristine installation
you might need to do `sudo sed -i '/cdrom/d' /etc/apt/sources.list` before calling the setup script.

For some of the changes to kick in, you might want to restart your shell after running the script. This
is especially important for validaiton layers to be found by the executable as the setup process modifies your
`~/.profile`. Alternatively do `source ~/.profile` if the prior didn't work.

This will only compile the library, if you want to compile one of the examples do:

`meson compile <example>`

For example:

`meson compile BaseExample`

#### Problems

If you run into problems with the script and it doesn't run properly. You might need to manually run the installation process.

* First make sure you have installed the necessary system packages (look at `setup.sh` to see which ones they are).

* Then run

    `pip install -r Scripts/installer/python_requirements`

    to install the necessary python packages.

* Then export the environment variable:

    `exort PKG_CONFIG_PATH=echo $(realpath .)/build`.

* Run the 2 lines:

    `conan profile update settings.compiler.libcxx=libstdc++11 default`

    `conan install . -if build -s build_type=Debug`

  To setup and instal dependencies through conan.

* Run

    `export VK_LAYER_PATH=$(sed -n 's/prefix=//p' build/vulkan-validationlayers.pc)`

  To setup validation layers.

* Finally do:

    `conan source . -if build`

    `conan build . -if build`

    The first command installs some dependencies that are not provided as conan packages. The second command builds all dependencies and targets in NeverEngine for the first time (remember to always build through meson afterwards, conan is just to setup the project).

* Make sure your c++ version is at least gcc 11 or equivalent.

* Make sure your meson verison is at least 0.59.1.

## Gallery

Gaussian Subdivision cube

![Cube](Docs/images/gcube.png)

Custom Terrain Generation Algorithm

![Terrain](Docs/images/Terrain.png)

## Supporting Development

If you like this project and what is in it, you can buy me a coffee,
or for long term support you can be my patron :)

[Donate](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=23JGHZRD4692J&item_name=Support+the+development+of+my+engine.&currency_code=CAD)

[Patreon](https://www.patreon.com/Makogan)

## Contributing

NeverEngine welcomes anyone that wants to contribute to the project to do so. However, we
ask that you first read the project's [contributing guide](Docs/CONTRIBUTING.md).

### Contributors

* Camilo Talero

* Kiarash Jamali

* KadaB

### Supporters

* Josh Beaker