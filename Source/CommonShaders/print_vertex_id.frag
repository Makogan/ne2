#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec2 uv;
layout(location = 1) in flat int digit_num;
layout(location = 2) in flat int in_code;

layout(location = 0) out vec4 color_out;

layout(binding = 1) uniform sampler2D font_atlas;

const int number_offset = 16;
const int characters_in_side = 10;

vec2 CodeToUV(int code)
{
    code = code + number_offset;
    float x = code % characters_in_side;
    float y = code / characters_in_side;

    const float scale = 1.f / characters_in_side;

    float cut_off = 1.f / float(digit_num);
    float u = mod(uv.x, cut_off) * digit_num;

    return (vec2(x, y) + vec2(u, uv.y)) * scale;
}

void main()
{
    int digit = int(floor((1.f - uv.x) * digit_num));
    int code = (in_code / int(pow(10, digit))) % 10;

    float color = texture(font_atlas, CodeToUV(code)).r;
    color_out = vec4(vec3(color), color);
}