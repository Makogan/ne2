#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(points) in;
layout(triangle_strip, max_vertices = 6) out;

layout(location = 0) in vec3[] in_positions;
layout(location = 1) in vec3[] in_normals;
layout(location = 2) in int[] in_ids;

layout(location = 0) out vec2 out_uv;
layout(location = 1) out flat int out_digit_num;
layout(location = 2) out flat int out_code;

layout(binding = 0) uniform MVPOnlyUbo
{
    mat4 model;
    mat4 view;
    mat4 proj;
};

const float side_length = 0.01;
const float outwards_offset = 0.05;

void main()
{
    const vec3 in_position = in_positions[0];
    const vec3 in_normal = in_normals[0];

    const int index = in_ids[0];
    const int digit_num = int(floor(log(max(index, 1)) / log(10) + 0.001)) + 1;
    const vec3 side = vec3(view[0][0], view[1][0], view[2][0]) * digit_num;
    const vec3 up = vec3(view[0][1], view[1][1], view[2][1]);

    // Create first triangle
    for(int i=0; i < 3; i++)
    {
        const int x = (i % 2);
        const int y = (i / 2);

        vec3 offset = 
            (side * (x - 0.5) + up * (y -0.5)) * side_length + in_normal * outwards_offset;
        gl_Position = proj * view * model * vec4(in_position + offset, 1.0);

        out_uv = vec2(x, y);
        out_code = index;
        out_digit_num = digit_num;

        EmitVertex();
    }
    EndPrimitive();

    // Create second triangle
    for(int i=3; i > 0; i--)
    {
        const int x = (i % 2);
        const int y = (i / 2);

        vec3 offset = 
            (side * (x - 0.5) + up * (y -0.5)) * side_length + in_normal * outwards_offset;
        gl_Position = proj * view * model * vec4(in_position + offset, 1.0);

        out_uv = vec2(x, y);
        out_code = index;
        out_digit_num = digit_num;

        EmitVertex();
    }
    EndPrimitive();
}