/**
 * Input Description:
 *
 * @depth_test: true
 * @topology: triangle list
 * @line_width: 1
 * @polygon_mode: fill
*/
#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 in_position;
layout(location = 1) in vec2 in_tex_coord;
layout(location = 2) in vec3 in_normal;

layout(location = 0) out vec3 position;
layout(location = 1) out vec2 tex_coord;
layout(location = 2) out vec3 normal;

layout(binding = 0) uniform MVPOnlyUbo {
    mat4 model;
    mat4 view;
    mat4 proj;
};

void main()
{
    vec3 model_position = (model * vec4(in_position, 1.0)).xyz;
    gl_Position = proj * view * vec4(model_position, 1.0);

    position = model_position;
    normal = (model * vec4(in_normal, 0)).xyz;
    tex_coord = in_tex_coord;
}