#version 450
#extension GL_ARB_separate_shader_objects : enable

/**
 * Input Description:
 *
 * @depth_test: true
 * @depth_write: false
 * @topology: point list
 * @polygon_mode: fill
*/

// Binding 0 {
layout(location = 0) in vec3 in_position;
// }

// Binding 1 {
layout(location = 1) in vec3 in_normal;
// }

layout(location = 0) out vec3 position;
layout(location = 1) out vec3 normal;
layout(location = 2) out int id;

void main()
{
    position = in_position;
    normal = in_normal;
    id = gl_VertexIndex;
}