vec4 BlinnPhong(
    vec3 position,
    vec3 normal,
    vec3 camera_position,
    vec3 albedo,
    vec3 light_position,
    vec3 light_color,
    float specularity)
{
    vec4 color = vec4(0);
    vec3 l = normalize(light_position - position);

    vec3 n = normalize(normal);
    vec3 e = normalize(camera_position - position);
    vec3 h = normalize(e + l);

    float intensity = length(light_color);
    light_color = normalize(light_color);
    color = vec4(
        albedo * (vec3(1.f - intensity) + intensity * max(0, dot(n, l))) +
            light_color * max(0, pow(dot(h, n), specularity)),
        1);

    return color;
}