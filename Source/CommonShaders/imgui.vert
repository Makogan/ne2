#version 450
#pragma shader_stage(vertex)
#extension GL_ARB_separate_shader_objects : enable

/**
 * Input Description:
 *
 * @depth_test: false
 * @topology: triangle list
 * @line_width: 1
 * @cull_mode: none
*/

layout(location = 0) in vec2 aPos;      // {format : R32G32Sfloat}
layout(location = 1) in vec2 aUV;       // {format : R32G32Sfloat}
layout(location = 2) in vec4 aColor;    // {format : R8G8B8A8Unorm}

layout(binding = 0) uniform ImguiData {
    vec2 scale;
    vec2 translate;
} pc;

out gl_PerVertex {
    vec4 gl_Position;
};

layout(location = 0) out struct {
    vec4 Color;
    vec2 UV;
} Out;

void main()
{
    Out.Color = aColor;
    Out.UV = aUV;
    gl_Position = vec4(aPos * pc.scale + pc.translate, 0, 1);
}