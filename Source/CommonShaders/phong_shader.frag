#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) out vec4 color_out;

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 tex_coord;
layout(location = 2) in vec3 normal;

layout(binding = 1) uniform CameraInfo {
    vec3 camera_position;
};

layout(binding = 2) uniform sampler2D image;

vec4 BlinnPhong(vec3 pos, vec3 normal)
{
	vec4 color = texture(image, tex_coord);
	vec3 l = vec3(1,1,-1);

    vec3 c = color.rgb;
	vec3 n = normalize(normal);
	vec3 e = camera_position - pos;
	e = normalize(e);
	vec3 h = normalize(e+l);

	color = vec4(c * (vec3(0.1) + 0.9 * max(0, dot(n,l))) +
		vec3(0.9) * max(0,pow(dot(h, n), 1)), 1);

    return color;
}

void main()
{
    color_out = BlinnPhong(position, normal);
}