#pragma once

#include "Core/Core.hpp"

class GLFWwindow;

namespace NEPeripherals
{
class Window : public NECore::NEWindow
{
    unsigned int width = 500;
    unsigned int height = 500;
    GLFWwindow* window;
    bool size_changed = false;

public:
    Window(){};
    Window(unsigned int width, unsigned int height);
    ~Window() override;

    bool IsOpen() const override { return !glfwWindowShouldClose(window); }

    GLFWwindow* GetGLFWWindow() override { return window; }

    std::pair<uint, uint> GetDimensions() const override { return {width, height}; }

    void SetDimensions(uint w, uint h) override { width  = w; height = h; }

    void UpdateDimensions() override;

    void Update() override { glfwPollEvents(); }

    bool CheckSizeChanged() const override { return size_changed; }

    void SetSizeChanged(bool c) override { size_changed = c; }
};

} // namespace NEPeripherals