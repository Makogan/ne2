/** @cond */
#include "Eigen/Dense"
/** @endcond */

#include "Peripherals.hpp"

namespace
{
void WindowSizeCallback(GLFWwindow* w, int width, int height)
{
    using namespace NEPeripherals;
    auto handler = reinterpret_cast<NEPeripherals::InputHandler*>(glfwGetWindowUserPointer(w));
    NEPeripherals::Window& window = handler->GetWindow();

    window.SetDimensions(width, height);
    window.SetSizeChanged(true);
}

} // namespace

std::pair<NECore::NEWindow*, NECore::InputHandler*> InitPeripherals()
{
    const bool glfw_success = NEPeripherals::SetupGLFW();
    Assert(glfw_success, "Failed to setup GLFW");
    auto window = new NEPeripherals::Window(800, 800);
    auto input_handler = new NEPeripherals::InputHandler(*window);

    glfwSetWindowUserPointer(window->GetGLFWWindow(), input_handler);
    glfwSetFramebufferSizeCallback(
        window->GetGLFWWindow(), WindowSizeCallback);

    return {window, input_handler};
}

void DestroyPeripherals(NECore::NEWindow* window, NECore::InputHandler* input_handler)
{
    delete input_handler;
    delete window;
    NEPeripherals::CleanGLFW();
}