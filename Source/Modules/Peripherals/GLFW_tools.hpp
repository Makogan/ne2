#pragma once

#define GLFW_INCLUDE_VULKAN

/** @cond */
#include <string>

#include "GLFW/glfw3.h"
/** @endcond */

namespace NEPeripherals
{
int SetupGLFW();

void CleanGLFW();

std::string GLFW_ErrorToString(int error);
} // namespace NEPeripherals
