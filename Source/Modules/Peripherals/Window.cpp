#include "Window.hpp"

/** @cond */
#include <GLFW/glfw3.h>

#include "vulkan/vulkan.hpp"
/** @endcond */

#include "OutOfModule.hpp"

using namespace std;
using uint = unsigned int;

namespace NEPeripherals
{
Window::Window(uint width, uint height)
{
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    window = glfwCreateWindow(width, height, "Vulkan window", nullptr, nullptr);

    glfwGetWindowSize(window, (int*)&(this->width), (int*)&(this->height));
    Assert(window != nullptr, "Error, window not created");
}

Window::~Window() { glfwDestroyWindow(window); }

void Window::UpdateDimensions()
{
    int w, h;
    glfwGetFramebufferSize(window, &w, &h);
    width = w;
    height = h;
}
} // Peripherals