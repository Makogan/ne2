#include "InputHandler.hpp"

#include "Window.hpp"

using namespace std;

// TODO (ongoing): Add all the necessary missing states for mouse, gamepad, keyboard...
// Function declarations -----------------------------------------------------------------
namespace
{
template<typename First, typename... T> bool IsIn(First&& first, T&&... t)
{
    return ((first == t) || ...);
}

NECore::MouseInputState CalculateMouseInputState(
    bool& left_pressed,
    bool& right_pressed,
    Eigen::Vector2f& position,
    Eigen::Vector2f& offset,
    NECore::MouseInputState current_state,
    GLFWwindow* window)
{
    using namespace NECore;
    // TODO (low): move these to callbacks to properly use glfw
    int current_left_state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
    int current_right_state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT);

    int width, height;
    glfwGetWindowSize(window, &width, &height);

    double new_x, new_y;
    glfwGetCursorPos(window, &new_x, &new_y);
    new_x = 2.f * (new_x / width) - 1.f;
    new_y = 2.f * (new_y / height) - 1.f;

    Eigen::Vector2f new_pos(new_x, new_y);
    offset = new_pos - position;
    position = new_pos;

    if(current_left_state == GLFW_PRESS && !left_pressed)
    {
        left_pressed = true;
        return MouseInputState::LEFT_DOWN;
    }
    if(current_left_state == GLFW_PRESS && left_pressed)
    {
        left_pressed = true;
        return MouseInputState::LEFT_DRAG;
    }
    if(current_left_state == GLFW_RELEASE && left_pressed)
    {
        left_pressed = false;
        return MouseInputState::LEFT_UP;
    }
    if(current_right_state == GLFW_PRESS && !right_pressed)
    {
        right_pressed = true;
        return MouseInputState::RIGHT_DOWN;
    }
    if(current_right_state == GLFW_PRESS && right_pressed)
    {
        right_pressed = true;
        return MouseInputState::RIGHT_DRAG;
    }
    if(current_right_state == GLFW_RELEASE && right_pressed)
    {
        right_pressed = false;
        return MouseInputState::RIGHT_UP;
    }
    if(current_state == MouseInputState::MOVE && offset.norm() > 0.0001)
        return current_state;

    return MouseInputState::NO_ACTION;
}

NECore::KeyActionState CalculateKeyActionState(
    NECore::KeyActionState prior_action,
    int glfw_action)
{
    using namespace NECore;
    if(prior_action == KeyActionState::IDLE)
    {
       if(glfw_action == GLFW_PRESS) return KeyActionState::PRESS;
       return KeyActionState::IDLE;
    }
    if(prior_action == KeyActionState::PRESS)
    {
       if(glfw_action == GLFW_PRESS) return KeyActionState::HELD;
       if(glfw_action == GLFW_REPEAT) return KeyActionState::HELD;
       if(glfw_action == GLFW_RELEASE) return KeyActionState::RELEASE;
       return KeyActionState::PRESS;
    }
    if(prior_action == KeyActionState::HELD)
    {
       if(glfw_action == GLFW_RELEASE) return KeyActionState::RELEASE;
       return KeyActionState::HELD;
    }
    if(prior_action == KeyActionState::RELEASE)
    {
       if(glfw_action == GLFW_PRESS) return KeyActionState::PRESS;
       if(glfw_action == GLFW_REPEAT) return KeyActionState::PRESS;
       if(glfw_action == GLFW_RELEASE) return KeyActionState::IDLE;
       return KeyActionState::IDLE;
    }
    return KeyActionState::IDLE;
}

}  // namespace

// Input Handler -------------------------------------------------------------------------
namespace NEPeripherals
{
InputHandler::InputHandler(Window& in_window)
    : window(&in_window)
{
    glfwSetScrollCallback(in_window.GetGLFWWindow(), ScrollCallback);
    glfwSetKeyCallback(in_window.GetGLFWWindow(), KeyCallback);

    for(auto& key_state : key_states) key_state = KeyActionState::IDLE;
}

void InputHandler::AddEvent(
    const MouseInputState state, MouseInputEvent input_event, void* data)
{
    mouse_event_registry[state].push_back(input_event);
    mouse_data_registry[state].push_back(data);
}

void InputHandler::AddEvent(
    const ScrollInputState state, ScrollEvent input_event, void* data)
{
    scroll_event_registry[state].push_back(input_event);
    scroll_data_registry[state].push_back(data);
}

void InputHandler::AddEvent(KeyEvent input_event, void* data)
{
    key_event_registry.push_back(input_event);
    key_data_registry.push_back(data);
}

void InputHandler::CallEvents()
{
    input_state = CalculateMouseInputState(
        left_button_pressed,
        right_button_pressed,
        mouse_position,
        mouse_offset,
        input_state,
        window->GetGLFWWindow());

    for(uint i = 0; i < mouse_event_registry[input_state].size(); i++)
    {
        auto event = mouse_event_registry[input_state][i];
        auto data = mouse_data_registry[input_state][i];
        event(
            data,
            mouse_position.x(),
            mouse_position.y(),
            mouse_offset.x(),
            mouse_offset.y());
    }

    auto s_state = (ScrollInputState)scroll_state;
    for(uint i = 0; i < scroll_event_registry[s_state].size(); i++)
    {
        auto event = scroll_event_registry[s_state][i];
        auto data = scroll_data_registry[s_state][i];
        event(data, scroll_offset.x(), scroll_offset.y());
    }

    for(uint i = 0; i < key_event_registry.size(); i++)
    {
        KeyEvent event = key_event_registry[i];
        auto data = key_data_registry[i];
        event(data, key_states);
    }

    scroll_state = MouseInputState::NO_ACTION;

    for(auto& key_action_state : key_states)
        if(key_action_state == KeyActionState::PRESS)
            key_action_state = KeyActionState::HELD;
}

// GLFW callbacks ------------------------------------------------------------------------
void InputHandler::CursorPositionCallback(GLFWwindow* window, double xpos, double ypos)
{
    auto& handler = *reinterpret_cast<InputHandler*>(glfwGetWindowUserPointer(window));

    handler.input_state = MouseInputState::MOVE;
}

void InputHandler::ScrollCallback(GLFWwindow* window, double x, double y)
{
    auto& handler = *reinterpret_cast<InputHandler*>(glfwGetWindowUserPointer(window));

    handler.scroll_state = ScrollInputState::SCROLL;
    handler.scroll_offset = Eigen::Vector2f(x, y);
}

// TODO(high): having only one possible active key at a time is an issue. The code
// should allow for multiple keys to be active at the same time.
void InputHandler::KeyCallback(
    GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if(key == GLFW_KEY_UNKNOWN) return;
    Assert(key - GLFW_KEY_SPACE < GLFW_KEY_LAST - GLFW_KEY_SPACE, "");
    auto& handler = *reinterpret_cast<InputHandler*>(glfwGetWindowUserPointer(window));

    const int key_index = key - GLFW_KEY_SPACE;
    handler.key_states[key_index] =
        CalculateKeyActionState(handler.key_states[key_index], action);
}
} // namespace NEPeripherals
