// clang-format off

#define ENUM_TO_STRING_CASE(ENUM) \
    case ENUM: return #ENUM;
#include "GLFW_tools.hpp"

// clang-format on

#include "OutOfModule.hpp"

using namespace std;

namespace NEPeripherals
{
// Convert GLFW error codes to a string
string GLFW_ErrorToString(int error)
{
    switch(error)
    {
        ENUM_TO_STRING_CASE(GLFW_NOT_INITIALIZED);
        ENUM_TO_STRING_CASE(GLFW_NO_CURRENT_CONTEXT);
        ENUM_TO_STRING_CASE(GLFW_INVALID_ENUM);
        ENUM_TO_STRING_CASE(GLFW_INVALID_VALUE);
        ENUM_TO_STRING_CASE(GLFW_OUT_OF_MEMORY);
        ENUM_TO_STRING_CASE(GLFW_API_UNAVAILABLE);
        ENUM_TO_STRING_CASE(GLFW_VERSION_UNAVAILABLE);
        ENUM_TO_STRING_CASE(GLFW_PLATFORM_ERROR);
        ENUM_TO_STRING_CASE(GLFW_FORMAT_UNAVAILABLE);
        ENUM_TO_STRING_CASE(GLFW_NO_WINDOW_CONTEXT);
        default: return "Unrecognized GLFW error code";
    }
}

// Terminate GLFW (cleans memory)
void CleanGLFW()
{
    glfwTerminate();
    Log::RecordLog("GLFW terminated");
}

// Initialize the GLFW library and log problems if any
int SetupGLFW()
{
    // Set callback for error reporting
    glfwSetErrorCallback([](int error, const char* description) {
        string err_code = "Error code: " + GLFW_ErrorToString(error);
        string err_msg = "Error message: " + string(description);
        string msg = err_code + "\n" + err_msg;

        Log::RecordLog(msg);
    });
    // If GLFW was initialized setup cleanup
    if(glfwInit() == GLFW_TRUE)
    {
        return GLFW_TRUE;
    }
    // Otherwise record failure
    else
    {
        Warn(false, "GLFW failed to initialize");
        return GLFW_FALSE;
    }
}

} // namespace NEPeripherals
