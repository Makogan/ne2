#pragma once

/** @cond */
#include <GLFW/glfw3.h>
#include <map>
#include <memory>
#include <vector>

#include "Eigen/Dense"
/** @endcond */

#include "Core/Core.hpp"

using uint = unsigned int;

namespace NEPeripherals
{

class Window;

inline std::string to_string(const NECore::KeyActionState state)
{
    switch(state)
    {
        case NECore::KeyActionState::IDLE: return "IDLE";
        case NECore::KeyActionState::PRESS: return "PRESS";
        case NECore::KeyActionState::HELD: return "HELD";
        case NECore::KeyActionState::RELEASE: return "RELEASE";
        default: return "KeyActionState unrecognized.";
    }
}

using namespace NECore;
class InputHandler : public NECore::InputHandler
{
    static void ScrollCallback(GLFWwindow* window, double x, double y);
    static void CursorPositionCallback(GLFWwindow* window, double xpos, double ypos);
    static void KeyCallback(
        GLFWwindow* window, int key, int scancode, int action, int mods);

    Window* window = nullptr;

    std::map<MouseInputState, std::vector<MouseInputEvent>> mouse_event_registry;
    std::map<MouseInputState, std::vector<void*>> mouse_data_registry;

    std::map<ScrollInputState, std::vector<ScrollEvent>> scroll_event_registry;
    std::map<ScrollInputState, std::vector<void*>> scroll_data_registry;

    std::vector<KeyEvent> key_event_registry;
    std::vector<void*> key_data_registry;

    std::array<KeyActionState, GLFW_KEY_LAST - GLFW_KEY_SPACE> key_states;

    // Input state.
    Eigen::Vector2f mouse_position = Eigen::Vector2f(0, 0);
    Eigen::Vector2f mouse_offset = Eigen::Vector2f(0, 0);
    bool left_button_pressed = false;
    bool right_button_pressed = false;

    uint scroll_state = NO_ACTION;
    Eigen::Vector2f scroll_offset = Eigen::Vector2f(0, 0);

    MouseInputState input_state = NO_ACTION;

  public:
    InputHandler() {}
    InputHandler(Window& window);

    Window& GetWindow() { return *window; }

    void AddEvent(const MouseInputState state, MouseInputEvent event, void* data) override;
    void AddEvent(const ScrollInputState state, ScrollEvent event, void* data) override;
    void AddEvent(KeyEvent event, void* data) override;

    void CallEvents() override;
};
} // namespace NEPeripherals
