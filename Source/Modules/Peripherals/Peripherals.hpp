#include "OutOfModule.hpp"
#include "Window.hpp"
#include "GLFW_tools.hpp"
#include "InputHandler.hpp"

std::pair<NECore::NEWindow*, NECore::InputHandler*> InitPeripherals();
void DestroyPeripherals(NECore::NEWindow* window, NECore::InputHandler* input_handler);