#define ENUM_TO_STRING_CASE(ENUM) \
    case ENUM:                    \
        return #ENUM;  //
#include "VulkanDebugging.hpp"

/** @cond */
#include <signal.h>
/** @endcond */

#include "OutOfModule.hpp"

using namespace std;
using namespace Log;

namespace Renderer {

// Function Declarations -----------------------------------------------------------------
string to_string(VkDebugUtilsMessageSeverityFlagBitsEXT message_severity);

// Function definitions ------------------------------------------------------------------
VKAPI_ATTR VkBool32 VKAPI_CALL DebugCallback(
    VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
    VkDebugUtilsMessageTypeFlagsEXT messageType,
    const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
    void* pUserData)
{
    // Always return false (true aborts the call that triggered the error)
    if(messageSeverity >= VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT)
    {
        if(messageSeverity == VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT)
        {
            Warn(
                false,
                "Vulkan message\n"
                "Message ID name: {0}\n"
                "Message: {1}\n"
                "Severity: {2}\n",
                string(pCallbackData->pMessageIdName),
                string(pCallbackData->pMessage),
                to_string(messageSeverity));
            return VK_FALSE;
        }
        else
        {
            Assert(
                false,
                "Vulkan error\n"
                "Message ID name: {0}\n"
                "Message: {1}\n"
                "Severity: {2}\n",
                string(pCallbackData->pMessageIdName),
                string(pCallbackData->pMessage),
                to_string(messageSeverity));
            return VK_FALSE;
        }
    }
    return VK_FALSE;
}

// Helper Functions ----------------------------------------------------------------------
string to_string(VkDebugUtilsMessageSeverityFlagBitsEXT message_severity)
{
    switch(message_severity)
    {
        ENUM_TO_STRING_CASE(VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT)
        ENUM_TO_STRING_CASE(VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT)
        ENUM_TO_STRING_CASE(VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT)
        ENUM_TO_STRING_CASE(VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT)
        ENUM_TO_STRING_CASE(VK_DEBUG_UTILS_MESSAGE_SEVERITY_FLAG_BITS_MAX_ENUM_EXT)
        default: return "Unrecognized enum: " + std::to_string((long)message_severity);
    }
}

} // Renderer