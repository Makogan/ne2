#pragma once

/** @cond */
#include <map>

#include "vk_mem_alloc.h"
#include "vulkan/vulkan.hpp"
/** @endcond */

#include "OutOfModule.hpp"

namespace Renderer {
class HardwareInterface;

class VulkanMemory
{
  private:
    std::map<std::uint64_t, VmaAllocation> buffer_memory_allocations;
    std::map<std::uint64_t, VmaAllocation> image_memory_allocations;

    HardwareInterface* h_interface;

    VmaAllocator vma_allocator;

  public:
    VulkanMemory(){};
    VulkanMemory(HardwareInterface* hi);
    void Destroy();

    std::pair<vk::Image, vk::UniqueImageView> CreateImage(
        uint32_t width,
        uint32_t height,
        uint32_t depth,
        vk::Format format,
        vk::ImageTiling tiling,
        vk::ImageUsageFlags usage,
        vk::ImageLayout initial_layout = vk::ImageLayout::eUndefined);

    void DestroyImage(vk::Image image);

    void DestroyBuffer(vk::Buffer buffer);

    vk::UniqueImageView CreateImageView(vk::Image image, vk::Format format);

    vk::Buffer CreateBuffer(
        size_t size, vk::BufferUsageFlags type);

    vk::Buffer CreateFilledBuffer(
        const void* object, uint32_t size, vk::BufferUsageFlags type);

    VmaAllocationInfo GetMemoryAllocationInfo(vk::Buffer buffer)
    {
        auto handle = (uint64_t)(VkBuffer)buffer;
        Assert(buffer_memory_allocations.count(handle) > 0, "");
        VmaAllocationInfo info;
        vmaGetAllocationInfo(vma_allocator, buffer_memory_allocations[handle], &info);
        return info;
    }

    VmaAllocationInfo GetMemoryAllocationInfo(vk::Image image)
    {
        auto handle = (uint64_t)(VkImage)image;
        VmaAllocationInfo info;
        vmaGetAllocationInfo(vma_allocator, image_memory_allocations[handle], &info);
        return info;
    }

    void UpdateBuffer(const vk::Buffer& buffer, const void* object_data, size_t size);

    void UpdateBuffer(
        const vk::Buffer& buffer,
        const std::byte datum,
        uint32_t copy_num,
        const uint offset = 0);

    void CopyBufferToImage(
        vk::Buffer buffer,
        vk::Image image,
        uint32_t width,
        uint32_t height,
        uint32_t depth = 1,
        vk::ImageAspectFlags aspect = vk::ImageAspectFlagBits::eColor);

    void CopyImageToBuffer(
        vk::Image image,
        uint32_t width,
        uint32_t height,
        uint32_t depth,
        vk::Buffer buffer,
        vk::ImageAspectFlags aspect = vk::ImageAspectFlagBits::eColor);

    void CopyImageToBuffer(
        vk::Image image,
        uint32_t width,
        uint32_t height,
        uint32_t depth,
        uint32_t channel_num,
        vk::Format format,
        void* buffer,
        size_t size,
        vk::ImageAspectFlags aspect = vk::ImageAspectFlagBits::eColor);

    void CopyPixelToBuffer(
        vk::Image image,
        uint32_t x,
        uint32_t y,
        uint32_t z,
        void* buffer,
        vk::Format format,
        uint channel_num,
        vk::ImageAspectFlags aspect);

    void CopyImageToImage(
        const vk::Image image_src,
        const vk::Image image_dst,
        uint32_t width,
        uint32_t height,
        vk::ImageAspectFlags aspect_src = vk::ImageAspectFlagBits::eColor,
        vk::ImageAspectFlags aspect_dst = vk::ImageAspectFlagBits::eColor);

    void ReadBuffer(vk::Buffer buffer, void* container, size_t size);
};

} // Renderer