#include "VkExtensionsStubs.hpp"
#include "OutOfModule.hpp"

// Extension function stubs --------------------------------------------------------------
VkResult (*vkCreateDebugUtilsMessengerEXT_NE)(
    VkInstance, const VkDebugUtilsMessengerCreateInfoEXT*,
    const VkAllocationCallbacks*, VkDebugUtilsMessengerEXT*);
VkResult vkCreateDebugUtilsMessengerEXT(VkInstance instance,
    const VkDebugUtilsMessengerCreateInfoEXT* info,
    const VkAllocationCallbacks* callbacks, VkDebugUtilsMessengerEXT* messenger)
{
    return vkCreateDebugUtilsMessengerEXT_NE(instance, info, callbacks, messenger);
}

void (*vkDestroyDebugUtilsMessengerEXT_NE)(
    VkInstance instance, VkDebugUtilsMessengerEXT debugMessenger,
    const VkAllocationCallbacks* pAllocator);
void vkDestroyDebugUtilsMessengerEXT(VkInstance instance,
    VkDebugUtilsMessengerEXT debugMessenger, const VkAllocationCallbacks* pAllocator)
{
    vkDestroyDebugUtilsMessengerEXT_NE(instance, debugMessenger, pAllocator);
}

VkResult (*vkSetDebugUtilsObjectNameEXT_NE)(VkDevice device,
    const VkDebugUtilsObjectNameInfoEXT* pNameInfo);
VkResult vkSetDebugUtilsObjectNameEXT(VkDevice device,
    const VkDebugUtilsObjectNameInfoEXT* pNameInfo)
{
    return vkSetDebugUtilsObjectNameEXT_NE(device, pNameInfo);
}

void (*vkCmdBeginRenderingKHR_NE)(VkCommandBuffer, const VkRenderingInfo*);
void vkCmdBeginRenderingKHR(VkCommandBuffer cmd, const VkRenderingInfo* render_info)
{
    vkCmdBeginRenderingKHR_NE(cmd, render_info);
}

void (*vkCmdEndRenderingKHR_NE)(VkCommandBuffer cmd);
void vkCmdEndRenderingKHR(VkCommandBuffer cmd)
{
    vkCmdEndRenderingKHR_NE(cmd);
}

void (*vkCmdSetScissor_NE)(VkCommandBuffer cmd, uint32_t, uint32_t, const VkRect2D*);
void vkCmdSetScissor(
    VkCommandBuffer cmd,
    uint32_t first_scissor,
    uint32_t scissor_count,
    const VkRect2D* p_scissors)
{
    vkCmdSetScissor_NE(cmd, first_scissor, scissor_count, p_scissors);
}

void (*vkCmdSetViewport_NE)(VkCommandBuffer cmd, uint32_t, uint32_t, const VkViewport*);
void vkCmdSetViewport(
    VkCommandBuffer cmd,
    uint32_t first_viewport,
    uint32_t viewport_count,
    const VkViewport* p_viewport)
{
    vkCmdSetViewport_NE(cmd, first_viewport, viewport_count, p_viewport);
}

void (*vkGetPhysicalDeviceProperties2KHR_NE)(
    VkPhysicalDevice device, VkPhysicalDeviceProperties2* properties);
void vkGetPhysicalDeviceProperties2KHR(
    VkPhysicalDevice device, VkPhysicalDeviceProperties2* properties)
{
    vkGetPhysicalDeviceProperties2KHR_NE(device, properties);
}

// Loader --------------------------------------------------------------------------------
void LoadExtensionStubs(vk::Instance instance)
{
    vkCreateDebugUtilsMessengerEXT_NE = (PFN_vkCreateDebugUtilsMessengerEXT) instance.getProcAddr(
        "vkCreateDebugUtilsMessengerEXT");
    Assert(vkCreateDebugUtilsMessengerEXT_NE,
        "Failed to find extension function: vkCreateDebugUtilsMessengerEXT");

    vkDestroyDebugUtilsMessengerEXT_NE =
        (PFN_vkDestroyDebugUtilsMessengerEXT) vkGetInstanceProcAddr(
            instance, "vkDestroyDebugUtilsMessengerEXT");
    Assert(vkDestroyDebugUtilsMessengerEXT_NE,
        "Failed to find extension function: vkDestroyDebugUtilsMessengerEXT");

    vkSetDebugUtilsObjectNameEXT_NE =
        (PFN_vkSetDebugUtilsObjectNameEXT) vkGetInstanceProcAddr(
            instance, "vkSetDebugUtilsObjectNameEXT");
    Assert(vkSetDebugUtilsObjectNameEXT_NE,
        "Failed to find extension function: vkSetDebugUtilsObjectNameEXT");

    vkCmdBeginRenderingKHR_NE = (PFN_vkCmdBeginRenderingKHR) vkGetInstanceProcAddr(
        instance, "vkCmdBeginRenderingKHR");
    Assert(vkCmdBeginRenderingKHR_NE,
        "Failed to find extension function: vkCmdBeginRenderingKHR");

    vkCmdEndRenderingKHR_NE = (PFN_vkCmdEndRenderingKHR) vkGetInstanceProcAddr(
        instance, "vkCmdEndRenderingKHR");
    Assert(vkCmdEndRenderingKHR_NE,
        "Failed to find extension function: vkCmdEndRenderingKHR");

    vkCmdSetScissor_NE = (PFN_vkCmdSetScissor) vkGetInstanceProcAddr(
        instance, "vkCmdSetScissor");
    Assert(vkCmdSetScissor_NE,
        "Failed to find extension function: vkCmdSetScissor");

    vkCmdSetViewport_NE = (PFN_vkCmdSetViewport) vkGetInstanceProcAddr(
        instance, "vkCmdSetViewport");
    Assert(vkCmdSetViewport_NE,
        "Failed to find extension function: vkCmdSetViewport");

    // vkGetPhysicalDeviceProperties2KHR_NE = (PFN_vkGetPhysicalDeviceProperties2KHR) vkGetInstanceProcAddr(
    //     instance, "vkGetPhysicalDeviceProperties2KHR");
    // Assert(vkGetPhysicalDeviceProperties2KHR_NE,
    //     "Failed to find extension function: vkGetPhysicalDeviceProperties2KHR");
}
