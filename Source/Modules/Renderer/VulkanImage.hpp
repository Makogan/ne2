#pragma once

#include "HardwareInterface.hpp"
#include "VulkanMemory.hpp"
#include "OutOfModule.hpp"

namespace Renderer
{
class VulkanImage
{
  private:
    static HardwareInterface* h_interface;
    static VulkanMemory* memory;
    vk::UniqueImageView image_view;
    vk::Image image;

    int width = 0;
    int height = 0;
    int depth = 0;
    int dimension = 2;
    int channel_num = 0;
    vk::Format format = vk::Format::eR8G8B8A8Unorm;
    vk::SamplerAddressMode tiling_mode = vk::SamplerAddressMode::eRepeat;
    vk::Filter filter = vk::Filter::eNearest;
    vk::ImageLayout layout = vk::ImageLayout::eUndefined;
    vk::ImageAspectFlags aspect = vk::ImageAspectFlagBits::eColor;

    void LoadBuffer(
        const void* buffer,
        const vk::Extent3D& extent,
        const int channel_num,
        const vk::Format format);

    static VulkanImage empty_2D_image;
    static VulkanImage empty_3D_image;

  public:
    // TODO (low): This is really ugly, global pointers should be avoided.
    static void SetHardwareInterface(HardwareInterface* hi);
    static void SetMemory(VulkanMemory* mem);

    static VulkanImage& GetEmpty2DImage() { return empty_2D_image; }
    static VulkanImage& GetEmpty3DImage() { return empty_3D_image; }

    static void Clean()
    {
        empty_2D_image = VulkanImage();
        empty_3D_image = VulkanImage();
    }

    static void CopyImage(VulkanImage& image_src, VulkanImage& image_dst);

    static void CopyImage(VulkanImage& image_src, void* buffer_dst, size_t size);

    VulkanImage(){};
    VulkanImage(
        const vk::Format format,
        const vk::Extent2D& extent,
        const uint channel_num = 4,
        vk::ImageUsageFlags usage = vk::ImageUsageFlagBits::eSampled);
    VulkanImage(
        const vk::Format format,
        const vk::Extent3D& extent,
        const uint channel_num = 4,
        vk::ImageUsageFlags usage = vk::ImageUsageFlagBits::eSampled);
    VulkanImage(
        const std::vector<std::byte>& buffer,
        const vk::Extent2D& extent,
        const uint channel_num,
        const vk::Format format = vk::Format::eR8G8B8A8Unorm,
        const vk::SamplerAddressMode tiling_mode = vk::SamplerAddressMode::eRepeat);

    VulkanImage(
        const std::vector<std::byte>& buffer,
        const vk::Extent3D& extent,
        const uint channel_num,
        const vk::Format format = vk::Format::eR8G8B8A8Unorm,
        const vk::SamplerAddressMode tiling_mode = vk::SamplerAddressMode::eRepeat);

    VulkanImage(
        const void* buffer,
        const vk::Extent2D& extent,
        const uint channel_num,
        const vk::Format format = vk::Format::eR8G8B8A8Unorm,
        const vk::SamplerAddressMode tiling_mode = vk::SamplerAddressMode::eRepeat);

    VulkanImage(
        const void* buffer,
        const vk::Extent3D& extent,
        const uint channel_num,
        const vk::Format format = vk::Format::eR8G8B8A8Unorm,
        const vk::SamplerAddressMode tiling_mode = vk::SamplerAddressMode::eRepeat);

    void Destroy() { memory->DestroyImage(this->image); }

    vk::Image GetImage() { return image; }

    vk::ImageView GetImageView() const { return *image_view; }

    uint GetDimension() const { return dimension; }

    vk::SamplerAddressMode GetTilingMode() const { return tiling_mode; }

    vk::Filter GetFilter() const { return filter; }

    vk::Format GetFormat() const { return format; }

    uint GetWidth() const { return width; }

    uint GetHeight() const { return height; }

    uint GetDepth() const { return depth; }

    uint GetChannelNum() const { return channel_num; }

    vk::ImageLayout GetImageLayout() { return layout; }

    vk::ImageAspectFlags GetAspect() { return aspect; }

    void SetTilingMode(vk::SamplerAddressMode mode) { tiling_mode = mode; }
    // TODO (medium): This should probably be private.
    void SetImageLayout(vk::ImageLayout new_layout) { layout = new_layout; }

    template<vk::ImageLayout NewLayout>
    static void TransitionImageLayout(
        VulkanImage& image, vk::PipelineStageFlags source_stage);

    void Clear(
        const vk::ClearColorValue color =
            vk::ClearColorValue(std::array<float, 4>({0, 0, 0, 0})));
};

// Template defintions -------------------------------------------------------------------

namespace _implementation_details
{
template<vk::ImageLayout Layout>
static constexpr vk::PipelineStageFlags DeduceDestinationStage()
{
    if constexpr(
        Layout == vk::ImageLayout::eTransferSrcOptimal ||
        Layout == vk::ImageLayout::eTransferDstOptimal)
    { return vk::PipelineStageFlagBits::eTransfer; }

    if constexpr(Layout == vk::ImageLayout::eShaderReadOnlyOptimal)
    { return vk::PipelineStageFlagBits::eFragmentShader; }

    if constexpr(Layout == vk::ImageLayout::eGeneral)
    { return vk::PipelineStageFlagBits::eComputeShader; }

    if constexpr(Layout == vk::ImageLayout::eColorAttachmentOptimal)
    { return vk::PipelineStageFlagBits::eColorAttachmentOutput; }

    if constexpr(Layout == vk::ImageLayout::eDepthStencilAttachmentOptimal)
    { return vk::PipelineStageFlagBits::eEarlyFragmentTests; }

    return {};
}

template<vk::ImageLayout Layout> static constexpr vk::AccessFlags DeduceAccessFlags()
{
    if constexpr(Layout == vk::ImageLayout::eTransferSrcOptimal)
    { return vk::AccessFlagBits::eTransferRead; }

    if constexpr(Layout == vk::ImageLayout::eTransferDstOptimal)
    { return vk::AccessFlagBits::eTransferWrite; }

    if constexpr(Layout == vk::ImageLayout::eShaderReadOnlyOptimal)
    { return vk::AccessFlagBits::eShaderWrite; }

    if constexpr(Layout == vk::ImageLayout::eGeneral)
    { return vk::AccessFlagBits::eShaderWrite | vk::AccessFlagBits::eShaderRead; }

    if constexpr(Layout == vk::ImageLayout::eColorAttachmentOptimal)
    { return vk::AccessFlagBits::eColorAttachmentWrite; }

    if constexpr(Layout == vk::ImageLayout::eDepthStencilAttachmentOptimal)
    {
        return vk::AccessFlagBits::eDepthStencilAttachmentWrite |
            vk::AccessFlagBits::eDepthStencilAttachmentRead;
    }

    assert(false);

    return {};
}
}  // namespace _implementation_details

template<vk::ImageLayout NewLayout>
void VulkanImage::TransitionImageLayout(
    VulkanImage& image, vk::PipelineStageFlags source_stage)
{
    using namespace _implementation_details;

    // Avoid doing a useless transition.
    if(image.layout == NewLayout) return;

    vk::ImageMemoryBarrier barrier = {};
    barrier.oldLayout = image.layout;
    barrier.newLayout = NewLayout;
    barrier.dstAccessMask = DeduceAccessFlags<NewLayout>();
    barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.image = image.GetImage();
    barrier.subresourceRange = vk::ImageSubresourceRange(image.aspect, 0, 1, 0, 1);

    vk::PipelineStageFlags destination_stage = DeduceDestinationStage<NewLayout>();

    vk::CommandBuffer command_buffer =
        HardwareInterface::BeginSingleTimeCommands(*h_interface);
    command_buffer.pipelineBarrier(
        source_stage, destination_stage, {}, 0, nullptr, 0, nullptr, 1, &barrier);
    HardwareInterface::EndSingleTimeCommands(*h_interface, command_buffer);

    image.layout = barrier.newLayout;
}

// Template instantiations.
extern template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eUndefined>(
    VulkanImage& image, vk::PipelineStageFlags source_stage);
extern template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eGeneral>(
    VulkanImage& image, vk::PipelineStageFlags source_stage);
extern template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eColorAttachmentOptimal>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);
extern template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eDepthStencilAttachmentOptimal>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);
extern template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eDepthStencilReadOnlyOptimal>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);
extern template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eShaderReadOnlyOptimal>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);
extern template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eTransferSrcOptimal>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);
extern template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eTransferDstOptimal>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);
extern template void VulkanImage::TransitionImageLayout<vk::ImageLayout::ePreinitialized>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);
extern template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eDepthReadOnlyStencilAttachmentOptimal>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);
extern template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eDepthAttachmentStencilReadOnlyOptimal>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);
extern template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eDepthAttachmentOptimal>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);
extern template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eDepthReadOnlyOptimal>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);
extern template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eStencilAttachmentOptimal>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);
extern template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eStencilReadOnlyOptimal>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);
extern template void VulkanImage::TransitionImageLayout<vk::ImageLayout::ePresentSrcKHR>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);
extern template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eSharedPresentKHR>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);
extern template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eFragmentDensityMapOptimalEXT>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);
extern template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eFragmentShadingRateAttachmentOptimalKHR>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);
extern template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eReadOnlyOptimalKHR>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);
extern template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eAttachmentOptimalKHR>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);
extern template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eDepthAttachmentOptimalKHR>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);
extern template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eDepthAttachmentStencilReadOnlyOptimalKHR>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);
extern template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eDepthReadOnlyOptimalKHR>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);
extern template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eDepthReadOnlyStencilAttachmentOptimalKHR>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);
extern template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eShadingRateOptimalNV>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);
extern template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eStencilAttachmentOptimalKHR>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);
extern template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eStencilReadOnlyOptimalKHR>(
    VulkanImage& image, vk::PipelineStageFlags source_stage);

} // renderer