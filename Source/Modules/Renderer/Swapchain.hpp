#pragma once

#include "HardwareInterface.hpp"
#include "VulkanMemory.hpp"
#include "OutOfModule.hpp"

#include "VulkanImage.hpp"

namespace Renderer {

class VulkanImage;

class Swapchain
{
public:
    HardwareInterface* h_interface;
    VulkanMemory* memory;

    vk::UniqueSwapchainKHR swapchain;

    std::vector<vk::Image> swapchain_images;
    std::vector<vk::UniqueImageView> image_views;
    std::vector<vk::UniqueSemaphore> render_finished_sems;
    std::vector<vk::UniqueFence> in_flight_fences;
    std::vector<vk::UniqueSemaphore> img_available_sems;

    vk::Extent2D extent;
    vk::PresentModeKHR mode;
    VulkanImage depth_image;
    vk::SurfaceFormatKHR selected_format;

    int current_frame = 0;
    uint active_image_index = 0;
    bool use_vsync;

public:
    Swapchain(){}
    Swapchain(HardwareInterface* hi, VulkanMemory* mem, bool use_vsync);

    vk::SwapchainKHR& GetSwapchainKHR() { return *swapchain; }
    const vk::Extent2D& GetExtent() const { return extent; }
    const vk::PresentModeKHR& GetPresentModeKHR() { return mode; }
    vk::Image GetActiveSwapChainImage() { return swapchain_images[active_image_index]; }
    VulkanImage& GetDepthImage() { return depth_image; }

    void DrawToScreen(
        vk::CommandBuffer& cmd,
        const vk::Pipeline& pipeline,
        const NECore::RenderRequest& render_request);

    void Sync();

    void Update(bool use_vsync);

    const vk::SurfaceFormatKHR& GetSwapchainFormat() { return selected_format; }
};

} // Renderer