#include "GlslParsing.hpp"

/** @cond */

#include <cstdint>
#include <map>
#include <vector>

#include "spirv_glsl.hpp"
#include "vulkan/vulkan.hpp"
/** @endcond */

namespace Renderer
{
GlslUniformMetadata ExtractGlslUniformMetadata(
	const std::vector<uint32_t>& spirv_source,
	vk::ShaderStageFlagBits stage)
{
    spirv_cross::CompilerGLSL glsl(spirv_source);
    spirv_cross::ShaderResources resources = glsl.get_shader_resources();

	GlslUniformMetadata meta_data = {};
    for (spirv_cross::Resource& resource : resources.uniform_buffers)
	{
		const spirv_cross::SPIRType &base_type = glsl.get_type(resource.base_type_id);

		meta_data.uniform_layouts.push_back({});
		vk::DescriptorSetLayoutBinding& layout_binding = meta_data.uniform_layouts.back();

        layout_binding.binding = glsl.get_decoration(resource.id, spv::DecorationBinding);
		layout_binding.descriptorType = vk::DescriptorType::eUniformBuffer;
		layout_binding.descriptorCount = base_type.array.empty() ? 1 : base_type.array[0];
		layout_binding.stageFlags = stage;

        const size_t block_size = (glsl.get_declared_struct_size(base_type));
		meta_data.uniform_size_map.insert({layout_binding.binding, block_size});
	}

	for (spirv_cross::Resource& resource : resources.sampled_images)
	{
		const spirv_cross::SPIRType &type = glsl.get_type(resource.type_id);

		meta_data.uniform_layouts.push_back({});
		vk::DescriptorSetLayoutBinding& layout_binding = meta_data.uniform_layouts.back();

        layout_binding.binding = glsl.get_decoration(resource.id, spv::DecorationBinding);
		layout_binding.descriptorType = vk::DescriptorType::eCombinedImageSampler;
		layout_binding.descriptorCount = type.array.empty() ? 1 : type.array[0];
		layout_binding.stageFlags = stage;
	}

	for (spirv_cross::Resource& resource : resources.storage_images)
	{
		const spirv_cross::SPIRType &base_type = glsl.get_type(resource.base_type_id);

		meta_data.uniform_layouts.push_back({});
		vk::DescriptorSetLayoutBinding& layout_binding = meta_data.uniform_layouts.back();

        layout_binding.binding = glsl.get_decoration(resource.id, spv::DecorationBinding);
		layout_binding.descriptorType = vk::DescriptorType::eStorageImage;
		layout_binding.descriptorCount = base_type.array.empty() ? 1 : base_type.array[0];
		layout_binding.stageFlags = stage;
	}

	for (spirv_cross::Resource& resource : resources.storage_buffers)
	{
		const spirv_cross::SPIRType &base_type = glsl.get_type(resource.base_type_id);

		meta_data.uniform_layouts.push_back({});
		vk::DescriptorSetLayoutBinding& layout_binding = meta_data.uniform_layouts.back();

        layout_binding.binding = glsl.get_decoration(resource.id, spv::DecorationBinding);
		layout_binding.descriptorType = vk::DescriptorType::eStorageBuffer;
		layout_binding.descriptorCount = base_type.array.empty() ? 1 : base_type.array[0];
		layout_binding.stageFlags = stage;
	}

	return meta_data;
}
} // Renderer