#pragma once

/** @cond */
#include <map>
#include <set>

#include "vulkan/vulkan.hpp"
/** @endcond */

#include "ShaderProgram.hpp"

namespace NECore
{
struct ResourceHandle;
};

namespace Renderer
{

class HardwareInterface;
class VulkanMemory;
class VulkanImage;

class Pipeline
{
private:
    HardwareInterface* h_interface;
    VulkanMemory* memory;

    vk::UniqueDescriptorPool descriptor_pool;
    vk::UniqueDescriptorSetLayout descriptor_set_layout;
    vk::UniqueDescriptorSet descriptor_set;
    vk::UniquePipelineLayout pipeline_layout;
    vk::UniquePipeline vk_pipeline;

    std::map<uint, vk::Buffer> binding_uniform_buffers_map;
    std::map<uint, std::vector<vk::UniqueSampler>> binding_sampler_map;
    std::set<uint> image_storage_bindings;

    ShaderProgram program;

public:
    Pipeline() {}
    Pipeline(
        HardwareInterface* hi,
        VulkanMemory* mem,
        ShaderProgram& program,
        vk::Format attachment_format);

    vk::Pipeline& GetVkPipeline() { return *vk_pipeline; }

    void UpdateUniforms(
        vk::CommandBuffer& cmd,
        const NECore::UniformBufferDataContainer& uniform_buffer_data,
        const std::vector<std::tuple<VulkanImage*, size_t, size_t>>& uniform_image_data,
        const std::vector<NECore::GpuDataDescriptor>& buffer_inputs,
        const vk::PipelineBindPoint pipeline_binding_point);
};

} // Renderer