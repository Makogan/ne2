#pragma once

/** @cond */
#include <cstdint>
#include <map>
#include <vector>

#include "vulkan/vulkan.hpp"
/** @endcond */

namespace Renderer
{
struct GlslUniformMetadata
{
    std::vector<vk::DescriptorSetLayoutBinding> uniform_layouts;
    std::map<uint, size_t> uniform_size_map;
};

GlslUniformMetadata ExtractGlslUniformMetadata(
    const std::vector<uint32_t>& spirv_source,
    vk::ShaderStageFlagBits);
} // Renderer