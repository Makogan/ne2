#pragma once

/** @cond */
#include "vulkan/vulkan.hpp"
/** @endcond */

#include "OutOfModule.hpp"


VkResult vkCreateDebugUtilsMessengerEXT(
    VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT* info,
    const VkAllocationCallbacks* callbacks, VkDebugUtilsMessengerEXT* messenger);

void vkDestroyDebugUtilsMessengerEXT(
    VkInstance instance, VkDebugUtilsMessengerEXT debugMessenger,
    const VkAllocationCallbacks* pAllocator);

VkResult vkSetDebugUtilsObjectNameEXT(VkDevice device,
    const VkDebugUtilsObjectNameInfoEXT* pNameInfo);

void vkCmdBeginRenderingKHR(VkCommandBuffer cmd, const VkRenderingInfo* render_info);

void vkCmdEndRenderingKHR(VkCommandBuffer cmd);

void vkCmdSetScissor(
    VkCommandBuffer cmd,
    uint32_t first_scissor,
    uint32_t scissor_count,
    const VkRect2D* p_scissors);

void vkCmdSetViewport(
    VkCommandBuffer cmd,
    uint32_t first_viewport,
    uint32_t viewport_count,
    const VkViewport* p_viewport);


void LoadExtensionStubs(vk::Instance instance);

