#define STRINGIFY_LIMITS(struct, field) #field ": " + to_string(struct.field) + "\n"
#define STRINGIFY_FEATURES(struct, field) #field ": " + IsSupported(struct.field) + "\n"

#include "Utils.hpp"

using namespace std;

namespace Renderer {

// TODO(as needed): fill this with all missing format enumerators.
size_t FormatToChannelPixelSize(const vk::Format format)
{
    switch(format)
    {
    case vk::Format::eR8G8B8A8Srgb:
        return 1;
        break;
    case vk::Format::eR8Unorm:
        return 1;
        break;
    case vk::Format::eR8G8Snorm:
        return 1;
        break;
    case vk::Format::eR8G8B8A8Unorm:
        return 1;
        break;
    case vk::Format::eR8G8B8A8Snorm:
        return 1;
        break;
    case vk::Format::eB8G8R8A8Unorm:
        return 1;
        break;
    case vk::Format::eR32Sint:
        return 4;
        break;
    case vk::Format::eR32Sfloat:
        return 4;
        break;
    case vk::Format::eR32G32Sfloat:
        return 4;
        break;
    case vk::Format::eR32G32B32Sfloat:
        return 4;
        break;
    case vk::Format::eR32G32B32A32Sfloat:
        return 4;
        break;
    case vk::Format::eD32Sfloat:
        return 4;
        break;
    case vk::Format::eD32SfloatS8Uint:
        return 5;
        break;

    default:
        Assert(false, "Unrecognized format: {0}", vk::to_string(format));
        break;
    }
    return 1;
}
} // Renderer

namespace vk
{
inline string IsSupported(bool s) { return s ? "Supported" : "Not Supported"; }
string to_string(const vk::PhysicalDeviceFeatures& features)
{
    return
        STRINGIFY_FEATURES(features, robustBufferAccess)
        STRINGIFY_FEATURES(features, fullDrawIndexUint32)
        STRINGIFY_FEATURES(features, imageCubeArray)
        STRINGIFY_FEATURES(features, independentBlend)
        STRINGIFY_FEATURES(features, geometryShader)
        STRINGIFY_FEATURES(features, tessellationShader)
        STRINGIFY_FEATURES(features, sampleRateShading)
        STRINGIFY_FEATURES(features, dualSrcBlend)
        STRINGIFY_FEATURES(features, logicOp)
        STRINGIFY_FEATURES(features, multiDrawIndirect)
        STRINGIFY_FEATURES(features, drawIndirectFirstInstance)
        STRINGIFY_FEATURES(features, depthClamp)
        STRINGIFY_FEATURES(features, depthBiasClamp)
        STRINGIFY_FEATURES(features, fillModeNonSolid)
        STRINGIFY_FEATURES(features, depthBounds)
        STRINGIFY_FEATURES(features, wideLines)
        STRINGIFY_FEATURES(features, largePoints)
        STRINGIFY_FEATURES(features, alphaToOne)
        STRINGIFY_FEATURES(features, multiViewport)
        STRINGIFY_FEATURES(features, samplerAnisotropy)
        STRINGIFY_FEATURES(features, textureCompressionETC2)
        STRINGIFY_FEATURES(features, textureCompressionASTC_LDR)
        STRINGIFY_FEATURES(features, textureCompressionBC)
        STRINGIFY_FEATURES(features, occlusionQueryPrecise)
        STRINGIFY_FEATURES(features, pipelineStatisticsQuery)
        STRINGIFY_FEATURES(features, vertexPipelineStoresAndAtomics)
        STRINGIFY_FEATURES(features, fragmentStoresAndAtomics)
        STRINGIFY_FEATURES(features, shaderTessellationAndGeometryPointSize)
        STRINGIFY_FEATURES(features, shaderImageGatherExtended)
        STRINGIFY_FEATURES(features, shaderStorageImageExtendedFormats)
        STRINGIFY_FEATURES(features, shaderStorageImageMultisample)
        STRINGIFY_FEATURES(features, shaderStorageImageReadWithoutFormat)
        STRINGIFY_FEATURES(features, shaderStorageImageWriteWithoutFormat)
        STRINGIFY_FEATURES(features, shaderUniformBufferArrayDynamicIndexing)
        STRINGIFY_FEATURES(features, shaderSampledImageArrayDynamicIndexing)
        STRINGIFY_FEATURES(features, shaderStorageBufferArrayDynamicIndexing)
        STRINGIFY_FEATURES(features, shaderStorageImageArrayDynamicIndexing)
        STRINGIFY_FEATURES(features, shaderClipDistance)
        STRINGIFY_FEATURES(features, shaderCullDistance)
        STRINGIFY_FEATURES(features, shaderFloat64)
        STRINGIFY_FEATURES(features, shaderInt64)
        STRINGIFY_FEATURES(features, shaderInt16)
        STRINGIFY_FEATURES(features, shaderResourceResidency)
        STRINGIFY_FEATURES(features, shaderResourceMinLod)
        STRINGIFY_FEATURES(features, sparseBinding)
        STRINGIFY_FEATURES(features, sparseResidencyBuffer)
        STRINGIFY_FEATURES(features, sparseResidencyImage2D)
        STRINGIFY_FEATURES(features, sparseResidencyImage3D)
        STRINGIFY_FEATURES(features, sparseResidency2Samples)
        STRINGIFY_FEATURES(features, sparseResidency4Samples)
        STRINGIFY_FEATURES(features, sparseResidency8Samples)
        STRINGIFY_FEATURES(features, sparseResidency16Samples)
        STRINGIFY_FEATURES(features, sparseResidencyAliased)
        STRINGIFY_FEATURES(features, variableMultisampleRate)
        STRINGIFY_FEATURES(features, inheritedQueries);
}

}  // namespace vk
