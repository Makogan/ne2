#include "Swapchain.hpp"

/** @cond */
#include <set>
/** @endcond */

using namespace std;

const uint MAX_FRAMES_IN_FLIGHT = 3;

namespace
{
// Internals -----------------------------------------------------------------------------
vk::SurfaceFormatKHR SelectSurfaceFormat(
    vk::PhysicalDevice& phys_device, vk::SurfaceKHR& surface)
{
    // Get supported formats
    auto [result, formats] = phys_device.getSurfaceFormatsKHR(surface);
    Assert(
        result == vk::Result::eSuccess && formats.size() > 0,
        "Failed to query surface formats");

    // Attempt to find the most suitable format. If it isn't found, return the first one
    for(auto& surf_format: formats)
    {
        // If the format supports 4 channels and non linbear sRGB, return this format
        if(surf_format.format == vk::Format::eB8G8R8A8Unorm &&
           surf_format.colorSpace == vk::ColorSpaceKHR::eSrgbNonlinear)
            return surf_format;
    }
    return formats[0];
}

vk::PresentModeKHR SelectPresentationMode(
    vk::PhysicalDevice& phys_device, vk::SurfaceKHR& surface, bool use_vsync)
{
    auto [result, present_modes] = phys_device.getSurfacePresentModesKHR(surface);
    Assert(
        result == vk::Result::eSuccess && present_modes.size() > 0,
        "Failed to query surface presetnation modes");
    // Attempt to return the best presentation orders (best to worse) if available.
    // Else, return the first one found.
    set<vk::PresentModeKHR> p_modes(present_modes.begin(), present_modes.end());
    if(!use_vsync && p_modes.find(vk::PresentModeKHR::eImmediate) != p_modes.end())
        return vk::PresentModeKHR::eImmediate;
    if(p_modes.find(vk::PresentModeKHR::eFifo) != p_modes.end())
        return vk::PresentModeKHR::eFifo;
    else if(p_modes.find(vk::PresentModeKHR::eFifoRelaxed) != p_modes.end())
        return vk::PresentModeKHR::eFifoRelaxed;
    else if(p_modes.find(vk::PresentModeKHR::eMailbox) != p_modes.end())
        return vk::PresentModeKHR::eMailbox;

    return present_modes[0];
}

vk::UniqueSwapchainKHR CreateSwapChain(
    Renderer::HardwareInterface& hi,
    vk::Extent2D& extent,
    uint min_image_num,
    bool use_vsync,
    vk::PresentModeKHR& present_mode)
{
    vk::PhysicalDevice& phys_device = hi.GetPhysicalDevice();
    vk::Device& device = hi.GetDevice();
    vk::SurfaceKHR& surface = hi.GetSurface();
    // Choose color format (bits per channel, color space...).
    vk::SurfaceFormatKHR selected_format = SelectSurfaceFormat(phys_device, surface);
    // Chose swapchain presentation mode (immediate, first in first out...).
    present_mode = SelectPresentationMode(phys_device, surface, use_vsync);
    auto[result_surf, capabilities] = phys_device.getSurfaceCapabilitiesKHR(surface);
    Assert(result_surf == vk::Result::eSuccess, "Failed to get surface capabilities");

    extent.width = std::clamp(
        capabilities.currentExtent.width,
        capabilities.minImageExtent.width,
        capabilities.maxImageExtent.width);

    extent.height = std::clamp(
        capabilities.currentExtent.height,
        capabilities.minImageExtent.height,
        capabilities.maxImageExtent.height);

    vk::SwapchainCreateInfoKHR create_info = {};
    create_info.surface = surface;
    create_info.minImageCount = min_image_num;
    create_info.imageFormat = selected_format.format;
    create_info.imageColorSpace = selected_format.colorSpace;
    create_info.imageExtent = extent;
    create_info.imageArrayLayers = 1;
    create_info.imageUsage =
        vk::ImageUsageFlagBits::eColorAttachment | vk::ImageUsageFlagBits::eTransferSrc;
    create_info.imageSharingMode = vk::SharingMode::eExclusive;
    create_info.queueFamilyIndexCount = 0;
    create_info.pQueueFamilyIndices = nullptr;
    create_info.preTransform = capabilities.currentTransform;
    create_info.compositeAlpha = vk::CompositeAlphaFlagBitsKHR::eOpaque;
    create_info.presentMode = present_mode;
    create_info.clipped = VK_TRUE;

    auto [result_swap, swap_chain] = device.createSwapchainKHRUnique(create_info);
    Assert(result_swap == vk::Result::eSuccess, "Code: {0}", vk::to_string(result_swap));
    return move(swap_chain);
}

} // namespace

namespace Renderer
{

Swapchain::Swapchain(HardwareInterface* hi, VulkanMemory* mem, bool use_vsync)
    : h_interface(hi)
    , memory(mem)
    , selected_format(GetSurfaceFormat(
        h_interface->GetPhysicalDevice(), h_interface->GetSurface()))
    , use_vsync(use_vsync)
{
    Update(use_vsync);

    render_finished_sems = h_interface->CreateSemaphores(MAX_FRAMES_IN_FLIGHT);
    in_flight_fences = h_interface->CreateFences(MAX_FRAMES_IN_FLIGHT);
    img_available_sems =
        h_interface->CreateSemaphores(MAX_FRAMES_IN_FLIGHT);
}

void Swapchain::DrawToScreen(
    vk::CommandBuffer& cmd,
    const vk::Pipeline& pipeline,
    const NECore::RenderRequest& render_request)
{
        vk::Device& device = h_interface->GetDevice();
    vk::RenderingAttachmentInfoKHR color_attachment{};
    color_attachment.imageView = *image_views[current_frame];
    color_attachment.imageLayout = vk::ImageLayout::eAttachmentOptimalKHR;
    color_attachment.loadOp =
        render_request.should_clear?
            vk::AttachmentLoadOp::eClear :
            vk::AttachmentLoadOp::eLoad; // Use eLoad to not clear.
    color_attachment.storeOp = vk::AttachmentStoreOp::eStore;
    color_attachment.clearValue.color = render_request.color_clear;

    vk::RenderingAttachmentInfoKHR depth_stencil_attachment{};
    depth_stencil_attachment.imageView = depth_image.GetImageView();
    depth_stencil_attachment.imageLayout = vk::ImageLayout::eDepthAttachmentOptimalKHR;
    depth_stencil_attachment.loadOp =
        render_request.should_clear?
            vk::AttachmentLoadOp::eClear :
            vk::AttachmentLoadOp::eLoad;
    depth_stencil_attachment.storeOp = vk::AttachmentStoreOp::eStore;
    depth_stencil_attachment.clearValue.depthStencil =
        vk::ClearDepthStencilValue(1.0f, 0.0f);

    vk::RenderingInfoKHR rendering_info = {};
    rendering_info.renderArea = vk::Rect2D({0, 0}, {extent.width, extent.height});
    rendering_info.layerCount = 1;
    rendering_info.colorAttachmentCount = 1;
    rendering_info.pColorAttachments = &color_attachment;
	rendering_info.pDepthAttachment = &depth_stencil_attachment;
	rendering_info.pStencilAttachment = &depth_stencil_attachment;

    vk::Viewport viewport{};
    viewport.x = 0.0f;
    viewport.y = 0.0f;
    viewport.width = extent.width;
    viewport.height = extent.height;
    viewport.minDepth = 0.0f;
    viewport.maxDepth = 1.0f;

    const auto& rs = render_request.scissor;
    vk::Rect2D scissor{};
    scissor.offset = vk::Offset2D(
        render_request.scissor.offset.x,
        render_request.scissor.offset.y);
    scissor.extent = vk::Extent2D(
        rs.extent.width <= -1 ? extent.width : rs.extent.width,
        rs.extent.height <= -1 ? extent.height : rs.extent.height);

    cmd.setScissor(0, 1, &scissor);
    cmd.setViewport(0, 1, &viewport);

    cmd.beginRenderingKHR(rendering_info);

    DEBUG_VAR(vk::Result fence_result =)
    device.waitForFences(
        1,
        &*in_flight_fences[current_frame],
        VK_TRUE,
        std::numeric_limits<uint64_t>::max());
    Assert(fence_result == vk::Result::eSuccess, "StartPass fence waiting failed.");

    h_interface->Draw(render_request.mesh_input);
    cmd.endRenderingKHR();
}

void Swapchain::Sync()
{
        vk::Device& device = h_interface->GetDevice();
    vk::Result image_result;
    do
    {
        auto[result, image_index] = device.acquireNextImageKHR(
            *swapchain,
            std::numeric_limits<uint64_t>::max(),
            *img_available_sems[current_frame],
            nullptr);
        image_result = result;
        active_image_index = image_index;
        if(image_result == vk::Result::eErrorOutOfDateKHR)
            Update(use_vsync);
    }
    while(image_result == vk::Result::eErrorOutOfDateKHR);

    Assert(image_result == vk::Result::eSuccess, "");

    vk::Queue& graphic_queue = h_interface->GetGraphicQueue();

    DEBUG_VAR(const vk::Result result =)
    device.resetFences(1, &*in_flight_fences[current_frame]);
    Assert(result == vk::Result::eSuccess, "");

    vk::Semaphore wait_semaphores[] = {*img_available_sems[current_frame]};
    vk::PipelineStageFlags wait_stages[] = {
        vk::PipelineStageFlagBits::eColorAttachmentOutput
    };

    vk::Semaphore signal_semaphores[] = {*render_finished_sems[current_frame]};
    vk::SubmitInfo submit_info(
        1, wait_semaphores, wait_stages, 0, nullptr, 1, signal_semaphores);
    vk::Result submit_result =
        graphic_queue.submit(1, &submit_info, *in_flight_fences[current_frame]);
    Assert(submit_result == vk::Result::eSuccess, "Command submission failed.");

    DEBUG_VAR(vk::Result wait_result =)
    device.waitForFences(
        1,
        &*in_flight_fences[current_frame],
        VK_TRUE,
        std::numeric_limits<uint64_t>::max());
    Assert(wait_result == vk::Result::eSuccess, "");

    vk::PresentInfoKHR present_info = {};
    present_info.waitSemaphoreCount = 1;
    present_info.pWaitSemaphores = signal_semaphores;
    present_info.swapchainCount = 1;
    present_info.pSwapchains = &*swapchain;
    present_info.pImageIndices = &active_image_index;

    vk::Result present_result = graphic_queue.presentKHR(&present_info);
    if(present_result == vk::Result::eErrorOutOfDateKHR)
        Update(use_vsync);

    Assert(
        present_result == vk::Result::eSuccess ||
        present_result == vk::Result::eErrorOutOfDateKHR,
        vk::to_string(present_result));
    current_frame = (current_frame + 1) % MAX_FRAMES_IN_FLIGHT;
}

void Swapchain::Update(bool use_vsync)
{
    swapchain.reset();
    swapchain = CreateSwapChain(*h_interface, extent, MAX_FRAMES_IN_FLIGHT, use_vsync, mode);
    Assert(extent.width > 0, "");
    Assert(extent.height > 0, "");
    vk::Device& device = h_interface->GetDevice();
    auto[sc_img_result, swap_images] = device.getSwapchainImagesKHR(*swapchain);
    image_views.resize(swap_images.size());
    for(size_t i = 0; i < swap_images.size(); i++)
    {
        vk::ImageMemoryBarrier barrier = {};
        barrier.oldLayout = vk::ImageLayout::eUndefined;
        barrier.newLayout = vk::ImageLayout::ePresentSrcKHR;
        barrier.dstAccessMask = vk::AccessFlagBits::eColorAttachmentWrite;
        barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        barrier.image = swap_images[i];
        barrier.subresourceRange =
            vk::ImageSubresourceRange(vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1);
        barrier.subresourceRange =
            vk::ImageSubresourceRange(vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1);


        vk::CommandBuffer command_buffer =
            HardwareInterface::BeginSingleTimeCommands(*h_interface);
        command_buffer.pipelineBarrier(
            vk::PipelineStageFlagBits::eEarlyFragmentTests,
            vk::PipelineStageFlagBits::eColorAttachmentOutput,
            {},
            0,
            nullptr,
            0,
            nullptr,
            1,
            &barrier);
        HardwareInterface::EndSingleTimeCommands(*h_interface, command_buffer);


        image_views[i] = memory->CreateImageView(swap_images[i], selected_format.format);
    }
    swapchain_images = swap_images;

    depth_image = VulkanImage(
        vk::Format::eD32SfloatS8Uint,
        vk::Extent2D(extent),
        4,
        vk::ImageUsageFlagBits::eDepthStencilAttachment);
}

} // Renderer