#pragma once

/** @cond */
#include "vulkan/vulkan.hpp"
/** @endcond */

#include "OutOfModule.hpp"

namespace Renderer {

VKAPI_ATTR VkBool32 VKAPI_CALL DebugCallback(
    VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
    VkDebugUtilsMessageTypeFlagsEXT messageType,
    const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
    void* pUserData);

auto inline GetObjectType(vk::Buffer& t) { return vk::ObjectType::eBuffer; }
auto inline GetObjectType(vk::CommandBuffer& t) { return vk::ObjectType::eCommandBuffer; }
auto inline GetObjectType(vk::CommandPool& t) { return vk::ObjectType::eCommandPool; }
auto inline GetObjectType(vk::DebugUtilsMessengerEXT& t)
{
    return vk::ObjectType::eDebugUtilsMessengerEXT;
}
auto inline GetObjectType(vk::DescriptorPool& t)
{
    return vk::ObjectType::eDescriptorPool;
}
auto inline GetObjectType(vk::DescriptorSet& t) { return vk::ObjectType::eDescriptorSet; }
auto inline GetObjectType(vk::DescriptorSetLayout& t)
{
    return vk::ObjectType::eDescriptorSetLayout;
}
auto inline GetObjectType(vk::Device& t) { return vk::ObjectType::eDevice; }
auto inline GetObjectType(vk::DeviceMemory& t) { return vk::ObjectType::eDeviceMemory; }
auto inline GetObjectType(vk::Fence& t) { return vk::ObjectType::eFence; }
auto inline GetObjectType(vk::Framebuffer& t) { return vk::ObjectType::eFramebuffer; }
auto inline GetObjectType(vk::Image& t) { return vk::ObjectType::eImage; }
auto inline GetObjectType(vk::ImageView& t) { return vk::ObjectType::eImageView; }
auto inline GetObjectType(vk::Instance& t) { return vk::ObjectType::eInstance; }
auto inline GetObjectType(vk::PhysicalDevice& t)
{
    return vk::ObjectType::ePhysicalDevice;
}
auto inline GetObjectType(vk::Pipeline& t) { return vk::ObjectType::ePipeline; }
auto inline GetObjectType(vk::PipelineLayout& t)
{
    return vk::ObjectType::ePipelineLayout;
}
auto inline GetObjectType(vk::RenderPass& t) { return vk::ObjectType::eRenderPass; }
auto inline GetObjectType(vk::Sampler& t) { return vk::ObjectType::eSampler; }
auto inline GetObjectType(vk::Semaphore& t) { return vk::ObjectType::eSemaphore; }
auto inline GetObjectType(vk::ShaderModule& t) { return vk::ObjectType::eShaderModule; }
auto inline GetObjectType(vk::SurfaceKHR& t) { return vk::ObjectType::eSurfaceKHR; }
auto inline GetObjectType(vk::SwapchainKHR& t) { return vk::ObjectType::eSwapchainKHR; }
auto inline GetObjectType(vk::Queue& t) { return vk::ObjectType::eQueue; }
//
template<typename T> void _SetName(vk::Device& device, T& obj, const std::string& name)
{
    using type_t = typename T::CType;
    vk::DebugUtilsObjectNameInfoEXT naming_info(
        GetObjectType(obj), reinterpret_cast<std::uint64_t>((type_t)obj), name.c_str());

    DEBUG_VAR(const vk::Result result =) device.setDebugUtilsObjectNameEXT(naming_info);
    Assert(result == vk::Result::eSuccess, "Could not setup debug messenger");
}

} // Renderer