#include "Pipeline.hpp"

#include "OutOfModule.hpp"
#include "Core/Core.hpp"

#include "HardwareInterface.hpp"
#include "VulkanMemory.hpp"
#include "VulkanImage.hpp"

namespace
{

const uint MAXIMUM_SHADER_STAGE_COUNT = 5;
const uint MAXIMUM_DESCRIPTOR_SIZE = 512;

vk::UniquePipeline CreateGraphicsPipeline(
    vk::Device& device,
    vk::PipelineLayout& pipeline_layout,
    Renderer::ShaderProgram& program,
    vk::Format attachment_format)
{
    const auto& modules = program.GetShaderModules();
    const auto& stage_flags = program.GetShaderStageFlags();
    const auto& binding_descriptions = program.GetVertexBindingDescriptions();
    const auto& attribute_descriptions = program.GetAttributeDescriptions();
    const auto& shader_config = program.GetShaderConfig();
    const uint attachment_count = program.GetAttachmentCount();

    Assert(modules.size() == stage_flags.size(), "");

    const uint shader_stage_count = stage_flags.size();
    Assert(shader_stage_count <= MAXIMUM_SHADER_STAGE_COUNT, "");
    std::array<vk::PipelineShaderStageCreateInfo, MAXIMUM_SHADER_STAGE_COUNT> shader_infos;

    for(uint i=0; i < shader_stage_count; i++)
    {
        shader_infos[i].stage = stage_flags[i];
        shader_infos[i].module = *modules[i];
        shader_infos[i].pName = "main";
    }

    vk::PipelineVertexInputStateCreateInfo vertex_input_info{};
    vertex_input_info.vertexAttributeDescriptionCount =
        static_cast<uint32_t>(attribute_descriptions.size());
    vertex_input_info.pVertexAttributeDescriptions = attribute_descriptions.data();
    vertex_input_info.vertexBindingDescriptionCount = binding_descriptions.size();
    vertex_input_info.pVertexBindingDescriptions = binding_descriptions.data();

    // Select rasterization algorithm and indexed drawing.
    vk::PipelineInputAssemblyStateCreateInfo input_assembly{};
    input_assembly.topology = shader_config.topology;
    input_assembly.primitiveRestartEnable = VK_FALSE;

    // Setup rasterization details.
    vk::PipelineRasterizationStateCreateInfo rasterizer = {};
    rasterizer.depthClampEnable = VK_FALSE;
    rasterizer.rasterizerDiscardEnable = VK_FALSE;
    rasterizer.polygonMode = shader_config.polygon_mode;
    rasterizer.cullMode = shader_config.cull_mode;
    rasterizer.frontFace = shader_config.front_face;
    rasterizer.depthBiasEnable = VK_FALSE;
    rasterizer.depthBiasConstantFactor = 0.0;
    rasterizer.depthBiasClamp = 0.0;
    rasterizer.depthBiasSlopeFactor = 0.0;
    rasterizer.lineWidth = shader_config.line_width;

    // VkPhysicalDeviceConservativeRasterizationPropertiesEXT conservativeRasterProps{};
    // conservativeRasterProps.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_CONSERVATIVE_RASTERIZATION_PROPERTIES_EXT;

    // VkPipelineRasterizationConservativeStateCreateInfoEXT conservativeRasterStateCI{};
    // conservativeRasterStateCI.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_CONSERVATIVE_STATE_CREATE_INFO_EXT;
    // conservativeRasterStateCI.conservativeRasterizationMode = VK_CONSERVATIVE_RASTERIZATION_MODE_OVERESTIMATE_EXT;
    // conservativeRasterStateCI.extraPrimitiveOverestimationSize = conservativeRasterProps.maxExtraPrimitiveOverestimationSize;

    // // Conservative rasterization state has to be chained into the pipeline rasterization state create info structure
    // rasterizer.pNext = &conservativeRasterStateCI;

    vk::PipelineMultisampleStateCreateInfo multisampling{};
    multisampling.sampleShadingEnable = VK_FALSE;
    multisampling.rasterizationSamples = vk::SampleCountFlagBits::e1;

    std::vector<vk::PipelineColorBlendAttachmentState> color_blends(attachment_count);
    for(vk::PipelineColorBlendAttachmentState& color_blend : color_blends)
    {
        color_blend.blendEnable = attachment_format != vk::Format::eR32G32B32A32Sfloat;
        color_blend.srcColorBlendFactor = vk::BlendFactor::eSrcAlpha;
        color_blend.dstColorBlendFactor = vk::BlendFactor::eOneMinusSrcAlpha;
        color_blend.colorBlendOp = vk::BlendOp::eAdd;
        color_blend.srcAlphaBlendFactor = vk::BlendFactor::eSrcAlpha;
        color_blend.dstAlphaBlendFactor = vk::BlendFactor::eOneMinusSrcAlpha;
        color_blend.alphaBlendOp = vk::BlendOp::eAdd;
        color_blend.colorWriteMask =
            vk::ColorComponentFlagBits::eR |
            vk::ColorComponentFlagBits::eG |
            vk::ColorComponentFlagBits::eB |
            vk::ColorComponentFlagBits::eA;
    }

    vk::PipelineColorBlendStateCreateInfo color_blending{};
    color_blending.logicOpEnable = VK_FALSE;
    color_blending.logicOp = vk::LogicOp::eCopy;
    color_blending.attachmentCount = color_blends.size();
    color_blending.pAttachments = color_blends.data();
    color_blending.blendConstants[0] = 0.0f;
    color_blending.blendConstants[1] = 0.0f;
    color_blending.blendConstants[2] = 0.0f;
    color_blending.blendConstants[3] = 0.0f;

    const std::array<vk::DynamicState, 2> dynamic_state_enables =
    { vk::DynamicState::eViewport, vk::DynamicState::eScissor };
    vk::PipelineDynamicStateCreateInfo dynamic_state = {};
    dynamic_state.dynamicStateCount = dynamic_state_enables.size();
    dynamic_state.pDynamicStates = dynamic_state_enables.data();

    vk::PipelineDepthStencilStateCreateInfo depth_stencil{};
    depth_stencil.depthTestEnable = shader_config.depth_test;
    depth_stencil.depthWriteEnable = shader_config.depth_write;
    depth_stencil.depthCompareOp = shader_config.depth_compare;
    depth_stencil.depthBoundsTestEnable = false;
    depth_stencil.stencilTestEnable = false;
    depth_stencil.minDepthBounds = 0.0f;
    depth_stencil.maxDepthBounds = 1.0f;

    vk::PipelineViewportStateCreateInfo viewport_state = {};
    viewport_state.viewportCount = 1;
    viewport_state.scissorCount = 1;

    vk::GraphicsPipelineCreateInfo pipeline_info{};
    pipeline_info.stageCount = shader_stage_count;
    pipeline_info.pStages = shader_infos.data();
    pipeline_info.pVertexInputState = &vertex_input_info;
    pipeline_info.pInputAssemblyState = &input_assembly;
    pipeline_info.pViewportState = &viewport_state;
    pipeline_info.pRasterizationState = &rasterizer;
    pipeline_info.pMultisampleState = &multisampling;
    pipeline_info.pColorBlendState = &color_blending;
    pipeline_info.layout = pipeline_layout;
    pipeline_info.pDynamicState = &dynamic_state;
    pipeline_info.pDepthStencilState = &depth_stencil;
    pipeline_info.basePipelineHandle = nullptr;

    std::vector<vk::Format> attachment_formats(attachment_count, attachment_format);
    vk::PipelineRenderingCreateInfoKHR pipeline_rendering_createInfo{};
    pipeline_rendering_createInfo.colorAttachmentCount = attachment_formats.size();
    pipeline_rendering_createInfo.pColorAttachmentFormats = attachment_formats.data();
    pipeline_rendering_createInfo.depthAttachmentFormat = vk::Format::eD32SfloatS8Uint;
    pipeline_rendering_createInfo.stencilAttachmentFormat = vk::Format::eD32SfloatS8Uint;

    pipeline_info.pNext = &pipeline_rendering_createInfo;

    auto [result, graphics_pipeline] =
        device.createGraphicsPipelineUnique({}, pipeline_info);
    Assert(result == vk::Result::eSuccess, "");

    return std::move(graphics_pipeline);
}

vk::UniquePipeline CreateComputePipeline(
    vk::Device& device,
    vk::PipelineLayout& pipeline_layout,
    Renderer::ShaderProgram& program,
    vk::Format attachment_format)
{
    const auto& shader_modules = program.GetShaderModules();
    Assert(shader_modules.size() == 1, "");
    const auto& stage_flags = program.GetShaderStageFlags();
    Assert(stage_flags.size() == 1, "");

    vk::PipelineShaderStageCreateInfo shader_stage_info = {};
    shader_stage_info.stage = stage_flags[0];
    shader_stage_info.module = *shader_modules[0];
    shader_stage_info.pName = "main";

    vk::ComputePipelineCreateInfo create_info = {};
    create_info.stage = shader_stage_info;
    create_info.layout = pipeline_layout;

    auto [result, compute_pipeline] = device.createComputePipelineUnique({}, create_info);
    Assert(result == vk::Result::eSuccess, "");

    return std::move(compute_pipeline);
}

vk::UniquePipeline CreatePipeline(
    vk::Device& device,
    vk::PipelineLayout& pipeline_layout,
    Renderer::ShaderProgram& program,
    vk::Format attachment_format)
{
    if(program.IsCompute())
    {
        return CreateComputePipeline(device, pipeline_layout, program, attachment_format);
    }
    else
    {
        return CreateGraphicsPipeline(device, pipeline_layout, program, attachment_format);
    }
}

vk::UniqueDescriptorSetLayout CreateDescriptorSetLayout(
    vk::Device& device, Renderer::ShaderProgram& program)
{
    Assert((long unsigned int)(VkDevice)device != 0, "");
    const std::vector<vk::DescriptorSetLayoutBinding>& binding_layouts =
        program.GetUniformLayoutDescriptions();

    vk::DescriptorSetLayoutCreateInfo info = {};
    info.bindingCount = (uint32_t)binding_layouts.size();
    info.pBindings = binding_layouts.data();

    auto [result, d_set_layout] = device.createDescriptorSetLayoutUnique(info);
    Assert(result == vk::Result::eSuccess, "Failed to create descriptor set layout");

    Renderer::_SetName(device, *d_set_layout, "descriptor_set_layout");
    return std::move(d_set_layout);
}

vk::UniquePipelineLayout CreatePipelineLayout(
    vk::Device& device, const vk::DescriptorSetLayout& descriptor_set_layout)
{
    vk::PipelineLayoutCreateInfo pipeline_layout_info = {};
    pipeline_layout_info.setLayoutCount = 1;
    pipeline_layout_info.pSetLayouts = &descriptor_set_layout;
    auto [result, pipeline_layout] =
        device.createPipelineLayoutUnique(pipeline_layout_info);
    Assert(result == vk::Result::eSuccess, "Failed to create pipeline layout!");

    Renderer::_SetName(device, *pipeline_layout, "Pipeline layout");
    return std::move(pipeline_layout);
}

vk::UniqueDescriptorPool CreateDescriptorPool(
    vk::Device& device)
{
    std::array<vk::DescriptorPoolSize, 4> pool_sizes = {};
    pool_sizes[0] = vk::DescriptorPoolSize(
        vk::DescriptorType::eUniformBuffer, MAXIMUM_DESCRIPTOR_SIZE);
    pool_sizes[1] = vk::DescriptorPoolSize(
        vk::DescriptorType::eCombinedImageSampler, MAXIMUM_DESCRIPTOR_SIZE);
    pool_sizes[2] = vk::DescriptorPoolSize(
        vk::DescriptorType::eStorageImage, MAXIMUM_DESCRIPTOR_SIZE);
    pool_sizes[3] = vk::DescriptorPoolSize(
        vk::DescriptorType::eStorageBuffer, MAXIMUM_DESCRIPTOR_SIZE);

    const auto c_flags = vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet;
    vk::DescriptorPoolCreateInfo pool_info = {};
    pool_info.flags = c_flags;
    pool_info.maxSets = MAXIMUM_DESCRIPTOR_SIZE;
    pool_info.poolSizeCount = (uint32_t)pool_sizes.size();
    pool_info.pPoolSizes = pool_sizes.data();

    auto [result, descriptor_pool] = device.createDescriptorPoolUnique(pool_info);
    Assert(result == vk::Result::eSuccess, "Failed to create descriptor pool");

    Renderer::_SetName(device, *descriptor_pool, "descriptor_pool");
    return std::move(descriptor_pool);
}

vk::UniqueSampler CreateTextureSampler(
    vk::Device& device,
    const vk::SamplerCreateInfo& sampler_info,
    const std::string& debug_name)
{
    auto tmp = sampler_info;
    // tmp.magFilter = vk::Filter::eNearest;
    // tmp.minFilter = vk::Filter::eNearest;

    auto [result, texture_sampler] = device.createSamplerUnique(tmp);
    Assert(result == vk::Result::eSuccess, "Failed to create texture sampler");

    Renderer::_SetName(device, *texture_sampler, debug_name);

    return std::move(texture_sampler);
}

} // namespace

// Class implementation ------------------------------------------------------------------
namespace Renderer
{

Pipeline::Pipeline(
    HardwareInterface* hi,
    VulkanMemory* mem,
    ShaderProgram& shader_program,
    vk::Format attachment_format)
    : h_interface(hi)
    , memory(mem)
    , descriptor_pool(CreateDescriptorPool(h_interface->GetDevice()))
    , descriptor_set_layout(CreateDescriptorSetLayout(
        h_interface->GetDevice(),
        shader_program))
    , pipeline_layout(CreatePipelineLayout(
        h_interface->GetDevice(),
        *descriptor_set_layout
    ))
    , vk_pipeline(CreatePipeline(
        h_interface->GetDevice(),
        *pipeline_layout,
        shader_program,
        attachment_format))
    , program(std::move(shader_program))
{
    vk::Device device = h_interface->GetDevice();

    vk::DescriptorSetAllocateInfo alloc_info = {};
    alloc_info.descriptorPool = *descriptor_pool;
    alloc_info.descriptorSetCount = 1;
    alloc_info.pSetLayouts = &*descriptor_set_layout;

    auto [result, descriptor_sets] = device.allocateDescriptorSetsUnique(alloc_info);
    Assert(result == vk::Result::eSuccess, "Failed to allocate descriptor sets");

    descriptor_set = std::move(descriptor_sets[0]);
    // Pre-create vulkan buffers to store each uniform's data in the current shader.
    for(auto [binding, byte_size] :  program.GetUniformBindingSizeMap())
    {
        binding_uniform_buffers_map.insert(
            {
                binding,
                memory->CreateBuffer(byte_size, vk::BufferUsageFlagBits::eUniformBuffer)
            });
    }

    // Create samplers for each sampler declared in the shader.
    const std::vector<vk::DescriptorSetLayoutBinding>& binding_layouts =
        program.GetUniformLayoutDescriptions();
    const std::map<uint, vk::SamplerCreateInfo>& sampler_config_map =
        program.GetSamplerConfigMap();
    for(const auto& uniform_descriptor : binding_layouts)
    {
        if(uniform_descriptor.descriptorType == vk::DescriptorType::eCombinedImageSampler)
        {
            binding_sampler_map.emplace(std::make_pair(
                uniform_descriptor.binding,
                std::vector<vk::UniqueSampler>()));

            std::vector<vk::UniqueSampler>& sampler_list =
                binding_sampler_map.at(uniform_descriptor.binding);
                sampler_list.reserve(uniform_descriptor.descriptorCount);
            for(size_t i = 0; i < uniform_descriptor.descriptorCount; i++)
                sampler_list.push_back(
                    CreateTextureSampler(
                        h_interface->GetDevice(),
                        sampler_config_map.at(uniform_descriptor.binding),
                        "sampler" + std::to_string(i)));
        }
        else if(uniform_descriptor.descriptorType == vk::DescriptorType::eStorageImage)
        {
            image_storage_bindings.insert(uniform_descriptor.binding);
        }
    }
}

void Pipeline::UpdateUniforms(
    vk::CommandBuffer& cmd,
    const NECore::UniformBufferDataContainer& uniform_block_data,
    const std::vector<std::tuple<VulkanImage*, size_t, size_t>>& uniform_image_data,
    const std::vector<NECore::GpuDataDescriptor>& uniform_buffer_inputs,
    const vk::PipelineBindPoint pipeline_binding_point)
{
    std::vector<vk::WriteDescriptorSet> descriptor_writes(
        uniform_block_data.size() +
        uniform_image_data.size() +
        uniform_buffer_inputs.size());

    std::vector<vk::DescriptorBufferInfo> buffer_infos;
    buffer_infos.reserve(uniform_block_data.size() + uniform_buffer_inputs.size());
    std::vector<vk::DescriptorImageInfo> image_infos;
    image_infos.reserve(uniform_image_data.size());

    uint write_count = 0;
    for(auto& [binding, uniform_data] : uniform_block_data)
    {
        using namespace std;
        Assert(
            program.GetUniformBindingSizeMap().count(binding) > 0,
            "Binding {0} does not exist in this shader.",
            std::to_string(binding));
        Assert(
            uniform_data.size() == program.GetUniformBindingSizeMap().at(binding),
            "Binding {0} has size {1}, uniform data has size {2}",
            to_string(binding),
            to_string(program.GetUniformBindingSizeMap().at(binding)),
            to_string(uniform_data.size())
        );

        memory->UpdateBuffer(
            binding_uniform_buffers_map.at(binding),
            uniform_data.data(),
            program.GetUniformBindingSizeMap().at(binding));

        buffer_infos.push_back({});
        vk::DescriptorBufferInfo& buffer_info = buffer_infos.back();
        buffer_info.buffer = binding_uniform_buffers_map.at(binding);
        buffer_info.offset = 0;
        buffer_info.range = program.GetUniformBindingSizeMap().at(binding);

        Assert(write_count < descriptor_writes.size(), "");
        vk::WriteDescriptorSet& descriptor_write = descriptor_writes[write_count];
        descriptor_write.dstSet = *descriptor_set;
        descriptor_write.dstBinding = binding;
        descriptor_write.dstArrayElement = 0;
        descriptor_write.descriptorCount = 1;
        descriptor_write.descriptorType = vk::DescriptorType::eUniformBuffer;
        descriptor_write.pBufferInfo = &buffer_info;

        write_count++;
    }

    for(auto& [resource, binding, index] : uniform_buffer_inputs)
    {
        using namespace std;

        NECore::BufferHandle buffer = resource.handle;

        buffer_infos.push_back({});
        vk::DescriptorBufferInfo& buffer_info = buffer_infos.back();
        buffer_info.buffer = CAST_TO_VK_BUFFER(buffer);
        buffer_info.offset = 0;
        buffer_info.range = VK_WHOLE_SIZE;

        Assert(write_count < descriptor_writes.size(), "");
        vk::WriteDescriptorSet& descriptor_write = descriptor_writes[write_count];
        descriptor_write.dstSet = *descriptor_set;
        descriptor_write.dstBinding = binding;
        descriptor_write.dstArrayElement = 0;
        descriptor_write.descriptorCount = 1;
        descriptor_write.descriptorType = vk::DescriptorType::eStorageBuffer;
        descriptor_write.pBufferInfo = &buffer_info;

        write_count++;
    }

    for(auto& [uniform_data, binding, index] : uniform_image_data)
    {
        Assert(write_count < descriptor_writes.size(), "");

        image_infos.push_back({});
        vk::DescriptorImageInfo& image_info = image_infos.back();

        Assert(write_count < descriptor_writes.size(), "");
        vk::WriteDescriptorSet& descriptor_write = descriptor_writes[write_count];
        if(image_storage_bindings.count(binding) == 0)
        {
            Assert(
                binding_sampler_map.count(binding),
                "Binding {0} does not exist in the current shader.",
                std::to_string(binding));
            Assert(
                index < binding_sampler_map.at(binding).size(),
                "Index {0}, binding {1}.",
                std::to_string(index),
                std::to_string(binding));

            VulkanImage::TransitionImageLayout<vk::ImageLayout::eShaderReadOnlyOptimal>(
                *uniform_data, vk::PipelineStageFlagBits::eBottomOfPipe);
            image_info.sampler = *binding_sampler_map.at(binding).at(index);
            image_info.imageView = uniform_data->GetImageView();
            image_info.imageLayout = uniform_data->GetImageLayout();

            descriptor_write.descriptorType = vk::DescriptorType::eCombinedImageSampler;
        }
        else
        {
            VulkanImage::TransitionImageLayout<vk::ImageLayout::eGeneral>(
                *uniform_data, vk::PipelineStageFlagBits::eBottomOfPipe);
            image_info.imageView = uniform_data->GetImageView();
            image_info.imageLayout = uniform_data->GetImageLayout();

            descriptor_write.descriptorType = vk::DescriptorType::eStorageImage;
        }

        descriptor_write.dstSet = *descriptor_set;
        descriptor_write.dstBinding = binding;
        descriptor_write.dstArrayElement = index;
        descriptor_write.descriptorCount = 1;
        descriptor_write.pImageInfo = &image_info;

        write_count++;
    }

    Assert(
        write_count == descriptor_writes.size(),
        "Respectively, {0} and {1}",
        std::to_string(write_count),
        std::to_string(descriptor_writes.size()));

    vk::Device& device = h_interface->GetDevice();
    device.updateDescriptorSets(
        (uint32_t)write_count, descriptor_writes.data(), 0, nullptr);

    cmd.bindDescriptorSets(
        pipeline_binding_point,
        *this->pipeline_layout,
        0,
        1,
        &*this->descriptor_set,
        0,
        nullptr);

    cmd.bindPipeline(pipeline_binding_point, *vk_pipeline);
}

} // Renderer