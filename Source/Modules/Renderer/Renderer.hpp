#include "Core/Gallery.hpp"

namespace Renderer
{
NECore::VulkanMetaData InitializeRendering(
    GLFWwindow*, bool use_vsync);

void DeInitializeRendering(NECore::VulkanMetaData&);

void Update(void*);

NECore::BufferHandle AllocateBuffer(void*, void*, size_t);

void DestroyBuffer(void* vk_data_ptr, NECore::BufferHandle buffer);

NECore::ImageHandle AllocateImage(
    void*, const NECore::RawImageData& , const NECore::ImageFormat image_format);

void ScreenShot(void* vk_data_ptr, void* buffer, size_t size);

void ReadImage(void* vk_data_ptr, void* buffer, size_t size, NECore::ImageHandle image_handle);

std::vector<std::byte> ReadPixel(
    void* vk_data_ptr,
    size_t x,
    size_t y,
    size_t z,
    NECore::ImageHandle image_handle);

NECore::ShaderHandle AddShaderProgram(void*, const std::vector<std::string>&);

void DrawToScreen(
    void*,
    const NECore::RenderRequest&,
    const NECore::UniformBufferDataContainer&);

void DrawOffScreen(
    void* vk_data_ptr,
    const NECore::RenderRequest& render_request,
    const NECore::UniformBufferDataContainer& uniform_container);

void GpuCompute(
    void*,
    const NECore::ComputeRequest&,
    const NECore::UniformBufferDataContainer&);

void EndFrame(void* vk_data_ptr);
}