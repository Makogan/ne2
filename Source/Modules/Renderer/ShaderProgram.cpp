#include "ShaderProgram.hpp"

/** @cond */
#include <filesystem>
#include <regex>

#include "shaderc/shaderc.hpp"
#include "cereal/archives/binary.hpp"
#include "cereal/types/map.hpp"
#include "cereal/types/memory.hpp"
#include "cereal/types/string.hpp"
#include "cereal/types/vector.hpp"
/** @endcond */

#include "GlslParsing.hpp"
#include "HardwareInterface.hpp"
#include "Utils.hpp"
#include "VulkanCereal.hpp"
#include "FileSystem/CacheUtils.hpp"

using namespace std;
using namespace shaderc;
namespace fs = std::filesystem;

namespace Renderer
{
// cereal function for serialization.
template <class Archive>
void serialize( Archive& archive, Renderer::ShaderConfig& shader_config )
{
    archive(
        CEREAL_NVP(shader_config.depth_test),
        CEREAL_NVP(shader_config.depth_write),
        CEREAL_NVP(shader_config.line_width),
        CEREAL_NVP(shader_config.topology),
        CEREAL_NVP(shader_config.polygon_mode),
        CEREAL_NVP(shader_config.use_viewport_extent),
        CEREAL_NVP(shader_config.render_extent),
        CEREAL_NVP(shader_config.cull_mode),
        CEREAL_NVP(shader_config.front_face)
    );
}
} // Renderer
namespace
{
// clang-format off
// Match the format layout of an attribute input.
const std::regex ATTRIBUTE_FORMAT_REGEX(R"(\/\/\s\{\s*format\s*:\s*(.*)\s*\})");
// Match custom comments declaring a binding group.
const std::regex BINDING_GROUP_REGEX(R"(\/\/\s*Binding\s+(\d+)\s*\{(\n?(?:[^\}]*)*)\})");
// Match block comments.
const std::regex BLOCK_REGEX(R"(\/\*(\s*\*.*\n)*)");
// Match the custom configuration options in the shader.
const std::regex CONFIG_REGEX(R"(@.*)");
// Match input layouts in the source file.
const std::regex INPUT_REGEX(R"(\h*layout\s*\(location\s*=\s*\d+\)\s*in.*)");
// Match output layouts in the source file.
const std::regex OUTPUT_REGEX(R"((\/\/)*\h*layout\s*\(location\s*=\s*\d+\)\s*out.*)");
// Match the location of an attribute or uniform.
const std::regex LOCATION_REGEX(R"(=\s*\d+\s*\))");
// Match one the reserved keywords.
const std::regex RESERVED_KEYWORD_REGEX(R"((float|int|vec2|vec3|vec4))");
// Match comment block with sampler configuration info.
const std::regex SAMPLER_CONFIGURATION_REGEX(
    R"(\/\*\*\s*\n\s*\*?\s*Sampler Description:\s*\n(\*.*\n)*\*\/(\s*\n)layout\(.*\)(\s*\n*)*uniform.*)");
// Match any sampler declaration.
const std::regex SAMPLER_DECLARATION_REGEX(
    R"(layout\(\s*binding\s*=\s*(.*)\)(\s*\n*)*uniform.*sampler[2-3]D\s*(.*);)");
// clang-format on

// Keep the following 2 declarations synced.
enum class ShaderInputType
{
    UNDEFINED = 0,
    FLOAT = UNDEFINED + 1,
    INT = FLOAT + 1,
    VEC2 = INT + 1,
    VEC3 = VEC2 + 1,
    VEC4 = VEC3 + 1
};
// How many components does the type have.
const vector<size_t> INPUT_TYPES_COMPONENT_MAP = {
    0,  // UNDEFINED
    1,  // FLOAT
    1,  // INT
    2,  // VEC2
    3,  // VEC3
    4,  // VEC4
};

const std::array<vk::Format, 6> FORMATS = {
    vk::Format::eUndefined,            // UNDEFINED
    vk::Format::eR32Sfloat,            // FLOAT
    vk::Format::eR32Sint,              // INT
    vk::Format::eR32G32Sfloat,         // VEC2
    vk::Format::eR32G32B32Sfloat,      // VEC3
    vk::Format::eR32G32B32A32Sfloat};  // VEC4

const unordered_map<std::string, ShaderInputType> STRING_INPUT_TYPES_MAP = {
    {"float", ShaderInputType::FLOAT},
    {"int",   ShaderInputType::INT},
    {"vec2",  ShaderInputType::VEC2},
    {"vec3",  ShaderInputType::VEC3},
    {"vec4",  ShaderInputType::VEC4},
};

const array<string, 9> RESERVED_INPUT_CONFIGS = {
    "@depth_test",
    "@depth_write",
    "@depth_compare",
    "@topology",
    "@line_width",
    "@polygon_mode",
    "@extent",
    "@cull_mode",
    "@front_face",
    };

// Constants to index into the above array.
const uint DEPTH_TEST = 0;
const uint DEPTH_WRITE = DEPTH_TEST + 1;
const uint DEPTH_COMPARE = DEPTH_WRITE + 1;
const uint TOPOLOGY = DEPTH_COMPARE + 1;
const uint LINE_WIDTH = TOPOLOGY + 1;
const uint POLYGON_MODE = LINE_WIDTH + 1;
const uint EXTENT = POLYGON_MODE + 1;
const uint CULL_MODE = EXTENT + 1;
const uint FRONT_FACE = CULL_MODE + 1;

const array<string, 1> RESERVED_OUTPUT_CONFIGS = {
    "@output_format",
    };
// Constants to index into the above array.
const uint OUTPUT_FORMAT = 0;

const array<string, 2> RESERVED_SAMPLER_CONFIGS = {
    "@tiling_mode",
    "@sampling_mode",
    };

// Constants to index into the above array.
const uint TILING_MODE = 0;
const uint SAMPLING_MODE = 0;

// Keep this synced with the class memebers of the shader program class.
struct ShaderIntermediaryData
{
    std::vector<std::vector<uint32_t>> spirv_data;
    std::vector<vk::ShaderStageFlagBits> shader_stage_flags;
    std::vector<vk::VertexInputBindingDescription> vertex_binding_descriptions;
    std::vector<vk::VertexInputAttributeDescription> attribute_descriptions;
    std::vector<vk::DescriptorSetLayoutBinding> uniform_layout_descriptions;

    std::map<uint, vk::SamplerCreateInfo> sampler_config_map;

    std::map<uint, size_t> uniform_binding_size_map;

    Renderer::ShaderConfig shader_config;

    uint attachment_count = 1;
    vk::Format attachment_output_format;
};

template <class Archive>
void serialize( Archive& archive, ShaderIntermediaryData& shader_data )
{
    archive(
        CEREAL_NVP(shader_data.spirv_data),
        CEREAL_NVP(shader_data.shader_stage_flags),
        CEREAL_NVP(shader_data.vertex_binding_descriptions),
        CEREAL_NVP(shader_data.attribute_descriptions),
        CEREAL_NVP(shader_data.uniform_layout_descriptions),
        CEREAL_NVP(shader_data.sampler_config_map),
        CEREAL_NVP(shader_data.uniform_binding_size_map),
        CEREAL_NVP(shader_data.shader_config),
        CEREAL_NVP(shader_data.attachment_count),
        CEREAL_NVP(shader_data.attachment_output_format)
    );
}

// This class is designed to be very short lived, do not cache it.
class NEShaderIncluder : public CompileOptions::IncluderInterface
{
    std::string name;
    std::string content;

    shaderc_include_result result;

  public:
    NEShaderIncluder()
    {
    }

    void RelativeInclude(const char* requested_source, const char* requesting_source)
    {
        namespace fs = std::filesystem;
        name = string(requested_source);
        const string relative_path_to_request =
            fs::path(requesting_source).parent_path().string() + "/";
        const string relative_path_to_target =
            fs::path(requested_source).parent_path().string() + "/";

        if(relative_path_to_request != relative_path_to_target)
            name = relative_path_to_request + name;

        content = Core::ReadFile(name);
    }

    void StandardInclude(const char* requested_source, const char* requesting_source)
    {
        name = string(requested_source);

        namespace fs = std::filesystem;
        auto path = fs::path("./CommonShaders") / fs::path(requested_source);

        Assert(fs::exists(path), "The included shader {0} is not a built-in shader.", name);

        content = Core::ReadFile(path.string());
    }

    shaderc_include_result* GetInclude(
        const char* requested_source,
        shaderc_include_type type,
        const char* requesting_source,
        size_t include_depth) override
    {
        if(type == shaderc_include_type::shaderc_include_type_relative)
            RelativeInclude(requested_source, requesting_source);
        else if(type == shaderc_include_type::shaderc_include_type_standard)
            StandardInclude(requested_source, requesting_source);
        else
            Assert(false, "Unrecognized shaderc_include_type {0}", to_string(type));

        result.source_name = name.data();
        result.source_name_length = name.size();

        result.content = content.data();
        result.content_length = content.size();

        return &result;
    };

    void ReleaseInclude(shaderc_include_result* data) override {}
};

inline vk::ShaderStageFlagBits ShaderKindToEnum(shaderc_shader_kind shader_stage)
{
    // TODO (low): Add the missing shaders
    std::map<shaderc_shader_kind, vk::ShaderStageFlagBits> shader_map = {
        {shaderc_shader_kind::shaderc_glsl_vertex_shader,
         vk::ShaderStageFlagBits::eVertex},
        {shaderc_shader_kind::shaderc_glsl_geometry_shader,
         vk::ShaderStageFlagBits::eGeometry},
        {shaderc_shader_kind::shaderc_glsl_fragment_shader,
         vk::ShaderStageFlagBits::eFragment},
        {shaderc_shader_kind::shaderc_glsl_compute_shader,
         vk::ShaderStageFlagBits::eCompute}};

    return shader_map[shader_stage];
}

shaderc_shader_kind inline SelectShaderType(const string& file_path)
{
    shaderc_shader_kind shader_type = shaderc_shader_kind::shaderc_glsl_vertex_shader;
    const std::string extension = file_path.substr(file_path.size() - 4);

    const std::array<std::string, 4> types = {"vert", "geom", "frag", "comp"};
    const std::array<shaderc_shader_kind, 4> enums =
    {
        shaderc_shader_kind::shaderc_glsl_vertex_shader,
        shaderc_shader_kind::shaderc_glsl_geometry_shader,
        shaderc_shader_kind::shaderc_glsl_fragment_shader,
        shaderc_shader_kind::shaderc_glsl_compute_shader
    };

    for(uint i=0; i < types.size(); i++)
    {
        if(extension == types[i]) return enums[i];
    }

    Assert(false, "Unrecognized shader type for file: " + file_path);

    return shader_type;
}

std::vector<uint32_t> CompileShaderToSpv(
    const string& shader_path,
    const string& source,
    shaderc_shader_kind shader_type)
{
    // Compile from GLSL to spirv.
    Compiler compiler;
    // Warning: Copying this object will invalidate it: https://github.com/google/shaderc/issues/1235
    CompileOptions options;
    options.SetIncluder(std::make_unique<NEShaderIncluder>());
    options.SetGenerateDebugInfo();
    shaderc::PreprocessedSourceCompilationResult pre_result =
        compiler.PreprocessGlsl(source, shader_type, shader_path.c_str(), options);
    Assert(
        pre_result.GetCompilationStatus() == shaderc_compilation_status_success,
        "Preprocess failed for file\n " + source + ":\n" + pre_result.GetErrorMessage());

    const string pre_passed_source(pre_result.begin());
    shaderc::SpvCompilationResult compilation_result = compiler.CompileGlslToSpv(
        pre_passed_source, shader_type, shader_path.c_str(), options);
    Assert(
        compilation_result.GetCompilationStatus() == shaderc_compilation_status_success,
        pre_passed_source + ":\n" + compilation_result.GetErrorMessage());

    // Extract spirv raw data into buffer.
    std::vector<uint32_t> spirv_shader;
    spirv_shader.assign(compilation_result.cbegin(), compilation_result.cend());

    return spirv_shader;
}

vk::UniqueShaderModule CreateShaderModule(
    vk::Device& device, const std::vector<uint32_t>& spirv_shader)
{
    // Create shader module.
    vk::ShaderModuleCreateInfo createInfo(
        {}, spirv_shader.size() * sizeof(uint32_t), spirv_shader.data());

    auto [error_code, shader_module] = device.createShaderModuleUnique(createInfo);
    Assert(
        error_code == vk::Result::eSuccess,
        "Couldn't create shader module.");

    return move(shader_module);
}

uint ExtractLayoutIndex(const std::string& layout_str)
{
    std::sregex_iterator location_match =
        std::sregex_iterator(layout_str.begin(), layout_str.end(), LOCATION_REGEX);
    string location_string = (*location_match).str();
    location_string = location_string.substr(1, location_string.size() - 2);

    return stoi(location_string);
}

uint CountOutputAttachments(const std::string& fragment_source)
{
    auto shader_begin = std::sregex_iterator(
        fragment_source.begin(), fragment_source.end(), OUTPUT_REGEX);
    auto shader_end = std::sregex_iterator();

    uint count = 0;
    for(std::sregex_iterator match = shader_begin; match != shader_end; match++)
    {
        std::string output_match_str = (*match).str();
        // Skip comments
        if(output_match_str[0] == '/' && output_match_str[1] == '/') continue;
        count++;
    }

    return count;
}

vk::CompareOp StringToVkCompare(const string& compare_str)
{
    if(compare_str == "never")
        return vk::CompareOp::eNever;
    else if(compare_str == "less")
        return vk::CompareOp::eLess;
    else if(compare_str == "equal")
        return vk::CompareOp::eEqual;
    else if(compare_str == "less or equal")
        return vk::CompareOp::eLessOrEqual;
    else if(compare_str == "greater")
        return vk::CompareOp::eGreater;
    else if(compare_str == "not equal")
        return vk::CompareOp::eNotEqual;
    else if(compare_str == "greater or equal")
        return vk::CompareOp::eGreaterOrEqual;
    else if(compare_str == "always")
        return vk::CompareOp::eAlways;
    else
    {
        Warn(false, "Unrecognized compare config: " + compare_str);
        return vk::CompareOp::eLess;
    }
}

vk::PrimitiveTopology StringToVkTopology(const string& topology_str)
{
    if(topology_str == "line list") return vk::PrimitiveTopology::eLineList;
    else if(topology_str == "line strip")
        return vk::PrimitiveTopology::eLineStrip;
    else if(topology_str == "path list")
        return vk::PrimitiveTopology::ePatchList;
    else if(topology_str == "point list")
        return vk::PrimitiveTopology::ePointList;
    else if(topology_str == "triangle fan")
        return vk::PrimitiveTopology::eTriangleFan;
    else if(topology_str == "triangle list")
        return vk::PrimitiveTopology::eTriangleList;
    else if(topology_str == "triangle strip")
        return vk::PrimitiveTopology::eTriangleStrip;
    else
    {
        Warn(false, "Unrecognized topology config: " + topology_str);
        return vk::PrimitiveTopology::eLineList;
    }
}

vk::PolygonMode StringToVkFillMode(const string& topology_str)
{
    if(topology_str == "fill") return vk::PolygonMode::eFill;
    else if(topology_str == "line")
        return vk::PolygonMode::eLine;
    else if(topology_str == "point")
        return vk::PolygonMode::ePoint;
    else if(topology_str == "fill rectangle nv")
        return vk::PolygonMode::eFillRectangleNV;
    else
    {
        Warn(false, "Unrecognized fill config: " + topology_str);
        return vk::PolygonMode::eFill;
    }
}

vk::Extent2D StringToVkExtent(const string& extent_str)
{
    vector<string> tokens = Core::Split(extent_str, ',');
    Assert(tokens.size() == 2, "");
    return {uint(stoi(tokens[0])), uint(stoi(tokens[1]))};
}

vk::CullModeFlagBits StringToCullMode(const string& cull_mode_str)
{
    if(cull_mode_str == "none") return vk::CullModeFlagBits::eNone;
    else if(cull_mode_str == "front")
        return vk::CullModeFlagBits::eFront;
    else if(cull_mode_str == "back")
        return vk::CullModeFlagBits::eBack;
    else if(cull_mode_str == "all")
        return vk::CullModeFlagBits::eFrontAndBack;
    else
    {
        Warn(false, "Unrecognized cull mode config: " + cull_mode_str);
        return vk::CullModeFlagBits::eNone;
    }
}

vk::FrontFace StringToFrontFace(const string& front_face_str)
{
    if(front_face_str == "clockwise") return vk::FrontFace::eClockwise;
    else if(front_face_str == "counter_clockwise")
        return vk::FrontFace::eCounterClockwise;
    else
    {
        Warn(false, "Unrecognized front face config: " + front_face_str);
        return vk::FrontFace::eClockwise;
    }
}

vk::Format StringToVkFormat(const string& format_str)
{
         if(format_str == "R32G32Sfloat") return vk::Format::eR32G32Sfloat;
    else if(format_str == "R32G32B32Sfloat") return vk::Format::eR32G32B32Sfloat;
    else if(format_str == "R32G32B32A32Sfloat") return vk::Format::eR32G32B32A32Sfloat;
    else if(format_str == "R8G8Snorm") return vk::Format::eR8G8Snorm;
    else if(format_str == "R8G8B8A8Snorm") return vk::Format::eR8G8B8A8Snorm;
    else if(format_str == "R8G8B8A8Unorm") return vk::Format::eR8G8B8A8Unorm;
    else if(format_str == "B8G8R8A8Unorm") return vk::Format::eB8G8R8A8Unorm;
    else
    {
        Warn(false, "Unrecognized format string: " + format_str);
        return vk::Format::eUndefined;
    }
}

vk::SamplerAddressMode StringToAddressMode(const string& mode_str)
{
    if(mode_str == "repeat") return vk::SamplerAddressMode::eRepeat;
    else if(mode_str == "clamp to edge")
        return vk::SamplerAddressMode::eClampToEdge;
    else
    {
        Warn(false, "Unrecognized adress mode string: " + mode_str);
        return vk::SamplerAddressMode::eRepeat;
    }
}

vk::Filter StringToSamplingMode(const string& mode_str)
{
    if(mode_str == "nearest") return vk::Filter::eNearest;
    else if(mode_str == "linear")
        return vk::Filter::eLinear;
    else
    {
        Warn(false, "Unrecognized filter mode string: " + mode_str);
        return vk::Filter::eNearest;
    }
}

std::pair<vk::Format, size_t> FindAttributeFormatAndStride(const std::string& match_str)
{
    // Find the type of the input attribute of the shader.
    std::sregex_iterator binding_match = std::sregex_iterator(
        match_str.begin(), match_str.end(), RESERVED_KEYWORD_REGEX);
    const string type_str = (*binding_match).str();
    const ShaderInputType type = STRING_INPUT_TYPES_MAP.at(type_str);
    const size_t component_num = INPUT_TYPES_COMPONENT_MAP.at(uint(type));

    std::smatch matches;
    const bool match_found = std::regex_search(match_str, matches, ATTRIBUTE_FORMAT_REGEX);
    const vk::Format format = match_found ?
        StringToVkFormat(matches.str(1)) : FORMATS[uint(type)];
    return {format, component_num * Renderer::FormatToChannelPixelSize(format)};
}

inline uint SumOfIntegers(const uint i) { return i * (i + 1) / 2; }

map<uint, vk::VertexInputBindingDescription> FindBindingGroups(const std::string& source)
{
    // Shader comments will specify how to group input into binding groups.
    set<uint> seen_layouts;
    map<uint, vk::VertexInputBindingDescription> layout_binding_map;
    auto source_begin =
        std::sregex_iterator(source.begin(), source.end(), BINDING_GROUP_REGEX);
    auto source_end = std::sregex_iterator();

    uint largest_binding = 0;
    uint smallest_binding = std::numeric_limits<uint>::max();
    DEBUG_VAR(uint total_binding_sum = 0);
    for(auto group_match = source_begin; group_match != source_end; group_match++)
    {
        const string group_str = (*group_match).str();
        const uint binding_index = stoi((*group_match).str(1));
        largest_binding = max(largest_binding, binding_index);
        smallest_binding = min(smallest_binding, binding_index);
        DEBUG_SNIPPET(total_binding_sum += binding_index);

        auto group_begin =
            std::sregex_iterator(group_str.begin(), group_str.end(), INPUT_REGEX);
        auto group_end = std::sregex_iterator();
        for(std::sregex_iterator match = group_begin; match != group_end; match++)
        {
            const uint layout_index = ExtractLayoutIndex((*match).str());

            // TODO (low): make this case insensitive
            bool is_instance_input = (*match).str().find("instance") != string::npos;

            // Since instance buffers cannot go into the same binding as regular input
            // buffers, they always go into the next binding.
            seen_layouts.insert(layout_index);
            layout_binding_map[layout_index].binding = binding_index + is_instance_input;
            layout_binding_map[layout_index].inputRate =
                is_instance_input?
                  vk::VertexInputRate::eInstance
                : vk::VertexInputRate::eVertex;
        }
    }
    source_begin = std::sregex_iterator(source.begin(), source.end(), INPUT_REGEX);
    // Layouts without explicit binding groups are assumed to belong to binding 0;
    // except if they are instance inputs, in which case they should go after the
    // largest found binding point.
    for(std::sregex_iterator match = source_begin; match != source_end; match++)
    {
        const uint layout_index = ExtractLayoutIndex((*match).str());
        // If the layout has not been seen, add it to the list.
        if(seen_layouts.find(layout_index) == seen_layouts.end())
        {
            // TODO (low): make this case insensitive
            const bool is_instance_input =
                (*match).str().find("instance") != string::npos;

            layout_binding_map[layout_index].binding =
                is_instance_input ? largest_binding + 1 : 0;
            layout_binding_map[layout_index].inputRate = is_instance_input
                ? vk::VertexInputRate::eInstance
                : vk::VertexInputRate::eVertex;
        }
    }

    Assert(
        smallest_binding == 0 || total_binding_sum == 0,
        "Smallest binding should be 0, smallest binding {0}.",
        to_string(smallest_binding));
    Assert(
        total_binding_sum == SumOfIntegers(largest_binding),
        "If this math check fails it means the binding locations are not continuous.\n"
        "Source:\n\n{0}",
        source);
    return layout_binding_map;
}

void ParseVertexInputLayouts(
    const std::string& vertex_shader_source,
    ShaderIntermediaryData& out_intermediary_data)
{
    // Determine which layouts go in which bindings.
    map<uint, vk::VertexInputBindingDescription> layout_binding_map =
        FindBindingGroups(vertex_shader_source);
    map<uint, vk::VertexInputBindingDescription> vertex_binding_descriptions;
    for(auto [_, meta_data]: layout_binding_map)
    {
        vertex_binding_descriptions[meta_data.binding].stride = 0;
    }

    auto inputs_begin = std::sregex_iterator(
        vertex_shader_source.begin(), vertex_shader_source.end(), INPUT_REGEX);
    auto inputs_end = std::sregex_iterator();
    // Iterate over every layout.
    for(std::sregex_iterator match = inputs_begin; match != inputs_end; match++)
    {
        const std::string match_str = (*match).str();

        const uint location = ExtractLayoutIndex(match_str);
        out_intermediary_data.attribute_descriptions.push_back({});

        vk::VertexInputBindingDescription& input_binding_description =
            vertex_binding_descriptions.at(layout_binding_map.at(location).binding);
        vk::VertexInputAttributeDescription& description =
            out_intermediary_data.attribute_descriptions.back();
        description.location = location;

        auto[format, stride] = FindAttributeFormatAndStride(match_str);

        description.binding = layout_binding_map.at(location).binding;
        // Determine the format from the string input, i.e float uses a different
        // data format from vec2 and so on.
        description.format = format;
        description.offset = input_binding_description.stride;

        input_binding_description.stride += stride;
        input_binding_description.binding = description.binding;
        input_binding_description.inputRate = layout_binding_map.at(location).inputRate;
    }

    for(auto& [binding, data]: vertex_binding_descriptions)
        out_intermediary_data.vertex_binding_descriptions.push_back(data);
}


Renderer::ShaderConfig ParseVertexShaderConfigurations(const string& vertex_source)
{
    Renderer::ShaderConfig config = {};

    auto shader_begin =
        std::sregex_iterator(vertex_source.begin(), vertex_source.end(), BLOCK_REGEX);
    auto shader_end = std::sregex_iterator();

    // Find all block comments.
    uint description_blocks_count = 0;
    for(std::sregex_iterator match = shader_begin; match != shader_end; match++)
    {
        std::string match_str = (*match).str();
        bool is_input_block = match_str.find("Input Description:") != string::npos;
        description_blocks_count += int(is_input_block);
        // Assert there's only one description block comment or less in the file.
        Warn(
            description_blocks_count <= 1,
            "Multiple input description blocks ({0}) on shader: \n{1}",
            to_string(description_blocks_count),
            vertex_source);
        if(!is_input_block) continue;

        auto config_begin =
            std::sregex_iterator(match_str.begin(), match_str.end(), CONFIG_REGEX);
        auto config_end = std::sregex_iterator();
        // Find all the defined configurations in the description block.
        for(std::sregex_iterator c_match = config_begin; c_match != config_end; c_match++)
        {
            string config_str = (*c_match).str();
            Assert(config_str.find(":") != string::npos, "");
            // Split the string into identifier and value.
            vector<string> tokens = Core::Split(config_str, ':');

            bool is_valid = false;
            // Identify the configuration keyword.
            uint reserved_index;
            for(reserved_index = 0; reserved_index < RESERVED_INPUT_CONFIGS.size();
                reserved_index++)
            {
                is_valid =
                    is_valid || (RESERVED_INPUT_CONFIGS[reserved_index] == tokens[0]);
                if(is_valid) break;
            }

            Assert(is_valid, "Invalid config option: {0}", tokens[0]);
            // TODO (low): make this check less naive and case insensitive.
            // TODO (medium): Ideally we want to avoid having such a large list of if elses.
            if(reserved_index == DEPTH_TEST) config.depth_test = tokens[1] == "true";
            else if(reserved_index == DEPTH_WRITE)
                config.depth_write = !(tokens[1] == "false");
            else if(reserved_index == DEPTH_COMPARE)
                config.depth_compare = StringToVkCompare(tokens[1]);
            else if(reserved_index == TOPOLOGY)
                config.topology = StringToVkTopology(tokens[1]);
            else if(reserved_index == LINE_WIDTH)
                config.line_width = stof(tokens[1]);
            else if(reserved_index == POLYGON_MODE)
                config.polygon_mode = StringToVkFillMode(tokens[1]);
            else if(reserved_index == EXTENT)
            {
                config.use_viewport_extent = false;
                config.render_extent = StringToVkExtent(tokens[1]);
            }
            else if(reserved_index == CULL_MODE)
                config.cull_mode = StringToCullMode(tokens[1]);
            else if(reserved_index == FRONT_FACE)
                config.front_face = StringToFrontFace(tokens[1]);
        }
    }

    return config;
}

vk::Format ParseFragmentShaderConfigurations(const std::string& fragment_source)
{
    auto shader_begin =
        std::sregex_iterator(fragment_source.begin(), fragment_source.end(), BLOCK_REGEX);
    auto shader_end = std::sregex_iterator();

    // Find all block comments.
    uint description_blocks_count = 0;
    for(std::sregex_iterator match = shader_begin; match != shader_end; match++)
    {
        std::string match_str = (*match).str();
        bool is_input_block = match_str.find("Output Description:") != string::npos;
        description_blocks_count += int(is_input_block);
        // Assert there's only one description block comment or less in the file.
        Warn(
            description_blocks_count <= 1,
            "Multiple input description blocks ({0}) on shader: \n{1}",
            to_string(description_blocks_count),
            fragment_source);
        if(!is_input_block) continue;

        auto config_begin =
            std::sregex_iterator(match_str.begin(), match_str.end(), CONFIG_REGEX);
        auto config_end = std::sregex_iterator();
        // Find all the defined configurations in the description block.
        for(std::sregex_iterator c_match = config_begin; c_match != config_end; c_match++)
        {
            string config_str = (*c_match).str();
            Assert(config_str.find(":") != string::npos, "");
            // Split the string into identifier and value.
            vector<string> tokens = Core::Split(config_str, ':');

            bool is_valid = false;
            // Identify the configuration keyword.
            uint reserved_index;
            for(reserved_index = 0; reserved_index < RESERVED_OUTPUT_CONFIGS.size();
                reserved_index++)
            {
                is_valid =
                    is_valid || (RESERVED_OUTPUT_CONFIGS[reserved_index] == tokens[0]);
                if(is_valid) break;
            }

            Assert(is_valid, "Invalid output config option: {0}", tokens[0]);
            // TODO (low): make this check less naive and case insensitive.
            // TODO (medium): Ideally we want to avoid having such a large list of if elses.
            if(reserved_index == OUTPUT_FORMAT) return StringToVkFormat(tokens[1]);
        }
    }

    return vk::Format::eUndefined;
}

void ParseSamplerParameters(
    const std::string& source,
    ShaderIntermediaryData& out_intermediary_data)
{
    auto shader_begin =
        std::sregex_iterator(source.begin(), source.end(), SAMPLER_CONFIGURATION_REGEX);
    auto shader_end = std::sregex_iterator();

    vk::SamplerCreateInfo default_sampler_info = {};
    default_sampler_info.magFilter = vk::Filter::eLinear;
    default_sampler_info.minFilter = vk::Filter::eLinear;
    default_sampler_info.mipmapMode = vk::SamplerMipmapMode::eNearest;
    default_sampler_info.addressModeU = vk::SamplerAddressMode::eClampToBorder;
    default_sampler_info.addressModeV = vk::SamplerAddressMode::eClampToBorder;
    default_sampler_info.addressModeW = vk::SamplerAddressMode::eClampToBorder;
    default_sampler_info.mipLodBias = 0;
    default_sampler_info.anisotropyEnable = VK_FALSE;
    default_sampler_info.maxAnisotropy = 16;
    default_sampler_info.compareEnable = VK_FALSE;
    default_sampler_info.compareOp = vk::CompareOp::eAlways;
    default_sampler_info.minLod = 0;
    default_sampler_info.maxLod = 0;
    default_sampler_info.borderColor = vk::BorderColor::eIntOpaqueBlack;
    default_sampler_info.unnormalizedCoordinates = VK_FALSE;

    // First parse all samplers with a matching config block.
    for(std::sregex_iterator match = shader_begin; match != shader_end; match++)
    {
        std::string match_str = (*match).str();

        vk::SamplerCreateInfo sampler_info = default_sampler_info;

        auto config_begin =
            std::sregex_iterator(match_str.begin(), match_str.end(), CONFIG_REGEX);
        auto config_end = std::sregex_iterator();
        for(std::sregex_iterator c_match = config_begin; c_match != config_end; c_match++)
        {
            string config_str = (*c_match).str();
            Assert(config_str.find(":") != string::npos, "");
            // Split the string into identifier and value.
            vector<string> tokens = Core::Split(config_str, ':');

            bool is_valid = false;
            // Identify the configuration keyword.
            uint reserved_index;
            for(reserved_index = 0; reserved_index < RESERVED_SAMPLER_CONFIGS.size();
                reserved_index++)
            {
                is_valid =
                    is_valid || (RESERVED_SAMPLER_CONFIGS[reserved_index] == tokens[0]);
                if(is_valid) break;
            }

            Assert(is_valid, "Invalid config option: {0}", tokens[0]);
            // TODO (low): make this check less naive and case insensitive.
            // TODO (medium): Ideally we want to avoid having such a large list of if elses.
            if(reserved_index == TILING_MODE)
            {
                const vk::SamplerAddressMode tiling_mode = StringToAddressMode(tokens[1]);
                sampler_info.addressModeU = tiling_mode;
                sampler_info.addressModeV = tiling_mode;
                sampler_info.addressModeW = tiling_mode;
            }
            else if(reserved_index == SAMPLING_MODE)
            {
                const vk::Filter sampling_mode = StringToSamplingMode(tokens[1]);
                sampler_info.magFilter = sampling_mode;
                sampler_info.minFilter = sampling_mode;
            }
        }

        std::smatch res;
        regex_search(match_str, res, SAMPLER_DECLARATION_REGEX);
        const uint binding = std::stoi(res[1]);
        out_intermediary_data.sampler_config_map.emplace(
            std::make_pair(binding, sampler_info));
    }

    shader_begin =
        std::sregex_iterator(source.begin(), source.end(), SAMPLER_DECLARATION_REGEX);
    // Then parse all samplers and initialize any missing ones.
    for(std::sregex_iterator match = shader_begin; match != shader_end; match++)
    {
        auto match_str = match->str();
        std::smatch res;
        regex_search(match_str, res, SAMPLER_DECLARATION_REGEX);
        const uint binding = std::stoi(res[1]);

        // Default initialize any missing smapler.
        if(out_intermediary_data.sampler_config_map.count(binding) == 0)
        {
            out_intermediary_data.sampler_config_map.emplace(
                std::make_pair(binding, default_sampler_info));
        }
    }
}

void MergeUniformDescriptors(
    const Renderer::GlslUniformMetadata& meta_data,
    ShaderIntermediaryData& out_intermediary_data)
{
    for(const auto& new_descriptor: meta_data.uniform_layouts)
    {
        const uint new_binding = new_descriptor.binding;
        // Try to find an existing binding description for the current binding value.
        auto existing_descriptor = std::find_if(
            out_intermediary_data.uniform_layout_descriptions.begin(),
            out_intermediary_data.uniform_layout_descriptions.end(),
            [new_binding](vk::DescriptorSetLayoutBinding& descriptor)
            {
                return descriptor.binding == new_binding;
            }
        );
        // There is no existing descriptor so let's add the new one.
        if(existing_descriptor == out_intermediary_data.uniform_layout_descriptions.end())
        {
            out_intermediary_data.uniform_layout_descriptions.push_back(
                new_descriptor);
            // Uniform buffers (e.g. a rotation matrix) require allocating a vk::Buffer.
            // For that we need to keep track of the size.
            if(new_descriptor.descriptorType == vk::DescriptorType::eUniformBuffer)
                out_intermediary_data.uniform_binding_size_map.insert(
                    {new_binding, meta_data.uniform_size_map.at(new_binding)}
            );
        }
        // Otherwise sanity check and merge the data.
        else
        {
            Assert(new_descriptor.descriptorType == existing_descriptor->descriptorType, "");
            Assert(new_descriptor.descriptorCount == existing_descriptor->descriptorCount, "");
            Assert(
                meta_data.uniform_size_map.at(new_binding) ==
                out_intermediary_data.uniform_binding_size_map.at(new_binding), "");
            // Update stages.
            existing_descriptor->stageFlags |= new_descriptor.stageFlags;
        }
    }
}

void ParseAndPrecompileShaders(
    const std::vector<std::string>& shader_paths,
    ShaderIntermediaryData& out_intermediary_data)
{
    for(const auto& path : shader_paths)
    {
        const std::string source = Core::ReadFile(path);
        const shaderc_shader_kind stage = SelectShaderType(path);

        ParseSamplerParameters(source, out_intermediary_data);

        if(stage == shaderc_shader_kind::shaderc_glsl_vertex_shader)
        {
            ParseVertexInputLayouts(source, out_intermediary_data);
            out_intermediary_data.shader_config = ParseVertexShaderConfigurations(source);
        }

        if(stage == shaderc_shader_kind::shaderc_glsl_fragment_shader)
        {
            out_intermediary_data.attachment_count = CountOutputAttachments(source);
            out_intermediary_data.attachment_output_format = ParseFragmentShaderConfigurations(source);
        }

        out_intermediary_data.spirv_data.push_back(
            CompileShaderToSpv(
                path,
                source,
                stage
            )
        );

        const vk::ShaderStageFlagBits vk_stage = ShaderKindToEnum(stage);

        out_intermediary_data.shader_stage_flags.push_back(vk_stage);

        const Renderer::GlslUniformMetadata meta_data =
            Renderer::ExtractGlslUniformMetadata(
                out_intermediary_data.spirv_data.back(), vk_stage);

        MergeUniformDescriptors(meta_data, out_intermediary_data);
    }
}

ShaderIntermediaryData InitializeIntermediaryData(
    const std::vector<std::string>& shader_paths)
{
    Assert(shader_paths.size(), "");
    std::string representative = shader_paths[0];
    std::replace(representative.begin(), representative.end(), '/', '_');
    representative = Core::SubstrUntilChar(representative, '.');

    const fs::path binary_path{"ShaderCache/" + representative + ".cereal"};

    ShaderIntermediaryData intermediary_data;
    intermediary_data.spirv_data.reserve(shader_paths.size());

    // If we have valid cached data, load from disk as it will be hundreds of times faster.
    if(CacheIsValid(binary_path, shader_paths))
    {
        std::ifstream is(binary_path, std::ios::binary);
        cereal::BinaryInputArchive archive_in( is );
        serialize(archive_in, intermediary_data);
    }
    // Otherwise do the expensive process of parsing and compiling the data.
    else
    {
        ParseAndPrecompileShaders(shader_paths, intermediary_data);

        std::filesystem::create_directories(binary_path.parent_path());
        std::ofstream os(binary_path, std::ios::binary);
        cereal::BinaryOutputArchive archive_out( os );
        serialize(archive_out, intermediary_data);
    }

    return intermediary_data;
}

} // namespace

// ===| Main class functions |=== --------------------------------------------------------

namespace Renderer
{
ShaderProgram::ShaderProgram(
    HardwareInterface* hi,
    const std::vector<std::string>& shader_paths)
{
        ShaderIntermediaryData intermediary_data = InitializeIntermediaryData(shader_paths);

    // At this point one way or another we have valid data to create shader modules.
    vk::Device& device = hi->GetDevice();
    shader_modules.reserve(shader_paths.size());
    for(const auto& shader_code : intermediary_data.spirv_data)
    {
        shader_modules.push_back(
            CreateShaderModule(device, shader_code)
        );
    }

    // Actually initialize all the data we need.
    shader_stage_flags = intermediary_data.shader_stage_flags;
    vertex_binding_descriptions = intermediary_data.vertex_binding_descriptions;
    attribute_descriptions = intermediary_data.attribute_descriptions;
    uniform_layout_descriptions = intermediary_data.uniform_layout_descriptions;
    uniform_binding_size_map = intermediary_data.uniform_binding_size_map;
    shader_config = intermediary_data.shader_config;
    attachment_count = intermediary_data.attachment_count;
    sampler_config_map = intermediary_data.sampler_config_map;
    attachment_output_format = intermediary_data.attachment_output_format;

    Assert(attachment_count > 0, "");
    Assert(shader_stage_flags.size() == shader_modules.size(), "");
}
} // Renderer