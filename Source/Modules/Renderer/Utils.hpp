//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file Utils.hpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2020-02-09
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#pragma once

/** @cond */
#include <map>
#include <string>

#include "vulkan/vulkan.hpp"
/** @endcond */

#include "Core/Log.hpp"

namespace Renderer {

template<typename U> auto CastVkArray(const std::vector<U>& unique_handles)
{
    std::vector<typename U::element_type> handles;
    for(auto& u_handle: unique_handles)
        handles.push_back(*u_handle);
    return handles;
}

inline std::string GetVKVersion(uint32_t mask)
{
    std::string version = std::to_string(VK_VERSION_MAJOR(mask));
    version += "." + std::to_string(VK_VERSION_MINOR(mask));
    version += "." + std::to_string(VK_VERSION_PATCH(mask));

    return version;
}

size_t FormatToChannelPixelSize(const vk::Format format);

} // Renderer

namespace vk
{
std::string to_string(const vk::PhysicalDeviceFeatures& features);

inline std::string to_string(const vk::QueueFamilyProperties& q_family)
{
    return "Queue number: " + std::to_string(q_family.queueCount) +
        "\n"
        "Queue flags: " +
        to_string(vk::QueueFlags(q_family.queueFlags)) + "\n";
}

inline std::string to_string(const vk::ExtensionProperties& properties)
{
    return std::string(properties.extensionName.data()) + std::string(", extension version ") +
        std::string(Renderer::GetVKVersion(properties.specVersion));
}

}  // namespace vk
