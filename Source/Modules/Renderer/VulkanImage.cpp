#include "VulkanImage.hpp"

/** @cond */
#include "stb_image.h"
/** @endcond */

#include "VulkanDebugging.hpp"
#include "Utils.hpp"
#include "Core/Log.hpp"

using namespace std;

namespace Renderer {
// Globals -------------------------------------------------------------------------------
HardwareInterface* VulkanImage::h_interface;
VulkanMemory* VulkanImage::memory;
VulkanImage VulkanImage::empty_2D_image;
VulkanImage VulkanImage::empty_3D_image;

// Static functions ----------------------------------------------------------------------
void VulkanImage::SetHardwareInterface(HardwareInterface* hi)
{
    Assert(h_interface == nullptr, "Pointer already set");
    h_interface = hi;
}

void VulkanImage::SetMemory(VulkanMemory* mem)
{
    vk::Device& device = h_interface->GetDevice();
    Assert(memory == nullptr, "Pointer already set");
    memory = mem;

    empty_2D_image =
        VulkanImage({(std::byte)255}, vk::Extent2D{1, 1}, 1, vk::Format::eR8Unorm);
    _SetName(device, *empty_2D_image.image_view, "empty_2d_image_view");
    empty_3D_image =
        VulkanImage({(std::byte)255}, vk::Extent3D{1, 1, 1}, 1, vk::Format::eR8Unorm);
    _SetName(device, *empty_3D_image.image_view, "empty_3d_image_view");
}

void VulkanImage::CopyImage(VulkanImage& image_src, VulkanImage& image_dst)
{
    Assert(image_src.memory == image_dst.memory, "");
    auto& memory = *image_src.memory;

    VulkanImage::TransitionImageLayout<vk::ImageLayout::eTransferSrcOptimal>(
        image_src, vk::PipelineStageFlagBits::eBottomOfPipe);
    VulkanImage::TransitionImageLayout<vk::ImageLayout::eTransferDstOptimal>(
        image_dst, vk::PipelineStageFlagBits::eBottomOfPipe);

    memory.CopyImageToImage(
        image_src.GetImage(),
        image_dst.GetImage(),
        image_src.GetWidth(),
        image_src.GetHeight(),
        image_src.aspect,
        image_dst.aspect);

    VulkanImage::TransitionImageLayout<vk::ImageLayout::eDepthStencilAttachmentOptimal>(
        image_src, vk::PipelineStageFlagBits::eTopOfPipe);
    VulkanImage::TransitionImageLayout<vk::ImageLayout::eDepthStencilAttachmentOptimal>(
        image_dst, vk::PipelineStageFlagBits::eTopOfPipe);
}

void VulkanImage::CopyImage(VulkanImage& image_src, void* buffer_dst, size_t size)
{
    const size_t channel_pixel_size = FormatToChannelPixelSize(image_src.format);
    const size_t image_size =
        image_src.width *
        image_src.height *
        max(1, image_src.depth) *
        image_src.channel_num *
        channel_pixel_size;
    Assert(size >= image_size, "");

    auto& memory = *image_src.memory;

    vk::Buffer transient_buffer =
        memory.CreateBuffer(image_size, vk::BufferUsageFlagBits::eTransferDst);

    VulkanImage::TransitionImageLayout<vk::ImageLayout::eTransferSrcOptimal>(
        image_src, vk::PipelineStageFlagBits::eBottomOfPipe);

    memory.CopyImageToBuffer(
        image_src.GetImage(),
        image_src.GetWidth(),
        image_src.GetHeight(),
        image_src.GetDepth(),
        transient_buffer,
        image_src.aspect);

    memory.ReadBuffer(transient_buffer, buffer_dst, size);

    memory.DestroyBuffer(transient_buffer);
}

// Function definitions ------------------------------------------------------------------
VulkanImage::VulkanImage(
    const vk::Format format,
    const vk::Extent2D& extent,
    const uint channel_num,
    vk::ImageUsageFlags usage)
    : VulkanImage(format, vk::Extent3D{extent, 0}, channel_num, usage)
{
}

VulkanImage::VulkanImage(
    const vk::Format format,
    const vk::Extent3D& extent,
    const uint channel_num,
    vk::ImageUsageFlags usage)
{
    this->format = format;
    this->width = extent.width;
    this->height = extent.height;
    this->depth = extent.depth;
    this->dimension = 3;
    this->channel_num = channel_num;

    vk::ImageUsageFlags attachment_usage = format == vk::Format::eD32SfloatS8Uint
        ? vk::ImageUsageFlagBits::eDepthStencilAttachment
        : vk::ImageUsageFlagBits::eColorAttachment;

    auto image_pair = memory->CreateImage(
        width,
        height,
        depth,
        format,
        vk::ImageTiling::eOptimal,
        attachment_usage | usage | vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eTransferSrc);

    layout = vk::ImageLayout::eUndefined;

    image = move(image_pair.first);
    image_view = move(image_pair.second);

    if(attachment_usage == vk::ImageUsageFlagBits::eDepthStencilAttachment)
    {
        aspect = vk::ImageAspectFlagBits::eDepth | vk::ImageAspectFlagBits::eStencil;
        TransitionImageLayout<vk::ImageLayout::eDepthStencilAttachmentOptimal>(
            *this, vk::PipelineStageFlagBits::eTopOfPipe);
    }
}

VulkanImage::VulkanImage(
    const vector<byte>& buffer,
    const vk::Extent2D& extent,
    const uint channel_num,
    const vk::Format format,
    const vk::SamplerAddressMode tiling_mode)
    : VulkanImage(buffer, vk::Extent3D(extent, 0), channel_num, format, tiling_mode)
{
    dimension = 2;
};

VulkanImage::VulkanImage(
    const vector<byte>& buffer,
    const vk::Extent3D& extent,
    const uint channel_num,
    const vk::Format format,
    const vk::SamplerAddressMode tiling_mode)
{
    dimension = 3;
    Assert(buffer.size() >= extent.width * extent.height * extent.depth * channel_num, "");
    LoadBuffer(buffer.data(), extent, channel_num, format);
    this->tiling_mode = tiling_mode;

    if(extent.depth > 0)
        VulkanImage::TransitionImageLayout<vk::ImageLayout::eGeneral>(
            *this, vk::PipelineStageFlagBits::eFragmentShader);
};

VulkanImage::VulkanImage(
    const void* buffer,
    const vk::Extent2D& extent,
    const uint channel_num,
    const vk::Format format,
    const vk::SamplerAddressMode tiling_mode)
    : VulkanImage(buffer, vk::Extent3D(extent, 0), channel_num, format, tiling_mode)
{
    dimension = 2;
};

VulkanImage::VulkanImage(
    const void* buffer,
    const vk::Extent3D& extent,
    const uint channel_num,
    const vk::Format format,
    const vk::SamplerAddressMode tiling_mode)
{
    LoadBuffer(buffer, extent, channel_num, format);
    dimension = 3;
    this->tiling_mode = tiling_mode;

    if(extent.depth > 0)
        VulkanImage::TransitionImageLayout<vk::ImageLayout::eGeneral>(
            *this, vk::PipelineStageFlagBits::eFragmentShader);
};

void VulkanImage::LoadBuffer(
    const void* buffer,
    const vk::Extent3D& extent,
    const int channel_num,
    const vk::Format image_format)
{
    Assert(channel_num > 0, "");
    this->format = image_format;
    this->width = extent.width;
    this->height = extent.height;
    this->depth = extent.depth;
    this->channel_num = channel_num;
    vk::Device& device = h_interface->GetDevice();

    // This is the total memory usage of the image. 2D images have a depth of 0, hence
    // the max. The size will vary depending on the format, for example an image with
    // R8B8G8A8 format uses one byte per pixel per channel (e.g. one byte for a red pixel
    // or one byte for an alpha pixel), but an R32 uses 4 bytes for each red pixel.
    const size_t channel_pixel_size = FormatToChannelPixelSize(image_format);
    vk::DeviceSize image_size =
        width * height * max(1, depth) * channel_num * channel_pixel_size;
    vk::Buffer staging_buffer =
        memory->CreateBuffer(image_size, vk::BufferUsageFlagBits::eTransferSrc);
    _SetName(device, staging_buffer, "image_staging_buffer");

    VmaAllocationInfo staging_allocation =
        memory->GetMemoryAllocationInfo(staging_buffer);

    void* data;
    DEBUG_VAR(vk::Result result =)
    device.mapMemory(
        staging_allocation.deviceMemory,
        staging_allocation.offset,
        image_size,
        vk::MemoryMapFlags(0),
        &data);
    Assert(result == vk::Result::eSuccess, "Image memory creation failed.");

    memcpy(data, buffer, image_size);
    device.unmapMemory(staging_allocation.deviceMemory);

    const vk::ImageUsageFlags use = format == vk::Format::eD32SfloatS8Uint ?
        vk::ImageUsageFlagBits::eDepthStencilAttachment :
        vk::ImageUsageFlagBits::eColorAttachment |
        vk::ImageUsageFlagBits::eSampled |
        vk::ImageUsageFlagBits::eStorage;

    aspect = format == vk::Format::eD32SfloatS8Uint ?
        vk::ImageAspectFlagBits::eDepth | vk::ImageAspectFlagBits::eStencil:
        vk::ImageAspectFlagBits::eColor;
    if(format == vk::Format::eD32Sfloat) aspect = vk::ImageAspectFlagBits::eDepth;

    auto image_pair = memory->CreateImage(
        width,
        height,
        depth,
        format,
        vk::ImageTiling::eOptimal,
            use | vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eTransferSrc);

    image = move(image_pair.first);
    image_view = move(image_pair.second);

    VulkanImage::TransitionImageLayout<vk::ImageLayout::eTransferDstOptimal>(
        *this, vk::PipelineStageFlagBits::eTransfer);

    // TODO(low): this is a hack that prevents copying depth values to a depth stencil
    // attachment. All images should be copyable.
    if(format != vk::Format::eD32SfloatS8Uint and format != vk::Format::eD32Sfloat)
        memory->CopyBufferToImage(
            staging_buffer,
            image,
            (uint32_t)width,
            (uint32_t)height,
            (uint32_t)depth,
            aspect);

    if(format == vk::Format::eD32SfloatS8Uint)
    {
        TransitionImageLayout<vk::ImageLayout::eDepthStencilAttachmentOptimal>(
            *this, vk::PipelineStageFlagBits::eTopOfPipe);
    }
    else
    {
        VulkanImage::TransitionImageLayout<vk::ImageLayout::eShaderReadOnlyOptimal>(
            *this, vk::PipelineStageFlagBits::eFragmentShader);
    }
    memory->DestroyBuffer(staging_buffer);
}

void VulkanImage::Clear(const vk::ClearColorValue color)
{
    // TODO(low): change image transition to be non templated instead.
    TransitionImageLayout<vk::ImageLayout::eTransferDstOptimal>(
        *this, vk::PipelineStageFlagBits::eBottomOfPipe);

    vk::CommandBuffer cmd = HardwareInterface::BeginSingleTimeCommands(*h_interface);
    const auto range = vk::ImageSubresourceRange(aspect, 0, 1, 0, 1);
    cmd.clearColorImage(image, layout, color, range);
    HardwareInterface::EndSingleTimeCommands(*h_interface, cmd);

    TransitionImageLayout<vk::ImageLayout::eShaderReadOnlyOptimal>(
        *this, vk::PipelineStageFlagBits::eBottomOfPipe);
}

// Template instantiations.
template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eUndefined>(
    VulkanImage& image, vk::PipelineStageFlags source_stage);
template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eGeneral>(
    VulkanImage& image, vk::PipelineStageFlags source_stage);
template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eColorAttachmentOptimal>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);
template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eDepthStencilAttachmentOptimal>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);
template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eDepthStencilReadOnlyOptimal>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);
template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eShaderReadOnlyOptimal>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);
template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eTransferSrcOptimal>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);
template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eTransferDstOptimal>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);
template void VulkanImage::TransitionImageLayout<vk::ImageLayout::ePreinitialized>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);
template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eDepthReadOnlyStencilAttachmentOptimal>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);
template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eDepthAttachmentStencilReadOnlyOptimal>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);
template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eDepthAttachmentOptimal>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);
template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eDepthReadOnlyOptimal>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);
template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eStencilAttachmentOptimal>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);
template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eStencilReadOnlyOptimal>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);
template void VulkanImage::TransitionImageLayout<vk::ImageLayout::ePresentSrcKHR>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);
template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eSharedPresentKHR>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);
template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eFragmentDensityMapOptimalEXT>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);
template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eFragmentShadingRateAttachmentOptimalKHR>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);
template void VulkanImage::TransitionImageLayout<vk::ImageLayout::eAttachmentOptimalKHR>(
	VulkanImage& image, vk::PipelineStageFlags source_stage);

} // Renderer