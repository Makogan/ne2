#include "Renderer.hpp"

/** @cond */
#include "vulkan/vulkan.hpp"
#include "shaderc/shaderc.hpp"
/** @endcond */

#include "Core/Gallery.hpp"

#include "Swapchain.hpp"
#include "HardwareInterface.hpp"
#include "VulkanMemory.hpp"
#include "VulkanImage.hpp"
#include "Pipeline.hpp"
#include "ShaderProgram.hpp"
#include "Utils.hpp"

using namespace std;

namespace
{
void TransitionSwapchainImageToTransferSrcOptimal(
    Renderer::HardwareInterface* h_interface, vk::Image image)
{
    vk::ImageMemoryBarrier barrier = {};
    barrier.oldLayout = vk::ImageLayout::ePresentSrcKHR;
    barrier.newLayout = vk::ImageLayout::eTransferSrcOptimal;

    barrier.dstAccessMask = vk::AccessFlagBits::eTransferRead;;
    barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.image = image;
    barrier.subresourceRange = vk::ImageSubresourceRange(vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1);

    vk::PipelineStageFlags source_stage = vk::PipelineStageFlagBits::eBottomOfPipe;
    vk::PipelineStageFlags destination_stage = vk::PipelineStageFlagBits::eTransfer;

    vk::CommandBuffer command_buffer =
        Renderer::HardwareInterface::BeginSingleTimeCommands(*h_interface);
    command_buffer.pipelineBarrier(
        source_stage, destination_stage, {}, 0, nullptr, 0, nullptr, 1, &barrier);
    Renderer::HardwareInterface::EndSingleTimeCommands(*h_interface, command_buffer);
}

void TransitionSwapchainImageToPresentKHR(
    Renderer::HardwareInterface* h_interface, vk::Image image)
{
    vk::ImageMemoryBarrier barrier = {};
    barrier.oldLayout = vk::ImageLayout::eTransferSrcOptimal;
    barrier.newLayout = vk::ImageLayout::ePresentSrcKHR;

    barrier.dstAccessMask = vk::AccessFlagBits::eColorAttachmentWrite;
    barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.image = image;
    barrier.subresourceRange =
        vk::ImageSubresourceRange(vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1);

    vk::PipelineStageFlags source_stage = vk::PipelineStageFlagBits::eBottomOfPipe;
    vk::PipelineStageFlags destination_stage =
        vk::PipelineStageFlagBits::eColorAttachmentOutput;

    vk::CommandBuffer command_buffer =
        Renderer::HardwareInterface::BeginSingleTimeCommands(*h_interface);
    command_buffer.pipelineBarrier(
        source_stage, destination_stage, {}, 0, nullptr, 0, nullptr, 1, &barrier);
    Renderer::HardwareInterface::EndSingleTimeCommands(*h_interface, command_buffer);
}

} // namespace

namespace Renderer
{
struct VulkanData
{
    HardwareInterface interface;
    VulkanMemory memory;
    Swapchain swapchain;
    std::vector<Pipeline> pipelines;
    std::vector<VulkanImage> images;
};

NECore::VulkanMetaData InitializeRendering(GLFWwindow* window, bool use_vsync)
{
    Assert(glfwInit(), "");
    NECore::VulkanMetaData vk_meta_data;
    vk_meta_data.vulkan_data = new VulkanData();

    VulkanData& vk_data = *reinterpret_cast<VulkanData*>(vk_meta_data.vulkan_data);
    vk_data.interface = HardwareInterface(vk_meta_data, window);
    vk_data.memory = VulkanMemory(&vk_data.interface);

    // TODO(low): no singletons
    VulkanImage::SetHardwareInterface(&vk_data.interface);
    VulkanImage::SetMemory(&vk_data.memory);

    vk_data.swapchain = Swapchain(&vk_data.interface, &vk_data.memory, use_vsync);

    return vk_meta_data;
}

void DeInitializeRendering(NECore::VulkanMetaData& vk_meta_data)
{
    VulkanData* vk_data = reinterpret_cast<VulkanData*>(vk_meta_data.vulkan_data);
    vk_data->memory.Destroy();
    VulkanImage::Clean();
    delete vk_data;
}

void GpuCompute(
    void* vk_data_ptr,
    const NECore::ComputeRequest& compute_request,
    const NECore::UniformBufferDataContainer& uniform_container)
{
        VulkanData& vk_data = *reinterpret_cast<VulkanData*>(vk_data_ptr);

    Pipeline& pipeline = vk_data.pipelines[(uint64_t)compute_request.shader];

    vk::CommandBuffer& cmd = vk_data.interface.StartCmdUpdate();

    std::vector<std::tuple<VulkanImage*, size_t, size_t>> shader_images;
    shader_images.reserve(compute_request.image_inputs.size());

    for(const auto&[resource, binding, index] : compute_request.image_inputs)
    {
        shader_images.push_back({&vk_data.images[resource.handle], binding, index});
    }

    pipeline.UpdateUniforms(
        cmd,
        uniform_container,
        shader_images,
        compute_request.buffer_inputs,
        vk::PipelineBindPoint::eCompute);

    const auto& work_groups = compute_request.work_groups;
    cmd.dispatch(work_groups[0], work_groups[1], work_groups[2]);

    Warn(
        !(work_groups[0] == 0 || work_groups[1] == 0 || work_groups[2] == 0),
        "One of the work group dimensions is 0, so the compute shader will do no work."
        "They are X {0}, Y {1}, Z {2}",
        to_string(work_groups[0]),
        to_string(work_groups[1]),
        to_string(work_groups[2]));
    vk_data.interface.EndCmdUpdate();

    vk::Queue& queue = vk_data.interface.GetComputeQueue();

    vk::SubmitInfo submit_info = {};
    submit_info.commandBufferCount = 1;
    submit_info.pCommandBuffers = &cmd;

    vk::Device& device = vk_data.interface.GetDevice();
    vk::FenceCreateInfo fence_create_info = {};
    fence_create_info.flags = {};
    auto [result, fence] = device.createFenceUnique(fence_create_info);
    Assert(result == vk::Result::eSuccess, "Failed to create compute fence");

    result = queue.submit(1, &submit_info, *fence);
    Assert(result == vk::Result::eSuccess, "Preparing compute Pipeline failed.");

    result =
        device.waitForFences(1, &*fence, VK_TRUE, std::numeric_limits<uint64_t>::max());
    Assert(
        result == vk::Result::eSuccess,
        "Compute waiting for fence failed with error {0}.",
        vk::to_string(result));
}

void DrawOffScreen(
    void* vk_data_ptr,
    const NECore::RenderRequest& render_request,
    const NECore::UniformBufferDataContainer& uniform_container)
{
        VulkanData& vk_data = *reinterpret_cast<VulkanData*>(vk_data_ptr);

    Pipeline& pipeline = vk_data.pipelines[(uint64_t)render_request.shader];

    vk::Device device =  vk_data.interface.GetDevice();
    vk::CommandBuffer& cmd = vk_data.interface.StartCmdUpdate();

    std::vector<std::tuple<VulkanImage*, size_t, size_t>> shader_images;
    shader_images.reserve(render_request.image_inputs.size());

    for(const auto&[resource, binding, index] : render_request.image_inputs)
    {
        shader_images.push_back({&vk_data.images[resource.handle], binding, index});
    }

    pipeline.UpdateUniforms(
        cmd,
        uniform_container,
        shader_images,
        render_request.buffer_inputs,
        vk::PipelineBindPoint::eGraphics);

    Assert(render_request.image_outputs.size(), "");
    const auto first_handle = render_request.image_outputs[0].first;
    const auto& image = vk_data.images[(uint64_t)first_handle];

    auto width = image.GetWidth();
    auto height = image.GetHeight();

    std::vector<vk::RenderingAttachmentInfoKHR> shader_image_outputs(
        render_request.image_outputs.size());

    NECore::ImageHandle depth_image_handle = NECore::ImageHandle((uint64_t)-1);
    for(const auto&[handle, binding] : render_request.image_outputs)
    {
        if(binding == size_t(-1))
        {
            depth_image_handle = handle;
            shader_image_outputs.pop_back();
            continue;
        }
        Assert(binding < shader_image_outputs.size(), "");
        const auto& image = vk_data.images[(uint64_t)handle];

        Assert(image.GetWidth() == width, "");
        Assert(image.GetHeight() == height, "");

        shader_image_outputs[binding].imageView = image.GetImageView();
        shader_image_outputs[binding].imageLayout = vk::ImageLayout::eAttachmentOptimalKHR;
        shader_image_outputs[binding].loadOp =
            render_request.should_clear?
            vk::AttachmentLoadOp::eClear :
            vk::AttachmentLoadOp::eLoad; // Use eLoad to not clear.
        shader_image_outputs[binding].storeOp = vk::AttachmentStoreOp::eStore;
        shader_image_outputs[binding].clearValue.color =
            vk::ClearColorValue(std::array<float, 4>{0.0f,0.0f,0.0f,0.0f});
    }

    vk::RenderingAttachmentInfoKHR depth_stencil_attachment{};
    vk::RenderingInfoKHR rendering_info = {};
    if((uint64_t)depth_image_handle != (uint64_t)-1)
    {
        const auto& depth_image = vk_data.images[(uint64_t)depth_image_handle];

        depth_stencil_attachment.imageView = depth_image.GetImageView();
        depth_stencil_attachment.imageLayout = vk::ImageLayout::eDepthAttachmentOptimalKHR;
        depth_stencil_attachment.loadOp =
            render_request.should_clear?
                vk::AttachmentLoadOp::eClear :
                vk::AttachmentLoadOp::eLoad;
        depth_stencil_attachment.storeOp = vk::AttachmentStoreOp::eStore;
        depth_stencil_attachment.clearValue.depthStencil =
            vk::ClearDepthStencilValue(1.0f, 0.0f);

        rendering_info.pDepthAttachment = &depth_stencil_attachment;
	    rendering_info.pStencilAttachment = &depth_stencil_attachment;
    }

    rendering_info.renderArea = vk::Rect2D({0, 0}, {width, height});
    rendering_info.layerCount = 1;
    rendering_info.colorAttachmentCount = shader_image_outputs.size();
    rendering_info.pColorAttachments = shader_image_outputs.data();

    vk::Viewport viewport{};
    viewport.x = 0.0f;
    viewport.y = 0.0f;
    viewport.width = width;
    viewport.height = height;
    viewport.minDepth = 0.0f;
    viewport.maxDepth = 1.0f;

    const auto& rs = render_request.scissor;
    vk::Rect2D scissor{};
    scissor.offset = vk::Offset2D(
        render_request.scissor.offset.x,
        render_request.scissor.offset.y);
    scissor.extent = vk::Extent2D(
        rs.extent.width <= -1 ? width : rs.extent.width,
        rs.extent.height <= -1 ? height : rs.extent.height);

    cmd.setScissor(0, 1, &scissor);
    cmd.setViewport(0, 1, &viewport);

    cmd.beginRenderingKHR(rendering_info);

    vk_data.interface.Draw(render_request.mesh_input);
    cmd.endRenderingKHR();

    vk_data.interface.EndCmdUpdate();

    vk::SubmitInfo submit_info = {};
    submit_info.commandBufferCount = 1;
    submit_info.pCommandBuffers = &cmd;

    vk::FenceCreateInfo fence_create_info = {};
    fence_create_info.flags = {};
    auto [result, fence] = device.createFenceUnique(fence_create_info);
    Assert(result == vk::Result::eSuccess, "Failed to create draw to screen fence.");

    vk::Queue& graphic_queue = vk_data.interface.GetGraphicQueue();
    result = graphic_queue.submit(1, &submit_info, *fence);
    if(result != vk::Result::eSuccess)
    {
        Warn(false, "Failed to submit render command");
    }

    result =
        device.waitForFences(1, &*fence, VK_TRUE, std::numeric_limits<uint64_t>::max());
    Assert(result == vk::Result::eSuccess, "End pass waiting for fence failed.");

    result = graphic_queue.waitIdle();
    if(result != vk::Result::eSuccess)
    {
        Warn(false, "Waiting for queue failed.");
    }
}

void DrawToScreen(
    void* vk_data_ptr,
    const NECore::RenderRequest& render_request,
    const NECore::UniformBufferDataContainer& uniform_container)
{
        VulkanData& vk_data = *reinterpret_cast<VulkanData*>(vk_data_ptr);

    Pipeline& pipeline = vk_data.pipelines[(uint64_t)render_request.shader];

    vk::Device device =  vk_data.interface.GetDevice();
    vk::CommandBuffer& cmd = vk_data.interface.StartCmdUpdate();

    std::vector<std::tuple<VulkanImage*, size_t, size_t>> shader_images;
    shader_images.reserve(render_request.image_inputs.size());

    for(const auto&[resource, binding, index] : render_request.image_inputs)
    {
        shader_images.push_back({&vk_data.images[resource.handle], binding, index});
    }
    pipeline.UpdateUniforms(
        cmd,
        uniform_container,
        shader_images,
        render_request.buffer_inputs,
        vk::PipelineBindPoint::eGraphics);

    vk_data.swapchain.DrawToScreen(cmd, pipeline.GetVkPipeline(), render_request);
    vk_data.interface.EndCmdUpdate();

    vk::SubmitInfo submit_info = {};
    submit_info.commandBufferCount = 1;
    submit_info.pCommandBuffers = &cmd;

    vk::FenceCreateInfo fence_create_info = {};
    fence_create_info.flags = {};
    auto [result, fence] = device.createFenceUnique(fence_create_info);
    Assert(result == vk::Result::eSuccess, "Failed to create draw to screen fence.");

    vk::Queue& graphic_queue = vk_data.interface.GetGraphicQueue();
    result = graphic_queue.submit(1, &submit_info, *fence);
    if(result != vk::Result::eSuccess)
    {
        Warn(false, "Failed to submit render command");
    }

    result =
        device.waitForFences(1, &*fence, VK_TRUE, std::numeric_limits<uint64_t>::max());
    Assert(result == vk::Result::eSuccess, "End pass waiting for fence failed.");

    result = graphic_queue.waitIdle();
    if(result != vk::Result::eSuccess)
    {
        Warn(false, "Waiting for queue failed.");
    }
}

void EndFrame(void* vk_data_ptr)
{
    VulkanData& vk_data = *reinterpret_cast<VulkanData*>(vk_data_ptr);
    Assert(vk_data_ptr, "");

    vk_data.swapchain.Sync();
}

void Update(void* vk_data_ptr)
{
    Assert(vk_data_ptr, "");
    VulkanData& vk_data = *reinterpret_cast<VulkanData*>(vk_data_ptr);

    vk_data.interface.StartCmdUpdate();
}

NECore::BufferHandle AllocateBuffer(void* vk_data_ptr, void* data, size_t size)
{
    Assert(vk_data_ptr, "");
    Assert(data, "");
    VulkanData& vk_data = *reinterpret_cast<VulkanData*>(vk_data_ptr);

    const vk::BufferUsageFlags usage =
        vk::BufferUsageFlagBits::eVertexBuffer |
        vk::BufferUsageFlagBits::eIndexBuffer |
        vk::BufferUsageFlagBits::eStorageBuffer;
    return CAST_BUFFER_TO_UINT64(vk_data.memory.CreateFilledBuffer(data, size, usage));
}

void DestroyBuffer(void* vk_data_ptr, NECore::BufferHandle buffer)
{
    Assert(vk_data_ptr, "");
    VulkanData& vk_data = *reinterpret_cast<VulkanData*>(vk_data_ptr);

    vk_data.memory.DestroyBuffer(CAST_TO_VK_BUFFER(buffer));
}

NECore::ImageHandle AllocateImage(
    void* vk_data_ptr,
    const NECore::RawImageData& image,
    const NECore::ImageFormat image_format)
{
    Assert(vk_data_ptr, "");
    Assert(image.data, "Image data pointer is null.");
    VulkanData& vk_data = *reinterpret_cast<VulkanData*>(vk_data_ptr);

    const auto format = (vk::Format)image_format;
    if(image.depth == 0 || image.depth == 1)
        vk_data.images.push_back(
            VulkanImage(
                image.data,
                vk::Extent2D(image.width, image.height),
                image.channel_num,
                format));
    else
        vk_data.images.push_back(
            VulkanImage(
                image.data,
                vk::Extent3D(image.width, image.height, image.depth),
                image.channel_num,
                format));

    return {vk_data.images.size() - 1};
}

void ScreenShot(void* vk_data_ptr, void* buffer, size_t size)
{
    Assert(vk_data_ptr, "");
    Assert(buffer, "");
    VulkanData& vk_data = *reinterpret_cast<VulkanData*>(vk_data_ptr);

    vk::Image sc_image = vk_data.swapchain.GetActiveSwapChainImage();
    const vk::Extent2D& extent = vk_data.swapchain.GetExtent();

    TransitionSwapchainImageToTransferSrcOptimal(&vk_data.interface, sc_image);

    vk_data.memory.CopyImageToBuffer(
        sc_image,
        extent.width,
        extent.height,
        1,
        4,
        vk_data.swapchain.GetSwapchainFormat().format,
        buffer,
        size);

    TransitionSwapchainImageToPresentKHR(&vk_data.interface, sc_image);
}

void ReadImage(void* vk_data_ptr, void* buffer, size_t size, NECore::ImageHandle image_handle)
{
    Assert(vk_data_ptr, "");
    Assert(buffer, "");
    VulkanData& vk_data = *reinterpret_cast<VulkanData*>(vk_data_ptr);

    VulkanImage& image = vk_data.images[(uint64_t)image_handle];

    VulkanImage::TransitionImageLayout<vk::ImageLayout::eTransferSrcOptimal>(
        image, vk::PipelineStageFlagBits::eBottomOfPipe);

    Assert(image.GetDepth() <= 1, "Depth is {0}", std::to_string(image.GetDepth()));
    Assert(
        size >= image.GetWidth() *
               image.GetHeight() *
               std::max((uint)1, image.GetDepth()) *
               image.GetChannelNum() *
               FormatToChannelPixelSize(image.GetFormat()),
        "Are {0} and {1}",
        std::to_string(size),
        std::to_string(
            image.GetWidth() *
            image.GetHeight() *
            std::max((uint)1, image.GetDepth()) *
            image.GetChannelNum() *
            FormatToChannelPixelSize(image.GetFormat())
        )
    );

    vk_data.memory.CopyImageToBuffer(
        image.GetImage(),
        image.GetWidth(),
        image.GetHeight(),
        image.GetDepth(),
        image.GetChannelNum(),
        image.GetFormat(),
        buffer,
        size,
        image.GetAspect() ==
            vk::ImageAspectFlagBits::eColor?
                vk::ImageAspectFlagBits::eColor : vk::ImageAspectFlagBits::eDepth
        );

}

std::vector<std::byte> ReadPixel(
    void* vk_data_ptr,
    size_t x,
    size_t y,
    size_t z,
    NECore::ImageHandle image_handle)
{
    Assert(vk_data_ptr, "");
    VulkanData& vk_data = *reinterpret_cast<VulkanData*>(vk_data_ptr);

    VulkanImage& image = vk_data.images[(uint64_t)image_handle];

    const size_t pixel_size =
        image.GetChannelNum() * FormatToChannelPixelSize(image.GetFormat());
    std::vector<std::byte> buffer(pixel_size);

    VulkanImage::TransitionImageLayout<vk::ImageLayout::eTransferSrcOptimal>(
        image, vk::PipelineStageFlagBits::eBottomOfPipe);

    Assert(image.GetDepth() <= 1, "Depth is {0}", std::to_string(image.GetDepth()));
    Assert(x < image.GetWidth(), "");
    Assert(y < image.GetHeight(), "");
    Assert(z < image.GetDepth() || z == 0, "");

    vk_data.memory.CopyPixelToBuffer(
        image.GetImage(),
        x,
        y,
        z,
        buffer.data(),
        image.GetFormat(),
        image.GetChannelNum(),
        image.GetAspect());

    return buffer;
}


NECore::ShaderHandle AddShaderProgram(
    void* vk_data_ptr,
    const std::vector<std::string>& shader_paths)
{
    Assert(vk_data_ptr, "");
    VulkanData& vk_data = *reinterpret_cast<VulkanData*>(vk_data_ptr);

    Renderer::ShaderProgram shader_program(&(vk_data.interface), shader_paths);

    const vk::Format format = shader_program.GetAttachmentOutputFormat();
    vk_data.pipelines.push_back(
        Pipeline(
            &(vk_data.interface),
            &(vk_data.memory),
            shader_program,
            format == vk::Format::eUndefined ?
                vk_data.swapchain.GetSwapchainFormat().format : format));

    return {vk_data.pipelines.size() - 1};
}
}