#pragma once

#define GLFW_INCLUDE_VULKAN

/** @cond */
#include <vector>

#include "GLFW/glfw3.h"
#include "vulkan/vulkan.hpp"
/** @endcond */

#include "VkExtensionsStubs.hpp"
#include "VulkanDebugging.hpp"
#include "OutOfModule.hpp"

using uint = unsigned int;

namespace Renderer
{
class HardwareInterface
{
private:
    // Warning: Changing the order of these declarations will likely break everything!
    vk::UniqueInstance instance;
    vk::UniqueSurfaceKHR surface;
    vk::PhysicalDevice physical_device;
    vk::UniqueDevice device;
    vk::UniqueDebugUtilsMessengerEXT debug_messenger;
    vk::UniqueCommandPool cmd_pool;
    vk::UniqueCommandBuffer cmd_buffer;

    int32_t queue_family = -1;
    vk::Queue graphic_queue;
    vk::Queue compute_queue;

 public:
    static vk::CommandBuffer BeginSingleTimeCommands(HardwareInterface& h_interface);
    static void EndSingleTimeCommands(
        HardwareInterface& h_interface, vk::CommandBuffer& command_buffer);

    HardwareInterface(){};
    HardwareInterface(NECore::VulkanMetaData& vk_meta_out, GLFWwindow* window);

    vk::Instance& GetInstance() { return *instance; }

    vk::PhysicalDevice& GetPhysicalDevice() { return physical_device; }

    vk::Device& GetDevice() { return *device; }

    vk::SurfaceKHR& GetSurface() { return *surface; }

    vk::CommandPool& GetCommandPool() { return *cmd_pool; }

    vk::CommandBuffer& GetCommandBuffer() { return *cmd_buffer; }

    vk::Queue& GetGraphicQueue() { return graphic_queue; }

    vk::Queue& GetComputeQueue() { return compute_queue; }

    int32_t GetQueueFamily() { return queue_family; }

    vk::CommandBuffer& StartCmdUpdate();

    void EndCmdUpdate();

    void Draw(const NECore::GraphicsInputDescription& inputs);

    std::vector<vk::UniqueSemaphore> CreateSemaphores(uint32_t semaphore_num);

    std::vector<vk::UniqueFence> CreateFences(uint32_t fence_num);
};

vk::SurfaceFormatKHR GetSurfaceFormat(
    vk::PhysicalDevice& phys_device, vk::SurfaceKHR& surface);
} // Renderer