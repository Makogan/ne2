#pragma once

/** @cond */
#include <map>

#include "vulkan/vulkan.hpp"
/** @endcond */

#include "OutOfModule.hpp"
#include "Core/Enumerators.hpp"

namespace Renderer
{
class HardwareInterface;

struct ShaderConfig
{
    bool depth_test = true;
    bool depth_write = true;
    float line_width = 3;
    vk::PrimitiveTopology topology = vk::PrimitiveTopology::eTriangleList;
    vk::PolygonMode polygon_mode = vk::PolygonMode::eFill;
    bool use_viewport_extent = true;
    vk::Extent2D render_extent = vk::Extent2D(-1, -1);
    vk::CullModeFlagBits cull_mode = vk::CullModeFlagBits::eBack;
    vk::FrontFace front_face = vk::FrontFace::eClockwise;
    vk::CompareOp depth_compare = vk::CompareOp::eLess;
};

class ShaderProgram
{
  private:
    std::map<uint, size_t> uniform_binding_size_map;

    std::map<uint, vk::SamplerCreateInfo> sampler_config_map;

    std::vector<vk::UniqueShaderModule> shader_modules;
    std::vector<vk::ShaderStageFlagBits> shader_stage_flags;
    std::vector<vk::VertexInputBindingDescription> vertex_binding_descriptions;
    std::vector<vk::VertexInputAttributeDescription> attribute_descriptions;
    std::vector<vk::DescriptorSetLayoutBinding> uniform_layout_descriptions;

    ShaderConfig shader_config;

    uint attachment_count = 1;
    vk::Format attachment_output_format;

  public:
    ShaderProgram(){};

    ShaderProgram(
        HardwareInterface* hi,
        const std::vector<std::string>& shader_paths);

    const std::vector<vk::UniqueShaderModule>& GetShaderModules() const
    { return shader_modules; }

    const std::vector<vk::ShaderStageFlagBits>& GetShaderStageFlags() const
    { return shader_stage_flags; }

    const std::vector<vk::VertexInputBindingDescription>& GetVertexBindingDescriptions() const
    { return vertex_binding_descriptions; }

    const std::vector<vk::VertexInputAttributeDescription>& GetAttributeDescriptions() const
    { return attribute_descriptions; }

    const std::vector<vk::DescriptorSetLayoutBinding>& GetUniformLayoutDescriptions() const
    { return uniform_layout_descriptions; }

    const std::map<uint, size_t>& GetUniformBindingSizeMap() const
    { return uniform_binding_size_map; }

    const std::map<uint, vk::SamplerCreateInfo>& GetSamplerConfigMap() const
    { return sampler_config_map; }
    

    const ShaderConfig& GetShaderConfig() const { return shader_config; }

    uint GetAttachmentCount() const { return attachment_count; }

    vk::Format GetAttachmentOutputFormat() const { return attachment_output_format; }

    bool IsCompute() const
    {
      return
      (shader_stage_flags.size() == 1) &&
      (shader_stage_flags[0] == vk::ShaderStageFlagBits::eCompute);
    }
};
} // Renderer