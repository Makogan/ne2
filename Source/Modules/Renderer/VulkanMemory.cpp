#define VMA_IMPLEMENTATION
#include "VulkanMemory.hpp"

/** @cond */
#include <algorithm>
#include <assert.h>
/** @endcond */

#include "HardwareInterface.hpp"
#include "Utils.hpp"
#include "VulkanDebugging.hpp"

using namespace std;

namespace Renderer {
// Function declarations -----------------------------------------------------------------
namespace
{
vk::UniqueImageView CreateImageViewInternal(
    vk::Device& device,
    vk::Image& image,
    vk::Format format,
    vk::ImageAspectFlags image_aspect = vk::ImageAspectFlagBits::eColor,
    vk::ImageViewType dimension = vk::ImageViewType::e2D);
}  // namespace

// Function definitions ------------------------------------------------------------------
VulkanMemory::VulkanMemory(HardwareInterface* hi)
    : h_interface(hi)
{
    VmaAllocatorCreateInfo allocator_info = {};
    allocator_info.physicalDevice = hi->GetPhysicalDevice();
    allocator_info.device = hi->GetDevice();
    allocator_info.instance = hi->GetInstance();

    vmaCreateAllocator(&allocator_info, &vma_allocator);
}

void VulkanMemory::Destroy()
{
    for(auto& buffer_allocation_pair: buffer_memory_allocations)
    {
        vmaDestroyBuffer(
            vma_allocator,
            (VkBuffer)buffer_allocation_pair.first,
            buffer_allocation_pair.second);
    }

    for(auto& image_allocation_pair: image_memory_allocations)
    {
        vmaDestroyImage(
            vma_allocator,
            (VkImage)image_allocation_pair.first,
            image_allocation_pair.second);
    }

    vmaDestroyAllocator(vma_allocator);
}

pair<vk::Image, vk::UniqueImageView> VulkanMemory::CreateImage(
    uint32_t width,
    uint32_t height,
    uint32_t depth,
    vk::Format format,
    vk::ImageTiling tiling,
    vk::ImageUsageFlags usage,
    vk::ImageLayout initial_layout)
{
    vk::Device& device = h_interface->GetDevice();

    const bool is_3D_image = depth > 0;
    // Set image creation information.
    vk::ImageCreateInfo image_info = {};
    image_info.imageType = is_3D_image ? vk::ImageType::e3D : vk::ImageType::e2D;
    image_info.format = format;
    image_info.extent = vk::Extent3D(width, height, max(uint32_t(1), depth));
    image_info.mipLevels = 1;
    image_info.arrayLayers = 1;
    image_info.samples = vk::SampleCountFlagBits::e1;
    image_info.tiling = tiling;
    image_info.usage = usage;

    image_info.sharingMode = vk::SharingMode::eExclusive;
    image_info.initialLayout = initial_layout;

    VmaAllocationCreateInfo allocation_info = {};
    allocation_info.usage = VMA_MEMORY_USAGE_AUTO_PREFER_DEVICE;

    VmaAllocation allocation;
    vk::Image image;
    VkResult result = vmaCreateImage(
        vma_allocator,
        (VkImageCreateInfo*)&image_info,
        &allocation_info,
        (VkImage*)&image,
        &allocation,
        nullptr);

    Assert(result == (uint)vk::Result::eSuccess,
        "Failed to create image.\nError code: " + vk::to_string((vk::Result)result));

    image_memory_allocations[(uint64_t)((VkImage)image)] = allocation;
    _SetName(device, image, "image");

    vk::ImageAspectFlags aspect = vk::ImageAspectFlagBits::eColor;
    if(usage & vk::ImageUsageFlagBits::eDepthStencilAttachment)
        aspect = vk::ImageAspectFlagBits::eDepth;

    vk::UniqueImageView image_view = CreateImageViewInternal(
        device, image, format, aspect, vk::ImageViewType(image_info.imageType));

    return {image, move(image_view)};
}

void VulkanMemory::DestroyImage(vk::Image image)
{
    Assert(image_memory_allocations.count((uint64_t)((VkImage)image)), "");
    vmaDestroyImage(
        vma_allocator,
        (VkImage)image,
        image_memory_allocations.at((uint64_t)((VkImage)image)));

    image_memory_allocations.erase((uint64_t)((VkImage)image));
}

void VulkanMemory::DestroyBuffer(vk::Buffer buffer)
{
    Assert(buffer_memory_allocations.count((uint64_t)(VkBuffer)buffer), "");
    auto buffer_allocation = buffer_memory_allocations.at((uint64_t)(VkBuffer)buffer);
    vmaDestroyBuffer(vma_allocator, buffer, buffer_allocation);
    buffer_memory_allocations.erase((uint64_t)(VkBuffer)buffer);
}

vk::UniqueImageView VulkanMemory::CreateImageView(vk::Image image, vk::Format format)
{
    vk::Device& device = h_interface->GetDevice();
    return CreateImageViewInternal(device, image, format);
}

vk::Buffer VulkanMemory::CreateBuffer(
    size_t size, vk::BufferUsageFlags type)
{
    vk::Device& device = h_interface->GetDevice();

    vk::BufferCreateInfo buffer_info = {};
    buffer_info.size = size;
    buffer_info.usage = type;

    buffer_info.sharingMode = vk::SharingMode::eExclusive;

    VmaAllocationCreateInfo alloc_info = {};
    alloc_info.usage = VMA_MEMORY_USAGE_AUTO;

    alloc_info.requiredFlags =
        VkMemoryPropertyFlags(
            vk::MemoryPropertyFlagBits::eHostVisible |
            vk::MemoryPropertyFlagBits::eDeviceLocal);

    VmaAllocation allocation;
    vk::Buffer buffer;
    VkResult result = vmaCreateBuffer(
        vma_allocator,
        (VkBufferCreateInfo*)&buffer_info,
        &alloc_info,
        (VkBuffer*)&buffer,
        &allocation,
        nullptr);

    Assert(
        result == (uint)vk::Result::eSuccess,
        "Failed to create buffer:\n" + to_string((vk::Result)result));

    buffer_memory_allocations[(uint64_t)((VkBuffer)buffer)] = allocation;
    _SetName(device, buffer, "buffer");
    return buffer;
}

vk::Buffer VulkanMemory::CreateFilledBuffer(
    const void* object, uint32_t size, vk::BufferUsageFlags type)
{
        auto device = h_interface->GetDevice();

    using namespace vk;
    DeviceSize buffer_size = size;
    // Create buffer optimized for data copying
    vk::Buffer staging_buffer = CreateBuffer(
        buffer_size,
        BufferUsageFlagBits::eTransferSrc);
    VmaAllocationInfo staging_buffer_allocation = GetMemoryAllocationInfo(staging_buffer);
    // Copy data from pointer onto vulkan buffer
    void* data;
    DEBUG_VAR(vk::Result result =)
    device.mapMemory(
        staging_buffer_allocation.deviceMemory,
        staging_buffer_allocation.offset,
        buffer_size,
        MemoryMapFlags(0),
        &data);
    Assert(result == vk::Result::eSuccess, "Filled buffer memory creation failed.");
    memcpy(data, object, buffer_size);

    device.unmapMemory(staging_buffer_allocation.deviceMemory);
    // Create a buffer optimized for selected useage (e.g uniform, vertex...)

    vk::Buffer buffer =
        CreateBuffer(buffer_size, BufferUsageFlagBits::eTransferDst | type);
    // Copy data from staging buffer onto final buffer
    vk::CommandBuffer c_buffer = HardwareInterface::BeginSingleTimeCommands(*h_interface);
    vk::BufferCopy copy_region(0, 0, buffer_size);
    c_buffer.copyBuffer(staging_buffer, buffer, 1, &copy_region);
    HardwareInterface::EndSingleTimeCommands(*h_interface, c_buffer);

    _SetName(device, buffer, "filled_buffer");

    DestroyBuffer(staging_buffer);
    return buffer;
}

void VulkanMemory::ReadBuffer(vk::Buffer buffer, void* container, size_t size)
{
    auto device = h_interface->GetDevice();

    using namespace vk;
    VmaAllocationInfo buffer_allocation = GetMemoryAllocationInfo(buffer);
    // Copy data from pointer onto vulkan buffer
    void* data;
    DEBUG_VAR(vk::Result result =)
    device.mapMemory(
        buffer_allocation.deviceMemory,
        buffer_allocation.offset,
        size,
        MemoryMapFlags(0),
        &data);
    Assert(result == vk::Result::eSuccess, "Filled buffer memory creation failed.");
    memcpy(container, data, size);

    device.unmapMemory(buffer_allocation.deviceMemory);
}

// Warning: this can overwrite other buffers, use with care.
void VulkanMemory::UpdateBuffer(
    const vk::Buffer& buffer, const void* object_data, size_t size)
{
    auto device = h_interface->GetDevice();
    auto memory_allocation = GetMemoryAllocationInfo(buffer);
    Assert(size <= memory_allocation.size, "");
    void* data;
    vk::Result result = device.mapMemory(
        memory_allocation.deviceMemory,
        memory_allocation.offset,
        size,
        vk::MemoryMapFlags(0),
        &data);

    Assert(result == vk::Result::eSuccess, "Updating buffer failed.");

    memcpy(data, object_data, size);
    device.unmapMemory(memory_allocation.deviceMemory);
}

void VulkanMemory::UpdateBuffer(
    const vk::Buffer& buffer,
    const std::byte datum,
    uint32_t copy_count,
    const uint offset)
{
    auto device = h_interface->GetDevice();
    VmaAllocationInfo memory_allocation = GetMemoryAllocationInfo(buffer);
    Assert(copy_count + offset <= memory_allocation.size, "");
    void* data;
    vk::Result result = device.mapMemory(
        memory_allocation.deviceMemory,
        memory_allocation.offset,
        copy_count,
        vk::MemoryMapFlags(0),
        &data);

    Assert(result == vk::Result::eSuccess, "Updating buffer failed.");

    std::byte* target = static_cast<std::byte*>(data);
    for(uint i = 0; i < copy_count; i++)
    {
        *(target + i + offset) = datum;
    }
    device.unmapMemory(memory_allocation.deviceMemory);
}

void VulkanMemory::CopyBufferToImage(
    vk::Buffer buffer, vk::Image image, uint32_t width, uint32_t height, uint32_t depth,
    vk::ImageAspectFlags aspect)
{
    vk::CommandBuffer command_buffer =
        HardwareInterface::BeginSingleTimeCommands(*h_interface);

    vk::BufferImageCopy region(
        0,
        0,
        0,
        vk::ImageSubresourceLayers(aspect, 0, 0, 1),
        {0, 0, 0},
        {width, height, max((uint)1, depth)});

    command_buffer.copyBufferToImage(
        buffer, image, vk::ImageLayout::eTransferDstOptimal, 1, &region);

    HardwareInterface::EndSingleTimeCommands(*h_interface, command_buffer);
}

void VulkanMemory::CopyImageToBuffer(
    vk::Image image,
    uint32_t width,
    uint32_t height,
    uint32_t depth,
    uint32_t channel_num,
    vk::Format format,
    void* buffer,
    size_t size,
    vk::ImageAspectFlags aspect)
{
    const size_t channel_pixel_size = FormatToChannelPixelSize(format);
    const size_t image_size =
        width *
        height *
        std::max((uint)1, depth) *
        channel_num *
        channel_pixel_size;
    Assert(size >= image_size, "");

    vk::Buffer transient_buffer =
        CreateBuffer(image_size, vk::BufferUsageFlagBits::eTransferDst);

    CopyImageToBuffer(
        image,
        width,
        height,
        depth,
        transient_buffer,
        aspect);

    ReadBuffer(transient_buffer, buffer, image_size);

    DestroyBuffer(transient_buffer);
}

void VulkanMemory::CopyImageToBuffer(
    vk::Image image,
    uint32_t width,
    uint32_t height,
    uint32_t depth,
    vk::Buffer buffer,
    vk::ImageAspectFlags aspect)
{
    vk::CommandBuffer command_buffer =
        HardwareInterface::BeginSingleTimeCommands(*h_interface);

    vk::BufferImageCopy region(
        0,
        0,
        0,
        vk::ImageSubresourceLayers(aspect, 0, 0, 1),
        {0, 0, 0},
        {width, height, max((uint)1, depth)});

    command_buffer.copyImageToBuffer(
        image, vk::ImageLayout::eTransferSrcOptimal, buffer, 1, &region);

    HardwareInterface::EndSingleTimeCommands(*h_interface, command_buffer);
}

void VulkanMemory::CopyPixelToBuffer(
    vk::Image image,
    uint32_t x,
    uint32_t y,
    uint32_t z,
    void* buffer,
    vk::Format format,
    uint channel_num,
    vk::ImageAspectFlags aspect)
{
    const size_t channel_pixel_size = FormatToChannelPixelSize(format);
    const size_t pixel_size = channel_num * channel_pixel_size;

    vk::Buffer transient_buffer =
        CreateBuffer(pixel_size, vk::BufferUsageFlagBits::eTransferDst);

    vk::CommandBuffer command_buffer =
        HardwareInterface::BeginSingleTimeCommands(*h_interface);

    vk::BufferImageCopy region(
        0,
        0,
        0,
        vk::ImageSubresourceLayers(aspect, 0, 0, 1),
        {(int32_t)x, (int32_t)y, (int32_t)z},
        {1, 1, 1});

    command_buffer.copyImageToBuffer(
        image, vk::ImageLayout::eTransferSrcOptimal, transient_buffer, 1, &region);

    HardwareInterface::EndSingleTimeCommands(*h_interface, command_buffer);

    ReadBuffer(transient_buffer, buffer, pixel_size);

    DestroyBuffer(transient_buffer);
}

void VulkanMemory::CopyImageToImage(
    const vk::Image image1,
    const vk::Image image2,
    uint32_t width,
    uint32_t height,
    vk::ImageAspectFlags aspect1,
    vk::ImageAspectFlags aspect2)
{
    vk::CommandBuffer command_buffer =
        HardwareInterface::BeginSingleTimeCommands(*h_interface);

    vk::ImageCopy image_copy = {};
    image_copy.srcSubresource.aspectMask = aspect1;
    image_copy.srcSubresource.layerCount = 1;
    image_copy.dstSubresource.aspectMask = aspect2;
    image_copy.dstSubresource.layerCount = 1;
    image_copy.extent.width = width;
    image_copy.extent.height = height;
    image_copy.extent.depth = 1;

    command_buffer.copyImage(
        image1,
        vk::ImageLayout::eTransferSrcOptimal,
        image2,
        vk::ImageLayout::eTransferDstOptimal,
        1,
        &image_copy);

    HardwareInterface::EndSingleTimeCommands(*h_interface, command_buffer);
}

// Internal functions --------------------------------------------------------------------
namespace {
vk::UniqueImageView CreateImageViewInternal(
    vk::Device& device,
    vk::Image& image,
    vk::Format format,
    vk::ImageAspectFlags image_aspect,
    vk::ImageViewType dimension)
{
    vk::ImageSubresourceRange sub_resource_range = {};
    sub_resource_range.aspectMask = image_aspect;
    sub_resource_range.baseMipLevel = 0;
    sub_resource_range.levelCount = 1;
    sub_resource_range.baseArrayLayer = 0;
    sub_resource_range.layerCount = 1;

    //https://www.khronos.org/registry/vulkan/specs/1.1-extensions/man/html/VkImageViewCreateInfo.html
    vk::ImageViewCreateInfo create_info(
        {},
        image,
        dimension,
        format,
        vk::ComponentMapping(),
        sub_resource_range);
    auto [result, image_view] = device.createImageViewUnique(create_info);
    Assert(result == vk::Result::eSuccess, "Failed to create image view!");

    return move(image_view);
}

}  // namespace

} // Renderer