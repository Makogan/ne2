#pragma once

/** @cond */
#include "vulkan/vulkan.hpp"
/** @endcond */

namespace vk
{
// cereal functions for serialization.
template <class Archive>
void serialize( Archive& archive, DescriptorSetLayoutBinding& descriptor )
{
    archive(
        CEREAL_NVP(descriptor.binding),
        CEREAL_NVP(descriptor.descriptorType),
        CEREAL_NVP(descriptor.descriptorCount),
        (uint32_t&)descriptor.stageFlags
    );
}

template <class Archive>
void serialize( Archive& archive, VertexInputAttributeDescription& descriptor )
{
    archive(
        CEREAL_NVP(descriptor.location),
        CEREAL_NVP(descriptor.binding),
        CEREAL_NVP(descriptor.format),
        CEREAL_NVP(descriptor.offset)
    );
}

template <class Archive>
void serialize( Archive& archive, VertexInputBindingDescription& descriptor )
{
    archive(
        CEREAL_NVP(descriptor.binding),
        CEREAL_NVP(descriptor.stride),
        CEREAL_NVP(descriptor.inputRate)
    );
}

template <class Archive>
void serialize( Archive& archive, Extent2D& extent )
{
    archive(
        CEREAL_NVP(extent.width),
        CEREAL_NVP(extent.height)
    );
}

template <class Archive>
void serialize( Archive& archive, SamplerCreateInfo& sampler_info )
{
    archive(
        CEREAL_NVP(sampler_info.magFilter),
        CEREAL_NVP(sampler_info.minFilter),
        CEREAL_NVP(sampler_info.mipmapMode),
        CEREAL_NVP(sampler_info.addressModeU),
        CEREAL_NVP(sampler_info.addressModeV),
        CEREAL_NVP(sampler_info.addressModeW),
        CEREAL_NVP(sampler_info.mipLodBias),
        CEREAL_NVP(sampler_info.anisotropyEnable),
        CEREAL_NVP(sampler_info.maxAnisotropy),
        CEREAL_NVP(sampler_info.compareEnable),
        CEREAL_NVP(sampler_info.compareOp),
        CEREAL_NVP(sampler_info.minLod),
        CEREAL_NVP(sampler_info.maxLod),
        CEREAL_NVP(sampler_info.borderColor),
        CEREAL_NVP(sampler_info.unnormalizedCoordinates)
    );
}

}
