#include "HardwareInterface.hpp"

/** @cond */
#include <algorithm>
#include <map>
#include <set>
#include <vector>
/** @endcond */

#include "VkExtensionsStubs.hpp"
#include "VulkanDebugging.hpp"

using namespace std;
using namespace Log;

namespace Renderer {

// Globals -------------------------------------------------------------------------------
const std::vector<const char*> VALIDATION_LAYERS = {
    "VK_LAYER_KHRONOS_validation",
};

const std::vector<const char*> INSTANCE_EXTENSIONS = {
    VK_EXT_DEBUG_UTILS_EXTENSION_NAME,
    VK_KHR_GET_PHYSICAL_DEVICE_PROPERTIES_2_EXTENSION_NAME
};

const std::vector<const char*> DEVICE_EXTENSIONS = {
    VK_KHR_SWAPCHAIN_EXTENSION_NAME,
    VK_KHR_DYNAMIC_RENDERING_EXTENSION_NAME,
    VK_EXT_EXTENDED_DYNAMIC_STATE_EXTENSION_NAME,
    VK_EXT_CONSERVATIVE_RASTERIZATION_EXTENSION_NAME,
};

namespace NEVK
{
inline std::string GetVKVersion(uint32_t mask)
{
    std::string version = std::to_string(VK_VERSION_MAJOR(mask));
    version += "." + std::to_string(VK_VERSION_MINOR(mask));
    version += "." + std::to_string(VK_VERSION_PATCH(mask));

    return version;
}
std::string to_string(const vk::PhysicalDeviceProperties& properties)
{
    std::string message = "";
    message += "API version: " +  GetVKVersion(properties.apiVersion) + "\n";
    message += "Driver version: " + GetVKVersion(properties.driverVersion) + "\n";
    message += "Vendor ID: " + std::to_string(properties.vendorID) + "\n";
    message += "Device ID: " + std::to_string(properties.deviceID) + "\n";
    message += "Device type: " + vk::to_string(properties.deviceType) + "\n";
    message += "Device name: " + std::string(properties.deviceName.data()) + "\n";
    return message;
}
} // NEVK

// Function declarations -----------------------------------------------------------------
namespace
{
static bool CheckAvailableLayers(NECore::VulkanMetaData& vk_meta_out);
static vk::UniqueInstance CreateInstance(NECore::VulkanMetaData& vk_meta_out);
static vk::UniqueSurfaceKHR CreateSurface(vk::Instance& instance, GLFWwindow* window);
static vk::UniqueDebugUtilsMessengerEXT CreateDebugMessenger(vk::Instance& instance);
static vk::PhysicalDevice PickPhysicalDevice(
    vk::Instance& instance,
    const vk::SurfaceKHR& surface,
    const vector<const char*>& device_extensions,
    NECore::VulkanMetaData& vk_meta_out);

// Create a logical device
static vk::UniqueDevice CreateLogicalDevice(
    vk::Instance& instance,
    const vk::SurfaceKHR& surface,
    vk::PhysicalDevice& phys_device,
    const vector<const char*>& device_extensions,
    NECore::VulkanMetaData& vk_meta_out);

static vk::UniqueCommandPool CreateCommandPool(HardwareInterface& hi);

static bool RequestExtensions(vector<const char*>& ret_vec);

static set<vk::PhysicalDevice> FilterDevices(
    const vector<vk::PhysicalDevice>& devices,
    const vk::SurfaceKHR& surface,
    const vector<const char*>& device_extensions);

static bool VerifyExtensionsSupport(
    const vk::PhysicalDevice& device, const std::vector<const char*>& device_extensions);

int32_t _FindGraphicsAndComputeQueueFamily(
    const vk::PhysicalDevice& device, const vk::SurfaceKHR& surface);
}  // namespace

// Static functions ----------------------------------------------------------------------
vk::CommandBuffer HardwareInterface::BeginSingleTimeCommands(
    HardwareInterface& h_interface)
{
    vk::Device& device = h_interface.GetDevice();
    vk::CommandPool& cmd_pool = h_interface.GetCommandPool();

    vk::CommandBufferAllocateInfo alloc_info(
        cmd_pool, vk::CommandBufferLevel::ePrimary, 1);

    auto [result, command_buffers] = device.allocateCommandBuffers(alloc_info);
    Assert(
        result == vk::Result::eSuccess,
        "Single time command failed to allocate buffers.");

    vk::CommandBufferBeginInfo begin_info(vk::CommandBufferUsageFlagBits::eOneTimeSubmit);
    result = command_buffers[0].begin(&begin_info);
    Assert(result == vk::Result::eSuccess, "Single time command failed to begin.");

    return command_buffers[0];
}

void HardwareInterface::EndSingleTimeCommands(
    HardwareInterface& h_interface, vk::CommandBuffer& command_buffer)
{
    vk::Device& device = h_interface.GetDevice();
    vk::CommandPool& cmd_pool = h_interface.GetCommandPool();
    vk::Queue& graphic_queue = h_interface.GetGraphicQueue();

    {
        DEBUG_VAR(vk::Result result =) command_buffer.end();
        Assert(result == vk::Result::eSuccess, "End single time command failed.");
    }

    vk::FenceCreateInfo fence_create_info = {};
    fence_create_info.flags = {};
    auto [result, fence] = device.createFenceUnique(fence_create_info);
    Assert(result == vk::Result::eSuccess, "Failed to create end pass fence");

    vk::SubmitInfo submit_info = {};
    submit_info.commandBufferCount = 1;
    submit_info.pCommandBuffers = &command_buffer;

    result = graphic_queue.submit(1, &submit_info, *fence);
    Assert(result == vk::Result::eSuccess, "Single time command failed to submit.");

    result =
        device.waitForFences(1, &*fence, VK_TRUE, std::numeric_limits<uint64_t>::max());

    Assert(result == vk::Result::eSuccess, "Single Time waiting for fence failed.");

    result = graphic_queue.waitIdle();
    Assert(result == vk::Result::eSuccess, "Single time command failed to wait.");

    device.freeCommandBuffers(cmd_pool, 1, &command_buffer);
}

// Class functions -----------------------------------------------------------------------
HardwareInterface::HardwareInterface(NECore::VulkanMetaData& vk_meta_out, GLFWwindow* window)
    : instance(CreateInstance(vk_meta_out))
    , surface(CreateSurface(*instance, window))
    , physical_device(PickPhysicalDevice(
        *instance, *surface, DEVICE_EXTENSIONS, vk_meta_out))
    , device(CreateLogicalDevice(
        *instance, *surface, physical_device, DEVICE_EXTENSIONS, vk_meta_out))
{
    auto device_name = physical_device.getProperties().deviceName;
    _SetName(*device, physical_device, device_name);
    _SetName(
        *device,
        *device,
        "Logical device: " + std::string(device_name));
    _SetName(*device, *surface, "Main surface");

    debug_messenger = CreateDebugMessenger(*instance);

    queue_family = _FindGraphicsAndComputeQueueFamily(physical_device, *surface);
    Assert(queue_family >= 0, "Queue family not found.");
    const vk::QueueFamilyProperties queue_properties =
        physical_device.getQueueFamilyProperties()[queue_family];

    const int compute_queue_index = queue_properties.queueCount >= 2 ? 1 : 0;
    graphic_queue = device->getQueue(queue_family, 0);
    compute_queue = device->getQueue(queue_family, compute_queue_index);
    _SetName(*device, graphic_queue, "Main queue");

    cmd_pool = CreateCommandPool(*this);
}

vk::CommandBuffer& HardwareInterface::StartCmdUpdate()
{
    vk::CommandBufferAllocateInfo alloc_info(
        *cmd_pool, vk::CommandBufferLevel::ePrimary, 1);

    auto [result, buffers] = device->allocateCommandBuffersUnique(alloc_info);
    Assert(result == vk::Result::eSuccess, "Failed to create command buffers");

    cmd_buffer = move(buffers[0]);

    vk::CommandBufferBeginInfo begin_info{};

    vk::Result result_b = cmd_buffer->begin(&begin_info);
    Assert(result_b == vk::Result::eSuccess, "Failed to begin recording command buffer!");

    _SetName(*device, *cmd_buffer, "main cmd buffer");

    return *cmd_buffer;
}

void HardwareInterface::Draw(const NECore::GraphicsInputDescription& inputs)
{
        vk::Buffer vk_index_buffer = CAST_TO_VK_BUFFER(inputs.vertex_inputs.index_buffer);
    vk::Buffer vk_instance_buffer = CAST_TO_VK_BUFFER(inputs.instance_buffer);

    const uint vert_buffer_count = inputs.vertex_inputs.buffers.size();
    vector<vk::DeviceSize> offsets(vert_buffer_count, 0);

    const uint instance_buffer_binding_point = vert_buffer_count;
    if(inputs.instance_count > 0)
        cmd_buffer->bindVertexBuffers(
            instance_buffer_binding_point, 1, &vk_instance_buffer, offsets.data());

    uint instance_count = std::max(uint(1), inputs.instance_count);

    const uint vertex_buffer_first_binding_point = 0;
    cmd_buffer->bindVertexBuffers(
        vertex_buffer_first_binding_point,
        vert_buffer_count,
        (vk::Buffer*)inputs.vertex_inputs.buffers.data(),
        offsets.data());

    const uint first_instance = 0;
    // If the index buffer is 0 it was not allocated, so we don't use the index buffer.
    if((unsigned long int)((VkBuffer)(vk_index_buffer)) != 0)
    {
        cmd_buffer->bindIndexBuffer(vk_index_buffer, 0, vk::IndexType::eUint32);
        cmd_buffer->drawIndexed(
            inputs.element_count,
            instance_count,
            inputs.index_offset,
            inputs.vertex_offset,
            first_instance);
    }
    else
    {
        cmd_buffer->draw(
            inputs.element_count,
            instance_count,
            inputs.vertex_offset,
            first_instance);
    }
}

void HardwareInterface::EndCmdUpdate()
{
    DEBUG_VAR(vk::Result result =) cmd_buffer->end();
    Assert(result == vk::Result::eSuccess, "Failed to record command buffer!");
}

vector<vk::UniqueSemaphore> HardwareInterface::CreateSemaphores(uint32_t semaphore_num)
{
    std::vector<vk::UniqueSemaphore> semaphores(semaphore_num);
    vk::SemaphoreCreateInfo semaphore_info = {};
    for(size_t i = 0; i < semaphore_num; i++)
    {
        auto [result, semaphore] = device->createSemaphoreUnique(semaphore_info);
        Assert(result == vk::Result::eSuccess, "Could not create semaphore");
        semaphores[i] = std::move(semaphore);
        _SetName(*device, *semaphores[i], "Semaphore: " + to_string(i));
    }

    return semaphores;
}

vector<vk::UniqueFence> HardwareInterface::CreateFences(uint32_t fence_num)
{
    std::vector<vk::UniqueFence> fences(fence_num);
    vk::FenceCreateInfo fence_info(vk::FenceCreateFlagBits::eSignaled);
    for(size_t i = 0; i < fence_num; i++)
    {
        auto [result, fence] = device->createFenceUnique(fence_info);
        Assert(
            result == vk::Result::eSuccess,
            "Failed to create synchronization objects for a frame!");
        fences[i] = std::move(fence);
    }

    return fences;
}

// Helper functions ----------------------------------------------------------------------

vk::SurfaceFormatKHR GetSurfaceFormat(
    vk::PhysicalDevice& phys_device, vk::SurfaceKHR& surface)
{
    // Get supported formats
    auto [result, formats] = phys_device.getSurfaceFormatsKHR(surface);
    Assert(
        result == vk::Result::eSuccess && formats.size() > 0,
        "Failed to query surface formats");

    // Attempt to find the most suitable format. If it isn't found, return the first one
    for(auto& surf_format: formats)
    {
        // If the format supports 4 channels and non linbear sRGB, return this format
        if(surf_format.format == vk::Format::eB8G8R8A8Unorm &&
           surf_format.colorSpace == vk::ColorSpaceKHR::eSrgbNonlinear)
            return surf_format;
    }
    return formats[0];
}

// Internal functions --------------------------------------------------------------------
namespace
{
// Create a vulkan instance
static vk::UniqueInstance CreateInstance(NECore::VulkanMetaData& vk_meta_out)
{
    Assert(CheckAvailableLayers(vk_meta_out), "Validation layers requested, but not available!");

    std::vector<const char*> required_extensions;
    DEBUG_VAR(bool success =) RequestExtensions(required_extensions);
    Assert(
        success, "Missing a required extension");

    auto enabled = vk::ValidationFeatureEnableEXT::eBestPractices;
    vk::ValidationFeaturesEXT features;
    features.enabledValidationFeatureCount = 0;
    features.pEnabledValidationFeatures = &enabled;

    // Setup general information about the current application.
    vk::ApplicationInfo program_info(
        "NeverEngine",
        VK_MAKE_VERSION(1, 0, 0),
        "No Engine",
        VK_MAKE_VERSION(1, 0, 0),
        VK_API_VERSION_1_2);
    // Create Vulkan instance to communicate with the loader.
    vk::InstanceCreateInfo create_info = {};
    create_info.pNext = &features;
    create_info.pApplicationInfo = &program_info,
    create_info.enabledLayerCount = static_cast<uint32_t>(VALIDATION_LAYERS.size()),
    create_info.ppEnabledLayerNames = VALIDATION_LAYERS.data(),
    create_info.enabledExtensionCount = static_cast<uint32_t>(required_extensions.size()),
    create_info.ppEnabledExtensionNames = required_extensions.data();

    auto [result, instance] = vk::createInstanceUnique(create_info);
    Assert(result == vk::Result::eSuccess, "Error: Failed to create instance");

    // Load Extension functions
    LoadExtensionStubs(*instance);
    return move(instance);
}

static vk::UniqueSurfaceKHR CreateSurface(vk::Instance& instance, GLFWwindow* window)
{
    Assert(
        window, "GLFW window pointer is null.");
    Assert(
        glfwVulkanSupported(), "Vulkan is not minimally supported in this environment.");

    VkSurfaceKHR surface;
    Assert(
        glfwCreateWindowSurface(instance, window, nullptr, &surface) == VK_SUCCESS,
        "Failed to create window surface!");

    return vk::UniqueSurfaceKHR(surface, instance);
}

static vk::UniqueDebugUtilsMessengerEXT CreateDebugMessenger(vk::Instance& instance)
{
    {  // Anonymous namespace
        typedef vk::DebugUtilsMessageSeverityFlagBitsEXT vkd;
        typedef vk::DebugUtilsMessageTypeFlagBitsEXT vkm;

        auto debug_flags = vkd::eVerbose | vkd::eWarning | vkd::eError | vkd::eInfo;
        auto message_flags = vkm::eGeneral | vkm::eValidation | vkm::ePerformance;
        vk::DebugUtilsMessengerCreateInfoEXT createDebugInfo(
            {}, debug_flags, message_flags, DebugCallback, nullptr);

        VkDebugUtilsMessengerEXT messenger;
        auto info = static_cast<VkDebugUtilsMessengerCreateInfoEXT>(createDebugInfo);
        auto result =
            vkCreateDebugUtilsMessengerEXT(instance, &info, nullptr, &messenger);

        Assert(result == VK_SUCCESS, "Failed to create debug messenger");

        return vk::UniqueDebugUtilsMessengerEXT(messenger, instance);

    }  // Close anonymous namespace
}

static vk::PhysicalDevice PickPhysicalDevice(
    vk::Instance& instance,
    const vk::SurfaceKHR& surface,
    const vector<const char*>& device_extensions,
    NECore::VulkanMetaData& vk_meta_out)
{
    // Get physical devices in the system.
    auto [result, all_devices] = instance.enumeratePhysicalDevices();
    Assert(result == vk::Result::eSuccess, "Failed to find GPU with Vulkan support.");
    Assert(!all_devices.empty(), "No suitable devices found.");

    // Filter out devices that don't support the surface or extensions.
    std::set<vk::PhysicalDevice> filtered_devices =
        FilterDevices(all_devices, surface, device_extensions);
    Assert(!filtered_devices.empty(), "");

    // Find the best device under the qualitative heuristic "Discrete GPU > anything else."
    vk::PhysicalDevice chosen_device;
    chosen_device = *std::max_element(
        filtered_devices.begin(),
        filtered_devices.end(),
        [](vk::PhysicalDevice d1, vk::PhysicalDevice d2) {
            // Permute enumerator values such that discrete GPUs become the maximum.
            // https://www.khronos.org/registry/vulkan/specs/1.1-extensions/man/html/VkPhysicalDeviceType.html
            int p1 = ((int)d1.getProperties().deviceType + 2) % 5;
            int p2 = ((int)d2.getProperties().deviceType + 2) % 5;

            return p1 < p2;
        });

    vk_meta_out.device_properties = NEVK::to_string(chosen_device.getProperties());
    return chosen_device;
}

static vk::UniqueDevice CreateLogicalDevice(
    vk::Instance& instance,
    const vk::SurfaceKHR& surface,
    vk::PhysicalDevice& phys_device,
    const vector<const char*>& device_extensions,
    NECore::VulkanMetaData& vk_meta_out)
{
    int32_t queue_family = _FindGraphicsAndComputeQueueFamily(phys_device, surface);
    Assert(queue_family >= 0, "Queue family not found.");
    const vk::QueueFamilyProperties queue_properties =
        phys_device.getQueueFamilyProperties()[queue_family];

    const uint queue_num = std::min((uint32_t)2, queue_properties.queueCount);
    std::array<float, 2> priorities = {1.0, 1.0};
    std::vector<vk::DeviceQueueCreateInfo> queue_create_infos = {
        vk::DeviceQueueCreateInfo(
            vk::DeviceQueueCreateFlags(), queue_family, queue_num, priorities.data()),
    };
    // Get the features of the selected device.
    vk::PhysicalDeviceFeatures device_features = phys_device.getFeatures();
    // The enabledLayerCount and ppEnabledLayerNames parameters are deprecated,
    // they should always be 0 and nullptr.
    // https://www.khronos.org/registry/vulkan/specs/1.1-extensions/man/html/VkDeviceCreateInfo.html
    vk::DeviceCreateInfo create_info = {};
    create_info.queueCreateInfoCount = queue_create_infos.size();
    create_info.pQueueCreateInfos = queue_create_infos.data();
    create_info.enabledLayerCount = 0;
    create_info.ppEnabledLayerNames = nullptr;
    create_info.enabledExtensionCount = device_extensions.size();
    create_info.ppEnabledExtensionNames = device_extensions.data();
    create_info.pEnabledFeatures = &device_features;

    vk::PhysicalDeviceDynamicRenderingFeaturesKHR dynamic_rendering = {};
    dynamic_rendering.dynamicRendering = true;

    vk::PhysicalDeviceDescriptorIndexingFeatures indexing_features = {};
    indexing_features.shaderSampledImageArrayNonUniformIndexing = true;
    indexing_features.pNext = &dynamic_rendering;

    create_info.pNext = &indexing_features;

    auto [result, device] = phys_device.createDeviceUnique(create_info);
    Assert(
        result == vk::Result::eSuccess,
        "Failed to create logical device with error {0}.",
        vk::to_string(result));

    return move(device);
}

static vk::UniqueCommandPool CreateCommandPool(HardwareInterface& hi)
{
    auto device = hi.GetDevice();
    auto queue_family = hi.GetQueueFamily();

    vk::CommandPoolCreateInfo command_pool_info({}, queue_family);
    auto [result, cmd_pool] = device.createCommandPoolUnique(command_pool_info);
    Assert(result == vk::Result::eSuccess, "Failed to create command pool!");

    return move(cmd_pool);
}

// Find available validation layers.
static bool CheckAvailableLayers(NECore::VulkanMetaData& vk_meta_out)
{
    // Query validation layers currently installed.
    auto [result, availableLayers] = vk::enumerateInstanceLayerProperties();
    Assert(result == vk::Result::eSuccess, "Error: Failed to query validation layers");

    // For every required layer, see if that layer is in the supported layers.
    std::vector<const char*>& found_layers = vk_meta_out.found_layers;
    for(const char* layer_name: VALIDATION_LAYERS)
    {
        // Check if the current required layer is supported.
        bool layer_found = false;
        for(const auto& layer_properties: availableLayers)
        {
            if(strcmp(layer_name, layer_properties.layerName) == 0)
            {
                layer_found = true;
                found_layers.push_back(layer_name);
                break;
            }
        }
        // If a required layer wasn't found, return.
        if(!layer_found)
        {
            Warn(false, "Required validation layer not found: " + string(layer_name));
            return false;
        }
    }
    return true;
}

// Find and request mandatory extensions.
static bool RequestExtensions(vector<const char*>& ret_vec)
{
    // Get glfw required extensions.
    uint32_t glfwExtensionCount = 0;
    const char** glfwExtensions;
    glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);
    vector<const char*> required_extensions(
        glfwExtensions, glfwExtensions + glfwExtensionCount);
    required_extensions.insert(
        required_extensions.end(),
        INSTANCE_EXTENSIONS.begin(),
        INSTANCE_EXTENSIONS.end());

    // Find all available Vulkan extensions.
    auto [result, extensions] = vk::enumerateInstanceExtensionProperties();
    Assert(
        result == vk::Result::eSuccess, "Error: Failed to request available extensions");

    bool all_extensions_found = true;
    // For each required extension, see if that extension is in the supported extensions.
    for(const auto& rextension: required_extensions)
    {
        bool found = false;
        vk::ExtensionProperties extension_info;
        for(const auto& extension: extensions)
        {
            if(strcmp(extension.extensionName, rextension) == 0)
            {
                found = true;
                auto index = &extension - &extensions[0];
                extension_info = extension;
                // If the current extension was found, remove it from the list.
                extensions.erase(extensions.begin() + index);
                break;
            }
        }

        Warn(found, "Instance extension {0} not found.", std::string(rextension));

        all_extensions_found &= found;
    }
    // Put all required extensions into.
    ret_vec = required_extensions;
    return all_extensions_found;
}

// Find a suitable graphics queue in the selected device.
int32_t _FindGraphicsAndComputeQueueFamily(
    const vk::PhysicalDevice& physical_device, const vk::SurfaceKHR& surface)
{
    vector<vk::QueueFamilyProperties> queue_families =
        physical_device.getQueueFamilyProperties();
    int32_t q_family_index = -1;
    // Iterate through each queue family in this device.
    for(auto& q_family: queue_families)
    {
        // Get the flag bits for the kind of queue (e.g graphics, compute ...).
        vk::QueueFlags mask_bits = q_family.queueFlags;
        uint c_index = &q_family - &queue_families[0];
        // Check that this queue supports presentation to the surface.
        auto [result, is_supported] =
            physical_device.getSurfaceSupportKHR(c_index, surface);

        // If these properties are true we have found an appropriate queue.
        if((mask_bits & vk::QueueFlagBits::eGraphics) &&
           (mask_bits & vk::QueueFlagBits::eCompute) && is_supported &&
           q_family.queueCount >= 1)
        {
            q_family_index = c_index;
            break;
        }
    }

    return q_family_index;
}

std::string EnumerateDevicesAsString(const vector<vk::PhysicalDevice>& input_devices)
{
    std::string device_names = "";
    for(const vk::PhysicalDevice& device: input_devices)
    {
        device_names += std::string(device.getProperties().deviceName.data()) + "\n";
    }

    return device_names;
}

// Return a set of available devices that support the requested surface and extensions.
static set<vk::PhysicalDevice> FilterDevices(
    const vector<vk::PhysicalDevice>& input_devices,
    const vk::SurfaceKHR& surface,
    const vector<const char*>& device_extensions)
{
    set<vk::PhysicalDevice> filtered_devices = {};
    for(const vk::PhysicalDevice& device: input_devices)
    {
        // Check there are graphic queue families.
        int32_t graphics_q = _FindGraphicsAndComputeQueueFamily(device, surface);

        if(graphics_q < 0) continue;
        // Check device supports all extensions.
        bool supports_extensions = VerifyExtensionsSupport(device, device_extensions);

        if(!supports_extensions) continue;
        // Check device supports anisotropy.
        vk::PhysicalDeviceFeatures device_features = device.getFeatures();

        if(!device_features.samplerAnisotropy) continue;
        filtered_devices.insert(device);
    }

    Assert(
        filtered_devices.size(),
        "Couldn't find appropriate physical device among:\n{0}",
        EnumerateDevicesAsString(input_devices));
    return filtered_devices;
}

// Verify that this device supports all required device extensions.
static bool VerifyExtensionsSupport(
    const vk::PhysicalDevice& device, const std::vector<const char*>& device_extensions)
{
    auto [result, d_extensions] = device.enumerateDeviceExtensionProperties();
    set<string> r_extensions(device_extensions.begin(), device_extensions.end());
    // Eliminate from the set all extensions supported by the device.
    bool supports_extensions = true;
    for(auto& extension: d_extensions)
    {
        if(r_extensions.count(string(extension.extensionName.data())) < 0)
        {
            supports_extensions = false;
            break;
        }
    }

    return supports_extensions;
}

}  // namespace

} // Renderer
