#pragma once

#include <string>

namespace CLI
{
class CLIOptions
{
    bool should_capture = false;
    bool terminate_on_capture = false;
    unsigned int capture_frame_num = 1;
    std::string screen_shot_path = "screen_shot.png";

public:
    CLIOptions(int argc, const char ** argv);

    template<typename Fun>
    void CaptureDelayed(const Fun& capture, const unsigned int frame_count)
    {
        if(frame_count == capture_frame_num && should_capture)
        {
            capture(screen_shot_path);
            if(terminate_on_capture)
                exit(EXIT_SUCCESS);
        }
    }
};

} // namespace CLI