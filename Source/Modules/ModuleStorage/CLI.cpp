#include "CLI.hpp"

#include <filesystem>

#include "ArgParse/argparse.hpp"

namespace fs = std::filesystem;
using namespace argparse;

namespace CLI
{

CLIOptions::CLIOptions(int argc, const char ** argv)
{
    ArgumentParser parser(fs::path( argv[0] ).filename().c_str(), "Argument parser example");
    parser.add_argument()
      .names({"-c", "--capture"})
      .description(
          "If enabled, a screenshot will be taken, by default it will be frame 0. "
          "Use -f to select the frame at which the capture will be taken.");

    parser.add_argument().names({"-f", "--frame"})
      .description("The frame number at which the capture will be executed.");

    parser.add_argument()
      .names({"-p", "--path"})
      .description(
          "Path at which the screenshot will be saved.");

    parser.enable_help();

    auto err = parser.parse(argc, argv);
    if (err)
    {
        std::cout << err << std::endl;
        exit(EXIT_SUCCESS);
    }

    if (parser.exists("help"))
    {
        parser.print_help();
        exit(EXIT_SUCCESS);
    }

    if (parser.exists("c"))
    {
        should_capture = true;
        terminate_on_capture = true;
        capture_frame_num = parser.exists("f") ? parser.get<unsigned int>("f") : 0;
    }

    if (parser.exists("p"))
    {
        screen_shot_path = parser.get<std::string>("p");
    }
}

} // namespace CLI