#pragma once

#include "imgui.h"

/** @cond */
#include <vector>

#include "imgui_impl_glfw.h"
/** @endcond */

namespace ModuleStorage
{
class ModuleStorage;
}

namespace NECore
{
class Gallery;
class ImageHandle;
}

ImGuiContext* InitImgui(ModuleStorage::ModuleStorage& module, NECore::Gallery& gallery);

void RenderImgui(
    ModuleStorage::ModuleStorage& module, 
    NECore::Gallery& gallery,
    const std::vector<std::pair<NECore::ImageHandle, size_t>>& image_outputs = {});
