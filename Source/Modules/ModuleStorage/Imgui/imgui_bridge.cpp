#include "imgui_bridge.hpp"

/** @cond */
#include "imgui_impl_glfw.h"
/** @endcond */

#include "Core/Core.hpp"
#include "Core/Gallery.hpp"
#include "ImageManipulation/CpuImage.hpp"
#include "ModuleStorage/ModuleStorage.hpp"

using namespace std;

namespace {
struct vec2 {float x; float y;};
struct ImguiData
{
    vec2 scale;
    vec2 translate;
};

NECore::RawMeshData SerializeImguiData(
    std::pair<std::vector<std::byte>, std::vector<uint>>& draw_data)
{
    return {{draw_data.first}, {draw_data.second}};
}
} // namespace

NECore::ShaderHandle imgui_shader;

ImGuiContext* InitImgui(ModuleStorage::ModuleStorage& module, NECore::Gallery& gallery)
{
    ImGuiContext* imgui_context = ImGui::CreateContext();

    ImGuiIO& io = ImGui::GetIO();

    unsigned char* pixels;
    int width, height;
    io.Fonts->GetTexDataAsRGBA32(&pixels, &width, &height);

    CpuImage font_image(pixels, width, height, 4);
    uint font_id = gallery.StoreImage<CpuImage::GetImageData>(
        font_image, "__ImguiFont", NECore::ImageFormat::R8G8B8A8_UNORM);

    io.Fonts->SetTexID((ImTextureID)(intptr_t)font_id);
    ImGui_ImplGlfw_InitForVulkan(module.GetWindow().GetGLFWWindow(), true);

    imgui_shader = module.AddShader(
        {"./CommonShaders/imgui.vert",
         "./CommonShaders/imgui.frag"});
    
    return imgui_context;
}

void RenderImgui(
    ModuleStorage::ModuleStorage& module, 
    NECore::Gallery& gallery, 
    const std::vector<std::pair<NECore::ImageHandle, size_t>>& image_outputs)
{
    ImDrawData* draw_data = ImGui::GetDrawData();
    if(draw_data == nullptr) { return; }
    if(draw_data->CmdListsCount == 0) return;

    Assert(draw_data->Valid, "ImGui data is invalid.");

    std::vector<std::byte> vertex_data;
    std::vector<uint> index_data;
    size_t vtx_dst = 0;
    size_t idx_dst = 0;

    for(int i = 0; i < draw_data->CmdListsCount; i++)
    {
        const ImDrawList* cmd_list = draw_data->CmdLists[i];
        vertex_data.resize(
            vertex_data.size() + cmd_list->VtxBuffer.Size * sizeof(ImDrawVert));
        memcpy(
            vertex_data.data() + vtx_dst * sizeof(ImDrawVert),
            cmd_list->VtxBuffer.Data,
            cmd_list->VtxBuffer.Size * sizeof(ImDrawVert));
        index_data.resize(
            index_data.size() + cmd_list->IdxBuffer.Size);
        for(int i=0; i < cmd_list->IdxBuffer.Size; i++)
            index_data[idx_dst + i] = (uint)(cmd_list->IdxBuffer[i]);

		vtx_dst += cmd_list->VtxBuffer.Size;
		idx_dst += cmd_list->IdxBuffer.Size;
    }

    auto data = std::make_pair(vertex_data, index_data);
    gallery.StoreMesh<SerializeImguiData>(data, "__ImguiGeom");

    ImguiData imgui_uniform = {};
    imgui_uniform.scale = {2.f / draw_data->DisplaySize.x, 2.f / draw_data->DisplaySize.y};
    imgui_uniform.translate = {
        -1.0f - draw_data->DisplayPos.x * imgui_uniform.scale.x,
        -1.0f - draw_data->DisplayPos.y * imgui_uniform.scale.y};

    ImVec2 clip_off = draw_data->DisplayPos;         // (0,0) unless using multi-viewports
    ImVec2 clip_scale = draw_data->FramebufferScale; // (1,1) unless using retina display which are often (2,2)

    NECore::ImageHandle font_atlas = gallery.GetGpuImageData("__ImguiFont");
    NECore::GpuMeshData imgui_buffer = gallery.GetGpuMeshData("__ImguiGeom");
    int global_vtx_offset = 0;
	int global_idx_offset = 0;
    for(int i = 0; i < draw_data->CmdListsCount; i++)
    {
        ImDrawList* draw_list = draw_data->CmdLists[i];
        for(const ImDrawCmd& cmd: draw_list->CmdBuffer)
        {
            ImVec4 clip_rect;
            clip_rect.x = (cmd.ClipRect.x - clip_off.x) * clip_scale.x;
            clip_rect.y = (cmd.ClipRect.y - clip_off.y) * clip_scale.y;
            clip_rect.z = (cmd.ClipRect.z - clip_off.x) * clip_scale.x;
            clip_rect.w = (cmd.ClipRect.w - clip_off.y) * clip_scale.y;
            NECore::Scissor scissor;
            scissor.offset.x = (int32_t)(clip_rect.x);
            scissor.offset.y = (int32_t)(clip_rect.y);
            scissor.extent.width = (uint32_t)(clip_rect.z - clip_rect.x);
            scissor.extent.height = (uint32_t)(clip_rect.w - clip_rect.y);

            NECore::RenderRequest render_request = {};
            render_request.shader = imgui_shader;
            render_request.mesh_input.vertex_inputs = imgui_buffer;
            render_request.mesh_input.element_count = cmd.ElemCount;
            render_request.mesh_input.index_offset = cmd.IdxOffset + global_idx_offset;
            render_request.mesh_input.vertex_offset = cmd.VtxOffset + global_vtx_offset;

            render_request.image_inputs = {{font_atlas, 1}};
            render_request.should_clear = false;
            render_request.scissor = scissor;
            render_request.image_outputs = image_outputs;

            module.Draw(
                render_request,
                imgui_uniform, 0
            );
        }
        global_idx_offset += draw_list->IdxBuffer.Size;
        global_vtx_offset += draw_list->VtxBuffer.Size;
    }
}

