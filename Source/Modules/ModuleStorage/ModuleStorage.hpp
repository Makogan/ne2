#pragma once

/** @cond */
#include <string>
/** @endcond */

#include "Core/Gallery.hpp"
#include "OutOfModule.hpp"
#include "CLI.hpp"
#include "../Renderer/Renderer.hpp"

namespace ModuleStorage
{
namespace _details
{

template<typename UBO>
std::pair<uint, std::vector<std::byte>> SerializeUniform(UBO& ubo, uint binding)
{
    // Create raw binary buffers of the uniform data
    std::vector<std::byte> ubo_buffer(sizeof(UBO));
    memcpy(ubo_buffer.data(), (void*)&ubo, sizeof(UBO));
    return {binding, ubo_buffer};
}

inline void SerializeArguments(NECore::UniformBufferDataContainer& data) {}

template<typename T1, typename... Ts>
inline void SerializeArguments(
    NECore::UniformBufferDataContainer& data, T1& uniform, uint binding, Ts&... args)
{
    data.push_back(SerializeUniform(uniform, binding));
    SerializeArguments(data, args...);
}

template<typename ...Types>
constexpr std::size_t ArgNum(Types&& ... args){
    constexpr std::size_t arg_num = std::tuple_size<std::tuple<Types...>>::value;
    return arg_num;
}
} // _details

class ModuleStorage
{
    NECore::NEWindow* window;
    NECore::InputHandler* input_handler;
    NECore::VulkanMetaData vk_meta_data;

    size_t frame_count = 0;
    CLI::CLIOptions cli_options;

public:

    ModuleStorage(int argc, const char ** argv);
    ~ModuleStorage();

    bool FrameUpdate();

    NECore::ShaderHandle AddShader(const std::vector<std::string>& paths);

    template<class... Ubos>
    void Draw(const NECore::RenderRequest& graphics_data, const Ubos&... args);

    template<class... Ubos>
    void Compute(const NECore::ComputeRequest& compute_data, const Ubos&... args);

    void EndFrame();

    NECore::NEWindow& GetWindow() { return *window; }

    NECore::InputHandler& GetInputHandler() { return *input_handler; }

    void GpuScreenShot(const std::string& path);

    std::vector<std::byte> ReadGpuImage(
        NECore::ImageHandle image,
        size_t width,
        size_t height,
        size_t channels,
        size_t channel_size);

    std::vector<std::byte> ReadGpuPixel(
        NECore::ImageHandle image,
        size_t x,
        size_t y,
        size_t z);
};

template<class... Ubos>
void ModuleStorage::Draw(const NECore::RenderRequest& render_request, const Ubos&... args)
{
        size_t arg_num = _details::ArgNum(args...);
    NECore::UniformBufferDataContainer ubos;
    ubos.reserve(arg_num);
    _details::SerializeArguments(ubos, args...);

    if(render_request.image_outputs.empty())
        Renderer::DrawToScreen(vk_meta_data.vulkan_data, render_request, ubos);
    else
       Renderer::DrawOffScreen(vk_meta_data.vulkan_data, render_request, ubos);
}

template<class... Ubos>
void ModuleStorage::Compute(
    const NECore::ComputeRequest& compute_request, const Ubos&... args)
{
        size_t arg_num = _details::ArgNum(args...);
    NECore::UniformBufferDataContainer ubos;
    ubos.reserve(arg_num);
    _details::SerializeArguments(ubos, args...);
    Renderer::GpuCompute(vk_meta_data.vulkan_data, compute_request, ubos);
}

NECore::BufferHandle GpuAllocateBuffer(void* data, size_t size);

void GpuDestroyBuffer(NECore::BufferHandle buffer);

NECore::ImageHandle GpuAllocateImage(
    const NECore::RawImageData& ,
    const NECore::ImageFormat);

} // ModuleStorage