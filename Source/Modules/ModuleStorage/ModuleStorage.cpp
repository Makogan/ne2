#include "ModuleStorage.hpp"

/** @cond */
#include <iostream>
#include <dlfcn.h>
#include <filesystem>

#include "GLFW/glfw3.h"
/** @endcond */

#include "OutOfModule.hpp"
#include "Core/Gallery.hpp"
#include "ImageManipulation/CpuImage.hpp"
#include "../Peripherals/Peripherals.hpp"

using namespace std;
namespace fs = std::filesystem;

namespace {
NECore::VulkanDataOpaquePtr global_vulkan_data;
} // namespace

namespace ModuleStorage
{
void LogLoadedState(const NECore::VulkanMetaData& vk_meta_data)
{
    Log::RecordLog("===| Validation Layers |===");
    for(auto& extension : vk_meta_data.found_layers)
    {
        Log::RecordLog(extension);
    }
    Log::RecordLog("===| Device Properties |===");
    Log::RecordLog(vk_meta_data.device_properties);
}

ModuleStorage::ModuleStorage(int argc, const char ** argv)
    : cli_options(argc, argv)
{
    auto [window_ptr, handler_ptr] = InitPeripherals();
    window = window_ptr;
    input_handler = handler_ptr;

    vk_meta_data = Renderer::InitializeRendering(window->GetGLFWWindow(), false);
    global_vulkan_data = vk_meta_data.vulkan_data;

    LogLoadedState(vk_meta_data);
}

ModuleStorage::~ModuleStorage()
{
    Renderer::DeInitializeRendering(vk_meta_data);
    DestroyPeripherals(window, input_handler);
}

NECore::ShaderHandle ModuleStorage::AddShader(const std::vector<std::string>& paths)
{
    return Renderer::AddShaderProgram(global_vulkan_data, paths);
}

bool ModuleStorage::FrameUpdate()
{
    Renderer::Update(global_vulkan_data);

    window->SetSizeChanged(false);
    input_handler->CallEvents();
    window->Update();

    auto capture = [this](const std::string& path)
    { this->GpuScreenShot(path); };
    cli_options.CaptureDelayed(capture, frame_count);

    frame_count++;

    return window->IsOpen();
}

void ModuleStorage::EndFrame()
{
    Renderer::EndFrame(global_vulkan_data);
}

void ModuleStorage::GpuScreenShot(const std::string& path)
{
    auto[width, height] = window->GetDimensions();
    std::vector<std::byte> buffer(width * height * 4);
    Renderer::ScreenShot(global_vulkan_data, buffer.data(), buffer.size());

    CpuImage image(buffer.data(), width, height, 4);


    image.SaveToPng(path);
}

std::vector<std::byte> ModuleStorage::ReadGpuImage(
    NECore::ImageHandle image,
    size_t width,
    size_t height,
    size_t channels,
    size_t channel_size)
{
    std::vector<std::byte> buffer(width * height * channels * channel_size);
    Renderer::ReadImage(global_vulkan_data, buffer.data(), buffer.size(), image);
    return buffer;
}

std::vector<std::byte> ModuleStorage::ReadGpuPixel(
    NECore::ImageHandle image,
    size_t x,
    size_t y,
    size_t z)
{
    return Renderer::ReadPixel(global_vulkan_data, x, y, z, image);
}

// Non dependent -------------------------------------------------------------------------
NECore::BufferHandle GpuAllocateBuffer(void* data, size_t size)
{
    return Renderer::AllocateBuffer(global_vulkan_data, data, size);
}

void GpuDestroyBuffer(NECore::BufferHandle buffer)
{
    Renderer::DestroyBuffer(global_vulkan_data, buffer);
}

NECore::ImageHandle GpuAllocateImage(
    const NECore::RawImageData& data, const NECore::ImageFormat image_format)
{
    return Renderer::AllocateImage(global_vulkan_data, data, image_format);
}

} // ModuleStorage
