#include "Log.hpp"

/** @cond */
#include <algorithm>
#include <chrono>
#include <ctime>
#include <fstream>
#include <iostream>
#include <regex>
#include <sstream>
#include <sys/stat.h>
#include <cxxabi.h>
#include <signal.h>
/** @endcond */

#include "Profiler/profiler.hpp"

// Globals -------------------------------------------------------------------------------
namespace Log
{
const char* CHECK = "✓";
const char* CROSS = "✗";

// Internal declaration for helping functions
const std::time_t GetTime();
std::string GetLogFile();

// Initialization of global constants
const time_t LOG_START_TIME = GetTime();
const std::string& LOG_DIR = "logs/";
const std::string& LOG_PATH = GetLogFile();

// Width of the log
const uint LOG_WIDTH = 80;
/**
 * @brief Frame for log section titles
 *
 */
const std::string TITLE_BAR = std::string(LOG_WIDTH, '=');
/**
 * @brief Bar to separate text regions
 *
 */
const std::string SEPARATOR_BAR = std::string(LOG_WIDTH, '-');
/**
 * @brief Frame for log errors
 *
 */
const std::string ERROR_BAR = std::string(LOG_WIDTH, '!');

// Function declarations
// -----------------------------------------------------------------
static std::string Beautyfy(const std::string&);

// Function definitions
// ------------------------------------------------------------------ Get
// current time
const std::time_t GetTime()
{
    auto execution_start = std::chrono::system_clock::now();
    return std::chrono::system_clock::to_time_t(execution_start);
};

// Construct log file based on time
std::string GetLogFile()
{
    /*
    auto time = LOG_START_TIME;
    std::string log_prefix = LOG_DIR + std::ctime(&time);
    mkdir(LOG_DIR.c_str());
    std::replace(log_prefix.begin(), log_prefix.end(), ' ', '_');

    const auto strEnd = log_prefix.find_last_not_of('\n');
    log_prefix = log_prefix.substr(0, strEnd);
    return log_prefix + ".log";*/
    return {};
}

// Record a message
void RecordLog(const std::string& message)
{
    Beautyfy(message);
    std::ofstream file_stream;
    file_stream.open(LOG_PATH, std::ios_base::app);
    file_stream << message << std::endl;
    file_stream.close();

    std::cout << message << std::endl;
}

void RecordLog(const std::string& message1, const std::string& message2, int alignment)
{
    RecordLog(message1, message2, alignment, ' ');
}

void RecordLog(
    const std::string& message1, const std::string& msg2, int alignment, char fill)
{
    std::string message2 = msg2;
    std::istringstream stream(message1);
    std::string last_line;
    for(std::string line; std::getline(stream, line);)
        last_line = line;

    int gap = alignment - last_line.length();
    if(gap > 0) message2 = std::string(gap, fill) + message2;

    const std::string& message = message1 + message2;
    Beautyfy(message);
    std::ofstream file_stream;
    file_stream.open(LOG_PATH, std::ios_base::app);
    file_stream << message << std::endl;
    file_stream.close();

    std::cout << message << std::endl;
}
// Record a message and style it
void RecordLogHeader(const std::string& message)
{
    RecordLog(TITLE_BAR);
    RecordLog(message);
    RecordLog(TITLE_BAR + "\n");
}

void RecordLogHeader(
    const std::string& message1, const std::string& message2, int alignement)
{
    RecordLog(TITLE_BAR);
    RecordLog(message1, message2, alignement);
    RecordLog(TITLE_BAR + "\n");
}

void RecordLogError(const std::string& message)
{
    RecordLog(ERROR_BAR);
    RecordLog(message, "", LOG_WIDTH);
    RecordLog(ERROR_BAR + "\n");
}
// Record a message and the time of the message
void RecordLogTime(const std::string& message)
{
    auto execution_start = std::chrono::system_clock::now();
    std::time_t start_time = std::chrono::system_clock::to_time_t(execution_start);

    const std::string& message_t = message + std::ctime(&start_time);
    Beautyfy(message_t);
    std::ofstream file_stream;
    file_stream.open(LOG_PATH, std::ios_base::app);
    file_stream << message_t;
    file_stream.close();

    std::cout << message_t;
}

// Wrap text by maximum alignment
std::string WrapText(const std::string& text, int maximum)
{
    std::string result = "";
    std::istringstream stream(text);
    std::string word;
    std::string line = "";
    while(stream >> word)
    {
        if((line.length() + word.length()) > (uint)maximum)
        {
            result += line + "\n";
            line = word + " ";
        }
        else
            line += word + " ";
    }
    return result + line + "\n";
}

std::string Demangle(const std::string& name)
{
    int status;
    char* realname = abi::__cxa_demangle(name.c_str(), 0, 0, &status);
    const std::string demangled_name = std::string(realname);
    free(realname);
    return demangled_name;
}

// Helper functions
// ----------------------------------------------------------------------
static const char* RED = "\033[0;31m";
static const char* GREEN = "\033[0;32m";
static const char* YELLOW = "\033[0;33m";
static const char* BLUE = "\033[0;34m";
static const char* WHITE = "\033[0;37m";
// Check that a characeter is a plausible version character
bool IsVersionCharacter(char c)
{
    if(c >= '0' && c <= '9') return true;
    if(c == '.') return true;
    if(c == ' ') return true;
    if(c == '\t') return true;
    return false;
}
// Add color encoding for linux shells
std::string Beautyfy(const std::string& string)
{
    std::string str = string;
    if(str.find(ERROR_BAR) != std::string::npos) { return RED + str + BLUE; }
    if(str.find("Warning:") != std::string::npos) { return str = RED + str + BLUE; }
    // Default text color
    std::string color_code = BLUE;
    // Red text for failures
    if(str.find(CROSS) != std::string::npos) color_code = RED;
    // Green text for success
    else if(str.find(CHECK) != std::string::npos)
        color_code = GREEN;
    // Search for a version string (case insensitive)
    auto lowered = str;
    std::transform(lowered.begin(), lowered.end(), lowered.begin(), ::tolower);
    size_t index = lowered.find("version");
    // If version string is found, add additional color formatting
    if(index != std::string::npos)
    {
        size_t end = index + sizeof("version");
        int dot_limit = 3;
        // Search for the end of the version string
        while(end < str.size() && IsVersionCharacter(str[end]) && dot_limit > 0)
        {
            if(str[end] == '.') dot_limit--;
            end++;
        }
        // Add color formatting
        str.insert(index, YELLOW);
        str.insert(end + sizeof(YELLOW) - 1, color_code);
    }

    return color_code + str;
}
namespace details
{
std::string GetProgramName()
{
#ifdef __linux__
    return std::string(program_invocation_name);
#elif _WIN64
    return "unkown";
#endif
}

void DAssert(
    bool expr,
    const std::string& expr_str,
    const std::string& base,
    const std::string& file,
    const std::string& function,
    const int line,
    const std::vector<std::string>& msgs,
    const ErrorLevel level)
{
    if(!expr)
    {
        const bool is_error = level == LOG_ERROR;
        const std::string extra_lines = is_error ? "" : "\n";
        std::string final = base;
        for(uint count = 0; count < msgs.size(); count++)
        {
            auto& msg = msgs[count];
            std::regex target_re(R"(\{)" + std::to_string(count) + R"(\})");
            final = std::regex_replace(final, target_re, msg);
        }

        const auto color = is_error ? RED : YELLOW;
        std::cerr << extra_lines << color << GetProgramName() << ": " << file
                  << ":" << line << ":\n"
                  << function << "\n"
                  << "Failed check: " << expr_str << "\n\n"
                  << final << WHITE << extra_lines << std::endl;
        if(is_error) raise(SIGSEGV);
    }
}
}  // namespace details
}  // namespace Log

