#pragma once

/** @cond */
#include <cstdint>
#include <array>
#include <string>

#include "GLFW/glfw3.h"
/** @endcond */

#include "Log.hpp"
#include "TypeAliases.hpp"

inline constexpr std::byte operator"" _byte(unsigned long long arg) noexcept
{
    return (std::byte)(arg);
}

namespace NECore {

using VulkanDataOpaquePtr = void*;

struct VulkanMetaData
{
    std::vector<const char*> found_layers;
    std::string device_properties;
    VulkanDataOpaquePtr vulkan_data;
};

struct ModuleData
{
    std::string renderer_so_path;
    std::string peripheral_so_path;
};

struct GpuMeshData
{
    std::vector<BufferHandle> buffers;
    BufferHandle index_buffer = 0;
    size_t index_count = 0;
};

struct GraphicsInputDescription
{
    GpuMeshData vertex_inputs;
    BufferHandle instance_buffer;  // Optional.
    uint instance_count = 0;       // Optional, 0 means not instanced.
    size_t vertex_offset = 0;      // Optional, inner offset of the vertex data.
    size_t index_offset = 0;       // Optional, inner offset of the index data.
    size_t element_count = vertex_inputs.index_count;     // Optional, number of subvertices to render.
};

struct Scissor
{
    struct Vec2 { int32_t x; int32_t y; };
    struct Extent { int32_t width; int32_t height; };
    Vec2 offset = {0, 0};
    Extent extent = {-1, -1};
};

struct ResourceHandle
{
    uint64_t handle;

    ResourceHandle(BufferHandle b) : handle(b) {}
    ResourceHandle(ImageHandle i) : handle(i) {}
};

struct GpuDataDescriptor
{
    ResourceHandle handle;
    size_t binding = 0;
    size_t index = 0; // Optional use > 0 if the uniform is an array, e.g a sampler array.

    GpuDataDescriptor(BufferHandle bhandle, size_t binding)
        : handle(bhandle), binding(binding) {}
    GpuDataDescriptor(ImageHandle ihandle, size_t binding)
        : handle(ihandle), binding(binding) {}
    GpuDataDescriptor(BufferHandle bhandle, size_t binding, size_t index)
        : handle(bhandle), binding(binding), index(index) {}
    GpuDataDescriptor(ImageHandle ihandle, size_t binding, size_t index)
        : handle(ihandle), binding(binding), index(index) {}
};

struct RenderRequest
{
    ShaderHandle shader;
    GraphicsInputDescription mesh_input;
    std::vector<GpuDataDescriptor> image_inputs;
    std::vector<GpuDataDescriptor> buffer_inputs;
    bool should_clear = true;
    std::array<float, 4> color_clear = {0, 0, 0, 0};
    Scissor scissor;
    std::vector<std::pair<ImageHandle, size_t>> image_outputs;
};

struct ComputeRequest
{
    ShaderHandle shader;
    std::vector<GpuDataDescriptor> image_inputs;
    std::vector<GpuDataDescriptor> buffer_inputs;
    std::array<uint, 3> work_groups = {1, 0, 0};
};

class NEWindow
{
public:
    virtual ~NEWindow() = default;

    virtual bool IsOpen() const = 0;

    virtual GLFWwindow* GetGLFWWindow() = 0;

    virtual std::pair<uint, uint> GetDimensions() const = 0;

    virtual void SetDimensions(uint, uint) = 0;

    virtual void UpdateDimensions() = 0;

    virtual void Update() = 0;

    virtual bool CheckSizeChanged() const = 0;

    virtual void SetSizeChanged(bool) = 0;
};

enum MouseInputState
{
    // clang-format off
    NO_ACTION =     0,
    LEFT_DOWN =     1 << 1,
    LEFT_UP =       1 << 2,
    LEFT_DRAG =     1 << 3,
    RIGHT_DOWN =    1 << 4,
    RIGHT_UP =      1 << 5,
    RIGHT_DRAG =    1 << 6,
    MOVE =          1 << 7,
    // clang-format on
};

enum ScrollInputState
{
    SCROLL = 0b1,
};

enum KeyInputState
{
    NO_INPUT_KEY = 0,
    SPACE = 32,
};

enum KeyActionState
{
    IDLE = (1 << 0),
    PRESS = (1 << 1),
    HELD = (1 << 2),
    RELEASE = (1 << 3),
};

const int KEYBOARD_KEY_COUNT = 316;
using KeyStateMap = std::array<KeyActionState, KEYBOARD_KEY_COUNT>;
inline int KEY_INDEX(int key) { return key - ' '; }

class InputHandler
{
public:
    using MouseInputEvent =
        void (*)(void*,
        const float x_pos,
        const float y_pos,
        const float x_offset,
        const float y_offset);
    using ScrollEvent = void (*)(void*, const float x_offset, const float y_offset);
    using KeyEvent = void (*)(void*, const KeyStateMap&);

    virtual void AddEvent(const MouseInputState state, MouseInputEvent event, void* data) = 0;
    virtual void AddEvent(const ScrollInputState state, ScrollEvent event, void* data) = 0;
    virtual void AddEvent(KeyEvent event, void* data) = 0;

    virtual void CallEvents() = 0;

    virtual ~InputHandler() = default;
};

} // NECore