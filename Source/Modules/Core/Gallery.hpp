#pragma once

#include "TypeAliases.hpp"

/**@cond*/
#include <any>
#include <map>
#include <string>
#include <vector>
/**@endcond*/

#include "Core/Core.hpp"
#include "Core/Log.hpp"
#include "Core/Enumerators.hpp"
#include "Profiler/profiler.hpp"

namespace NECore
{
using RawMeshData = std::pair<std::vector<std::vector<std::byte>>, std::vector<uint>>;
struct RawImageData
{
    uint width, height, depth, channel_num;
    void* data;
};

namespace _details
{

template<typename T, RawMeshData (*Serialize)(T& t)>
RawMeshData SerializeMeshDataFromAny(std::any& input)
{
    return Serialize(std::any_cast<T&>(input));
}

// TODO (low): code duplication is bad, merge this with the above function.
template<typename T, RawMeshData (*Serialize)(const T& t)>
RawMeshData SerializeMeshDataFromAny(std::any& input)
{
    return Serialize(std::any_cast<T&>(input));
}

template<typename T, RawImageData (*Serialize)(T& t)>
RawImageData SerializeImageDataFromAny(std::any& input)
{
    return Serialize(std::any_cast<T&>(input));
}

template<typename T, std::vector<std::byte> (*Serialize)(T& t)>
std::vector<std::byte> SerializeBufferDataFromAny(std::any& input)
{
    return Serialize(std::any_cast<T&>(input));
}
}  // _details

using SerializeMeshData = RawMeshData (*)(std::any&);
struct MeshData
{
    SerializeMeshData serializer;
    std::any data;
};

using SerializeImageData = RawImageData (*)(std::any&);
struct ImageData
{
    SerializeImageData serializer;
    std::any data;
};

using SerializeBufferData = std::vector<std::byte> (*)(std::any&);
struct BufferData
{
    SerializeBufferData serializer;
    std::any data;
};

template<typename T>
std::vector<std::byte> SerializeVector(std::vector<T>& vec)
{
    std::vector<std::byte> buffer(vec.size() * sizeof(T));
    memcpy(buffer.data(), vec.data(), buffer.size());
    return buffer;
}

class Gallery
{
private:
    std::vector<GpuMeshData> gpu_meshes;
    std::vector<MeshData> cpu_meshes;

    std::vector<ImageHandle> gpu_images;
    std::vector<ImageData> cpu_images;

    std::vector<BufferHandle> gpu_buffers;
    std::vector<BufferData> cpu_buffers;

    std::map<std::string, uint> mesh_name_id_map;
    std::map<std::string, uint> image_name_id_map;
    std::map<std::string, uint> buffer_name_id_map;

    NECore::BufferHandle (*AllocateBuffer)(void* data, size_t size);
    void (*DestroyBuffer)(NECore::BufferHandle buffer);
    NECore::ImageHandle (*AllocateImage)(
        const NECore::RawImageData&, const NECore::ImageFormat);

public:
    Gallery() {}

    Gallery(
        NECore::BufferHandle (*b_allocator)(void* data, size_t size),
        void (*b_deleter)(NECore::BufferHandle),
        NECore::ImageHandle (*i_allocator)(
            const NECore::RawImageData&, const NECore::ImageFormat image_format))
        : AllocateBuffer(b_allocator)
        , DestroyBuffer(b_deleter)
        , AllocateImage(i_allocator)
    {}

    template<auto SerializingFunction, typename DataContainer>
    size_t StoreMesh(DataContainer container, const std::string& name);

    // Overloaded template, for when the container has a defined serializing method.
    template<typename T>
    uint StoreMesh(T& container, const std::string& name)
    { return StoreMesh<T::GetGeometryData>(container, name); }

    template<auto SerializingFunction, typename DataContainer>
    size_t StoreImage(
        DataContainer container,
        const std::string& name,
        const NECore::ImageFormat format = NECore::ImageFormat::R8G8B8A8_UNORM);

    template<auto SerializingFunction, typename DataContainer>
    size_t StoreBuffer(DataContainer container, const std::string& name);

    // Overloaded template, for when the container has a defined serializing method.
    template<typename T>
    uint StoreBuffer(const std::vector<T>& container, const std::string& name)
    { return StoreBuffer<SerializeVector<T>>(container, name); }

    template<typename T>
    uint StoreBuffer(const std::vector<T>&& container, const std::string& name)
    { return StoreBuffer<SerializeVector<T>>(container, name); }

    GpuMeshData GetGpuMeshData(size_t id);

    GpuMeshData GetGpuMeshData(const std::string& name);

    template<typename T> auto& GetCpuMeshData(uint id);

    template<typename T> auto& GetCpuMeshData(const std::string& name);

    ImageHandle GetGpuImageData(size_t id);

    ImageHandle GetGpuImageData(const std::string& name);

    template<typename T> auto& GetCpuImageData(size_t id);

    template<typename T> auto& GetCpuImageData(const std::string& name);

    BufferHandle GetGpuBufferData(size_t id);

    BufferHandle GetGpuBufferData(const std::string& name);

    template<typename T> auto& GetCpuBufferData(size_t id);

    template<typename T> auto& GetCpuBufferData(const std::string& name);

    void UpdateMeshData(uint id);

    void UpdateMeshData(const std::string& name);
};

template<auto SerializingFunction, typename DataContainer>
size_t Gallery::StoreMesh(DataContainer container, const std::string& name)
{
    Assert(AllocateBuffer, "");
    auto any_serialize =
        _details::SerializeMeshDataFromAny<DataContainer, SerializingFunction>;
    std::any geometry_container = std::make_any<DataContainer>(container);

    auto[buffers, indices] = any_serialize(geometry_container);
    Assert(
        buffers.size() && buffers[0].size(),
        "There should at least one buffer with one datum. Their sizes are "
        "respectively {0} and {1}.",
        std::to_string(buffers.size()),
        buffers.size()? std::to_string(buffers[0].size()) : "none");

    GpuMeshData geometry_info = {};
    // If the object already exists, replace it.
    if(mesh_name_id_map.count(name) != 0)
    {
        uint id = mesh_name_id_map.at(name);
        cpu_meshes[id].data = geometry_container;
        cpu_meshes[id].serializer = any_serialize;

        UpdateMeshData(id);
        return id;
    }
    // Otherwise make an allocation.
    else
    {
        for(const std::vector<std::byte>& buffer: buffers)
        {
            geometry_info.buffers.push_back(
                AllocateBuffer((void*)buffer.data(), buffer.size())
            );
        }

        if(indices.size() > 1)
            geometry_info.index_buffer =
                AllocateBuffer(indices.data(), indices.size() * sizeof(indices[0]));

        Assert(
            !indices.empty(),
            "No indices, if you want to render vertex only data, make an index array "
            "with a single entry, the number at index 0 will be treated as the element count");
        // If there's only one index we assume it's actually the element count.
        geometry_info.index_count = indices.size() == 1? indices[0] : indices.size();

        MeshData buffer_data;
        buffer_data.serializer = any_serialize;
        buffer_data.data = container;

        uint id = gpu_meshes.size();
        mesh_name_id_map.insert({name, id});
        gpu_meshes.push_back(geometry_info);
        cpu_meshes.push_back(buffer_data);

        return id;
    }

    return -1;
}

template<auto SerializingFunction, typename DataContainer>
size_t Gallery::StoreImage(
    DataContainer container,
    const std::string& name,
    const NECore::ImageFormat image_format)
{
    Assert(AllocateImage, "");
    auto any_serialize =
        _details::SerializeImageDataFromAny<DataContainer, SerializingFunction>;
    std::any image_container = std::make_any<DataContainer>(container);

    ImageData image_data;
    image_data.serializer = any_serialize;
    image_data.data = image_container;
    cpu_images.push_back(image_data);
    gpu_images.push_back(AllocateImage(any_serialize(image_container), image_format));

    image_name_id_map.insert({name, cpu_images.size() - 1});
    Assert(cpu_images.size() == gpu_images.size(), "");

    return cpu_images.size() - 1;
}

template<auto SerializingFunction, typename DataContainer>
size_t Gallery::StoreBuffer(DataContainer container, const std::string& name)
{
    Assert(AllocateBuffer, "");
    auto any_serialize =
        _details::SerializeBufferDataFromAny<DataContainer, SerializingFunction>;
    std::any buffer_container = std::make_any<DataContainer>(container);

    BufferData buffer_data;
    buffer_data.serializer = any_serialize;
    buffer_data.data = buffer_container;

    const std::vector<std::byte> raw_data = any_serialize(buffer_container);

    if(buffer_name_id_map.count(name) == 0)
    {
        cpu_buffers.push_back(buffer_data);
        gpu_buffers.push_back(AllocateBuffer((void*)raw_data.data(), raw_data.size()));

        buffer_name_id_map.insert({name, cpu_buffers.size() - 1});
        Assert(cpu_buffers.size() == gpu_buffers.size(), "");
    }
    else
    {
        const uint id = buffer_name_id_map.at(name);
        cpu_buffers[id] = buffer_data;
        DestroyBuffer(gpu_buffers[id]);
        gpu_buffers[id] = AllocateBuffer((void*)raw_data.data(), raw_data.size());
        Assert(cpu_buffers.size() == gpu_buffers.size(), "");
        return id;
    }

    return cpu_buffers.size() - 1;
}

template<typename T> auto& Gallery::GetCpuMeshData(uint id)
{
    std::any& geometry_container = cpu_meshes[id].data;

    Assert(
        geometry_container.type() == typeid(T),
        "Inernal type is {0}, but the requested type was {1}",
        std::string(Log::Demangle(geometry_container.type().name())),
        std::string(Log::Demangle(typeid(T).name())));
    return std::any_cast<T&>(geometry_container);
}

template<typename T> auto& Gallery::GetCpuMeshData(const std::string& name)
{
    Assert(
        mesh_name_id_map.count(name) > 0,
        "No mesh {0} exists in the gallery.",
        name);
    return GetCpuMeshData<T>(mesh_name_id_map.at(name));
}

template<typename T> auto& Gallery::GetCpuImageData(size_t id)
{
    std::any& image_container = cpu_images[id].data;

    Assert(
        image_container.type() == typeid(T),
        "Inernal type is {0}, but the requested type was {1}",
        std::string(Log::Demangle(image_container.type().name())),
        std::string(Log::Demangle(typeid(T).name())));
    return std::any_cast<T&>(image_container);
}

template<typename T> auto& Gallery::GetCpuImageData(const std::string& name)
{
    Assert(
        image_name_id_map.count(name) > 0,
        "No mesh {0} exists in the gallery.",
        name);
    return GetCpuImageData<T>(image_name_id_map.at(name));
}

template<typename T> auto& Gallery::GetCpuBufferData(size_t id)
{
    std::any& buffer_container = cpu_buffers[id].data;

    Assert(
        buffer_container.type() == typeid(T),
        "Inernal type is {0}, but the requested type was {1}",
        std::string(Log::Demangle(buffer_container.type().name())),
        std::string(Log::Demangle(typeid(T).name())));
    return std::any_cast<T&>(buffer_container);
}

template<typename T> auto& Gallery::GetCpuBufferData(const std::string& name)
{
    Assert(
        buffer_name_id_map.count(name) > 0,
        "No mesh {0} exists in the gallery.",
        name);
    return GetCpuBufferData<T>(buffer_name_id_map.at(name));
}


} // NECore