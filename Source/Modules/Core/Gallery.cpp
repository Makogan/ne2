#include "Gallery.hpp"

namespace NECore
{
void Gallery::UpdateMeshData(unsigned int id)
{
    MeshData& meta_data = cpu_meshes.at(id);
    // Call the stored function on the container to get the vertex information.
    auto [attributes, indices] = meta_data.serializer(meta_data.data);

    // If the number of vertices in the buffer is different, we have to delete the
    // prior buffer before making a new one.
    for(NECore::BufferHandle buffer: gpu_meshes.at(id).buffers)
        DestroyBuffer(buffer);

    gpu_meshes.at(id).buffers.clear();
    if(gpu_meshes[id].index_buffer != 0)
        DestroyBuffer(gpu_meshes[id].index_buffer);

    for(const std::vector<std::byte>& buffer: attributes)
    {
        gpu_meshes[id].buffers.push_back(
            AllocateBuffer((void*)buffer.data(), buffer.size())
        );
    }

    if(indices.size() > 1)
        gpu_meshes[id].index_buffer =
            AllocateBuffer(indices.data(), indices.size() * sizeof(indices[0]));
    Assert(!indices.empty(), "");
    gpu_meshes[id].index_count = indices.size() == 1? indices[0] : indices.size();
}

void Gallery::UpdateMeshData(const std::string& name)
{
    UpdateMeshData(mesh_name_id_map.at(name));
}

GpuMeshData Gallery::GetGpuMeshData(size_t id)
{
    Assert(id < gpu_meshes.size(), "");
    return gpu_meshes[id];
}

GpuMeshData Gallery::GetGpuMeshData(const std::string& name)
{
    Assert(mesh_name_id_map.count(name), "Mesh not found {0}", name);
    return GetGpuMeshData(mesh_name_id_map.at(name));
}

ImageHandle Gallery::GetGpuImageData(size_t id)
{
    Assert(id < gpu_images.size(), "");
    return gpu_images[id];
}

ImageHandle Gallery::GetGpuImageData(const std::string& name)
{
    Assert(image_name_id_map.count(name), "Image not found {0}", name);
    return GetGpuImageData(image_name_id_map.at(name));
}

BufferHandle Gallery::GetGpuBufferData(size_t id)
{
    Assert(id < gpu_buffers.size(), "");
    return gpu_buffers[id];
}

BufferHandle Gallery::GetGpuBufferData(const std::string& name)
{
    Assert(buffer_name_id_map.count(name), "Buffer not found {0}", name);
    return GetGpuBufferData(buffer_name_id_map.at(name));
}

} // NECore