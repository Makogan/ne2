#pragma once

/** @cond */
#include <cstdint>
#include <utility>
#include <vector>
/** @endcond */


namespace NECore
{
#define CAST_TO_VK_BUFFER(BufferHandle) (vk::Buffer)(VkBuffer)(uint64_t)BufferHandle
#define CAST_BUFFER_TO_UINT64(BufferHandle) (uint64_t)(VkBuffer)BufferHandle
struct BufferHandle
{
    uint64_t handle = 0;
    BufferHandle() {}
    BufferHandle(uint64_t h) : handle(h) {}
    operator uint64_t() const { return handle; }
};

struct ImageHandle
{
    uint64_t handle = 0;
    ImageHandle(uint64_t h) : handle(h) {}
    operator uint64_t() const { return handle; }
};

struct ShaderHandle
{
    uint64_t handle = 0;
    operator uint64_t() const { return handle; }
};

using UniformBufferData = std::pair<uint32_t, std::vector<std::byte>>;
using UniformBufferDataContainer = std::vector<UniformBufferData>;

using UniformImageData = std::pair<uint32_t, ImageHandle>;
using UniformImageDataContainer = std::vector<UniformImageData>;

} // NECore