#pragma once

/** @cond */
#include <fstream>
#include <string>
#include <vector>
/** @endcond */

#include "Log.hpp"

namespace Core
{
inline void LefTrim(std::string& str)
{
    size_t first = str.find_first_not_of(" \n\r\t");
    if(std::string::npos == first) { return; }
    str = str.substr(first, str.size());
}

inline void RightTrim(std::string& str)
{
    size_t last = str.find_last_not_of(" \n\r\t");
    if(std::string::npos == last) { return; }
    str = str.substr(0, last + 1);
}

inline void Trim(std::string& str)
{
    LefTrim(str);
    RightTrim(str);
}

inline std::vector<std::string> Split(const std::string& str, char separator = ' ')
{
    std::vector<std::string> tokens;
    std::string accumulated = "";
    for(auto c: str)
    {
        if(c != separator) accumulated += c;
        else
        {
            Trim(accumulated);
            tokens.push_back(accumulated);
            accumulated = "";
        }
    }
    Trim(accumulated);
    tokens.push_back(accumulated);

    return tokens;
}

inline std::string BaseNameWithoutExtension(const std::string& path)
{
    return Split(Split(path, '/').back(), '.')[0];
}

inline std::string SubstrUntilChar(std::string const& s, char separator)
{
    std::string::size_type pos = s.find(separator);
    if (pos != std::string::npos)
    {
        return s.substr(0, pos);
    }
    else
    {
        return s;
    }
}


// Read a file and put it into a string.
inline std::string ReadFile(const std::string& file_path)
{
    using namespace std;
    ifstream file(file_path);
    Assert(file.is_open(), "Failed to open file: " + file_path);
    stringstream sstream;
    sstream << file.rdbuf();
    string source = sstream.str();
    file.close();

    Assert(!source.empty(), "Source file is empty, something went wrong.");

    return source;
}

inline std::vector<std::byte> ReadBinaryFile(const std::string& file_path)
{
    std::ifstream file(file_path, std::ios::binary );
    file.seekg(0,std::ios::end);
    std::streampos length = file.tellg();
    file.seekg(0,std::ios::beg);

    std::vector<std::byte> buffer(length);
    file.read((char*)buffer.data(), length);

    return buffer;
}

inline void WriteFile(const std::string& file_path, const std::string& content)
{
    using namespace std;

    ofstream myfile;
    myfile.open(file_path);
    myfile << content;
    myfile.close();
}

inline void WriteFile(const std::string& file_path, const std::vector<std::byte>& content)
{
    using namespace std;

    ofstream myfile;
    myfile.open(file_path, ios::out | ios::binary);
    myfile.write((char*)content.data(), content.size());
    myfile.close();
}

inline void ToLowerCase(std::string& str)
{
    std::transform(str.begin(), str.end(), str.begin(), [](unsigned char c) {
        return std::tolower(c);
    });
}
}  // namespace Core

