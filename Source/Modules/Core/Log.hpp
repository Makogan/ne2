//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file Log.hpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2018-04-14
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#pragma once

/** @cond */
#include <vector>
#include <string>
/** @endcond */


using uint = unsigned int;

namespace Log
{

/**
 * @brief Set the log file string
 *
 */
void SetLogFile(const std::string& log_path);
/**
 * @brief Delete the log file
 *
 */
void WipeLog();
/**
 * @brief Append a message a the end of the log file
 *
 * @param message The message to record in the log
 */
void RecordLog(const std::string& message);
/**
 * @brief A function to print log messages with a specially aligned format
 *
 * The function records the first message plus some alignment plus the second
 * message. Space is added between the first and the second message such that
 * the second message always starts at the same character position.
 *
 * @param message1 The first part of the message
 * @param message2 The second part of the message
 * @param alignment The character position where the second part of the message
 * should be.
 */
void RecordLog(const std::string& message1, const std::string& message2, int alignment);
/**
 * @brief Similar to RecordLog(const std::string& message)
 *
 * @param message1 The first part of the message
 * @param message2 The second part of the message
 * @param alignment The character position where the second part of the message
 * should be.
 * @param fill the character that will be used to fill the aligned space
 */
void RecordLog(
    const std::string& message1, const std::string& message2, int alignment, char fill);
/**
 * @brief Similar to RecordLog(const std::string& message) except it
 * styles it with a title bar.
 *
 * @param message The message to style
 *
 */
void RecordLogHeader(const std::string& message);
/**
 * @brief Similar to RecordLog(std::string message) except it
 * styles it with a title bar.
 *
 * @param message1 The first part of the message
 * @param message2 The second part of the message
 * @param alignment The character position where the second part of the message
 * should be.
 */
void RecordLogHeader(
    const std::string& message1, const std::string& message2, int alignement);
/**
 * @brief Similar to RecordLog(std::string message) except it
 * styles it as an error message.
 *
 * @param message The message to record
 */
void RecordLogError(const std::string& message);
/**
 * @brief Record a message followed by the time and date at which this function
 * was called
 *
 * @param message
 */
void RecordLogTime(const std::string& message);
/**
 * @brief Wrap the text to so that no line exceeds the maximum
 *
 * @param text The text to modify
 * @param maximum The maximum line length
 * @return std::string The modified text
 */
std::string WrapText(const std::string& text, int maximum);
/**
 * @brief Demangle a c++ typeid string.
 *
 * @param name The id to demangle.
 * @return std::string The demangled id.
 */
std::string Demangle(const std::string& name);

/**
 * @brief NeverEngine version of assert, because the default one does not allow for
 * message formatting.
 *
 * Example usage:
 *
 * Assert(false, "I am {0}, and {1}.", "cool", "smart")
 *
 * Will print a formatted error message containing the string: "I am cool, and smart."
 *
 * @param expr Boolean expression, e.g var == 3
 * @param msg Base message, can be empty string, plain string or a string containing a
 * series of {i} markers to replace. e.g. "I am {0}, and {1}."
 * @param args A series of string arguments that will be replaced into the {i} markers.
 */
#ifdef NDEBUG
#    define DEBUG_VAR(var_expr)
#else /* NDEBUG */
#    define DEBUG_VAR(var_expr) var_expr
#endif

#if !defined(__PRETTY_FUNCTION__) && !defined(__GNUC__)

#endif

#ifdef __linux__

#else

#endif

#ifdef __linux__

#else

#endif

/**
 * @brief NeverEngine version of assert, because the default one does not allow for
 * message formatting.
 *
 * Example usage:
 *
 * Assert(false, "I am {0}, and {1}.", "cool", "smart")
 *
 * Will print a formatted error message containing the string: "I am cool, and smart."
 *
 * @param expr Boolean expression, e.g var == 3
 * @param msg Base message, can be empty string, plain string or a string containing a
 * series of {i} markers to replace. e.g. "I am {0}, and {1}."
 * @param args A series of string arguments that will be replaced into the {i} markers.
 */
#ifdef NDEBUG
#    define Assert(expr, msg, ARGS_DOTS) ((void)0)
#else /* NDEBUG */
#    define Assert(expr, msg, ...) \
        Log::details::DAssert(         \
            expr,                      \
            #expr,                     \
            msg,                       \
            __FILE__,                  \
            __PRETTY_FUNCTION__,       \
            __LINE__,                  \
            {__VA_ARGS__},                    \
            Log::details::LOG_ERROR)
#endif

/**
 * @brief Similar to Assert() but instead of terminating the program it just prints a
 * warning to the terminal.
 *
 * @param expr Boolean expression, e.g var == 3
 * @param msg Base message, can be empty string, plain string or a string containing a
 * series of {i} markers to replace. e.g. "I am {0}, and {1}."
 * @param args A series of string arguments that will be replaced into the {i} markers.
 */
#ifdef NDEBUG
#    define Warn(expr, msg, ARGS_DOTS) ((void)0)
#else /* NDEBUG */
#    define Warn(expr, msg, ...) \
        Log::details::DAssert(       \
            expr,                    \
            #expr,                   \
            msg,                     \
            __FILE__,                \
            __PRETTY_FUNCTION__,     \
            __LINE__,                \
            {__VA_ARGS__},                  \
            Log::details::LOG_WARNING)
#endif
/**
 * @brief Make the code inside of the macro available only in debug builds.
 * @param expr Code snippet to be compiled only in debug mode.
 */
#ifdef NDEBUG
#    define DEBUG_SNIPPET(expr) ((void)0)
#else /* NDEBUG */
#    define DEBUG_SNIPPET(expr) expr
#endif

namespace details
{
enum ErrorLevel
{
    LOG_MESSGAGE,
    LOG_WARNING,
    LOG_ERROR,
};

void DAssert(
    bool expr,
    const std::string& expr_str,
    const std::string& base,
    const std::string& file,
    const std::string& function,
    const int line,
    const std::vector<std::string>& msgs,
    const ErrorLevel level);
}  // namespace details
}  // namespace Log

