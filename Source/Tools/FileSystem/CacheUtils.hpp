#pragma once

#include <string>
#include <vector>

bool CacheIsValid(
    const std::string& cache_shader_path,
    const std::vector<std::string>& shader_paths);