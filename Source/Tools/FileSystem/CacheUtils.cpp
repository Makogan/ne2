#include "CacheUtils.hpp"

/** @cond */
#include <chrono>
#include <filesystem>
/** @endcond */

#include "Core/Log.hpp"

namespace fs = std::filesystem;

bool CacheIsValid(
    const std::string& cache_shader_path,
    const std::vector<std::string>& shader_paths)
{
    Assert(
        shader_paths.size() && fs::exists(shader_paths[0]),
        shader_paths[0]);
    // This is really messy because of the std stuff, the short of it is:
    // Find the youngest file in the shader paths, then check it against the
    // timestamp of the cached file.
    std::time_t youngest_file_ts =
        std::chrono::system_clock::to_time_t(
            std::chrono::file_clock::to_sys(fs::last_write_time(shader_paths[0])));
    for(auto file : shader_paths)
    {
        std::time_t current_timestamp =
            std::chrono::system_clock::to_time_t(
                std::chrono::file_clock::to_sys(fs::last_write_time(file)));
        double time_diff = difftime(youngest_file_ts, current_timestamp);
        if(time_diff < 0) youngest_file_ts = current_timestamp;
    }

    // All this is doing is comparing the youngest file time stamp with the cache.
    return fs::exists(cache_shader_path)
        && (difftime(youngest_file_ts, std::chrono::system_clock::to_time_t(
            std::chrono::file_clock::to_sys(fs::last_write_time(cache_shader_path)))) < 0);
}