#pragma once

using uint = unsigned int;

/**
 * @brief Compare 2 fpv's and determine whether they are within some epsilon of each other.
 *
 * @tparam T The fp type (e.g double).
 * @param a First value.
 * @param b Secont value.
 * @param epsilon Precission level (0.0001 by default).
 * @return true The values are within range.
 * @return false The values are outside the range.
 */
template <typename T>
constexpr bool IsCloseTo(const T a, const T b, const T epsilon = 0.0001)
{
    return abs(a - b) < epsilon;
}
/**
 * @brief Compare 2 fpv's and determine whether the first is less than the second.
 *  In mathematical terms we do $a < b + epsilon$
 *
 * @tparam T The fp type (e.g double).
 * @param a First value.
 * @param b Secont value.
 * @param epsilon Precission level (0.0001 by default).
 * @return true The first value is smaller.
 * @return false The second value is smaller.
 */
template <typename T>
constexpr bool IsLessThan(const T a, const T b, const T epsilon = 0.0001)
{
    return a < b + epsilon;
}
/**
 * @brief Round a number to $N$ decimal precision.
 *
 * @tparam T The fp type (e.g double).
 * @param v Value.
 * @param n Number of decimals to round to.
 * @return constexpr T ROunded result.
 */
template <typename T>
constexpr T RoundToNDecimalPlaces(const T v, unsigned int n)
{
    return floor(v * T(n)) / T(n);
}

/**
 * @brief Test that an FP value lies between 2 others.
 *
 * @tparam T Fp type (e.g double).
 * @param v Value to test
 * @param b1 Lower bound of the interval.
 * @param b2 Upper bound of the interval.
 * @return true $v \in [b1, b2]$
 * @return false  $v \notin [b1, b2]$
 */
template <typename T>
constexpr bool IsInRange(
    const T v,
    const T b1,
    const T b2,
    const T epsilon = static_cast<T>(0.0000000001))
{
    return (v >= b1 - epsilon) && (v <= b2 + epsilon);
}

