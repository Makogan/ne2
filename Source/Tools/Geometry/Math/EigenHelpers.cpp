//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file EigenHelpers.cpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2020-06-15
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#include "EigenHelpers.hpp"

/** @cond */
#include <iostream>
/** @endcond */

using namespace std;

std::pair<std::vector<float>, std::vector<uint>>
    GetFloatVectorsFromRowMatrix3f(std::any& input)
{
    auto& data = std::any_cast<Eigen::RowMatrix3f&>(input);
    uint rows = data.rows();
    uint size = rows * 3;
    std::vector<float> return_vec(size);
    memcpy(return_vec.data(), data.data(), size * sizeof(float));
    std::vector<uint> indices(rows);
    std::iota(indices.begin(), indices.end(), 0);
    return {return_vec, indices};
}

std::pair<std::vector<float>, std::vector<uint>>
    GetFloatVectorsFromGenericMatrix(std::any& input)
{
    auto& data = std::any_cast<Eigen::MatrixXd&>(input);
    std::vector<float> final;
    final.reserve(data.rows() * 3);
    for(uint i=0; i<data.rows(); i++)
    {
        final.push_back(data(i, 0));
        final.push_back(data(i, 1));
        final.push_back(data(i, 2));
    }

    vector<uint> indices(data.rows());
    iota(indices.begin(), indices.end(), 0);

    return {final, indices};
}

