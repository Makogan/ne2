#pragma once

/** @cond */
#include <type_traits>

#include "Eigen/Core"
/** @endcond */

namespace GLA
{
template<typename T> auto normalize(const T& t)
{
    return t / std::sqrt(t[0] * t[0] + t[1] * t[1] + t[2] * t[2]);
}

template<typename T, typename U, typename S=std::remove_reference<decltype(std::declval<T>()[0])>::type>
Eigen::Matrix<S, 3, 1> cross(const T& x, const U& y)
{
    return Eigen::Matrix<S, 3, 1>(
        x[1] * y[2] - y[1] * x[2], x[2] * y[0] - y[2] * x[0], x[0] * y[1] - y[0] * x[1]);
}

template<typename T> T dot(const Eigen::Vector<T, 2>& x, const Eigen::Vector<T, 2>& y)
{
    return x[0] * y[0] + x[1] * y[1];
}

template<typename T> T dot(const Eigen::Vector<T, 3>& x, const Eigen::Vector<T, 3>& y)
{
    return x[0] * y[0] + x[1] * y[1] + x[2] * y[2];
}

template<typename T, int N> T length(const Eigen::Vector<T, N>& x)
{
    return sqrt(dot(x, x));
}

template<typename V, typename T> V mix(const V& x, const V& y, const T t)
{
    return (T(1) - t) * x + t * y;
}

template<typename S, int R>
Eigen::Matrix<S, R, 1> max(
    const Eigen::Matrix<S, R, 1>& x,
    const Eigen::Matrix<S, R, 1>& y)
{
    return {std::max(x[0], y[0]), std::max(x[1], y[1]), std::max(x[2], y[2])};
}

template<typename S, int R>
Eigen::Matrix<S, R, 1> min(
    const Eigen::Matrix<S, R, 1>& x,
    const Eigen::Matrix<S, R, 1>& y)
{
    return {std::min(x[0], y[0]), std::min(x[1], y[1]), std::min(x[2], y[2])};
}

template<typename S, int R>
Eigen::Matrix<S, R, 1> clamp(
    const Eigen::Matrix<S, R, 1>& x,
    const Eigen::Matrix<S, R, 1>& min_val,
    const Eigen::Matrix<S, R, 1>& max_val)
{
    return GLA::min(GLA::max(x, min_val), max_val);
}

template<typename S, int R>
Eigen::Matrix<S, R, 1> clamp(
    const Eigen::Matrix<S, R, 1>& x,
    const Eigen::Matrix<S, R, 1>& y,
    S max_val)
{
    return GLA::clamp(x, y, Eigen::Matrix<S, R, 1>(max_val, max_val, max_val));
}


template<typename S>
S clamp(
    const S x,
    const S min_val,
    const S max_val)
{
    return std::min(std::max(x, min_val), max_val);
}

template<typename V> V abs(const V& x)
{
    return {std::abs(x.x()), std::abs(x.y()), std::abs(x.z())};
}

template<typename V> V sign(const V& x)
{
    if(x == 0) return 0;
    return x < 0? -1 : 1;
}

template<typename T, int N>
Eigen::Vector<T, N> floor(const Eigen::Vector<T, N>& x)
{
    Eigen::Vector<T, N> f;
    for(uint i = 0; i < N; i++)
    {
        f[i] = std::floor(x[i]);
    }
    return f;
}

template<typename T, int N>
Eigen::Vector<T, N> sign(const Eigen::Vector<T, N>& x)
{
    Eigen::Vector<T, N> result;
    for(int i=0; i<N; i++)
    {
        result[i] = x[i] == 0? 0 : x[i] < 0? -1 : 1;
    }
    return result;
}

template<typename T, int N>
Eigen::Vector<bool, N> greaterThan(
    const Eigen::Vector<T, N>& x,
    const Eigen::Vector<T, N>& y)
{
    Eigen::Vector<bool, N> result;
    for(int i = 0; i < N; i++)
    {
        result[i] = x[i] > y[i];
    }
    return result;
}

}  // namespace GLA

