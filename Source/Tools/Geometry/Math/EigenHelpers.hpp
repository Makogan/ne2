#pragma once

/** @cond */
#include <any>
#include <numeric>
#include <string>
#include <vector>

#include "Eigen/Dense"
/** @endcond */

#include "GLA.hpp"
#include "Core/Log.hpp"

using uint = unsigned int;

namespace Eigen
{
// Benchmarks showed this to be the fastest representation for iteration
typedef Eigen::Matrix<float, Eigen::Dynamic, 3, Eigen::RowMajor> RowMatrix3f;

}  // namespace Eigen

std::pair<std::vector<float>, std::vector<uint>> GetFloatVectorsFromRowMatrix3f(
    std::any& input);

std::pair<std::vector<float>, std::vector<uint>> GetFloatVectorsFromGenericMatrix(
    std::any& input);
// Modified from GLM
template<typename D>
Eigen::Matrix4f LookAt(
    const Eigen::Matrix<D, 3, 1>& eye,
    const Eigen::Matrix<D, 3, 1>& center,
    const Eigen::Matrix<D, 3, 1>& up)
{
    const Eigen::Matrix<D, 3, 1> f((center - eye).normalized());
    const Eigen::Matrix<D, 3, 1> s(f.cross(up).normalized());
    const Eigen::Matrix<D, 3, 1> u(s.cross(f));

    Eigen::Matrix<D, 4, 4> result = Eigen::Matrix<D, 4, 4>::Identity();
    result(0, 0) = s.x();
    result(0, 1) = s.y();
    result(0, 2) = s.z();

    result(1, 0) = u.x();
    result(1, 1) = u.y();
    result(1, 2) = u.z();

    result(2, 0) = -f.x();
    result(2, 1) = -f.y();
    result(2, 2) = -f.z();

    result(0, 3) = -s.dot(eye);
    result(1, 3) = -u.dot(eye);
    result(2, 3) = f.dot(eye);

    return result;
}

template<typename T>
Eigen::Matrix<T, 4, 4> Perspective(T fov, T aspect, T z_near, T z_far)
{
    Assert(aspect > std::numeric_limits<T>::epsilon(), "");

    const T tan_half_fovy = tan(fov / static_cast<T>(2));

    Eigen::Matrix<T, 4, 4> result = Eigen::Matrix<T, 4, 4>::Zero();
    result(0, 0) = static_cast<T>(1) / (aspect * tan_half_fovy);
    result(1, 1) = static_cast<T>(1) / (tan_half_fovy);
    result(2, 2) = -(z_far + z_near) / (z_far - z_near);
    result(3, 2) = -static_cast<T>(1);
    result(2, 3) = -(static_cast<T>(2) * z_far * z_near) / (z_far - z_near);
    return result;
}

template<typename D> D Radians(D degrees)
{
    Assert(
        std::numeric_limits<D>::is_iec559,
        "'Radians' only accept floating-point input");
    return degrees * static_cast<D>(0.01745329251994329576923690768489);
}

template<typename D>
Eigen::Matrix<D, 4, 4> Rotate(D angle, const Eigen::Matrix<D, 3, 1>& axis)
{
    Eigen::Matrix<D, 3, 3> rotation(Eigen::AngleAxis<D>(angle, axis));

    Eigen::Matrix<D, 4, 4> result;
    result << rotation.row(0), 0, rotation.row(1), 0, rotation.row(2), 0, 0, 0, 0, 1;

    return result;
}

template<typename D>
Eigen::Quaternion<D> RotateQuaternion(D angle, const Eigen::Matrix<D, 3, 1>& axis)
{
    return Eigen::Quaternion<D>(Eigen::AngleAxis<D>(angle, axis));
}

template<typename D, int N>
Eigen::Matrix<D, N + 1, 1> Append(const Eigen::Matrix<D, N, 1>& v, D s)
{
    auto vec = Eigen::Matrix<D, N + 1, 1>();
    vec << v, s;
    return vec;
}

template<typename D, int N, int M>
Eigen::Matrix<D, N + M, 1> Append(
    const Eigen::Matrix<D, N, 1>& v, const Eigen::Matrix<D, M, 1>& u)
{
    auto vec = Eigen::Matrix<D, N + M, 1>();
    vec << v, u;
    return vec;
}

template<typename D, int N>
Eigen::Matrix<D, N - 1, 1> XYZ(const Eigen::Matrix<D, N, 1>& v)
{
    static_assert(N >= 3);
    auto vec = Eigen::Matrix<D, N - 1, 1>();
    vec << v[0], v[1], v[2];
    return vec;
}

template<typename D, int N> Eigen::Matrix<D, N - 1, 1> XY(const Eigen::Matrix<D, N, 1>& v)
{
    static_assert(N >= 2);
    auto vec = Eigen::Matrix<D, N - 1, 1>();
    vec << v[0], v[1];
    return vec;
}

template<typename D> Eigen::Matrix<D, 4, 4> CastTo4D(const Eigen::Matrix<D, 3, 3>& mat3)
{
    Eigen::Matrix<D, 4, 4> mat4 = Eigen::Matrix<D, 4, 4>::Identity();
    mat4.block(0, 0, 3, 3) = mat3;

    return mat4;
}

template<typename D>
std::tuple<Eigen::Vector<D, 3>, Eigen::Quaternion<D>, Eigen::Vector<D, 3>>
TRSMatToComponents(const Eigen::Matrix<D, 4, 4>& mat)
{
    const Eigen::Vector<D, 3> translation = {mat(0, 3), mat(1, 3), mat(2, 3)};
    Eigen::Matrix<D, 3, 3> mat_rot = mat.block(0, 0, 3, 3);

    const Eigen::Vector<D, 3> scale = {
        mat_rot.row(0).norm(), mat_rot.row(1).norm(), mat_rot.row(2).norm()};
    mat_rot.row(0) = mat_rot.row(0) / scale[0];
    mat_rot.row(1) = mat_rot.row(1) / scale[1];
    mat_rot.row(2) = mat_rot.row(2) / scale[2];

    const Eigen::Quaternion<D> rotation(mat_rot);

    return {translation, rotation, scale};
}

template<typename D>
const Eigen::Matrix<D, 4, 4> BuildTRSMatrix(
    const Eigen::Vector<D, 3>& translation,
    const Eigen::Quaternion<D>& rotation,
    const Eigen::Vector<D, 3>& scales)
{
    Eigen::Matrix<D, 4, 4> trs_mat = Eigen::Matrix<D, 4, 4>::Identity();

    trs_mat.block(0, 3, 3, 1) = translation;
    trs_mat.block(0, 0, 3, 3) = rotation.normalized().toRotationMatrix();
    trs_mat.row(0) *= scales[0];
    trs_mat.row(1) *= scales[1];
    trs_mat.row(2) *= scales[2];

    return trs_mat;
}

