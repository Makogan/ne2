#pragma once

/** @cond */
#include <iostream>
#include <queue>
#include <vector>

#include "Eigen/Core"
/** @endcond */

#include "Geometry/Math/FlotingPointArithmetic.hpp"

using uint = unsigned int;

/**
 * @brief Shorthand to shorten code.
 *
 * @tparam Scalar Scalar.
 */
template<typename Scalar> using Vec = Eigen::Matrix<Scalar, 3, 1>;
/**
 * @brief Test collision of a line with a triangle.
 *
 * @param origin Start of the line.
 * @param ray Direction of the line.
 * @param triangle Triangle against which to test.
 * @return float Signed distance along the line at which the intersection happens.
 *  Infinity if no such value exists.
 */
float TriangleLineIntersection(
    const Eigen::Vector3d& origin,
    const Eigen::Vector3d& ray,
    const std::vector<Eigen::Vector3d>& triangle);
/**
 * @brief Compute the point of intersection of a line and a sphere.
 *
 * @tparam Scalar Scalar.
 * @param origin Ray origin.
 * @param ray Ray direction.
 * @param center Sphere center location.
 * @param radius Sphere radius.
 * @return Scalar Closest distance along the ray where the intersection is. i.e. the
 * interaction point is at $origin + t * ray$. Returns -1 if no intersection point is
 * found.
 */
template<typename Scalar>
Scalar LineSphereIntersection(
    const Vec<Scalar>& origin,
    const Vec<Scalar>& ray,
    const Vec<Scalar>& center,
    Scalar radius)
{
    Scalar a = ray.dot(ray);
    Scalar b = -Scalar(2) * center.dot(ray) + 2 * ray.dot(origin);
    Scalar c = -Scalar(2) * origin.dot(center) + center.dot(center) - radius * radius +
        origin.dot(origin);

    Scalar discriminant = b * b - 4 * a * c;

    Scalar t1, t2;
    if(discriminant < 0) { return -1; }

    else
    {
        t1 = (-b - sqrt(discriminant)) / (Scalar(2) * a);
        t2 = (-b + sqrt(discriminant)) / (Scalar(2) * a);
    }

    if(t1 < 0 && t2 >= 0) t1 = t2;
    else if(t1 >= 0 && t2 < 0)
        t2 = t1;

    return std::min(t1, t2);
}

// Taken from here: http://mathforum.org/library/drmath/view/62814.html
// (note there's a small error, the absolute value deletes the sign, I corrected it.)

/**
 * @brief Calculate the intersection of 2 lines.
 *
 * This calculates the intersection of 2 lines, $P_1 + a V_1$ and $P_2 + b V_2$
 * in 3D if it exists.
 *
 * Proof:
 * $     P_1 + a V_1 = P_2 + b V_2$
 * $\iff a V_1 = (P_2 - P_1) + b V_2$
 * $\iff a V_1 \times V_2 = (P_2 - P_1) \times V_2$
 * $\iff a = \frac{((P_2 - P_1) \times V_2) \cdot (n)} {(V_1 \times V_2) \cdot n}$
 *
 * With $n = \frac{(V_1 \times V_2)} {|V_1 \times V_2|}$
 *
 * A solution exists if and only if the above is well defined (no divisions by 0).
 *
 * The method will return 0 if the 2 origins are the same, and infinity if the 2 lines
 * are either parallel or not coplanar.
 *
 * @param origin1 Origin of the first line
 * @param ray1 Direction of the first line
 * @param origin2 Origin of the second line
 * @param ray2 Direction of the second line
 * @return double Distance in terms of `ray1` from `origin1` to the point of intersection.
 * i.e the intersection vertex is at `origin1 + return_value * ray1`
 */
double LineLineIntersection(
    const Eigen::Vector3d& origin1,
    const Eigen::Vector3d& ray1,
    const Eigen::Vector3d& origin2,
    const Eigen::Vector3d& ray2);
/**
 * @brief Test if a point is inside of a triangle. (The first 3 values of the array will
 * be used, all values threafter will be ignored).
 *
 * @param triangle The triangle against which to test.
 * @param point The point being tested.
 * @return true The point is inside the triangle.
 * @return false The point is outside the triangle.
 */
bool TestPointInTriangle(
    const std::vector<Eigen::Vector3d>& triangle, const Eigen::Vector3d& point);
/**
 * @brief Calculate the barycentric coordinates of a triangle.
 *
 * @param p Point to test.
 * @param a First point in the triangle.
 * @param b Second point in the triangle.
 * @param c Third point in the triangle.
 * @return std::tuple<float, float, float> u, v, w tuple.
 */
std::tuple<float, float, float> Barycentric(
    const Eigen::Vector3f& p,
    const Eigen::Vector3f& a,
    const Eigen::Vector3f& b,
    const Eigen::Vector3f& c);
/**
 * @brief Test that a point is in the segment described by $s_1$ and $s_2$.
 *
 * Segment inclusion means the point is both in the line and in between the 2 points.
 * If @include_boundary is true, the method returns true if $s_1 = p$ or $s_2 = p$, if
 * false it returns false in those cases.
 *
 * @param s1 First point in the segment.
 * @param s2 Second point in the segment.
 * @param p  Point to test.
 * @param include_boundary Whether to include the boundary points.
 * @return true The point is in the segment.
 * @return false The point is not in the segment.
 */
bool TestPointInSegment(
    const Eigen::Vector3d& s1,
    const Eigen::Vector3d& s2,
    const Eigen::Vector3d& p,
    bool include_boundary = true);
/**
 * @brief Get the signed angle between 2 unit directions. This function assumes all
 * preconditions are met. It should be given 2 unit directions and one of the 2 normals
 * defined by that plane. Other inputs are undefined behahoviour .
 *
 * @param d1 First unit direction.
 * @param d2 Second unit direction.
 * @param normal Normal to the plane.
 * @return double Signed angle from d1 to d2.
 */
double SignedAngle(
    const Eigen::Vector3d& d1, const Eigen::Vector3d& d2, const Eigen::Vector3d& normal);
/**
 * @brief Implementation of Djikstra's shortest path algorithm. (Doesn't handle forests).
 *
 * @tparam T Type encapsulating a node in the graph.
 * @param node_list An array of nodes (all nodes should be present).
 * @param GetNeighbours Function returning an std::vector of the neighbours of a node.
 * @param GetId Function returning the id (index in the node_list) of a node.
 * @param GetDistance Function returning the weight of the edge connecting 2 nodes.
 * @param start The root node from which to start Dikstra's
 * @return std::pair<std::vector<uint>, std::vector<double>> The first value will be the
 *  ancestry table of the tree. i.e the value at return1[i] is the parent of node i.
 *  The second value will be the total distance of a node. i.e the value at return1[i] is
 *  the total distance from start onto the node i.
 */
template<typename T>
std::pair<std::vector<uint>, std::vector<double>> Djikstra(
    const std::vector<T>& node_list,
    std::vector<T> (*GetNeighbours)(const T& t),
    uint (*GetId)(const T& t),
    double (*GetDistance)(const T& t1, const T& t2),
    uint start)
{
    std::vector<double> node_distance_map(
        node_list.size(), std::numeric_limits<double>::max());
    std::vector<uint> node_parent_map(node_list.size());
    std::vector<bool> node_visited_map(node_list.size(), false);

    typedef std::pair<double, uint> NodeInfo;
    std::priority_queue<NodeInfo, std::vector<NodeInfo>, std::greater<NodeInfo>> queue;

    node_distance_map[start] = 0;
    node_parent_map[start] = start;
    queue.push({0, start});

    while(!queue.empty())
    {
        auto [current_distance, current_node_index] = queue.top();
        queue.pop();
        auto current_node = node_list[current_node_index];

        node_visited_map[current_node_index] = true;

        auto neighbours = GetNeighbours(current_node);
        Assert(neighbours.size() > 0, "");
        auto& shortest_neighbour = neighbours[0];
        for(auto& neighbour: neighbours)
        {
            uint neighbour_id = GetId(neighbour);
            // Skip any node that has already been visited
            if(node_visited_map[neighbour_id]) continue;
            double distance = GetDistance(current_node, neighbour);
            double total_distance = distance + current_distance;

            // Overwrite prior distances if the current distance is shorter
            if(total_distance < node_distance_map[neighbour_id])
            {
                node_parent_map[neighbour_id] = current_node_index;
                node_distance_map[neighbour_id] = total_distance;
                queue.push({total_distance, neighbour_id});
            }
        }
    }

    return {node_parent_map, node_distance_map};
}
/**
 * @brief Express a point given in the standard basis into an arbitrary orthonormal basis.
 *
 * @param e1 First orthonormal basis vector.
 * @param e2 Second orthonormal basis vector.
 * @param e3 Third orthonormal basis vector.
 * @param origin Origin of the basis.
 * @param point Point to re-express.
 * @return Eigen::Vector3d Coordinates of the point in the given basis.
 */
Eigen::Vector3d ExpressInBasis(
    const Eigen::Vector3d& e1,
    const Eigen::Vector3d& e2,
    const Eigen::Vector3d& e3,
    const Eigen::Vector3d& origin,
    const Eigen::Vector3d& point);
/**
 * @brief Compute the intersection of a line and a triangle when they are both in the same
 * plane.
 *
 * (Currently it is assumed the line starts inside the triangle)
 *
 * @tparam Scalar Scalar type.
 * @param origin Start of the line.
 * @param dir Direction of the line.
 * @param t1 First point int he triangle.
 * @param t2 Second point int he triangle.
 * @param t3 Third point int he triangle.
 * @return std::tuple<Vec<Scalar>, bool, int> Tuple of:
 *  - The intersection point.
 *  - True if the line hits, false otherwise.
 *  - The index of the first point in the collided edge (0 for t1, 1 for t2, 2 for t3).
 */
template<typename Scalar>
std::tuple<Vec<Scalar>, bool, int> CoPlanarLineTriangleIntersection(
    const Vec<Scalar>& origin,
    const Vec<Scalar>& dir,
    const Vec<Scalar>& t1,
    const Vec<Scalar>& t2,
    const Vec<Scalar>& t3)
{
    const std::array<Vec<Scalar>, 3> points = {t1, t2, t3};

    for(uint i = 0; i < 3; i++)
    {
        Vec<Scalar> v1 = points[i];
        Vec<Scalar> v2 = points[(i + 1) % 3];

        Vec<Scalar> edge = v2 - v1;
        float u = LineLineIntersection(
            {v1.x(), v1.y(), v1.z()},
            {edge.x(), edge.y(), edge.z()},
            {origin.x(), origin.y(), origin.z()},
            {dir.x(), dir.y(), dir.z()});
        Vec<Scalar> intersection = v1 + edge * u;
        // u is the parameter of the first line. This will give the sign of the parameter
        // of the second line.
        float v = (intersection - origin).dot(dir);
        if(v >= 0 && u >= 0 && u <= 1.f + 0.0000000001) return {intersection, true, i};
    }

    return {{}, false, -1};
}
/**
 * @brief Intersection of a line with a polyline when they are both contained in the same
 * plane.
 *
 * @tparam Scalar
 * @param origin
 * @param dir
 * @param line
 * @param is_closed
 * @return std::tuple<Vec<Scalar>, bool, int>
 */
template<typename Scalar>
std::tuple<Vec<Scalar>, bool, int> CoPlanarLinePolyLineIntersection(
    const Vec<Scalar>& origin,
    const Vec<Scalar>& dir,
    const std::vector<Vec<Scalar>>& line,
    bool is_closed = false)
{
    const uint point_num = line.size();
    for(uint i = 0; i < point_num + is_closed - 1; i++)
    {
        Vec<Scalar> v1 = line[i];
        Vec<Scalar> v2 = line[(i + 1) % point_num];

        Vec<Scalar> edge = v2 - v1;
        float u = LineLineIntersection(
            {v1.x(), v1.y(), v1.z()},
            {edge.x(), edge.y(), edge.z()},
            {origin.x(), origin.y(), origin.z()},
            {dir.x(), dir.y(), dir.z()});
        Vec<Scalar> intersection = v1 + edge * u;
        // u is the parameter of the first line. This will give the sign of the parameter
        // of the second line.
        float v = (intersection - origin).dot(dir);
        const float epsilon = 0.00001;
        if(v >= 0 - epsilon && IsInRange(u, 0.f, 1.f, epsilon))
            return {intersection, true, i};
    }

    return {{}, false, -1};
}
/**
 * @brief Orthogonally project a point onto a line.
 *
 * @tparam Scalar scalar type.
 * @param point Point to project.
 * @param origin Start of line.
 * @param edge Direction of the line.
 * @return constexpr Vec<Scalar> The projection of point onto the line specified by origin
 * and edge.
 *
 * (Project onto axis aligned line)
 * \code {.cpp}
 * Eigen::Vector3f p1 = {0, 1, 0};
 * Eigen::Vector3f p2 = {0, 0, 0};
 * Eigen::Vector3f v1 = {1, 0, 0};
 *
 * Eigen::Vector3f projection = ProjectPointOntoLine(p1, p2, v1);
 *
 * EXPECT_NEAR(0, projection.x(), 0.00001);
 * EXPECT_NEAR(0, projection.y(), 0.00001);
 * EXPECT_NEAR(0, projection.z(), 0.00001);
 * \endcode
 */
template<typename Scalar>
constexpr Vec<Scalar> ProjectPointOntoLine(
    const Vec<Scalar>& point, const Vec<Scalar>& origin, const Vec<Scalar>& edge)
{
    const Vec<Scalar> dir = edge.normalized();
    const Vec<Scalar> v = (point - origin);
    return origin + v.dot(dir) * dir;
}
template<typename Scalar>
constexpr Vec<Scalar> ProjectPointOntoPlane(
    const Vec<Scalar>& point, const Vec<Scalar>& origin, const Vec<Scalar>& normal)
{
    const Vec<Scalar> v = (point - origin);
    return point - v.dot(normal) * normal;
}
/**
 * @brief Find the center of the osculating circle of 3 unaligned points.
 *
 * (The radius can be trivially computed from the center and one of the points).
 *
 * @tparam Scalar Scalar type.
 * @param p1 First unaligned point.
 * @param p2 Second unaligned point.
 * @param p3 Third unaligned point.
 * @return Vec<Scalar> Center of the osculating circle.
 *
 * Example:
 *
 * (Find osculating circle of basis vectors)
 * \code {.cpp}
 * Eigen::Vector3f p1 = {1, 0, 0};
 * Eigen::Vector3f p2 = {0, 1, 0};
 * Eigen::Vector3f p3 = {0, 0, 1};
 *
 * Eigen::Vector3f center = OsculatingCircle(p1, p2, p3);
 * EXPECT_NEAR(0.3333333, center.x(), 0.00001);
 * EXPECT_NEAR(0.3333333, center.y(), 0.00001);
 * EXPECT_NEAR(0.3333333, center.z(), 0.00001);
 * \endcode
 */
template<typename Scalar>
Vec<Scalar> OsculatingCircle(
    const Vec<Scalar>& p1, const Vec<Scalar>& p2, const Vec<Scalar>& p3)
{
    const Vec<Scalar> d1 = p2 - p1;
    const Vec<Scalar> d2 = p3 - p1;

    const Vec<Scalar> m1 = (p1 + p2) * (Scalar(1) / Scalar(2));
    const Vec<Scalar> m2 = (p1 + p3) * (Scalar(1) / Scalar(2));
    const Vec<Scalar> normal = d1.cross(d2).normalized();

    const Vec<Scalar> o1 = normal.cross(d1);
    const Vec<Scalar> o2 = normal.cross(d2);

    const Vec<double> v1 = {m1.x(), m1.y(), m1.z()};
    const Vec<double> v2 = {o1.x(), o1.y(), o1.z()};
    const Vec<double> v3 = {m2.x(), m2.y(), m2.z()};
    const Vec<double> v4 = {o2.x(), o2.y(), o2.z()};
    const Scalar t = (Scalar) LineLineIntersection(v1, v2, v3, v4);

    return m1 + o1 * t;
}

template<typename Scalar>
constexpr Vec<Scalar> NumericGradient(
    const Vec<Scalar>& v, Scalar (*F)(const Vec<Scalar>&))
{
    const Scalar epsilon = 1e-3;
    return Vec<Scalar>(
        (F(v + Vec<Scalar>(epsilon, 0, 0)) - F(v - Vec<Scalar>(epsilon, 0, 0))) /
            (Scalar(2) * epsilon),
        (F(v + Vec<Scalar>(0, epsilon, 0)) - F(v - Vec<Scalar>(0, epsilon, 0))) /
            (Scalar(2) * epsilon),
        (F(v + Vec<Scalar>(0, 0, epsilon)) - F(v - Vec<Scalar>(0, 0, epsilon))) /
            (Scalar(2) * epsilon));
}

