//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file GeometryUtils.cpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2020-07-15
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#include "GeometryUtils.hpp"

/** @cond */
#include <algorithm>
#include <iostream>
/** @endcond */

#include "Geometry/Math/FlotingPointArithmetic.hpp"
#include "Core/Log.hpp"
#include "Eigen/Dense"

using namespace std;

float TriangleLineIntersection(
    const Eigen::Vector3d& origin,
    const Eigen::Vector3d& ray,
    const std::vector<Eigen::Vector3d>& triangle)
{
    Eigen::Vector3d s = origin - triangle[0];
    Eigen::Vector3d e1 = triangle[1] - triangle[0];
    Eigen::Vector3d e2 = triangle[2] - triangle[0];

    Eigen::Matrix3d mt;
    mt << s, e1, e2;
    Eigen::Matrix3d mu;
    mu << -ray, s, e2;
    Eigen::Matrix3d mv;
    mv << -ray, e1, s;
    Eigen::Matrix3d md;
    md << -ray, e1, e2;

    float t = mt.determinant() / md.determinant();
    float u = mu.determinant() / md.determinant();
    float v = mv.determinant() / md.determinant();

    int is_inside_triangle =
        t >= 0 && (u + v) <= 1 && (u + v) >= 0 && u <= 1 && u >= 0 && v <= 1 && v >= 0;

    // Return t if it is inside the triangle, -1 otherwise
    return -(1 - is_inside_triangle) + t * is_inside_triangle;
}

double LineLineIntersection(
    const Eigen::Vector3d& origin1,
    const Eigen::Vector3d& ray1,
    const Eigen::Vector3d& origin2,
    const Eigen::Vector3d& ray2)
{
    if(abs((origin1 - origin2).dot(origin1 - origin2) - 1.0) < 0.000001) return 0;
    auto n1 = (origin2 - origin1).cross(ray2);
    auto n2 = ray1.cross(ray2);

    // Use this to test whether the vectors point in the same or opposite directions
    auto n = n2.normalized();
    // If n2 is the 0 vector or if the cross products are not colinear, no solution exists
    if(n2.norm() < 0.00001 || abs(abs(n1.dot(n)) - n1.norm()) > 0.000001)
        return std::numeric_limits<double>::infinity();

    return n1.dot(n) / n2.dot(n);
}

bool TestPointInTriangle(
    const std::vector<Eigen::Vector3d>& triangle, const Eigen::Vector3d& p)
{
    Eigen::Vector3d d1 = triangle[0] - p;
    Eigen::Vector3d d2 = triangle[1] - p;
    Eigen::Vector3d d3 = triangle[2] - p;

    Eigen::Vector3d e1 = triangle[1] - triangle[0];
    Eigen::Vector3d e2 = triangle[2] - triangle[0];

    double triangle_area = e1.cross(e2).norm();

    double subtriangles_area =
        d1.cross(d2).norm() + d1.cross(d3).norm() + d3.cross(d2).norm();

    return abs(subtriangles_area - triangle_area) < 0.000001;
}

std::tuple<float, float, float> Barycentric(
    const Eigen::Vector3f& p,
    const Eigen::Vector3f& a,
    const Eigen::Vector3f& b,
    const Eigen::Vector3f& c)
{
    Eigen::Vector3f v0 = b - a, v1 = c - a, v2 = p - a;
    float d00 = v0.dot(v0);
    float d01 = v0.dot(v1);
    float d11 = v1.dot(v1);
    float d20 = v2.dot(v0);
    float d21 = v2.dot(v1);
    float denom = d00 * d11 - d01 * d01;
    float v = (d11 * d20 - d01 * d21) / denom;
    float w = (d00 * d21 - d01 * d20) / denom;
    float u = 1.0f - v - w;

    return {u, v, w};
}

bool VertexCompare(const Eigen::Vector3d& v1, const Eigen::Vector3d& v2)
{
    const float epsilon = 0.00001;
    if(abs(v1.x() - v2.x()) > epsilon)
    {
        if(v1.x() > v2.x()) return false;
    }
    if(abs(v1.y() - v2.y()) > epsilon)
    {
        if(v1.y() > v2.y()) return false;
    }
    if(abs(v1.z() - v2.z()) > epsilon)
    {
        if(v1.z() > v2.z()) return false;
    }

    return true;
}

bool TestPointInSegment(
    const Eigen::Vector3d& s1,
    const Eigen::Vector3d& s2,
    const Eigen::Vector3d& p,
    bool include_boundary)
{
    const double epsilon = 0.0000001;
    double d1 = (s1 - p).norm();
    double d2 = (s2 - p).norm();
    if(!include_boundary && (IsCloseTo(d1, 0.0) || IsCloseTo(d2, 0.0))) return false;
    return IsCloseTo(d1 + d2, (s1 - s2).norm(), epsilon);
}

double SignedAngle(
    const Eigen::Vector3d& d1, const Eigen::Vector3d& d2, const Eigen::Vector3d& normal)
{
    const double epsilon = 0.00000000001;
    if(abs(abs(d1.dot(d2)) - 1.0) < epsilon) return 0;
    auto angle_normal = d1.cross(d2).normalized();
    return round(angle_normal.dot(normal)) *
        acos(std::clamp(d1.dot(d2), -1 + epsilon, 1 - epsilon));
}

Eigen::Vector3d ExpressInBasis(
    const Eigen::Vector3d& e1,
    const Eigen::Vector3d& e2,
    const Eigen::Vector3d& e3,
    const Eigen::Vector3d& origin,
    const Eigen::Vector3d& point)
{
    const auto ppoint = point - origin;
    const double epsilon = 0.0001;
    Assert(IsCloseTo(e1.norm(), 1.0, epsilon), "");
    Assert(IsCloseTo(e2.norm(), 1.0, epsilon), "");
    Assert(IsCloseTo(e3.norm(), 1.0, epsilon), "");
    Assert(IsCloseTo(e1.dot(e2), 0.0, epsilon), "");
    Assert(IsCloseTo(e2.dot(e3), 0.0, epsilon), "");
    Assert(IsCloseTo(e1.dot(e3), 0.0, epsilon), "");

    return {e1.dot(ppoint), e2.dot(ppoint), e3.dot(ppoint)};
}

template std::tuple<Vec<float>, bool, int> CoPlanarLineTriangleIntersection(
    const Vec<float>& origin,
    const Vec<float>& dir,
    const Vec<float>& t1,
    const Vec<float>& t2,
    const Vec<float>& t3);

template std::tuple<Vec<double>, bool, int> CoPlanarLineTriangleIntersection(
    const Vec<double>& origin,
    const Vec<double>& dir,
    const Vec<double>& t1,
    const Vec<double>& t2,
    const Vec<double>& t3);

