#pragma once

/** @cond */
#include <array>
#include <map>
#include <numeric>
#include <vector>
/** @endcond */

template<typename V, typename SdfFun, typename S>
std::pair<bool, V> FindBestFitCell(
    const V& point,
    SdfFun DistanceFunction,
    const S side_length_x,
    const S side_length_y,
    const S side_length_z)
{
    V cell_center = point + V(0.5 * side_length_x, 0.5 * side_length_y, 0.5 * side_length_z);

    const S surface_penalty = static_cast<S>(10);
    auto LossFunction = [&](const V& v)
    {
        const S eccentric_penalty = (cell_center - v).dot(cell_center - v);
        return DistanceFunction(v) * DistanceFunction(v) * surface_penalty + eccentric_penalty;
    };

    auto Gradient = [&](const V& v)
    {
        const S epsilon = 1e-3;
        return V(
            (LossFunction(v + V(epsilon, 0, 0)) - LossFunction(v - V(epsilon, 0, 0))) /
                (S(2) * epsilon),
            (LossFunction(v + V(0, epsilon, 0)) - LossFunction(v - V(0, epsilon, 0))) /
                (S(2) * epsilon),
            (LossFunction(v + V(0, 0, epsilon)) - LossFunction(v - V(0, 0, epsilon))) /
                (S(2) * epsilon));
    };
    std::array<V, 8> corners;
    std::array<S, 8> signed_distances;

    // Evaluate the distance function at the 8 corners of this cell.
    for(uint i = 0; i < 8; i++)
    {
        int x = (i % 8) / 4;
        int y = (i % 4) / 2;
        int z = (i % 2) / 1;
        corners[i] = point + V(x * side_length_x, y * side_length_y, z * side_length_z);
        signed_distances[i] = DistanceFunction(corners[i]);
    }

    // Test all edges in the cell, if at least one crosses the boundary then this cell
    // contains a boundary vertex.

    // Look at the Z aligned edges and test if they cross the boundary.
    bool cell_crosses_boundary = false;
    for(uint dx = 0; dx < 2; dx++)
    {
        for(uint dy = 0; dy < 2; dy++)
        {
            if((signed_distances[dx * 4 + dy * 2 + 0] *
                signed_distances[dx * 4 + dy * 2 + 1]) <= 0)
            {
                cell_crosses_boundary = true;
            }
        }
    }

    // Look at the Y aligned edges and test if they cross the boundary.
    for(uint dx = 0; dx < 2; dx++)
    {
        for(uint dz = 0; dz < 2; dz++)
        {
            if((signed_distances[dx * 4 + 0 * 2 + dz] *
                signed_distances[dx * 4 + 1 * 2 + dz]) <= 0)
            {
                cell_crosses_boundary = true;
            }
        }
    }

    // Look at the X aligned edges and test if they cross the boundary.
    for(uint dy = 0; dy < 2; dy++)
    {
        for(uint dz = 0; dz < 2; dz++)
        {
            if((signed_distances[0 * 4 + dy * 2 + dz] *
                signed_distances[1 * 4 + dy * 2 + dz]) <= 0)
            {
                cell_crosses_boundary = true;
            }
        }
    }

    V p = cell_center;
    // Gradient descent from the center of the cell to some optimum.
    if(cell_crosses_boundary)
    {
        for(uint i = 0; i < 50; i++)
        {
            V new_p = p - Gradient(p) * static_cast<S>(0.01);
            S step = (p - new_p).norm();

            if(step <= 0.00000000001) break;

            p = new_p;
        }

        return {true, p};
    }
    return {false, {}};
}

template<typename V, typename SdfFun, typename S = decltype(V()[0])>
std::pair<std::vector<V>, std::vector<uint>> DualContouring(
    SdfFun DistanceFunction,
    const S x_span = S(10),
    const S y_span = S(10),
    const S z_span = S(10),
    const unsigned int x_resolution = 10,
    const unsigned int y_resolution = 10,
    const unsigned int z_resolution = 10)
{
    std::vector<V> vertices;
    vertices.reserve(x_resolution * y_resolution);
    std::map<std::tuple<uint, uint, uint>, uint> vertex_index_map;
    std::vector<unsigned int> connectivity;
    connectivity.reserve(x_resolution * y_resolution);

    const S side_length_x = x_span / S(x_resolution - 1);
    const S side_length_y = y_span / S(y_resolution - 1);
    const S side_length_z = z_span / S(z_resolution - 1);
    for(unsigned int i = 0; i < x_resolution; i++)
    {
        const S x = S(i) * side_length_x - S(0.5) * x_span;
        for(unsigned int j = 0; j < y_resolution; j++)
        {
            const S y = S(j) * side_length_y - S(0.5) * y_span;
            for(unsigned int k = 0; k < z_resolution; k++)
            {
                const S z = S(k) * side_length_z - S(0.5) * z_span;

                auto [found, v] =
                    FindBestFitCell(
                        V(x, y, z),
                        DistanceFunction,
                        side_length_x,
                        side_length_y,
                        side_length_z);
                if(found)
                {
                    vertex_index_map.insert({{i, j, k}, vertices.size()});
                    vertices.push_back(v);
                }
            }
        }
    }

    // The range of each iteration skips the first index to prevent sampling outside the
    // bounds of the array. However, this still spans all cells. So the full space
    // contained in the grid is tested.
    for(unsigned int i = 1; i < x_resolution; i++)
    {
        const S x = S(i) * side_length_x - S(0.5) * x_span;
        for(unsigned int j = 1; j < y_resolution; j++)
        {
            const S y = S(j) * side_length_y - S(0.5) * y_span;
            for(unsigned int k = 1; k < x_resolution; k++)
            {
                const S z = S(k) * side_length_z - S(0.5) * z_span;

                const S origin = DistanceFunction(V(x, y, z));
                const S sign_z1 = DistanceFunction(V(x, y, z + side_length_z));
                // In the following snippets, the boolean arithmetic is emulating a
                // row swap. i.e. the sign of the variable determines the winding order
                // of the indices.
                if(origin * sign_z1 <= 0)
                {
                    const int sz1 = int(sign_z1 <= 0);
                    connectivity.push_back(vertex_index_map.at({i - 1, j - 1, k}));
                    connectivity.push_back(vertex_index_map.at({i - 0, j - !sz1, k}));
                    connectivity.push_back(vertex_index_map.at({i - 0, j - sz1, k}));

                    connectivity.push_back(vertex_index_map.at({i - 1, j - 1, k}));
                    connectivity.push_back(vertex_index_map.at({i - sz1, j - 0, k}));
                    connectivity.push_back(vertex_index_map.at({i - !sz1, j - 0, k}));
                }

                const S sign_y1 = DistanceFunction(V(x, y + side_length_y, z));
                if(origin * sign_y1 <= 0)
                {
                    const int sy1 = int(sign_y1 <= 0);
                    connectivity.push_back(vertex_index_map.at({i - 0, j, k - !sy1}));
                    connectivity.push_back(vertex_index_map.at({i - 1, j, k - 1}));
                    connectivity.push_back(vertex_index_map.at({i - 0, j, k - sy1}));

                    connectivity.push_back(vertex_index_map.at({i - sy1, j, k - 0}));
                    connectivity.push_back(vertex_index_map.at({i - 1, j, k - 1}));
                    connectivity.push_back(vertex_index_map.at({i - !sy1, j, k - 0}));
                }

                const S sign_x1 = DistanceFunction(V(x + side_length_x, y, z));
                if(origin * sign_x1 <= 0)
                {
                    const int sx1 = int(sign_x1 <= 0);
                    connectivity.push_back(vertex_index_map.at({i, j - !sx1, k - 0}));
                    connectivity.push_back(vertex_index_map.at({i, j - 1, k - 1}));
                    connectivity.push_back(vertex_index_map.at({i, j - sx1, k - 0}));

                    connectivity.push_back(vertex_index_map.at({i, j - 0, k - sx1}));
                    connectivity.push_back(vertex_index_map.at({i, j - 1, k - 1}));
                    connectivity.push_back(vertex_index_map.at({i, j - 0, k - !sx1}));
                }
            }
        }
    }

    return {vertices, connectivity};
}

template<typename V, typename SdfFun, typename S = decltype(V()[0])>
std::pair<std::vector<V>, std::vector<uint>> DualContouring(
    SdfFun DistanceFunction,
    const S span = S(10),
    const unsigned int resolution = 10)
{
    return DualContouring<V>(DistanceFunction, span, span, span, resolution, resolution, resolution);
}