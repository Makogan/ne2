#pragma once

/** @cond */
#include <iostream>
#include <vector>
/** @endcond */

#include "Geometry/Math/EigenHelpers.hpp"

// B-Spline ------------------------------------------------------------------------------
class BSpline
{
    std::vector<Eigen::Vector3f> control_points;
    std::vector<float> knots;
    uint order = 4;

  public:
    static std::vector<std::vector<float>> VisualizeIntervals(BSpline& spline, float t);
    struct DeboorData
    {
        std::vector<std::vector<Eigen::Vector3f>> intermediate_points;
        std::vector<std::vector<Eigen::Vector3f>> intermediate_lines;
    };
    static DeboorData VisualizeDeBoors(BSpline& spline, float t);
    BSpline() {}
    BSpline(const std::vector<Eigen::Vector3f>& control_points);
    BSpline(const std::vector<Eigen::Vector3f>& control_points, uint order);

    Eigen::Vector3f operator()(float t);
    void AddControlPoint(Eigen::Vector3f point);
    void SetControlPoint(uint i, Eigen::Vector3f point) { control_points[i] = point; }
    void MoveControlPoint(uint i, Eigen::Vector3f offset) { control_points[i] += offset; }

    void SetOrder(uint new_order);

    uint GetOrder() { return order; }
    uint GetKnotIndex(float t);
    float GetKnot(uint i) { return knots[i]; }
    std::vector<Eigen::Vector3f> GetLineVertices();
    std::vector<Eigen::Vector3f> GetControlPoints() { return control_points; }
    std::vector<float> GetKnots() { return knots; }
};

// Shapes --------------------------------------------------------------------------------
std::pair<std::vector<Eigen::Vector3f>, std::vector<uint>> GenerateSphere(
    float radius = 1, uint u_resolution = 100, uint v_resolution = 100);

std::pair<std::vector<Eigen::Vector3f>, std::vector<uint>> GenerateEllipsoid(
    float a = 1,
    float b = 1,
    float c = 1,
    uint u_resolution = 100,
    uint v_resolution = 100);

std::vector<Eigen::Vector2f> GenerateEllipsoidUvs(
    float a, float b, float c, uint u_resolution, uint v_resolution);

std::pair<std::vector<Eigen::Vector3f>, std::vector<uint>> GenerateCylinder(
    float radius = 1,
    float height = 1,
    uint u_resolution = 100,
    uint v_resolution = 100,
    bool y_is_up = false);

std::pair<std::vector<Eigen::Vector3f>, std::vector<uint>> GenerateClosedCylinder(
    float radius = 1, float height = 1, uint u_resolution = 100, uint v_resolution = 100);

std::pair<std::vector<Eigen::Vector3f>, std::vector<uint>> GenerateCone(
    float radius = 1, float height = 1, uint u_resolution = 100, uint v_resolution = 100);

std::pair<std::vector<Eigen::Vector3f>, std::vector<uint>> GenerateClosedCone(
    float radius = 1, float height = 1, uint u_resolution = 100, uint v_resolution = 100);

std::pair<std::vector<Eigen::Vector3f>, std::vector<uint>> Generate3DArrow(
    float radius = 1, float height = 1, uint u_resolution = 100, uint v_resolution = 100);

// Simple Curves -------------------------------------------------------------------------
Eigen::RowMatrix3f Generate3DHemiCircle(
    float radius,
    const Eigen::Vector3f& axis,
    const Eigen::Vector3f& start,
    uint divisions = 100);

// Utilities -----------------------------------------------------------------------------
std::vector<Eigen::Vector3f> GenerateSharpNormals(
    const std::vector<Eigen::Vector3f>& vertices, const std::vector<uint>& indices);

std::vector<Eigen::Vector3f> GenerateSmoothNormals(
    const std::vector<Eigen::Vector3f>& vertices, const std::vector<uint>& indices);

template<typename VEC> std::vector<VEC> CatmullClarkSubdivideCurve(std::vector<VEC> curve)
{
    std::vector<VEC> result;
    uint n = curve.size();
    for(uint i = 0; i < n; i++)
    {
        result.push_back(curve[i]);
        result.push_back((curve[i] + curve[(i + 1) % n]) / 2.f);
    }

    curve = result;
    result.clear();
    n = curve.size();
    for(uint i = 0; i < n; i++)
    {
        result.push_back((curve[i] + curve[(i + 1) % n]) / 2.f);
    }

    return result;
}

