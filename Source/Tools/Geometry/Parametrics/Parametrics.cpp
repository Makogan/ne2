#include "Parametrics.hpp"

/** @cond */
#include <algorithm>
#include <array>
#include <assert.h>
#include <cmath>
#include <map>
#include <set>

#include "Eigen/Geometry"
/** @endcond */

#include "Core/Log.hpp"

using namespace std;
using namespace Eigen;

// Internal Functions --------------------------------------------------------------------
namespace
{
vector<float> CalculateKnots(uint point_num, uint order);
}
// Cylinder ---------------------------------------------------------------
BSpline::BSpline(const vector<Eigen::Vector3f>& control_points, uint order)
    : control_points(control_points)
    , order(order)
{
    assert(control_points.size() >= order);
    knots = CalculateKnots(control_points.size(), order);
}

BSpline::BSpline(const vector<Eigen::Vector3f>& control_points)
    : control_points(control_points)
{
    assert(control_points.size() >= order);
    knots = CalculateKnots(control_points.size(), order);
}

void BSpline::AddControlPoint(Eigen::Vector3f point)
{
    control_points.push_back(point);
    knots = CalculateKnots(control_points.size(), order);
}

uint BSpline::GetKnotIndex(float t)
{
    t = std::clamp(t, knots[0], knots.back() - 0.0001f);
    uint k = 0;
    while(!(t >= knots[k] && t < knots[k + 1]))
    {
        k += 1;
    }
    return k;
}

void BSpline::SetOrder(uint new_order)
{
    order = new_order;
    knots = CalculateKnots(control_points.size(), order);
}

vector<vector<float>> BSpline::VisualizeIntervals(BSpline& spline, float t)
{
    uint k = spline.GetKnotIndex(t);

    uint p = spline.order - 1;

    vector<vector<float>> knot_pairs;
    auto& knots = spline.knots;
    for(uint r = 1; r < p + 1; r++)
    {
        knot_pairs.push_back(vector<float>());
        for(uint j = p; j > r - 1; j--)
        {
            knot_pairs[r - 1].push_back(knots[j + k - p]);
            knot_pairs[r - 1].push_back(knots[j + 1 + k - r]);
            knot_pairs[r - 1].push_back(t);
        }
    }

    return knot_pairs;
}

BSpline::DeboorData BSpline::VisualizeDeBoors(BSpline& spline, float t)
{
    DeboorData data;
    uint k = spline.GetKnotIndex(t);

    uint p = spline.order - 1;
    vector<Eigen::Vector3f> points(spline.order);
    for(uint i = 0; i < p + 1; i++)
    {
        points[i] = spline.control_points[i + k - p];
    }

    for(uint r = 1; r < p + 1; r++)
    {
        data.intermediate_lines.push_back(vector<Eigen::Vector3f>());
        data.intermediate_points.push_back(vector<Eigen::Vector3f>());
        for(uint j = p; j > r - 1; j--)
        {
            float alpha = (t - spline.knots[j + k - p]) /
                (spline.knots[j + 1 + k - r] - spline.knots[j + k - p]);

            data.intermediate_lines[r - 1].push_back(points[j]);
            data.intermediate_lines[r - 1].push_back(points[j - 1]);
            points[j] = (1.f - alpha) * points[j - 1] + alpha * points[j];
            data.intermediate_points[r - 1].push_back(points[j]);
        }
    }

    return data;
}

Eigen::Vector3f BSpline::operator()(float t)
{
    uint k = GetKnotIndex(t);

    uint p = order - 1;
    vector<Eigen::Vector3f> points(order);
    for(uint i = 0; i < p + 1; i++)
    {
        points[i] = control_points[i + k - p];
    }

    for(uint r = 1; r <= p; r++)
    {
        for(uint j = p; j >= r; j--)
        {
            float alpha =
                (t - knots[j + k - p]) / (knots[j + 1 + k - r] - knots[j + k - p]);
            points[j] = (1.f - alpha) * points[j - 1] + alpha * points[j];
        }
    }

    return points[p];
}

vector<Eigen::Vector3f> BSpline::GetLineVertices()
{
    if(control_points.size() < order) return {Eigen::Vector3f(0)};
    vector<Eigen::Vector3f> ret_vec;
    for(float t = 0; t <= 1 + 0.0001; t += 0.01)
    {
        ret_vec.push_back((*this)(t));
    }

    return ret_vec;
}

// Shapes --------------------------------------------------------------------------------
pair<vector<Eigen::Vector3f>, vector<uint>> GenerateSphere(
    float radius, uint u_resolution, uint v_resolution)
{
    vector<Eigen::Vector3f> points;
    vector<uint> indices;
    for(uint i = 0; i < u_resolution; i++)
    {
        float u = ((float(i) + 0.5) / float(u_resolution)) * M_PI - M_PI / 2.f;
        assert(!isnan(u));
        for(uint j = 0; j < v_resolution; j++)
        {
            float v = float(j) * 2.f * M_PI / float(v_resolution);
            points.push_back(
                Eigen::Vector3f(cos(u) * cos(v), cos(u) * sin(v), sin(u)) * radius);

            assert(
                !isnan(points.back().x()) && !isnan(points.back().y()) &&
                !isnan(points.back().z()));
        }
    }
    // Generate the caps to prevent degenerate geometry
    points.push_back(Eigen::Vector3f(0, 0, -radius));
    for(uint j = 0; j < v_resolution; j++)
    {
        indices.push_back(j);
        indices.push_back(points.size() - 1);
        indices.push_back((j + 1) % v_resolution);
    }

    points.push_back(Eigen::Vector3f(0, 0, radius));
    for(uint j = 0; j < v_resolution; j++)
    {
        indices.push_back(points.size() - 1);
        indices.push_back(v_resolution * (u_resolution - 1) + j);
        indices.push_back(v_resolution * (u_resolution - 1) + (j + 1) % v_resolution);
    }

    for(uint i = 0; i < u_resolution - 1; i++)
    {
        for(uint j = 0; j < v_resolution; j++)
        {
            // Grab the 4 points in a quad face in the sphere
            uint p00 = (i * (v_resolution) + j);
            uint p01 = (i * (v_resolution) + ((j + 1) % v_resolution));
            uint p10 = ((i + 1) * v_resolution + j);
            uint p11 = ((i + 1) * v_resolution + ((j + 1) % v_resolution));

            // Add the first triangle
            indices.push_back(p00);
            indices.push_back(p01);
            indices.push_back(p11);

            // Add the second triangle
            indices.push_back(p00);
            indices.push_back(p11);
            indices.push_back(p10);
        }
    }

    return {points, indices};
}

pair<vector<Eigen::Vector3f>, vector<uint>> GenerateEllipsoid(
    float a, float b, float c, uint u_resolution, uint v_resolution)
{
    vector<Eigen::Vector3f> points;
    vector<uint> indices;
    for(uint i = 0; i < u_resolution; i++)
    {
        float u = ((float(i) + 0.5) / float(u_resolution)) * M_PI - M_PI / 2.f;

        Assert(!isnan(u), "");
        for(uint j = 0; j < v_resolution; j++)
        {
            float v = float(j) * 2.f * M_PI / float(v_resolution);

            points.push_back(
                Eigen::Vector3f(a * cos(u) * cos(v), b * cos(u) * sin(v), c * sin(u)));

            Assert(
                !isnan(points.back().x()) && !isnan(points.back().y()) &&
                    !isnan(points.back().z()),
                "");
        }
    }
    // Generate the caps to prevent degenerate geometry
    points.push_back(Eigen::Vector3f(0, 0, -c));
    for(uint j = 0; j < v_resolution; j++)
    {
        indices.push_back(j);
        indices.push_back(points.size() - 1);
        indices.push_back((j + 1) % v_resolution);
    }

    points.push_back(Eigen::Vector3f(0, 0, c));
    for(uint j = 0; j < v_resolution; j++)
    {
        indices.push_back(points.size() - 1);
        indices.push_back(v_resolution * (u_resolution - 1) + j);
        indices.push_back(v_resolution * (u_resolution - 1) + (j + 1) % v_resolution);
    }

    for(uint i = 0; i < u_resolution - 1; i++)
    {
        for(uint j = 0; j < v_resolution; j++)
        {
            // Grab the 4 points in a quad face in the sphere
            uint p00 = (i * (v_resolution) + (j % v_resolution));
            uint p01 = (i * (v_resolution) + ((j + 1) % v_resolution));
            uint p10 = ((i + 1) * v_resolution + (j % v_resolution));
            uint p11 = ((i + 1) * v_resolution + ((j + 1) % v_resolution));

            Assert((i + 1) < u_resolution, "");
            Assert(p00 < points.size(), "");
            Assert(p01 < points.size(), "");
            Assert(p10 < points.size(), "");
            Assert(p11 < points.size(), "");
            Assert(
                p00 != p01 && p00 != p11 && p00 != p10 && p01 != p10 && p01 != p11 &&
                    p10 != p11,
                "");

            // Add the first triangle
            indices.push_back(p00);
            indices.push_back(p01);
            indices.push_back(p11);

            // Add the second triangle
            indices.push_back(p00);
            indices.push_back(p11);
            indices.push_back(p10);
        }
    }

    return {points, indices};
}

vector<Eigen::Vector2f> GenerateEllipsoidUvs(
    float a, float b, float c, uint u_resolution, uint v_resolution)
{
    vector<Eigen::Vector2f> uvs;
    for(uint i = 0; i < u_resolution; i++)
    {
        for(uint j = 0; j < v_resolution; j++)
        {
            uvs.push_back(Eigen::Vector2f(
                float(j) / float(v_resolution), (float(i) + 0.5) / float(u_resolution)));
        }
    }

    uvs.push_back({1, 0});
    uvs.push_back({0, 1});
    return uvs;
}

pair<vector<Eigen::Vector3f>, vector<uint>> GenerateCylinder(
    float radius, float height, uint u_resolution, uint v_resolution, bool y_is_up)
{
    vector<Eigen::Vector3f> points;
    vector<uint> indices;
    for(uint i = 0; i < u_resolution; i++)
    {
        float u = float(i) / float(u_resolution - 1);
        for(uint j = 0; j < v_resolution; j++)
        {
            float v = float(j) * 2.f * M_PI / float(v_resolution);

            const float sin_component = sin(v) * radius;
            const float height_component = u * height;
            points.push_back(
                {cos(v) * radius,
                 height_component * y_is_up + sin_component * !y_is_up,
                 height_component * !y_is_up + sin_component * y_is_up});
        }
    }

    for(uint i = 0; i < u_resolution - 1; i++)
    {
        for(uint j = 0; j < v_resolution; j++)
        {
            // Grab the 4 points in a quad face in the cylinder
            uint p00 = (i * v_resolution + j);
            uint p01 = (i * v_resolution + ((j + 1) % v_resolution));
            uint p10 = ((i + 1) * v_resolution + j);
            uint p11 = ((i + 1) * v_resolution + ((j + 1) % v_resolution));

            // Add the first triangle
            indices.push_back(p00);
            indices.push_back(p01);
            indices.push_back(p11);

            // Add the second triangle
            indices.push_back(p00);
            indices.push_back(p11);
            indices.push_back(p10);
        }
    }

    return {points, indices};
}

pair<vector<Eigen::Vector3f>, vector<uint>> GenerateClosedCylinder(
    float radius, float height, uint u_resolution, uint v_resolution)
{
    vector<Eigen::Vector3f> points;
    vector<uint> indices;
    for(uint i = 0; i < u_resolution; i++)
    {
        // Map the u parameter to the range [-1, 2]
        float u = float(i) / float(u_resolution - 1) * 3.f - 1.f;
        for(uint j = 0; j < v_resolution; j++)
        {
            float interval_distance = std::min(abs(u), abs(u - 1));
            // radius on [0,1]; (1 - interval_distance) everywhere else
            float current_radius =
                u >= 0 && u <= 1 ? radius : (1.f - interval_distance) * radius;
            float current_height = std::clamp(u, 0.f, 1.f) * height;
            float v = float(j) * 2.f * M_PI / float(v_resolution);
            points.push_back(
                {cos(v) * current_radius, sin(v) * current_radius, current_height});
        }
    }

    for(uint i = 0; i < u_resolution - 1; i++)
    {
        for(uint j = 0; j < v_resolution; j++)
        {
            // Grab the 4 points in a quad face in the cylinder
            uint p00 = (i * v_resolution + j);
            uint p01 = (i * v_resolution + ((j + 1) % v_resolution));
            uint p10 = ((i + 1) * v_resolution + j);
            uint p11 = ((i + 1) * v_resolution + ((j + 1) % v_resolution));

            // Add the first triangle
            indices.push_back(p00);
            indices.push_back(p01);
            indices.push_back(p11);

            // Add the second triangle
            indices.push_back(p00);
            indices.push_back(p11);
            indices.push_back(p10);
        }
    }

    return {points, indices};
}

pair<vector<Eigen::Vector3f>, vector<uint>> GenerateCone(
    float radius, float height, uint u_resolution, uint v_resolution)
{
    vector<Eigen::Vector3f> points;
    vector<uint> indices;
    for(uint i = 0; i < u_resolution; i++)
    {
        float u = float(i) / float(u_resolution - 1);
        for(uint j = 0; j < v_resolution; j++)
        {
            float v = float(j) * 2.f * M_PI / float(v_resolution - 1);

            points.push_back(
                {cos(v) * radius * (1.f - u), sin(v) * radius * (1.f - u), u * height});
        }
    }

    for(uint i = 0; i < u_resolution - 1; i++)
    {
        for(uint j = 0; j < v_resolution; j++)
        {
            // Grab the 4 points in a quad face in the cylinder
            uint p00 = (i * v_resolution + j);
            uint p01 = (i * v_resolution + ((j + 1) % v_resolution));
            uint p10 = ((i + 1) * v_resolution + j);
            uint p11 = ((i + 1) * v_resolution + ((j + 1) % v_resolution));

            // Add the first triangle
            indices.push_back(p00);
            indices.push_back(p01);
            indices.push_back(p11);

            // Add the second triangle
            indices.push_back(p00);
            indices.push_back(p11);
            indices.push_back(p10);
        }
    }

    return {points, indices};
}

pair<vector<Eigen::Vector3f>, vector<uint>> GenerateClosedCone(
    float radius, float height, uint u_resolution, uint v_resolution)
{
    vector<Eigen::Vector3f> points;
    vector<uint> indices;
    for(uint i = 0; i < u_resolution; i++)
    {
        // Map the parameter to the range [-1, 1]
        // The offset is to leave a hole in the cap.
        float u = ((float(i) + 0.5) / float(u_resolution + 1)) * 2.f - 1.f;
        for(uint j = 0; j < v_resolution; j++)
        {
            float v = float(j) * 2.f * M_PI / float(v_resolution);
            // From -1 to 0 create the base, after, ceate the cone
            float current_radius = u < 0 ? radius * (1.f + u) : radius * (1.f - u);
            float current_height = u < 0 ? 0 : u * height;

            points.push_back(
                {cos(v) * current_radius, sin(v) * current_radius, current_height});
        }
    }

    // Generate the caps to prevent degenerate geometry
    points.push_back(Eigen::Vector3f(0, 0, 0));
    for(uint j = 0; j < v_resolution; j++)
    {
        indices.push_back(j);
        indices.push_back(points.size() - 1);
        indices.push_back((j + 1) % v_resolution);
    }

    points.push_back(Eigen::Vector3f(0, 0, height));
    for(uint j = 0; j < v_resolution; j++)
    {
        indices.push_back(points.size() - 1);
        indices.push_back(v_resolution * (u_resolution - 1) + j);
        indices.push_back(v_resolution * (u_resolution - 1) + (j + 1) % v_resolution);
    }

    for(uint i = 0; i < u_resolution - 1; i++)
    {
        for(uint j = 0; j < v_resolution; j++)
        {
            // Grab the 4 points in a quad face in the cylinder
            uint p00 = (i * v_resolution + j);
            uint p01 = (i * v_resolution + ((j + 1) % v_resolution));
            uint p10 = ((i + 1) * v_resolution + j);
            uint p11 = ((i + 1) * v_resolution + ((j + 1) % v_resolution));

            // Add the first triangle
            indices.push_back(p00);
            indices.push_back(p01);
            indices.push_back(p11);

            // Add the second triangle
            indices.push_back(p00);
            indices.push_back(p11);
            indices.push_back(p10);
        }
    }

    return {points, indices};
}

std::pair<std::vector<Eigen::Vector3f>, std::vector<uint>> Generate3DArrow(
    float radius, float height, uint u_resolution, uint v_resolution)
{
    // Arrow top is 1/4th of the total height
    height /= 4.f;
    auto [cylinder_vs, cylinder_idcs] =
        GenerateClosedCylinder(radius * 0.7, height * 3, u_resolution, v_resolution);
    auto [cone_vs, cone_idcs] =
        GenerateClosedCone(radius, height, u_resolution - 1, v_resolution);

    uint index_offset = cylinder_vs.size();
    cylinder_idcs.reserve(index_offset + cone_idcs.size());
    cylinder_vs.reserve(cylinder_vs.size() + cone_vs.size());

    for(auto& i: cone_idcs)
        cylinder_idcs.push_back(i + index_offset);
    for(auto& v: cone_vs)
        cylinder_vs.push_back(v + Eigen::Vector3f(0, 0, height * 3));

    return {cylinder_vs, cylinder_idcs};
}

// Simple Curves -------------------------------------------------------------------------
Eigen::RowMatrix3f Generate3DHemiCircle(
    float radius,
    const Eigen::Vector3f& axis,
    const Eigen::Vector3f& start,
    uint divisions)
{
    RowMatrix3f points(divisions, 3);
    for(uint i = 0; i < divisions; i++)
    {
        float u = float(i) * M_PI / float(divisions - 1);
        float angle_sin = sin(u / 2.f);
        Eigen::Quaternionf rotation = {
            (float)cos(u / 2.f),
            axis.x() * angle_sin,
            axis.y() * angle_sin,
            axis.z() * angle_sin};

        points.row(i) = rotation * start;
    }

    return points;
}

// Utilities -----------------------------------------------------------------------------
std::vector<Eigen::Vector3f> GenerateSharpNormals(
    const std::vector<Eigen::Vector3f>& vertices, const std::vector<uint>& indices)
{
    assert(indices.size() % 3 == 0);

    std::vector<Eigen::Vector3f> normals(vertices.size(), Eigen::Vector3f(0, 0, 0));
    for(uint i = 0; i < indices.size(); i += 3)
    {
        Eigen::Vector3f v1 = vertices[indices[i + 0]];
        Eigen::Vector3f v2 = vertices[indices[i + 1]];
        Eigen::Vector3f v3 = vertices[indices[i + 2]];

        Eigen::Vector3f d1 = v2 - v1;
        Eigen::Vector3f d2 = v3 - v1;

        assert(indices[i + 0] < vertices.size());
        assert(indices[i + 1] < vertices.size());
        assert(indices[i + 2] < vertices.size());
        assert(
            indices[i] != indices[i + 1] && indices[i] != indices[i + 2] &&
            indices[i + 1] != indices[i + 2]);

        Eigen::Vector3f normal = d2.cross(d1).normalized();

        normals[indices[i + 0]] += normal;
        normals[indices[i + 1]] += normal;
        normals[indices[i + 2]] += normal;
    }

    for(auto& normal: normals)
        normal = normal.normalized();

    assert(normals.size() == vertices.size());
    return normals;
}

std::vector<Eigen::Vector3f> GenerateSmoothNormals(
    const std::vector<Eigen::Vector3f>& vertices, const std::vector<uint>& indices)
{
    auto sharp_normals = GenerateSharpNormals(vertices, indices);
    std::vector<Eigen::Vector3f> smooth_normals(vertices.size(), {0, 0, 0});

    for(uint i: indices)
        smooth_normals[i] += sharp_normals[i];

    for(auto& normal: smooth_normals)
        normal.normalize();

    return smooth_normals;
}

// Internal functions --------------------------------------------------------------------
namespace
{
vector<float> CalculateKnots(uint point_num, uint order)
{
    uint m = point_num - 1;
    vector<float> knots(point_num + order);
    float u = 0;
    for(uint i = 0; i < order; i++)
    {
        knots[i] = u;
    }
    for(uint i = 0; i < m - order + 1; i++)
    {
        u += 1.f / (m - order + 2);
        knots[order + i] = u;
    }
    for(uint i = 0; i < order; i++)
    {
        knots[point_num + i] = 1;
    }

    return knots;
}

}  // namespace

