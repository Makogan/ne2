#include "HeatGeodesics.hpp"

/** @cond */
#include "Eigen/Sparse"
/** @endcond */

#include "MeshGradient.hpp"

using namespace std;

Eigen::VectorXf HeatGeodesics(
    const HMesh<VertexData>& mesh, const Eigen::VectorXf& sources)
{
    Eigen::DiagonalMatrix<float, Eigen::Dynamic> areas(sources.size());
    Eigen::SparseMatrix<float> laplace_operator(sources.size(), sources.size());

    std::vector<Eigen::Triplet<float> > triplets;

    areas.setZero();
    for(uint e = 0; e < mesh.Edges().size(); e += 2)
    {
        const auto& edge = mesh.Edges()[e];
        const int i = edge.Vert().ID();
        const int j = edge.Pair().Vert().ID();

        const Eigen::Vector3f e1 = -edge.Next().Dir<VertexData>().normalized();
        const Eigen::Vector3f e2 = edge.Prev().Dir<VertexData>().normalized();

        const float alpha = acos(e1.dot(e2));

        const Eigen::Vector3f p1 = -edge.Pair().Next().Dir<VertexData>().normalized();
        const Eigen::Vector3f p2 = edge.Pair().Prev().Dir<VertexData>().normalized();

        const float beta = acos(p1.dot(p2));

        const float laplace_coeff = 0.5f * ((1.f / tan(alpha)) + (1.f / tan(beta)));

        triplets.push_back({i, j, -laplace_coeff});
        triplets.push_back({j, i, -laplace_coeff});

        triplets.push_back({i, i, laplace_coeff});
        triplets.push_back({j, j, laplace_coeff});
    }

    laplace_operator.setFromTriplets(triplets.begin(), triplets.end());

    uint count = 0;
    for(auto& v: mesh.Verts())
    {
        std::vector<HMesh<VertexData>::MFace*> faces = v.ContainingFaces();
        for(auto f: faces)
        {
            areas.diagonal()[count] += f->Area<VertexData>();
        }

        areas.diagonal()[count] /= 3.0;

        count++;
    }

    Eigen::SparseLU<Eigen::SparseMatrix<float>, Eigen::COLAMDOrdering<int>> solver;

    const Eigen::SparseMatrix<float> a =
        Eigen::SparseMatrix<float>(areas) + 0.01 * laplace_operator;
    solver.analyzePattern(a);
    solver.factorize(a);

    Eigen::VectorXf heat = solver.solve(sources);
    Eigen::Matrix<float, Eigen::Dynamic, 3> gradient =
        CalculateSimplifiedVertexGradient<float>(mesh, heat);
    Assert(gradient.rows() == sources.size(), "");

    Eigen::VectorXf integrated_gradient(gradient.rows());
    integrated_gradient.setZero();

    for(const auto& face: mesh.Faces())
    {
        using Edge = HMesh<VertexData>::MEdge;
        const Edge& e1 = face.Edge();
        const Edge& e2 = e1.Next();
        const Edge& e3 = e2.Next();

        const Eigen::Vector3f d1 = e1.Dir<VertexData>();
        const Eigen::Vector3f d2 = e2.Dir<VertexData>();
        const Eigen::Vector3f d3 = e3.Dir<VertexData>();

        const Eigen::Vector3f nd1 = d1.normalized();
        const Eigen::Vector3f nd2 = d2.normalized();
        const Eigen::Vector3f nd3 = d3.normalized();

        const float cot_theta1 = 1.f / tan(acos(nd1.dot(-nd3)));
        const float cot_theta2 = 1.f / tan(acos(nd2.dot(-nd1)));
        const float cot_theta3 = 1.f / tan(acos(nd3.dot(-nd2)));

        const uint v1 = e1.Vert().ID();
        const uint v2 = e2.Vert().ID();
        const uint v3 = e3.Vert().ID();

        Assert(v1 < gradient.rows(), "");
        Assert(v2 < gradient.rows(), "");
        Assert(v3 < gradient.rows(), "");

        // TODO (medium): Why does this only work when the gradient is the same?
        // The gradient was tested to be correct.
        const Eigen::Vector3f g1 = -gradient.row(v1).normalized();
        const Eigen::Vector3f g2 = -gradient.row(v1).normalized();
        const Eigen::Vector3f g3 = -gradient.row(v1).normalized();

        integrated_gradient[v1] += cot_theta3 * d1.dot(g1) + cot_theta2 * (-d3.dot(g1));
        integrated_gradient[v2] += cot_theta1 * d2.dot(g2) + cot_theta3 * (-d1.dot(g2));
        integrated_gradient[v3] += cot_theta2 * d3.dot(g3) + cot_theta1 * (-d2.dot(g3));
    }

    solver.analyzePattern(-laplace_operator);
    solver.factorize(-laplace_operator);

    Eigen::VectorXf distances = solver.solve(integrated_gradient);

    const float minimum = *std::min_element(distances.begin(), distances.end());

    return distances.array() - minimum;
}

