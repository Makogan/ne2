#pragma once

/** @cond */
#include "Eigen/Dense"
/** @endcond */

#include "Geometry/HalfEdge/HMesh.hpp"

Eigen::VectorXf HeatGeodesics(
    const HMesh<VertexData>& mesh, const Eigen::VectorXf& sources);

