#include "HMesh.hpp"

/** @cond */
#include <filesystem>
#include <sys/stat.h>
#include <stdio.h>
#include <fstream>

#include "cereal/archives/binary.hpp"
#include "cereal/types/memory.hpp"
#include "cereal/types/vector.hpp"
/** @endcond */

#include "FileSystem/CacheUtils.hpp"
#include "Core/TextManipulation.hpp"

using namespace std;
namespace fs = std::filesystem;

namespace cereal
{
template <class Archive, class _Scalar, int _Rows, int _Cols, int _Options, int _MaxRows, int _MaxCols> inline
typename std::enable_if<traits::is_output_serializable<BinaryData<_Scalar>, Archive>::value, void>::type
save(Archive & ar, Eigen::Matrix<_Scalar, _Rows, _Cols, _Options, _MaxRows, _MaxCols> const & m)
{
    int32_t rows = m.rows();
    int32_t cols = m.cols();
    ar(rows);
    ar(cols);
    ar(binary_data(m.data(), rows * cols * sizeof(_Scalar)));
}

template <class Archive, class _Scalar, int _Rows, int _Cols, int _Options, int _MaxRows, int _MaxCols> inline
typename std::enable_if<traits::is_input_serializable<BinaryData<_Scalar>, Archive>::value, void>::type
load(Archive & ar, Eigen::Matrix<_Scalar, _Rows, _Cols, _Options, _MaxRows, _MaxCols> & m)
{
    int32_t rows;
    int32_t cols;
    ar(rows);
    ar(cols);

    m.resize(rows, cols);

    ar(binary_data(m.data(), static_cast<std::size_t>(rows * cols * sizeof(_Scalar))));
}
} // cereal

namespace
{
struct ObjIntermediaryData
{
    std::vector<Eigen::Vector3f> vertices;
    std::vector<Eigen::Vector2f> uvs;
    std::vector<Eigen::Vector3f> normals;
    std::vector<std::vector<uint>> vertex_indices;
    std::vector<std::vector<uint>> uv_indices;
    std::vector<std::vector<uint>> normal_indices;
};

template <class Archive>
void serialize( Archive& archive, ObjIntermediaryData& data )
{
    archive(
        CEREAL_NVP(data.vertices),
        CEREAL_NVP(data.uvs),
        CEREAL_NVP(data.normals),
        CEREAL_NVP(data.vertex_indices),
        CEREAL_NVP(data.uv_indices),
        CEREAL_NVP(data.normal_indices)
    );
}
} // namespace

long GetFileSize(std::string filename)
{
    struct stat stat_buf;
    int rc = stat(filename.c_str(), &stat_buf);
    return rc == 0 ? stat_buf.st_size : -1;
}

bool ParseFaceString(
    const char* line,
    int& out_vert,
    int& out_uv,
    int& out_normal,
    int& out_char_read)
{
    uint filled_count;

    filled_count = sscanf(
        line,
        "%i/%i/%i%n",
        &out_vert, &out_uv, &out_normal,
        &out_char_read);

    if(filled_count == 3) return true;

    filled_count = sscanf(
        line,
        "%i//%i%n",
        &out_vert, &out_normal,
        &out_char_read);
        out_uv = -1;

    if(filled_count == 2) return true;

    filled_count = sscanf(
        line,
        "%i/%i%n",
        &out_vert, &out_uv,
        &out_char_read);

    out_normal = -1;

    if(filled_count == 2) return true;

    filled_count = sscanf(
        line,
        "%i%n",
        &out_vert,
        &out_char_read);

    out_uv = -1;
    out_normal = -1;

    Assert(filled_count == 1, "");
    return filled_count == 1;
}

std::tuple<
    vector<Eigen::Vector3f>,  // Vertices
    vector<Eigen::Vector2f>,  // Texture coordinates
    vector<Eigen::Vector3f>,  // Normals
    vector<vector<uint>>,     // Vertex indices
    vector<vector<uint>>,     // UV indices
    vector<vector<uint>>>     // Normal indices
ParseObj(const string& path)
{
    ObjIntermediaryData intermediary_data = {};

    std::vector<Eigen::Vector3f>& vertices = intermediary_data.vertices;
    std::vector<Eigen::Vector2f>& uvs = intermediary_data.uvs;
    std::vector<Eigen::Vector3f>& normals = intermediary_data.normals;
    std::vector<std::vector<uint>>& vertex_indices = intermediary_data.vertex_indices;
    std::vector<std::vector<uint>>& uv_indices = intermediary_data.uv_indices;
    std::vector<std::vector<uint>>& normal_indices = intermediary_data.normal_indices;

    std::string name = path;
    // std::replace(name.begin(), name.end(), '/', '_');
    // name = Core::SubstrUntilChar(name, '.');

    // const fs::path binary_path{"MeshCache/" + name + ".cereal"};
    // if(CacheIsValid(binary_path, {path}))
    // {
    //     std::ifstream is(binary_path, std::ios::binary);
    //     cereal::BinaryInputArchive archive_in( is );
    //     serialize(archive_in, intermediary_data);
    //     return {vertices, uvs, normals, vertex_indices, uv_indices, normal_indices};
    // }

    const uint file_size = GetFileSize(path);
    vertices.reserve(file_size);
    uv_indices.reserve(file_size);
    normal_indices.reserve(file_size);
    vertex_indices.reserve(file_size);
    uv_indices.reserve(file_size);
    normal_indices.reserve(file_size);

    std::ifstream file_stream(path);

    // First parse the file to get all the vertex, coords and normals
    std::string line;
    while(getline(file_stream, line))
    {
        if(line == "") continue;
        uint offset = 0;
        for(; offset < line.size(); offset++)
        {
            if(line[offset] != ' ' && line[offset] != '\n')
                break;
        }

        Assert(offset + 2 < line.size(), "Line: {0}", line);
        if(line[offset] == 'v' && line[offset + 1] == ' ')
        {
            float x, y, z;
            int count = sscanf(line.c_str() + offset + 2, "%f %f %f", &x, &y, &z);

            assert(count == 3);
            vertices.push_back(Eigen::Vector3f(x, y, z));
        }
        else if(line[offset] == 'v' && line[offset + 1] == 't')
        {
            float u,v;
            sscanf(line.c_str() + offset + 2, "%f %f", &u, &v);

            uvs.push_back(Eigen::Vector2f(u, v));
        }
        else if(line[offset] == 'v' && line[offset + 1] == 'n')
        {
            float x, y, z;
            sscanf(line.c_str() + offset + 3, "%f %f %f", &x, &y, &z);

            normals.push_back(Eigen::Vector3f(x, y, z));
        }
        else if(line[offset] == 'f' && line[offset + 1] == ' ')
        {
            int vertex_index, uv_index, normal_index;

            vertex_indices.push_back({});
            uv_indices.push_back({});
            normal_indices.push_back({});

            offset++;
            while(offset < line.size()-1)
            {
                int read_count;

                ParseFaceString(
                    line.c_str() + offset,
                    vertex_index,
                    uv_index,
                    normal_index,
                    read_count);

                offset += read_count;

                vertex_indices.back().push_back(vertex_index - 1);
                if(uv_index > -1)
                    uv_indices.back().push_back(uv_index - 1);
                else
                    uv_indices.clear();
                if(normal_index > -1)
                    normal_indices.back().push_back(normal_index - 1);
                else
                    normal_indices.clear();
            }
            Assert(vertex_indices.back().size() == 3, "");
        }
    }

    // std::filesystem::create_directories(binary_path.parent_path());
    // std::ofstream os(binary_path, std::ios::binary);
    // cereal::BinaryOutputArchive archive_out( os );
    // serialize(archive_out, intermediary_data);

    return {vertices, uvs, normals, vertex_indices, uv_indices, normal_indices};
}

namespace _details {
void InitTopology(
    const uint vert_count,
    const std::vector<std::vector<uint>>& vertex_indices,
    std::vector<HVert>& out_verts,
    std::vector<HEdge>& out_edges,
    std::vector<HFace>& out_faces)
{
    Assert(vertex_indices.size() > 0, "");
    Assert(vertex_indices[0].size() > 0, "");

    out_verts.reserve(vert_count);
    out_edges.reserve(vertex_indices.size());
    out_faces.reserve(vertex_indices.size());

    std::map<std::pair<uint, uint>, uint> half_edge_map;
    // Iterate over the face connectivity information and create half edges.
    for(uint face_id = 0; face_id < vertex_indices.size(); face_id ++)
    {
        const uint face_valence = vertex_indices[face_id].size();
        assert(face_valence == 3);
        // Initialize the half edge pairs.
        for(uint i=0; i < face_valence; i++)
        {
            // Grab the indices of the 2 vertices in the half edge (ordered).
            const uint v_id_0 = vertex_indices[face_id][i];
            const uint v_id_1 = vertex_indices[face_id][(i + 1) % face_valence];
            // If the half edge is in the map then it was initialized.
            if(half_edge_map.count({v_id_0, v_id_1}) != 0) continue;

            // Create 2 half edges right beside each other in memory. They are the pair.
            half_edge_map.insert({{v_id_0, v_id_1}, out_edges.size() + 0});
            half_edge_map.insert({{v_id_1, v_id_0}, out_edges.size() + 1});
            out_edges.push_back({&out_verts, &out_edges, &out_faces});
            out_edges.push_back({&out_verts, &out_edges, &out_faces});

            // Set the pair information.
            const uint e0_id = out_edges.size() - 2;
            const uint e1_id = out_edges.size() - 1;
            auto& e0 = out_edges[e0_id];
            auto& e1 = out_edges[e1_id];
            e0.Pair(e1_id);
            e1.Pair(e0_id);

            // Set the vertex indices.
            e0.Vert(v_id_0);
            e1.Vert(v_id_1);
            out_verts[v_id_0].Edge(e0_id);
            out_verts[v_id_1].Edge(e1_id);
        }
    }

    // With the half edges created we can now initialize the connectivity.
    for(uint face_id = 0; face_id < vertex_indices.size(); face_id ++)
    {
        out_faces.push_back({&out_verts, &out_edges, &out_faces});
        auto& face = out_faces.back();

        std::vector<HEdge*> face_edges;
        face_edges.reserve(3);
        const uint face_valence = vertex_indices[face_id].size();
        // Initialize the half edge pairs.
        for(uint i=0; i < face_valence; i++)
        {
            // Initialize the half edge next and prev connectivity.
            const uint v_id_0 = vertex_indices[face_id][i];
            const uint v_id_1 = vertex_indices[face_id][(i + 1) % face_valence];

            const uint e_0_id = half_edge_map.at({v_id_0, v_id_1});

            face_edges.push_back(&(out_edges[e_0_id]));
        }
        ConnectFace(face, face_edges);
    }

    // Find, for each vertex, the outgoing half edge that is on the boundary.
    // TODO(medium): to save memory just mark the vertex edge member.
    std::vector<uint> boundaries(vert_count, ABSENT);
    for(uint i=0; i < out_edges.size(); i++)
    {
        const auto& edge = out_edges[i];
        if(edge.IsBoundary())
            boundaries[edge.Vert().ID()] = edge.ID();
    }

    // Now we can find the outgoing boundary edge at any point and attach both half edges.
    for(uint i=0; i < out_edges.size(); i++)
    {
        auto& edge = out_edges[i];
        if(edge.IsBoundary())
        {
            const uint next_v_id = edge.Pair().Vert().ID();
            const uint next_half_edge_id = boundaries[next_v_id];
            edge.Next(next_half_edge_id);
            out_edges[next_half_edge_id].Prev(edge.ID());
        }
    }
}

void FlipEdge(uint edge_id, std::vector<HEdge>& edges)
{
    HEdge& edge = edges[edge_id];
    // Gather the elements we need to modify the mesh.
    HVert& v1 = edge.VertD();
    HVert& v2 = edge.PrevD().VertD();
    HVert& v3 = edge.PairD().PrevD().VertD();
    HVert& v4 = edge.NextD().VertD();

    HEdge& e0f1 = edge;
    HEdge& e1f1 = edge.NextD();
    HEdge& e2f1 = edge.PrevD();
    HEdge& e0f2 = edge.PairD();
    HEdge& e1f2 = edge.PairD().NextD();
    HEdge& e2f2 = edge.PairD().PrevD();

    HFace& f1 = edge.FaceD();
    HFace& f2 = edge.PairD().FaceD();

    v1.Edge(e1f2.ID());
    v4.Edge(e1f1.ID());
    // Re-assign.
    ConnectFace(f1, e0f1, e2f1, e1f2);
    ConnectFace(f2, e0f2, e2f2, e1f1);

    AttachVertices(e0f1, v3, v2);
}

void FlipEdges(std::vector<HEdge>& edges)
{
    uint edge_num = edges.size();
    for(uint i = 0; i < edge_num; i += 2)
    {
        auto [v1, v2] = edges[i].VerticesD();
        // Never flip a boundary edge.
        if(edges[i].IsBoundary() || edges[i].Pair().IsBoundary()) continue;
        if(v1->Color() != v2->Color() && edges[i].Color() == TagColor::BLUE)
        {
            edges[i].Color(TagColor::BLACK);
            edges[i].PairD().Color(TagColor::BLACK);
            FlipEdge(i, edges);
        }
    }
}

void SplitEdge(
    uint edge_id,
    HVert& m,
    std::vector<HVert>& verts,
    std::vector<HEdge>& edges,
    std::vector<HFace>& faces)
{
    // Initialize 6 new half edges and get a reference to each.
    const uint ne = edges.size();
    edges.push_back({&verts, &edges, &faces});
    edges.push_back({&verts, &edges, &faces});
    edges.push_back({&verts, &edges, &faces});
    edges.push_back({&verts, &edges, &faces});
    edges.push_back({&verts, &edges, &faces});
    edges.push_back({&verts, &edges, &faces});

    HEdge& n1f1 = edges[ne + 0];
    HEdge& n2f3 = edges[ne + 1];
    HEdge& n2f2 = edges[ne + 2];
    HEdge& n1f4 = edges[ne + 3];
    HEdge& n0f3 = edges[ne + 4];
    HEdge& n0f4 = edges[ne + 5];

    // Allocate space for 2 new faces
    // Create the new faces.
    const uint nf = faces.size();
    faces.push_back({&verts, &edges, &faces});
    faces.push_back({&verts, &edges, &faces});

    HFace& f3 = faces[nf + 0];
    HFace& f4 = faces[nf + 1];

    // Gather the elements we need to modify the
    HEdge& e0f1 = edges[edge_id];
    HEdge& e1f1 = e0f1.NextD();
    HEdge& e2f1 = e0f1.PrevD();
    HEdge& e0f2 = e0f1.PairD();
    HEdge& e1f2 = e0f1.PairD().NextD();
    HEdge& e2f2 = e0f1.PairD().PrevD();

    HVert& v0 = e0f1.VertD();
    HVert& v1 = e0f1.PairD().VertD();
    HVert& v2 = e0f1.PrevD().VertD();
    HVert& v3 = e0f1.PairD().PrevD().VertD();

    HFace& f1 = e0f1.FaceD();
    HFace& f2 = e0f1.PairD().FaceD();

    ConnectFace(f1, e0f1, n1f1, e2f1);
    ConnectFace(f2, e0f2, e1f2, n2f2);
    ConnectFace(f3, n0f3, e1f1, n2f3);
    ConnectFace(f4, n0f4, n1f4, e2f2);

    Pair(n0f3, n0f4);
    Pair(n2f3, n1f1);
    Pair(n2f2, n1f4);

    AttachVertices(n2f3, v2, m);
    AttachVertices(n2f2, v3, m);
    AttachVertices(n0f4, v1, m);
    AttachVertices(e0f1, v0, m);

    Mark(n1f1, TagColor::BLUE);
    Mark(n2f2, TagColor::BLUE);

    e0f2.Sharpness(e0f1.Sharpness());
    n0f3.Sharpness(e0f1.Sharpness());
    n0f4.Sharpness(e0f1.Sharpness());
}

std::vector<std::vector<uint>> TriangleListToFaceList(const std::vector<uint>& indices)
{
    Assert(indices.size() % 3 == 0, "");
    std::vector<std::vector<uint>> new_indices(
        indices.size() / 3, std::vector<uint>(3));

    for(uint i=0; i < indices.size(); i += 3)
    {
        new_indices[i/3][0] = indices[i + 0];
        new_indices[i/3][1] = indices[i + 1];
        new_indices[i/3][2] = indices[i + 2];
    }

    return new_indices;
}
} // namespace _details