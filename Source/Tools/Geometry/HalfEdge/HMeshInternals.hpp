#pragma once

#include "Geometry/Math/GLA.hpp"
#include "Core/Log.hpp"

struct VertexData
{
    Eigen::Vector3f position;
    Eigen::Vector2f uv;
    Eigen::Vector3f normal;
};

enum class TagColor
{
    BLACK,  // Default
    BLUE,
    RED,
    YELLOW
};

static constexpr uint ABSENT = std::numeric_limits<uint>::max();

struct HEdge;
struct HFace;

/**
 * @brief Class encapsulating a half edge vertex.
 *
 */
struct HVert
{
private:
    uint edge = ABSENT;
    bool is_sharp = false;
    TagColor color = TagColor::BLACK;

public:
    bool is_steiner = false;
    std::vector<HVert>* verts;
    std::vector<HEdge>* edges;
    std::vector<HFace>* faces;
    void* vertex_data;

    HVert() = default;
    HVert(std::vector<HVert>* vs, std::vector<HEdge>* es, std::vector<HFace>* fs, void* vd)
        : verts(vs)
        , edges(es)
        , faces(fs)
        , vertex_data(vd){}

    void Update(std::vector<HVert>* vs, std::vector<HEdge>* es, std::vector<HFace>* fs, void* vd)
    { verts = vs; edges = es; faces = fs; vertex_data = vd; }

    void Edge(uint id) { edge = id; }
    const HEdge& Edge() const { return (*edges)[edge]; }
    HEdge& EdgeD() { return (*edges)[edge]; }

    template<typename V>
    V& DataD()
    {
        const auto vdata = reinterpret_cast<std::vector<V>*>(vertex_data);
        return (*vdata)[ID()];
    }

    template<typename V>
    const V& Data() const
    {
        const auto vdata = reinterpret_cast<std::vector<V>*>(vertex_data);
        return (*vdata)[ID()];
    }

    template<typename V>
    void Data(const V& v)
    {
        const auto vdata = reinterpret_cast<std::vector<V>*>(vertex_data);
        (*vdata)[ID()] = v;
    }

    const TagColor Color() const { return color; }
    void Color(TagColor c) { color = c; }

    std::vector<HEdge*> AdjacentEdges() const;

    std::vector<HVert*> NeighbourVerts() const;

    void IsSharp(bool b) { is_sharp = b; }
    bool IsSharp() const { return is_sharp; }

    std::vector<HFace*> ContainingFaces() const;

    uint ID() const
    {
        const uint id = this - &((*verts)[0]);
        Assert(id < (*verts).size(), "");
        return id;
    }

    bool EdgeIsAbsent() const { return edge == ABSENT; }
};

struct HEdge
{
private:
    uint vert = ABSENT;
    uint next = ABSENT;
    uint prev = ABSENT;
    uint pair = ABSENT;
    uint face = ABSENT;
    int sharpness = 0;

    TagColor color = TagColor::BLACK;

    std::vector<HVert>* verts;
    std::vector<HEdge>* edges;
    std::vector<HFace>* faces;

public:
    HEdge() = default;

    HEdge(std::vector<HVert>* vs, std::vector<HEdge>* es, std::vector<HFace>* fs)
        : verts(vs)
        , edges(es)
        , faces(fs)
    {
    }

    void Update(std::vector<HVert>* vs, std::vector<HEdge>* es, std::vector<HFace>* fs)
    { verts = vs; edges = es; faces = fs; }

    const HVert& Vert() const
    {
        Assert(vert != ABSENT, "Vertex is absent.");
        Assert(vert < (*verts).size(), "The vertex index is out of bounds.");
        return (*verts)[vert];
    }
    const HEdge& Next() const
    {
        Assert(next != ABSENT, "");
        Assert(next < (*edges).size(),
            "next exceeds edges range. Is {0} and only {1} are available",
            std::to_string(next),
            std::to_string((*edges).size()));
        return (*edges)[next];
    }

    const HEdge& Prev() const
    {
        Assert(prev != ABSENT, "");
        Assert(prev < (*edges).size(), "");
        return (*edges)[prev];
    }

    const HEdge& Pair() const
    {
        Assert(pair != ABSENT, "");
        return (*edges)[pair];
    }

    const HFace& Face() const
    {
        Assert(face != ABSENT, "");
        return (*faces)[face];
    }

    const TagColor Color() const { return color; }

    HVert& VertD()
    {
        Assert(vert != ABSENT, "");
        return (*verts)[vert];
    }

    HEdge& NextD()
    {
        Assert(next != ABSENT, "");
        return (*edges)[next];
    }

    HEdge& PrevD()
    {
        Assert(prev != ABSENT, "");
        return (*edges)[prev];
    }

    HEdge& PairD()
    {
        Assert(pair != ABSENT, "");
        return (*edges)[pair];
    }

    HFace& FaceD()
    {
        Assert(face != ABSENT, "");
        return (*faces)[face];
    }

    void Vert(uint id)
    {
        Assert(id < (*verts).size(), "");
        vert = id;
    }

    void Next(uint id)
    {
        Assert(id < (*edges).size(), "");
        next = id;
    }

    void Prev(uint id)
    {
        Assert(id < (*edges).size(), "");
        prev = id;
    }

    void Pair(uint id)
    {
        Assert(id < (*edges).size(), "");
        pair = id;
    }

    void Face(uint id)
    {
        Assert(id < (*faces).size(), "");
        face = id;
    }

    void Color(TagColor c) { color = c; }

    int Sharpness() const { return sharpness; }

    void Sharpness(int s)
    {
        sharpness = s;
        PairD().sharpness = s;
    }

    bool IsBoundary() const { return face == ABSENT; }

    uint ID() const
    {
        Assert(uint(this - &((*edges)[0])) < (*edges).size(),
            "{0}, {1}", std::to_string(this - &((*edges)[0])), std::to_string((*edges).size()));
        return this - &((*edges)[0]);
    }

    std::pair<const HVert*, const HVert*> const Vertices()
    {
        return {&Vert(), &(Next().Vert())};
    }

    template<typename T>
    auto Dir() const
    {
        return Next().Vert().Data<T>().position - Vert().Data<T>().position;
    }

    template<typename T>
    float Length() const
    {
        const auto dir = Dir<T>();
        return sqrt(dir.dot(dir));
    }

    std::pair<HVert*, HVert*> VerticesD() { return {&VertD(), &(NextD().VertD())}; }


};

/**
* @brief Class encapsulating a half edge face.
*
*/
struct HFace
{
private:
    std::vector<HVert>* verts;
    std::vector<HEdge>* edges;
    std::vector<HFace>* faces;

public:
    uint edge = ABSENT;
    std::vector<Eigen::Vector3f> face_normals;
    std::vector<Eigen::Vector2f> face_uvs;
    TagColor color = TagColor::BLACK;

    HFace() = default;
    HFace(std::vector<HVert>* vs, std::vector<HEdge>* es, std::vector<HFace>* fs)
        : verts(vs)
        , edges(es)
        , faces(fs)
    {
    }

    void Update(std::vector<HVert>* vs, std::vector<HEdge>* es, std::vector<HFace>* fs)
    { verts = vs; edges = es; faces = fs; }

    void Edge(uint id) { edge = id; }

    const HEdge& Edge() const { return (*edges)[edge]; }

    HEdge& EdgeD() { return (*edges)[edge]; }

    uint ID() const { return this - &((*faces)[0]); }

    const std::vector<Eigen::Vector2f>& FaceUvs() const { return face_uvs; }

    template <typename T>
    auto Normal() const { return GLA::normalize(UnormalizedNormal<T>()); }

    template <typename T>
    auto UnormalizedNormal() const;

    template <typename T>
    float Area() const { return UnormalizedNormal<T>().norm() / 2.f; }

    template <typename T>
    Eigen::Vector3f Centroid() const;

    std::vector<uint> VertexIds() const;

    std::vector<const HVert*> Vertices() const;

    std::vector<HVert*> VerticesD();

    std::vector<const HEdge*> Edges() const;

    std::vector<HEdge*> EdgesD();

    void Color(TagColor c) { color = c; }

    TagColor Color() const { return color; }
};

bool FindCommonEdge(const HFace& f1, const HFace&f2, uint& ret_edge);

template <typename VertData>
auto HFace::UnormalizedNormal() const
{
    using Scalar =
        std::remove_reference<decltype(std::declval<VertData>().position[0])>::type;
    const auto ids = VertexIds();
    const Eigen::Matrix<Scalar, 3, 1> e1 =
        (   (*verts)[ids[1]].Data<VertData>().position -
            (*verts)[ids[0]].Data<VertData>().position)
            .block(0, 0, 3, 1);
    const Eigen::Matrix<Scalar, 3, 1> e2 =
        (   (*verts)[ids[2]].Data<VertData>().position -
            (*verts)[ids[0]].Data<VertData>().position)
            .block(0, 0, 3, 1);
    return GLA::cross(e1, e2);
}

template <typename T>
Eigen::Vector3f HFace::Centroid() const
{
    const auto& v_edge = (*edges)[edge];
    const std::array<VertexData, 3> vertices = {
        v_edge.Vert().Data<T>(),
        v_edge.Next().Vert().Data<T>(),
        v_edge.Prev().Vert().Data<T>()};
    auto centroid = vertices[0].position;
    centroid += vertices[1].position;
    centroid += vertices[2].position;

    return centroid / 3.f;
}

/**
 * @brief Sets `e2` as the next of `e1` and `e1` as the prev of `e2`
 *
 * @param e1 First edge.
 * @param e2 Second edge.
 */
void AttachEdges(HEdge& e1, HEdge& e2);
/**
 * @brief Connect all the components of a face together.
 *
 * @param face Face where all the components are contained.
 * @param e1 First edge of the face.
 * @param e2 Second edge of the face.
 * @param e3 Third edge of the face.
 */
void ConnectFace(HFace& face, HEdge& e1, HEdge& e2, HEdge& e3);

void ConnectFace(HFace& face, std::vector<HEdge*>& edges);
/**
 * @brief Pair 2 edges together.
 *
 * @param e1 First edge.
 * @param e2 Second edge.
 */
void Pair(HEdge& e1, HEdge& e2);
/**
 * @brief Set the endpoints of an edge (affects both half edges in the edge).
 *
 * @param e1 One of the 2 half edges in the edge.
 * @param v1 Endpoint at `e1`.
 * @param v2 Endpoint at the pair of `e1`.
 */
void AttachVertices(HEdge& e1, HVert& v1, HVert& v2);
/**
 * @brief Mark an edge with a color.
 *
 * @param e Edge to mark.
 * @param color Color to use as mark.
 */
void Mark(HEdge& e, TagColor color);
