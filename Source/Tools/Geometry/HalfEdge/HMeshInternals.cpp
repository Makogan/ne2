#include "HMeshInternals.hpp"

#include <iostream>

std::vector<HEdge*> HVert::AdjacentEdges() const
{
    std::vector<HEdge*> adjacent_edges;
    Assert(edge != ABSENT, "");
    uint current_edge = edge;
    uint count = 0;
    do
    {
        adjacent_edges.push_back(&(*edges)[current_edge]);

        Assert((*edges)[current_edge].Prev().Pair().ID() != current_edge, "");
        current_edge = (*edges)[current_edge].Prev().Pair().ID();
    } while(current_edge != adjacent_edges[0]->ID() && count++ < 1000);
    return adjacent_edges;
}

std::vector<HVert*> HVert::NeighbourVerts() const
{
    std::vector<HEdge*> adjacent_edges = AdjacentEdges();
    std::vector<HVert*> neighbours;
    for(auto& n: adjacent_edges)
    {
        neighbours.push_back(&n->PairD().VertD());
    }
    return neighbours;
}

std::vector<HFace*> HVert::ContainingFaces() const
{
    std::vector<HEdge*> adjacent_edges = AdjacentEdges();
    std::vector<HFace*> containing_faces;
    for(auto& n: adjacent_edges)
    {
        containing_faces.push_back(&n->FaceD());
    }
    return containing_faces;
}

// Face ==================================================================================
std::vector<uint> HFace::VertexIds() const
{
    Assert(edge != ABSENT, "");
    const auto& start_edge = (*edges)[edge];

    return {start_edge.Vert().ID(), start_edge.Next().Vert().ID(), start_edge.Prev().Vert().ID()};

    std::vector<uint> ids;
    auto* current_edge = &start_edge;
    do
    {
        ids.push_back(current_edge->Vert().ID());
        current_edge = &current_edge->Next();
    } while(start_edge.ID() != current_edge->ID());

    Assert(ids.size() >= 3, "");

    return ids;
}

 std::vector<const HVert*> HFace::Vertices() const
{
    Assert(edge != ABSENT, "");
    const auto& start_edge = (*edges)[edge];

    std::vector<const HVert*> verts = {
       &start_edge.Vert(),
       &start_edge.Next().Vert(),
       &start_edge.Next().Next().Vert(),
    };
    // auto* current_edge = &start_edge;
    // do
    // {
    //     verts.push_back(&(current_edge->Vert()));
    //     current_edge = &current_edge->Next();
    // } while(start_edge.ID() != current_edge->ID());

    return verts;
}

std::vector<HVert*> HFace::VerticesD()
{
    Assert(edge != ABSENT, "");
    auto& start_edge = (*edges)[edge];

    std::vector<HVert*> verts;
    auto* current_edge = &start_edge;
    do
    {
        verts.push_back(&(current_edge->VertD()));
        current_edge = &current_edge->NextD();
    } while(start_edge.ID() != current_edge->ID());

    return verts;
}

std::vector<const HEdge*> HFace::Edges() const
{
    Assert(edge != ABSENT, "");
    const auto& start_edge = (*edges)[edge];

    std::vector<const HEdge*> local_edges;
    auto* current_edge = &start_edge;
    do
    {
        local_edges.push_back((current_edge));
        current_edge = &current_edge->Next();
    } while(start_edge.ID() != current_edge->ID());

    return local_edges;
}

std::vector<HEdge*> HFace::EdgesD()
{
    Assert(edge != ABSENT, "");
    auto& start_edge = (*edges)[edge];

    std::vector<HEdge*> local_edges;
    auto* current_edge = &start_edge;
    do
    {
        local_edges.push_back(current_edge);
        current_edge = &current_edge->NextD();
    } while(start_edge.ID() != current_edge->ID());

    return local_edges;
}

// Utilities =============================================================================
void AttachEdges(HEdge& e1, HEdge& e2)
{
    e1.Next(e2.ID());
    e2.Prev(e1.ID());
}

void ConnectFace(HFace& face, HEdge& e1, HEdge& e2, HEdge& e3)
{
    e1.Face(face.ID());
    e2.Face(face.ID());
    e3.Face(face.ID());

    face.Edge(e1.ID());

    AttachEdges(e1, e2);
    AttachEdges(e2, e3);
    AttachEdges(e3, e1);
}

void ConnectFace(HFace& face, std::vector<HEdge*>& edges)
{
    for(uint i=0; i < edges.size(); i++)
    {
        edges[i]->Face(face.ID());
        AttachEdges(*edges[i], *edges[(i + 1) % edges.size()]);
    }

    face.Edge(edges[0]->ID());
}

void Pair(HEdge& e1, HEdge& e2)
{
    e1.Pair(e2.ID());
    e2.Pair(e1.ID());
}

void AttachVertices(HEdge& e1, HVert& v1, HVert& v2)
{
    e1.Vert(v1.ID());
    v1.Edge(e1.ID());

    e1.PairD().Vert(v2.ID());
    v2.Edge(e1.Pair().ID());
}

void Mark(HEdge& e, TagColor color)
{
    e.Color(color);
    e.PairD().Color(color);
}

bool FindCommonEdge(const HFace& f1, const HFace&f2, uint& ret_edge)
{
    for(auto edge1 : f1.Edges() )
    {
        auto& pair = edge1->Pair();

        for(auto edge2 : f2.Edges() )
        {
            if(edge2->ID() == pair.ID())
            {
                ret_edge = edge2->ID();
                return true;
            }
        }
    }

    return false;
}