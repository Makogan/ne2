#pragma once

#include "HMesh.hpp"

struct HyperVertex
{
    Eigen::VectorXf position = Eigen::Vector3f(0, 0, 0);
    Eigen::Vector2f uv;
    Eigen::Vector3f normal;
};

 /**
 * @brief Split all edges in the mesh and then flip some of the newly introduced
 * edges. It creates a regular trianglular subdivision pattern but has no effect on
 * the shape. i.e. the new mesh has the exact same shape as the input.
 *
 * @param mesh Mesh to refine.
 * @param reset_colors Whether to reset the colors of the mesh.
 */
template<typename V>
requires(MC::HasBasicMeshMembers<V>)
void RefineEdges(HMesh<V>& mesh, bool reset_colors = false)
{
    const uint v_num_start = mesh.Verts().size();

    HMesh<V>::SplitEdges(mesh);
    // Color the newly introduced vertices.
    for(uint i = v_num_start; i < mesh.Verts().size(); i++)
        mesh.VertsD()[i].Color(TagColor::BLUE);
    HMesh<V>::FlipEdges(v_num_start, mesh.Edges().size(), mesh);

    if(reset_colors)
    {
        for(auto& v: mesh.VertsD())
            v.Color(TagColor::BLACK);
        for(auto& e: mesh.EdgesD())
            e.Color(TagColor::BLACK);
    }
}

/**
 * @brief Execute Loop subdivision on a mesh.
 *
 * @param mesh Mesh to subdivide.
 * @param preserve_boundary Whether the boundary should move or not. If true, the
 * boundary will be subdivided but won't move.
 */
template<typename V>
void LoopSubdivision(HMesh<V>& mesh, bool preserve_boundaries = false)
{
    // Capture whatever is the type of the position member.
    using Vec = decltype(std::declval<V>().position);
    using MEdge = typename HMesh<V>::MEdge;
    Assert(mesh.Edges().size() % 2 == 0, "");
    const uint o_vert_num = mesh.VertexData().size();
    std::vector<Vec> new_positions(o_vert_num + mesh.Edges().size() / 2);

    // Calculate the new positions of the edge vertices
    uint count = 0;
    for(uint i = 0; i < mesh.Edges().size(); i += 2)
    {
        auto* edge = &mesh.EdgesD()[i];
        if(edge->Pair().IsBoundary()) edge = &edge->PairD();
        Assert(o_vert_num + count < new_positions.size(), "");

        // Check if we are at the boundary.
        if(edge->IsBoundary() || edge->Sharpness() > 0)
        {
            const bool average_vertices = !preserve_boundaries || edge->Sharpness();
            auto& v1 = edge->Vert().template Data<HyperVertex>().position;
            auto& v2 = edge->Pair().Vert().template Data<HyperVertex>().position;
            // Boundary conditions.
            new_positions[o_vert_num + count] =
                !average_vertices * v1 + average_vertices * 0.5f * (v1 + v2);
            edge->VertD().IsSharp(true);
            edge->PairD().VertD().IsSharp(true);
        }
        else
        {
            const Vec& v1 = edge->Vert().template Data<HyperVertex>().position;
            const Vec& v2 = edge->Next().Vert().template Data<HyperVertex>().position;
            // Vertices opposite to the edge at each side
            const Vec& v3 = edge->Prev().Vert().template Data<HyperVertex>().position;
            const Vec& v4 = edge->Pair().Prev().Vert().template Data<HyperVertex>().position;

            Assert(o_vert_num + count < new_positions.size(), "");
            new_positions[o_vert_num + count] =
                3.f / 8.f * (v1 + v2) + 1.f / 8.f * (v3 + v4);
        }

        edge->Sharpness(std::max(0, edge->Sharpness() - 1));

        count++;
    }

    // Calculate the new positions of the original vertices
    for(uint i = 0; i < o_vert_num; i++)
    {
        const auto& vert = mesh.Verts()[i];
        auto& edge = mesh.VertsD()[i].EdgeD();
        Assert(edge.Vert().ID() == i, "");
        if(edge.IsBoundary() || vert.IsSharp() > 0)
        {
            const bool keep_vertices = preserve_boundaries || vert.IsSharp();
            Vec v = edge.Vert().template Data<HyperVertex>().position;
            Vec a = edge.Next().Vert().template Data<HyperVertex>().position;
            Vec b = edge.Prev().Vert().template Data<HyperVertex>().position;
            new_positions[i] = keep_vertices * v +
                !keep_vertices * ((1.f / 8.f) * (a + b) + (3.f / 4.f) * v);
        }
        else
        {
            const std::vector<MEdge*> neighbours = mesh.Verts()[i].AdjacentEdges();
            const uint n = neighbours.size();
            const float u = n == 3 ? 3.f / 16.f : 3.f / (8.f * n);

            Vec n_pos = (1.f - n * u) * mesh.VertexData()[i].position;

            for(auto e: neighbours)
                n_pos += u * e->Next().Vert().template Data<HyperVertex>().position;

            new_positions[i] = n_pos;
        }
    }

    RefineEdges(mesh);
    for(uint i = 0; i < mesh.Verts().size(); i++)
    {
        mesh.VertsD()[i].IsSharp(false);
    }

    Assert(new_positions.size() == mesh.Verts().size(), "");
    for(uint i = 0; i < new_positions.size(); i++)
        mesh.VertexDataD()[i].position = new_positions[i];
    for(uint i = o_vert_num; i < mesh.Verts().size(); i++)
        mesh.VertsD()[i].Color(TagColor::BLACK);
    for(uint i = 0; i < mesh.Edges().size(); i++)
        mesh.EdgesD()[i].Color(TagColor::BLACK);
}

std::pair<Eigen::Vector3f, Eigen::Matrix3f> DualToComponents(
    const Eigen::VectorXf& position);

// TODO (low): make this a template to handle any kind of vertex struct.
void GaussianSubdivideMesh(HMesh<HyperVertex>& mesh);

extern template class HMesh<HyperVertex>;

