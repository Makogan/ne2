#include "HMeshUtils.hpp"

namespace _details
{
std::vector<std::vector<uint>> FindBoundaries(const std::vector<HEdge>& edges)
{
    std::vector<bool> visited_edges(edges.size(), false);
    std::vector<std::vector<uint>> boundaries;
    boundaries.reserve(edges.size() / 10);

    for(uint edge_index = 0;
        edge_index < edges.size();
        edge_index++)
    {
        if(edges[edge_index].IsBoundary() && !visited_edges[edge_index])
        {
            boundaries.push_back({edge_index});
            uint current_loop_index = edges[edge_index].Next().ID();
            while(current_loop_index != edge_index || visited_edges[current_loop_index])
            {
                visited_edges[current_loop_index] = true;
                Assert(current_loop_index < edges.size(), "");
                boundaries.back().push_back(current_loop_index);
                current_loop_index = edges[current_loop_index].Next().ID();
            }
        }
    }

    return boundaries;
}
} // namespace _details