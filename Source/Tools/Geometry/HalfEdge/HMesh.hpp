#pragma once

/** @cond */
#include <any>
#include <array>
#include <cstdint>
#include <map>
#include <set>
#include <string>
#include <tuple>
#include <vector>

#include "Eigen/Dense"
/** @endcond */

#include "Geometry/GeometryUtils/GeometryUtils.hpp"
#include "Geometry/Math/EigenHelpers.hpp"
#include "Core/Log.hpp"
#include "HMeshInternals.hpp"
#include "MeshConstraints.hpp"
#include "Profiler/profiler.hpp"
#include "MiscUtils/VectorManipulation.hpp"

// In an ideal world tempaltes have n compilation overhead, in reality they do.
// So we move as many tempalte agnostic operations to non tempalted member functions.
namespace _details
{
void InitTopology(
    const uint vert_count,
    const std::vector<std::vector<uint>>& vertex_indices,
    std::vector<HVert>& out_verts,
    std::vector<HEdge>& out_edges,
    std::vector<HFace>& out_faces);

void FlipEdge(uint edge_id, std::vector<HEdge>& edges);

void FlipEdges(std::vector<HEdge>& edges);

void SplitEdge(
    uint edge_id,
    HVert& m,
    std::vector<HVert>& vert,
    std::vector<HEdge>& edges,
    std::vector<HFace>& faces);

std::vector<std::vector<uint>> TriangleListToFaceList(const std::vector<uint>& indices);

} // namespace _details

/**
 * @brief Half edge data structure.
 * Encapsulates a manifold mesh as a directed graph and provides useful operations for
 * geometric manipulation, such as edge spliting, flips, face splitting...
 *
 * For detailed information on the operations, see [the docs](Docs/HalfEdge.md)
 *
 * @tparam VertexData The underlying type that encapuslates vertex data. It MUST
 * have publically accessible members: position, uv, normal.
 */
template<typename V = VertexData>
requires(MC::HasBasicMeshMembers<V>)  //
    class HMesh
{
    using Vec3 = decltype(std::declval<V>().normal);
public:
    using VectorType = Vec3;
    using ContainerType = V;
    // Denote a missing value, treat this as equivalent to NULL.
    static constexpr uint ABSENT = std::numeric_limits<uint>::max();
    using MVert = HVert;
    using MEdge = HEdge;
    using MFace = HFace;

    void UpdatePointers();
private:
    friend MVert;
    friend MEdge;
    friend MFace;
    uint id;

    std::vector<V> vertex_data;
    std::vector<HVert> verts;
    std::vector<HEdge> edges;
    std::vector<HFace> faces;

    template<typename S = decltype(std::declval<V>().position[0])::type>
    void Init(
        const std::vector<Eigen::Matrix<S, 3, 1>>& vertices,
        const std::vector<Eigen::Matrix<S, 2, 1>>& uvs,
        const std::vector<Eigen::Matrix<S, 3, 1>>& normals,
        const std::vector<std::vector<uint>>& vertex_indices,
        const std::vector<std::vector<uint>>& uv_indices,
        const std::vector<std::vector<uint>>& normal_indices);

    void SetID(uint new_id) { id = new_id; }
    void SwapVertices(const uint vert_id_1, const uint vert_id_2);

  public:
    HMesh(){};
    HMesh(const std::string& file_path);
    HMesh(const std::vector<V>& data, const std::vector<std::vector<uint>>& connectivity);
    // Assumes triangular connectivity.
    HMesh(const std::vector<V>& data, const std::vector<uint>& connectivity);
    HMesh(const HMesh& other)
        : id(other.id)
        , vertex_data(other.vertex_data)
        , verts(other.verts)
        , edges(other.edges)
        , faces(other.faces)
    {
        UpdatePointers();
    }
    HMesh(HMesh&& other)
        : id(std::move(other.id))
        , vertex_data(std::move(other.vertex_data))
        , verts(std::move(other.verts))
        , edges(std::move(other.edges))
        , faces(std::move(other.faces))
    {
        other.UpdatePointers();
    }

    ~HMesh() = default;

    HMesh& operator=(const HMesh& other);
    HMesh& operator=(HMesh&& other);

    const std::vector<V>& VertexData() const { return vertex_data; };
    const std::vector<MVert>& Verts() const { return verts; };
    const std::vector<MEdge>& Edges() const { return edges; }
    const std::vector<MFace>& Faces() const { return faces; }

    std::vector<V>& VertexDataD() { return vertex_data; };
    std::vector<MVert>& VertsD() { return verts; };
    std::vector<MEdge>& EdgesD() { return edges; }
    std::vector<MFace>& FacesD() { return faces; }

    const auto& Positions(uint i) const { return vertex_data[i].position; };
    const auto& UVs(uint i) const { return vertex_data[i].uv; }
    const auto& Normals(uint i) const { return vertex_data[i].normal; }
    const std::vector<std::vector<uint>> Indices() const ;

    // Violates naming conventions for the sake of uniformity with the STL.
    const uint size() const { return vertex_data.size(); };

    std::vector<std::pair<MVert*, MVert*>> EdgeVertices();

    void CalculateNormals();

    static void SplitFace(uint face_id, HMesh<V>& mesh);

    static void SplitEdge(uint edge_id, HMesh<V>& mesh);

    static void SplitBoundaryEdge(uint face_id, HMesh<V>& mesh);

    static void SplitEdges(HMesh<V>& mesh);

    static void FlipEdge(uint edge_id, HMesh<V>& mesh);

    static bool CheckAfterFlipTopology(uint edge_id, HMesh<V>& mesh);

    static bool CheckAfterFlipGeometry(uint edge_id, HMesh<V>& mesh);

    static void FlipEdges(uint start, uint end, HMesh<V>& mesh);

    static MC::ByteBuffers GetGeometryData(HMesh<V>& mesh);

    static std::pair<uint, float> RayMeshIntersection(
        HMesh<V>& mesh, const Eigen::Vector3f& origin, const Eigen::Vector3f& ray);

    void SortBoundary();
};

// HE Operations--------------------------------------------------------------------------
template<typename V>
requires(MC::HasBasicMeshMembers<V>) void HMesh<V>::FlipEdge(uint edge_id, HMesh<V>& mesh)
{
    _details::FlipEdge(edge_id, mesh.edges);
}

template<typename V>
requires(MC::HasBasicMeshMembers<V>)
bool HMesh<V>::CheckAfterFlipTopology(uint edge_id, HMesh<V>& mesh)
{
    const auto& edge = mesh.edges[edge_id];
    if(edge.IsBoundary() or edge.Pair().IsBoundary()) return false;

    const uint v1 = edge.Prev().Vert().ID();
    const uint v2 = edge.Pair().Prev().Vert().ID();
    auto neighbours_v1 = mesh.verts[v1].NeighbourVerts();

    bool is_cycle = false;
    for(auto& n : neighbours_v1)
    {
        is_cycle = is_cycle || n->ID() == v2;
    }

    const auto f_id1 = edge.Next().Pair().Face().ID();
    const auto f_id2 = edge.Prev().Pair().Face().ID();
    const auto f_id3 = edge.Pair().Next().Pair().Face().ID();
    const auto f_id4 = edge.Pair().Prev().Pair().Face().ID();

    bool edge_shares_faces =
        (f_id1 == f_id2) or
        (f_id1 == f_id3) or
        (f_id1 == f_id4) or
        (f_id2 == f_id3) or
        (f_id2 == f_id4) or
        (f_id3 == f_id4);

    return !is_cycle and !edge_shares_faces;
}

template<typename V>
requires(MC::HasBasicMeshMembers<V>)
bool HMesh<V>::CheckAfterFlipGeometry(uint edge_id, HMesh<V>& mesh)
{
    using Scalar = std::remove_reference<decltype(std::declval<V>().position[0])>::type;
    using Vector = std::remove_reference<decltype(std::declval<V>().position)>::type;
    const auto& edge = mesh.edges[edge_id];

    const Vector& p1 = edge.Vert().template Data<V>().position;
    const Vector& p2 = edge.Next().Vert().template Data<V>().position;
    const Vector& p3 = edge.Prev().Vert().template Data<V>().position;
    const Vector& p4 = edge.Pair().Prev().Vert().template Data<V>().position;

    const Vector el = (p3 - p1).normalized();
    const Vector em = (p2 - p1).normalized();
    const Vector er = (p4 - p1).normalized();

    const Vector pl = (p3 - p2).normalized();
    const Vector pm = (p1 - p2).normalized();
    const Vector pr = (p4 - p2).normalized();

    const Scalar v1_angle = acos(el.dot(em)) + acos(em.dot(er));
    const Scalar v2_angle = acos(pl.dot(pm)) + acos(pm.dot(pr));
    const Scalar threshold = M_PI;

    return v1_angle < threshold and v2_angle < threshold;
}

template<typename V>
requires(MC::HasBasicMeshMembers<V>) void HMesh<V>::FlipEdges(
    uint start, uint end, HMesh<V>& mesh)
{
    Assert(start <= end, "Wrong order of parameters");
    Assert(end <= mesh.edges.size(), "Out of bounds error");
    _details::FlipEdges(mesh.edges);
}

// As specified in the docs diagrams: Docs/HalfEdge.md.
template<typename V>
requires(MC::HasBasicMeshMembers<V>) void HMesh<V>::SplitEdge(
    uint edge_id, HMesh<V>& mesh)
{
    // Create the new vertex.
    mesh.verts.push_back({&mesh.verts, &mesh.edges, &mesh.faces, &mesh.vertex_data});
    HVert& m = mesh.verts.back();

    mesh.vertex_data.push_back({});
    Assert(mesh.vertex_data.size() == mesh.verts.size(), "");

    const HVert& v0 = mesh.edges[edge_id].Vert();
    const HVert& v1 = mesh.edges[edge_id].Pair().Vert();

    _details::SplitEdge(edge_id, m, mesh.verts, mesh.edges, mesh.faces);

    // Make the value of the mid point. First grab each individual element then set.
    // First vertex data.
    const auto p0 = v0.Data<V>().position;
    const auto n0 = v0.Data<V>().normal;
    const auto u0 = v0.Data<V>().uv;
    // Second vertex data.
    const auto p1 = v1.Data<V>().position;
    const auto n1 = v1.Data<V>().normal;
    const auto u1 = v1.Data<V>().uv;
    m.Data<V>(V{
        0.5 * p0 + 0.5 * p1, 0.5 * u0 + 0.5 * u1, GLA::normalize((0.5 * n0 + 0.5 * n1))});
}

template<typename V>
requires(MC::HasBasicMeshMembers<V>) void HMesh<V>::SplitEdges(HMesh<V>& mesh)
{
    // Subroutine appends values to the arrays so this prevents an infinite loop.
    uint edge_num = mesh.edges.size();
    for(uint i = 0; i < edge_num; i += 2)
    {
        auto* edge = &mesh.edges[i];
        edge->Color(TagColor::YELLOW);
        edge->PairD().Color(TagColor::YELLOW);
        if(edge->IsBoundary()) edge = &(edge->PairD());
        if(edge->Pair().IsBoundary())
        {
            const uint id = edge->ID();
            Assert(!edge->IsBoundary() || !edge->PairD().IsBoundary(), "");
            SplitBoundaryEdge(id, mesh);
        }
        else
            SplitEdge(i, mesh);
    }
}

template<typename V>
requires(MC::HasBasicMeshMembers<V>) void HMesh<V>::SplitBoundaryEdge(
    uint edge_id, HMesh<V>& mesh)
{
    // Create a list of new edges to represent the new 2 faces
    const uint n = mesh.edges.size();
    mesh.edges.push_back({&mesh.verts, &mesh.edges, &mesh.faces});
    mesh.edges.push_back({&mesh.verts, &mesh.edges, &mesh.faces});
    mesh.edges.push_back({&mesh.verts, &mesh.edges, &mesh.faces});
    mesh.edges.push_back({&mesh.verts, &mesh.edges, &mesh.faces});

    HEdge& n1f1 = mesh.edges[n + 0];
    HEdge& n2f3 = mesh.edges[n + 1];
    HEdge& n0f3 = mesh.edges[n + 2];
    HEdge& n0f4 = mesh.edges[n + 3];

    // Create the new vertex
    mesh.verts.push_back({&mesh.verts, &mesh.edges, &mesh.faces, &mesh.vertex_data});
    mesh.vertex_data.push_back({});
    HVert& m = mesh.verts.back();
    // Create the new faces
    mesh.faces.push_back({&mesh.verts, &mesh.edges, &mesh.faces});
    HFace& f3 = mesh.faces.back();

    HEdge& edge = mesh.edges[edge_id];

    // Gather the elements we need to modify the mesh
    HEdge& e0f1 = edge;
    HEdge& e0f2 = edge.PairD();
    HEdge& e1f1 = edge.NextD();
    HEdge& e2f1 = edge.PrevD();
    HEdge& e2f2 = edge.PairD().PrevD();
    HEdge& e1f2 = edge.PairD().NextD();

    HVert& v0 = edge.VertD();
    HVert& v1 = edge.NextD().VertD();
    HVert& v2 = edge.PrevD().VertD();

    HFace& f1 = edge.FaceD();

    // Make the value of the mid point. First grab each individual element then set.
    // First vertex data.
    const auto p0 = v0.Data<V>().position;
    const auto n0 = v0.Data<V>().normal;
    const auto u0 = v0.Data<V>().uv;
    // Second vertex data.
    const auto p1 = v1.Data<V>().position;
    const auto n1 = v1.Data<V>().normal;
    const auto u1 = v1.Data<V>().uv;
    m.Data<V>(V{
        0.5 * p0 + 0.5 * p1, 0.5 * u0 + 0.5 * u1, GLA::normalize((0.5 * n0 + 0.5 * n1))});
    ConnectFace(f1, e0f1, n1f1, e2f1);
    ConnectFace(f3, n0f3, e1f1, n2f3);

    Pair(n2f3, n1f1);
    Pair(e0f1, e0f2);
    Pair(n0f3, n0f4);

    AttachEdges(e2f2, n0f4);
    AttachEdges(n0f4, e0f2);
    AttachEdges(e0f2, e1f2);

    AttachVertices(n1f1, m, v2);
    AttachVertices(e0f2, m, v0);
    AttachVertices(n0f3, m, v1);

    m.Edge(e0f2.ID());

    Mark(n1f1, TagColor::BLUE);
}

template<typename V>
requires(MC::HasBasicMeshMembers<V>) void HMesh<V>::SplitFace(
    uint face_id, HMesh<V>& mesh)
{
    mesh.vertex_data.push_back({});
    mesh.verts.push_back({&mesh.verts, &mesh.edges, &mesh.faces, &mesh.vertex_data});
    HVert& c = mesh.verts.back();

    const uint n = mesh.edges.size();
    mesh.edges.push_back({&mesh.verts, &mesh.edges, &mesh.faces});
    mesh.edges.push_back({&mesh.verts, &mesh.edges, &mesh.faces});
    mesh.edges.push_back({&mesh.verts, &mesh.edges, &mesh.faces});
    mesh.edges.push_back({&mesh.verts, &mesh.edges, &mesh.faces});
    mesh.edges.push_back({&mesh.verts, &mesh.edges, &mesh.faces});
    mesh.edges.push_back({&mesh.verts, &mesh.edges, &mesh.faces});

    HEdge& n00 = mesh.edges[n + 0];
    HEdge& n01 = mesh.edges[n + 1];
    HEdge& n10 = mesh.edges[n + 2];
    HEdge& n11 = mesh.edges[n + 3];
    HEdge& n20 = mesh.edges[n + 4];
    HEdge& n21 = mesh.edges[n + 5];

    const uint f = mesh.faces.size();
    mesh.faces.push_back({&mesh.verts, &mesh.edges, &mesh.faces});
    mesh.faces.push_back({&mesh.verts, &mesh.edges, &mesh.faces});

    MFace& f0 = mesh.faces[face_id];
    MFace& f1 = mesh.faces[f + 0];
    MFace& f2 = mesh.faces[f + 1];

    MEdge& e0 = f0.EdgeD();
    MEdge& e1 = f0.EdgeD().NextD();
    MEdge& e2 = f0.EdgeD().PrevD();

    MVert& v0 = e0.VertD();
    MVert& v1 = e1.VertD();
    MVert& v2 = e2.VertD();

    ConnectFace(f0, n00, e0, n10);
    ConnectFace(f1, n11, e1, n21);
    ConnectFace(f2, n20, e2, n01);

    Pair(n00, n01);
    Pair(n10, n11);
    Pair(n20, n21);

    AttachVertices(n00, c, v0);
    AttachVertices(n11, c, v1);
    AttachVertices(n20, c, v2);

    const auto& data0 = v0.Data<V>();
    const auto& data1 = v1.Data<V>();
    const auto& data2 = v2.Data<V>();
    c.Data<V>(
        {(data0.position + data1.position + data2.position) / 3.0,
         (data0.uv + data1.uv + data2.uv) / 3.0,
         {0, 0, 1}});
}

template<typename V>
requires(MC::HasBasicMeshMembers<V>)
const std::vector<std::vector<uint>> HMesh<V>::Indices() const
{
    std::vector<std::vector<uint>> indices;
    for(const auto& f: faces)
    {
        indices.push_back({});
        const std::vector<uint> ids = f.VertexIds();
        for(uint i = 0; i < ids.size(); i++)
        {
            indices.back().push_back(ids[i]);
        }
    }
    return indices;
}

// Helpers -------------------------------------------------------------------------------
template<typename V>
requires(MC::HasBasicMeshMembers<V>)
    std::vector<std::pair<typename HMesh<V>::MVert*, typename HMesh<V>::MVert*>>
    HMesh<V>::EdgeVertices()
{
    std::vector<std::pair<MVert*, MVert*>> vertex_pairs;
    for(auto& edge: edges)
    {
        if(edge.Color() == TagColor::BLACK)
        {
            edge.Color(TagColor::YELLOW);
            edge.PairD().Color(TagColor::YELLOW);
            vertex_pairs.push_back(edge.VerticesD());
        }
    }
    for(auto& edge: edges)
    {
        edge.Color(TagColor::BLACK);
    }

    return vertex_pairs;
}

std::tuple<
    std::vector<Eigen::Vector3f>,
    std::vector<Eigen::Vector2f>,
    std::vector<Eigen::Vector3f>,
    std::vector<std::vector<uint>>,
    std::vector<std::vector<uint>>,
    std::vector<std::vector<uint>>>
ParseObj(const std::string& path);

// Constructors --------------------------------------------------------------------------
template<typename V>
template<typename S>
void HMesh<V>::Init(
    const std::vector<Eigen::Matrix<S, 3, 1>>& vertices,
    const std::vector<Eigen::Matrix<S, 2, 1>>& uvs,
    const std::vector<Eigen::Matrix<S, 3, 1>>& normals,
    const std::vector<std::vector<uint>>& vertex_indices,
    const std::vector<std::vector<uint>>& uv_indices,
    const std::vector<std::vector<uint>>& normal_indices)
{
    Assert(vertices.size() > 0, "");
    Assert(vertex_indices.size() > 0, "");
    Assert(vertex_indices[0].size() > 0, "");
    this->vertex_data.reserve(vertices.size());

    // Initialize vertices
    for(uint v_id = 0; v_id < vertices.size(); v_id ++)
    {
        this->vertex_data.push_back({vertices[v_id]});
        this->verts.push_back(
            {&this->verts, &this->edges, &this->faces, &this->vertex_data});
    }

    _details::InitTopology(vertices.size(), vertex_indices, this->verts, this->edges, this->faces);

    // With the half edges created we can now initialize the connectivity.
    for(uint face_id = 0; face_id < vertex_indices.size(); face_id ++)
    {
        Assert(
            uv_indices.empty() ||
            uv_indices[face_id].size() == vertex_indices[face_id].size(),
            "");
         Assert(
            normal_indices.empty() ||
            normal_indices[face_id].size() == vertex_indices[face_id].size(),
            "");

        Assert(face_id < this->faces.size(), "");
        auto& face = this->faces[face_id];
        const uint face_valence = vertex_indices[face_id].size();
        // Initialize the half edge pairs.
        for(uint i=0; i < face_valence; i++)
        {
            // Initialize the half edge next and prev connectivity.
            const uint v_id_0 = vertex_indices[face_id][i];

            if(!uv_indices.empty())
            {
                const auto& uv = uvs[uv_indices[face_id][i]];
                face.face_uvs.push_back({uv[0], uv[1]});
                this->vertex_data[v_id_0].uv = uv;
            }

            if(!normal_indices.empty())
            {
                const auto& normal = normals[normal_indices[face_id][i]];
                face.face_normals.push_back({normal[0], normal[1], normal[2]});
                this->vertex_data[v_id_0].normal = normal;
            }
        }
    }

    if(normal_indices.size() == 0)
        CalculateNormals(); // Make sure this is the last instruction, ALWAYS.
}

template<typename V>
requires(MC::HasBasicMeshMembers<V>) HMesh<V>::HMesh(const std::string& file_path)
{
    static_assert(sizeof(std::declval<V>().normal) == 12);
    auto [vertices, uvs, normals, vertex_indices, uv_indices, normal_indices] =
        ParseObj(file_path);

    Init(vertices, uvs, normals, vertex_indices, uv_indices, normal_indices);
}

template<typename V>
requires(MC::HasBasicMeshMembers<V>) HMesh<V>::HMesh(
    const std::vector<V>& data, const std::vector<std::vector<uint>>& connectivity)
{
    using Scalar = std::remove_reference<decltype(std::declval<V>().position[0])>::type;

    std::vector<Eigen::Matrix<Scalar, 3, 1>> vertices;
    std::vector<Eigen::Matrix<Scalar, 2, 1>> uvs;
    std::vector<Eigen::Matrix<Scalar, 3, 1>> normals;
    for(const auto& datum: data)
    {
        vertices.push_back({datum.position[0], datum.position[1], datum.position[2]});
        uvs.push_back({datum.uv[0], datum.uv[1]});
        normals.push_back({datum.normal[0], datum.normal[1], datum.normal[2]});
    }
    Init(vertices, uvs, normals, connectivity, connectivity, connectivity);
}

template<typename V>
requires(MC::HasBasicMeshMembers<V>) HMesh<V>::HMesh(
    const std::vector<V>& data, const std::vector<uint>& connectivity)
    : HMesh(data, _details::TriangleListToFaceList(connectivity))
{}

template<typename V>
requires(MC::HasBasicMeshMembers<V>) HMesh<V>
&HMesh<V>::operator=(const HMesh<V>& other)
{
    id = other.id;
    vertex_data = other.vertex_data;
    verts = other.verts;
    edges = other.edges;
    faces = other.faces;
    UpdatePointers();
    return *this;
}

template<typename V>
requires(MC::HasBasicMeshMembers<V>) HMesh<V>
&HMesh<V>::operator=(HMesh<V>&& other)
{
    id = other.id;
    vertex_data = other.vertex_data;
    verts = other.verts;
    edges = other.edges;
    faces = other.faces;
    UpdatePointers();
    return *this;
}

// Utility -------------------------------------------------------------------------------
template<typename V>
requires(MC::HasBasicMeshMembers<V>) void HMesh<V>::CalculateNormals()
{
    using PVec = decltype(std::declval<V>().position);
    for(V& v: vertex_data)
        v.normal = {0, 0, 0};

    for(HFace& f: faces)
    {
        const std::vector<uint> v_ids = f.VertexIds();
        Assert(v_ids.size() == 3, "We have {0} ids.", std::to_string(v_ids.size()));

        Assert(v_ids[0] < vertex_data.size(), "Value {0}.", std::to_string(v_ids[0]));
        Assert(v_ids[1] < vertex_data.size(), "Value {0}.", std::to_string(v_ids[1]));
        Assert(v_ids[2] < vertex_data.size(), "Value {0}.", std::to_string(v_ids[2]));

        const PVec p1 = vertex_data[v_ids[0]].position;
        const PVec p2 = vertex_data[v_ids[1]].position;
        const PVec p3 = vertex_data[v_ids[2]].position;

        const Vec3 q1 = Vec3{p1[0], p1[1], p1[2]};
        const Vec3 q2 = Vec3{p2[0], p2[1], p2[2]};
        const Vec3 q3 = Vec3{p3[0], p3[1], p3[2]};

        const Vec3 e1 = GLA::normalize(q1 - q2);
        const Vec3 e2 = GLA::normalize(q2 - q3);
        const Vec3 e3 = GLA::normalize(q3 - q1);
        const Vec3 no = GLA::normalize(GLA::cross(-e2, e1));

        vertex_data[v_ids[0]].normal += no * acos(e1.dot(-e2));
        vertex_data[v_ids[1]].normal += no * acos(e2.dot(-e3));
        vertex_data[v_ids[2]].normal += no * acos(e3.dot(-e1));
    }

    for(V& v: vertex_data)
    {
        v.normal = GLA::normalize(v.normal);
    }
}

template<typename V>
requires(MC::HasBasicMeshMembers<V>)
    MC::ByteBuffers HMesh<V>::GetGeometryData(HMesh<V>& mesh)
{
    std::vector<float> element;
    for(const auto& data: mesh.vertex_data)
    {
        const auto normal = data.normal;
        element.push_back(data.position[0]);
        element.push_back(data.position[1]);
        element.push_back(data.position[2]);

        element.push_back(data.uv[0]);
        element.push_back(data.uv[1]);

        element.push_back(normal[0]);
        element.push_back(normal[1]);
        element.push_back(normal[2]);
    }

    std::vector<std::byte> ret(element.size() * sizeof(float));
    memcpy(ret.data(), element.data(), ret.size());

    const std::vector<std::vector<uint>> face_indices = mesh.Indices();

    DEBUG_SNIPPET(
        for(auto& face : face_indices)
            Assert(face.size() == 3, "");
    );

    return {{ret}, Flatten(face_indices)};
}

template<typename V>
requires(MC::HasBasicMeshMembers<V>) void HMesh<V>::UpdatePointers()
{
    for(auto& v: verts)
        v.Update(&verts, &edges, &faces, &vertex_data);
    for(auto& e: edges)
        e.Update(&verts, &edges, &faces);
    for(auto& f: faces)
        f.Update(&verts, &edges, &faces);
}

template<typename V>
requires(MC::HasBasicMeshMembers<V>) std::pair<uint, float> HMesh<V>::RayMeshIntersection(
    HMesh<V>& mesh, const Eigen::Vector3f& origin, const Eigen::Vector3f& ray)
{
    uint selected_face = ABSENT;
    float t = std::numeric_limits<float>::max();
    for(auto& face: mesh.faces)
    {
        auto m_verts = face.Vertices();
        Eigen::Vector3f v1 = m_verts[0]->template Data<V>().position;
        Eigen::Vector3f v2 = m_verts[1]->template Data<V>().position;
        Eigen::Vector3f v3 = m_verts[2]->template Data<V>().position;

        float current_t = TriangleLineIntersection(
            origin.cast<double>(),
            ray.cast<double>(),
            {v1.cast<double>(), v2.cast<double>(), v3.cast<double>()});
        if(current_t >= 0 && current_t < t)
        {
            t = current_t;
            selected_face = face.ID();
        }
    }
    t = selected_face == ABSENT ? -1 : t;
    return {selected_face, t};
}

template<typename V>
void HMesh<V>::SwapVertices(const uint vert_id_1, const uint vert_id_2)
{
    Assert(vert_id_1 < verts.size(), "");
    Assert(vert_id_2 < verts.size(), "");
    Assert(vert_id_1 != vert_id_2, "");

    using Vert = HVert;
    using Edge = HEdge;

    V& data1 = vertex_data[vert_id_1];
    Vert& vert1 = verts[vert_id_1];
    std::vector<Edge*> edges_1 = verts[vert_id_1].AdjacentEdges();

    V& data2 = vertex_data[vert_id_2];
    Vert& vert2 = verts[vert_id_2];
    std::vector<Edge*> edges_2 = verts[vert_id_2].AdjacentEdges();

    std::swap(data1, data2);
    std::swap(vert1, vert2);

    for(Edge* edge : edges_1)
        edge->Vert(vert_id_2);

    for(Edge* edge : edges_2)
        edge->Vert(vert_id_1);
}

template<typename V>
requires(MC::HasBasicMeshMembers<V>) void HMesh<V>::SortBoundary()
{
    uint low = 0;
    uint high = verts.size() - 1;

    while (low < high)
    {
        auto edges = verts[low].AdjacentEdges();
        bool is_boundary = false;
        for(auto edge : edges)
        {
            if(edge->IsBoundary())
            {
                is_boundary = true;
                break;
            }
        }
        if(is_boundary) { SwapVertices(low, high--); }
        else { low++; }
    }
}

template class HMesh<VertexData>;
