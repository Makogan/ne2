#pragma once
#include <cassert>

#include "HMesh.hpp"

struct Bit_vector
{
	explicit Bit_vector(unsigned int n = 0)
	: flag(n, 0)
	, counter(1)
	{
	}

	void clear(void)
	{
		flag.clear();
		counter = 1;
	}

	void resize(unsigned int n_) { flag.resize(n_, 0); }

	void reset_all(void)
	{
		++counter;
		if(counter == 0)
		{ // if we hit wrap-around, reset everything
			for(unsigned int i = 0; i < flag.size(); ++i)
			flag[i] = 0;
			counter = 1;
		}
	}

	bool operator[](unsigned int i) const { return flag[i] == counter; }

	void set(unsigned int i) { flag[i] = counter; }

	unsigned int size() { return flag.size(); }

	void swap(Bit_vector & othermarker)
	{
		std::swap(flag, othermarker.flag);
		std::swap(counter, othermarker.counter);
	}

protected:
	std::vector<unsigned int> flag;
	unsigned int counter;
};

template<typename V>
bool MeshIsSane(HMesh<V>& mesh)
{
    for(auto& face : mesh.Faces())
	{
        auto& he = face.Edge();
        assert(face.ID() == he.Face().ID());

        auto& next1 = he.Next();
        assert(next1.Face().ID() == face.ID());
        auto& next2 = next1.Next();
        assert(next2.Face().ID() == face.ID());
        auto& next3 = next2.Next();
        assert(next3.Face().ID() == face.ID());
        assert(next3.ID() == he.ID());
	}

    //
	// Check mutual relationship between half edges
	//
	for(auto& he : mesh.Edges())
	{
        assert(he.Pair().Pair().ID() == he.ID());
        assert(he.Next().Prev().ID() == he.ID());
        assert(he.Next().Prev().ID() == he.ID());
        assert(he.Prev().Next().ID() == he.ID());
        assert(he.Prev().ID() != he.ID());
        assert(he.Next().ID() != he.ID());
        assert(he.Prev().ID() != he.Next().ID());

        assert(!he.IsBoundary() || !he.Pair().IsBoundary());
	}

    Bit_vector is_vertex_visited(mesh.Verts().size());
    for(auto& vert : mesh.Verts())
	{
        is_vertex_visited.reset_all();
        int n_outgoing_boundary_half_edges = 0;
        int n_incoming_boundary_half_edges = 0;
        int n_traversed_edges = 0;

        assert(vert.Edge().ID() != HMesh<V>::ABSENT);

        auto vid = vert.ID();
        auto neighbours = vert.NeighbourVerts();
		auto edges = vert.AdjacentEdges();
		auto get_hf_set = []( const std::vector<HEdge*>& edges)
		{
			std::set<uint> set;
			for (auto ptr : edges) { set.insert(ptr->ID()); set.insert(ptr->Pair().ID()); }
			return set;
		};

		auto edge_set = get_hf_set(edges);
        for(auto& neighbour : neighbours)
        {
            // There should not be more than one edge between two vertices
            assert(!is_vertex_visited[neighbour->ID()] && "More than one edge between two vertices.");
            is_vertex_visited.set( neighbour->ID() );

			auto edges = neighbour->AdjacentEdges();
			auto ne_edges = get_hf_set(edges);

			std::vector<uint> intersection(20);
			auto it = std::set_intersection(
				edge_set.begin(),
				edge_set.end(),
				ne_edges.begin(),
				ne_edges.end(),
				intersection.begin()
			);

			if(it - intersection.begin() != 2)
			{
				std::cout <<"Vert " << vert.ID() << std::endl;
				std::cout <<"Neighbour " << neighbour->ID() << std::endl;
				std::cout << it - intersection.begin() << std::endl;
				for(uint i=0; i < it - intersection.begin(); i++)
				{
					std::cout<< "Edges " << intersection[i] << std::endl;
				}
				exit(0);
			}
        }

	} // End of vertices

    return true;
}