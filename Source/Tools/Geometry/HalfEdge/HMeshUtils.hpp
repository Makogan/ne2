#pragma once

#include "HMesh.hpp"

/** @cond */
#include <cmath>
#include <set>
#include <sstream>

#include "Eigen/Sparse"
#include "Eigen/SparseCholesky"
/** @endcond */

#include "Core/TextManipulation.hpp"

namespace _details {
    std::vector<std::vector<uint>> FindBoundaries(const std::vector<HEdge>& edges);
} // namespace _details

template<typename V>
HMesh<V> SubMesh(const HMesh<V>& mesh, const std::vector<uint>& face_indices)
{
    std::vector<V> sub_mesh_data;
    std::vector<uint> sub_mesh_connectivity;
    const auto& faces = mesh.Faces();
    const auto& data = mesh.VertexData();

    std::map<uint, uint> vertex_map;
    uint count = 0;
    for(auto fi: face_indices)
    {
        auto vertices = faces[fi].VertexIds();
        for(uint i = 0; i < 3; i++)
        {
            if(vertex_map.count(vertices[i]) == 0)
            {
                vertex_map.insert({vertices[i], count++});
                sub_mesh_data.push_back(data[vertices[i]]);
            }
        }
    }

    for(auto fi: face_indices)
    {
        auto vertices = faces[fi].VertexIds();
        for(uint i = 0; i < 3; i++)
        {
            sub_mesh_connectivity.push_back(vertex_map[vertices[i]]);
        }
    }

    return HMesh(sub_mesh_data, sub_mesh_connectivity);
}

template<typename V>
std::vector<uint> FaceFlooding(uint start_face_id, uint count, const HMesh<V>& mesh)
{
    std::vector<std::pair<uint, uint>> stack = {{start_face_id, count}};
    std::set<uint> flooded_faces = {start_face_id};
    while(!stack.empty())
    {
        auto [face_id, count] = stack.back();
        stack.pop_back();

        if(count-- <= 0) continue;

        auto edges = mesh.Faces()[face_id].Edges();
        for(auto edge: edges)
        {
            uint new_face_id = edge->Pair().Face().ID();
            if(new_face_id == HMesh<V>::ABSENT) continue;
            auto [_, is_new] = flooded_faces.insert(new_face_id);
            if(is_new) { stack.push_back({new_face_id, count}); }
        }
    }

    return std::vector<uint>(flooded_faces.begin(), flooded_faces.end());
}

template<typename V>
std::vector<uint> DeleteIsolatedVertices(HMesh<V>& mesh)
{
    std::vector<uint> old_new_map(mesh.Verts().size(), -1);
    std::vector<uint> new_old_map(mesh.Verts().size(), -1);

    uint old_v_id=0;
    uint new_v_id=0;

    std::vector<V> new_vertices;
    std::vector<uint> deleted_vertices;
    new_vertices.reserve(mesh.Verts().size());
    for(const auto& v : mesh.Verts())
    {
        if(!v.EdgeIsAbsent())
        {
            new_vertices.push_back(v.template Data<V>());
            old_new_map[old_v_id] = new_v_id;
            new_old_map[new_v_id] = old_v_id;
            new_v_id++;
        }
        else
        {
            Assert(false, "");
            deleted_vertices.push_back(v.ID());
        }

        old_v_id++;
    }

    std::vector<uint> new_triangles;
    new_triangles.reserve(mesh.Faces().size());

    for(const auto& f : mesh.Indices())
    {
        Assert(f.size() == 3, "");
        new_triangles.push_back(old_new_map.at(f[0]));
        new_triangles.push_back(old_new_map.at(f[1]));
        new_triangles.push_back(old_new_map.at(f[2]));
    }

    mesh = HMesh<V>(new_vertices, new_triangles);

    return deleted_vertices;
}

template<typename V>
std::vector<std::vector<uint>> FindBoundaries(const HMesh<V>& mesh)
{
    return _details::FindBoundaries(mesh.Edges());
}

template<typename V>
float DihedralDot(const HEdge& edge)
{
    return  edge.Face().template Normal<V>().dot(edge.Pair().Face().template Normal<V>());
}

template<typename V>
void Smoothen(HMesh<V>& mesh)
{
    using Vec = decltype(std::declval<V>().position);
    std::vector<Vec> new_positions(mesh.Verts().size());
    for(uint i=0; i < mesh.Verts().size(); i++)
    {
        const auto& vert = mesh.Verts()[i];
        auto neighbours = vert.NeighbourVerts();

        Vec average = neighbours[0]->template Data<V>().position * 0;
        for(auto neighbour : neighbours)
        {
            average += neighbour->template Data<V>().position;
        }
        new_positions[i] = average / float(neighbours.size());
    }

    for(uint i=0; i < mesh.Verts().size(); i++)
    {
        mesh.VertsD()[i].template DataD<V>().position = new_positions[i];
    }
}

template<typename V, typename F>
void SetNormals(HMesh<V>& mesh, F ComputeNormal)
{
    for(uint i=0; i < mesh.VertexData().size(); i++)
    {
        mesh.VertexDataD()[i].normal =
            ComputeNormal(mesh.VertexDataD()[i].position).normalized();
    }
}


template<typename V>
Eigen::Matrix<float, 6, 1> FindBounds(const HMesh<V>& mesh)
{
    using S = float;

    S max_x = std::numeric_limits<S>::lowest();
    S min_x = std::numeric_limits<S>::max();
    S max_y = std::numeric_limits<S>::lowest();
    S min_y = std::numeric_limits<S>::max();
    S max_z = std::numeric_limits<S>::lowest();
    S min_z = std::numeric_limits<S>::max();

    for(const auto& v : mesh.VertexData())
    {
        max_x = std::max(max_x, v.position[0]);
        max_y = std::max(max_y, v.position[1]);
        max_z = std::max(max_z, v.position[2]);

        min_x = std::min(min_x, v.position[0]);
        min_y = std::min(min_y, v.position[1]);
        min_z = std::min(min_z, v.position[2]);
    }

    return {min_x, min_y, min_z, max_x, max_y, max_z};
}

template <typename V>
void ExportToObj(
    const HMesh<V> &mesh,
    const std::string &path,
    const Eigen::Matrix4d& r = Eigen::Matrix4d::Identity(),
    bool use_mtl = true)
{
    Eigen::Matrix4d rotation = r;
    rotation.col(0) = rotation.col(0).normalized();
    rotation.col(1) = rotation.col(1).normalized();
    rotation.col(2) = rotation.col(2).normalized();
    rotation = rotation.inverse().eval();

    char tname[1024];
	sprintf(tname, "colors.mtl");
	FILE *cfile = fopen(tname, "wt");
	fprintf(cfile, "newmtl grey\nKd 0.6 0.6 0.6\n");
	fprintf(cfile, "newmtl blue\nKd 0.1 0.1 1.0\n");
    fprintf(cfile, "newmtl red\nKd 1.0 0.1 0.1\n");
    fprintf(cfile, "newmtl yellow\nKd 1.0 1.0 0.1\n");
	fclose(cfile);

    std::stringstream positions;
    std::stringstream normals;
    std::stringstream uvs;

    for (const auto &vert : mesh.Verts())
    {
        auto &v = vert.template Data<V>();

        Eigen::Vector4d position =
            (rotation * Edvec4(v.position.x(), v.position.y(), v.position.z(), 1.0));

        Eigen::Vector4d normal =
            (rotation * Edvec4(v.normal.x(), v.normal.y(), v.normal.z(), 0.0));

        positions
            << "v " << position.x()
            << " " << position.y()
            << " " << position.z()
            << "\n";
        normals
            << "vn " << normal.x()
            << " " << normal.y()
            << " " << normal.z()
            << "\n";
        uvs
            << "vt " << v.uv.x()
            << " " << v.uv.y()
            << "\n";
    }

    const auto &indices = mesh.Indices();
    std::stringstream faces;
    size_t face_id = 0;
    for (const auto &face : indices)
    {
        if(use_mtl)
        {
            if(mesh.Faces()[face_id].Color() == TagColor::RED)
                faces << "usemtl red\n";
            else if(mesh.Faces()[face_id].Color() == TagColor::YELLOW)
                faces << "usemtl yellow\n";
            else if(mesh.Faces()[face_id].Color() == TagColor::BLUE)
                faces << "usemtl blue\n";
            else
                faces << "usemtl grey\n";
        }
        faces << "f ";
        std::set<size_t> face_ids;
        for (size_t idx : face)
        {
            Assert(face_ids.insert(idx + 1).second, "");
            faces << idx + 1 << "/" << idx + 1 << "/" << idx + 1 << " ";
        }
        faces << "\n";
        face_id++;
    }

    std::string final_str = use_mtl? "mtllib colors.mtl\n" : "";

    final_str +=
        positions.str() +
        normals.str() +
        uvs.str() +
        faces.str();

    std::ofstream myfile;
    myfile.open(path);
    myfile << final_str;
    myfile.close();
}
