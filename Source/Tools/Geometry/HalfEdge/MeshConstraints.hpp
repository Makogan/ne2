#pragma once

/** @cond */
#include <concepts>

#include "Eigen/Core"
/** @endcond */

namespace MC
{
// === Interleaved POD ===
template<typename T> concept ImplementsBasicMeshGetters = requires(T a)
{
    // clang-format off
    { a.Position() };
    { a.UV() }->std::convertible_to<Eigen::Vector2f>;
    { a.Normal() }->std::convertible_to<Eigen::Vector3f>;
    // clang-format on
};

template<typename T> concept HasBasicMeshMembers = requires(T a)
{
    // clang-format off
    { a.position };
    { a.uv }->std::convertible_to<Eigen::Vector2f>;
    { a.normal }->std::convertible_to<Eigen::Vector3f>;
    // clang-format on
};

// === Size Accessors ===
template<typename T> concept ImplementsSizeGetter = requires(T m)
{
    {
        m.size()
    }
    ->std::convertible_to<std::size_t>;
};

template<typename T> concept HasSizeVariable = requires(T m)
{
    {
        m.size
    }
    ->std::convertible_to<std::size_t>;
};

template<typename T>
concept SizeIsAccessible = ImplementsSizeGetter<T> || HasSizeVariable<T>;

// === Index Accessors ===
template<typename T> concept ImplementsIndicesArrayIndexGetter = requires(T m, uint i)
{
    {m.Indices()[i]};
};
template<typename T> concept HasIndicesArray = requires(T m, uint i) { {m.indices[i]}; };
template<typename T>
concept IndicesArrayIsAccessible =
    ImplementsIndicesArrayIndexGetter<T> || HasIndicesArray<T>;

// === Position Accessors ===
template<typename T> concept ImplementsPositionsArrayIndexGetter = requires(T m, uint i)
{
    {m.Positions(i)};
};
template<typename T> concept HasPositionsArray = requires(T m, uint i)
{
    {m.positions[i]};
};
template<typename T>
concept PositionArrayIsAccessible =
    ImplementsPositionsArrayIndexGetter<T> || HasPositionsArray<T>;

// === Normal Accessors ===
template<typename T> concept ImplementsNormalsArrayIndexGetter = requires(T m, uint i)
{
    {m.Normals(i)};
};
template<typename T> concept HasNormalsArray = requires(T m, uint i) { {m.normals[i]}; };
template<typename T>
concept NormalArrayIsAccessible =
    ImplementsNormalsArrayIndexGetter<T> || HasNormalsArray<T>;

// === BiNormal Accessors ===
template<typename T> concept ImplementsBinormalsArrayIndexGetter = requires(T m, uint i)
{
    {m.Binormals(i)};
};
template<typename T> concept HasBinormalsArray = requires(T m, uint i) { {m.normals[i]}; };
template<typename T>
concept BinormalArrayIsAccessible =
    ImplementsBinormalsArrayIndexGetter<T> || HasBinormalsArray<T>;
// === UV Accessors ===
template<typename T> concept ImplementsUVsArrayIndexGetter = requires(T m, uint i)
{
    {m.UVs(i)};
};
template<typename T> concept HasUVsArray = requires(T m, uint i) { {m.uvs[i]}; };
template<typename T>
concept UVArrayIsAccessible = ImplementsUVsArrayIndexGetter<T> || HasUVsArray<T>;

// === Tangent Accessors ===
template<typename T> concept ImplementsTangentsArrayIndexGetter = requires(T m, uint i)
{
    {m.Tangents(i)};
};
template<typename T> concept HasTangentsArray = requires(T m, uint i)
{
    {m.tangents[i]};
};
template<typename T>
concept TangentArrayIsAccessible =
    ImplementsTangentsArrayIndexGetter<T> || HasTangentsArray<T>;

// === Joint Accessors ===
template<typename T> concept ImplementsJointsArrayIndexGetter = requires(T m, uint i)
{
    {
        m.Joints(i)
    }
    ->std::convertible_to<Eigen::Vector4i>;
};
template<typename T> concept HasJointsArray = requires(T m, uint i)
{
    {
        m.joints[i]
    }
    ->std::convertible_to<Eigen::Vector4i>;
};
template<typename T>
concept JointArrayIsAccessible = ImplementsJointsArrayIndexGetter<T> || HasJointsArray<T>;

template<typename T> concept ImplementsJointsArrayDirectGetter = requires(T m)
{
    {
        m.Joints()
    }
    ->std::convertible_to<const std::vector<Eigen::Vector4i>&>;
};

// === Weight Accessors ===
template<typename T> concept ImplementsWeightsArrayIndexGetter = requires(T m, uint i)
{
    {
        m.Weights(i)
    }
    ->std::convertible_to<Eigen::Vector4f>;
};

template<typename T> concept HasWeightsArray = requires(T m, uint i)
{
    {
        m.joints[i]
    }
    ->std::convertible_to<Eigen::Vector4f>;
};

template<typename T>
concept WeightArrayIsAccessible =
    ImplementsWeightsArrayIndexGetter<T> || HasWeightsArray<T>;

template<typename T> concept ImplementsWeightsArrayDirectGetter = requires(T m)
{
    {
        m.Weights()
    }
    ->std::convertible_to<const std::vector<Eigen::Vector4f>&>;
};
// === Serializer ===
using ByteBuffers = std::pair<std::vector<std::vector<std::byte>>, std::vector<uint>>;
template<typename T> concept HasGeometryDataSerializer = requires(T m)
{
    {
        T::GetGeometryData(m)
    }
    ->std::convertible_to<ByteBuffers>;
};

}  // namespace MC

