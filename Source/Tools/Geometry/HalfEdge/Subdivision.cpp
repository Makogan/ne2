#include "Subdivision.hpp"

using namespace std;

template class HMesh<HyperVertex>;

std::pair<Eigen::Vector3f, Eigen::Matrix3f> DualToComponents(
    const Eigen::VectorXf& position)
{
    Eigen::Vector3f dual_position;
    dual_position << position[0], position[1], position[2];
    Eigen::Matrix3f sigma_inv;
    for(uint k = 0; k < 6; k++)
    {
        uint x = (sqrt(1.0 + 8.0 * double(k)) - 1.0) / 2.0;
        uint y = k - x * (x + 1) / 2;

        sigma_inv(x, y) = position(3 + k);
        sigma_inv(y, x) = sigma_inv(x, y);
    }

    return {dual_position, sigma_inv};
}

void GaussianSubdivideMesh(HMesh<HyperVertex>& mesh)
{
    for(auto& vertex: mesh.VertexDataD())
    {
        if(vertex.position.cols() == 3)
        {
            Eigen::VectorXf position = vertex.position;
            Eigen::MatrixXf sigma_inv = Eigen::MatrixXf::Identity(3, 3);
            position.conservativeResize(9, 1);
            vertex.position << position(0), position(1), position(2), sigma_inv(0, 0),
                sigma_inv(1, 0), sigma_inv(1, 1), sigma_inv(2, 0), sigma_inv(2, 1),
                sigma_inv(2, 2);
        }
        else
        {
            auto [dual_position, sigma_inv] = DualToComponents(vertex.position);
            dual_position = sigma_inv * dual_position;
            vertex.position[0] = dual_position[0];
            vertex.position[1] = dual_position[1];
            vertex.position[2] = dual_position[2];
        }
    }

    LoopSubdivision(mesh);

    for(auto& vertex: mesh.VertexDataD())
    {
        Eigen::VectorXf& position = vertex.position;
        Eigen::Vector3f dual_position;
        dual_position << position(0), position(1), position(2);
        Eigen::MatrixXf sigma_inv(3, 3);
        for(uint k = 0; k < 6; k++)
        {
            uint x = (sqrt(1.0 + 8.0 * double(k)) - 1.0) / 2.0;
            uint y = k - x * (x + 1) / 2;

            sigma_inv(x, y) = position(3 + k);
            sigma_inv(y, x) = sigma_inv(x, y);
        }
        dual_position = sigma_inv.inverse() * dual_position;
        vertex.position[0] = dual_position(0);
        vertex.position[1] = dual_position(1);
        vertex.position[2] = dual_position(2);
    }
}

