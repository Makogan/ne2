#include "CpuImage.hpp"

/** @cond */
#include <string.h>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"
/** @endcond */

#include "Core/Log.hpp"
#include "Core/Gallery.hpp"

using namespace std;

// Static memembers ----------------------------------------------------------------------
uint CpuImage::_cpu_image_count = 0;

NECore::RawImageData CpuImage::GetImageData(CpuImage& image)
{
    return
    {
        image.GetWidth(),
        image.GetHeight(),
        1,
        image.GetChannelNum(),
        (void*)image.GetBuffer().data()
    };
}

// Instance functions --------------------------------------------------------------------
CpuImage::CpuImage(const void* data, int width, int height, int channel_num, int c_byte_size)
    : width(width)
    , height(height)
    , channel_num(channel_num)
    , component_byte_size(c_byte_size)
{
    if(data != nullptr)
        LoadBuffer(data, width, height, channel_num, c_byte_size);
    else
    {
        std::vector<std::byte> buffer(width * height * channel_num * c_byte_size);
        LoadBuffer(buffer.data(), width, height, channel_num, c_byte_size);
    }
}

CpuImage::CpuImage(const std::string& path)
{
    stbi_uc* img_data =
        stbi_load(path.c_str(), &width, &height, &channel_num, STBI_rgb_alpha);
    Assert(
        img_data != nullptr,
        "Failed to load texture image\n" + path + "\nPath is likely incorrect");
    const uint mandatory_channel_num = 4;
    LoadBuffer((void*)img_data, width, height, mandatory_channel_num);
    // The use of STBI_rgb_alpha guarantees a 4 channel image.
    channel_num = mandatory_channel_num;

    stbi_image_free(img_data);

    id = _cpu_image_count++;
}

void CpuImage::LoadBuffer(const void* bytes, int width, int height, int channel_num, int component_byte_size)
{
    size_t data_size = width * height * channel_num * component_byte_size;
    buffer.resize(data_size);
    memcpy(buffer.data(), bytes, data_size);
}

void CpuImage::SaveToPng(const std::string& path)
{
    // TODO(medium) add a check for the itnernal format, e.g. rgb vs bgr.
    Assert(buffer.size() % 4 == 0, "");
    for(uint i=0; i < buffer.size(); i += 4)
    {
        std::swap(buffer[i], buffer[i + 2]);
    }

    stbi_write_png(
        path.c_str(),
        width,
        height,
        channel_num,
        buffer.data(),
        width * channel_num);
}
