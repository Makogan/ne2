#pragma once

/** @cond */
#include <any>
#include <string>
#include <vector>
/** @endcond */

using uint = unsigned int;

namespace NECore {
struct RawImageData;
}

class CpuImage
{
  private:
    static uint _cpu_image_count;
    int width = 0;
    int height = 0;
    int channel_num = 0;
    int component_byte_size = 1;

    std::vector<std::byte> buffer;
    uint id = _cpu_image_count;

    void LoadBuffer(const void* buffer, int width, int height, int channel_num, int component_byte_size = 1);

  public:
    static NECore::RawImageData GetImageData(CpuImage& image);

    CpuImage() = default;
    CpuImage(const void* data, int width, int height, int channel_num, int component_byte_size = 1);
    CpuImage(const std::string& filepath);

    uint GetWidth() const { return width; }
    uint GetHeight() const { return height; }
    uint GetChannelNum() const { return channel_num; }
    const std::vector<std::byte>& GetBuffer() const { return buffer; }

    void SaveToPng(const std::string& path);
};

