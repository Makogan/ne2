#include "NumericIntegration.hpp"

using namespace std;

Eigen::Vector3f VerletIntegration(
    Eigen::Vector3f& prior_position,
    const Eigen::Vector3f& position,
    const Eigen::Vector3f& acceleration,
    const float prior_time_delta,
    const float current_time_delta)
{
    Eigen::Vector3f new_position = position +
        (position - prior_position) * (current_time_delta / prior_time_delta) +
        acceleration * (current_time_delta + prior_time_delta) * current_time_delta * 0.5;

    prior_position = position;

    return new_position;
}

