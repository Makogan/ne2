#include "SpringMass.hpp"

#include "NumericIntegration.hpp"

using namespace std;

namespace SM
{
//Eigen::Vector3f gravity(0, -9.81, 0);
Eigen::Vector3f gravity(0, 0, 0);

void UpdateSpringMassSystem(SpringMassSystem& system)
{
    std::vector<Eigen::Vector3f> forces(system.masses.size(), Eigen::Vector3f(0, 0, 0));
    for(auto& spring: system.springs)
    {
        const Mass& m1 = system.masses[spring.mass_1];
        const Mass& m2 = system.masses[spring.mass_2];

        const Eigen::Vector3f dir = (m1.position - m2.position);

        const float stretch = dir.norm() - spring.rest_length;

        const float stiffness = spring.stifness * system.stiffness_modifier;
        forces[spring.mass_1] += -stretch * dir.normalized() * stiffness;
        forces[spring.mass_2] += stretch * dir.normalized() * stiffness;
    }

    const std::chrono::steady_clock::time_point c_time =
        std::chrono::steady_clock::now();

    const float c_delta =  //
        std::chrono::duration_cast<std::chrono::microseconds>(
            c_time - system.last_time_stamp)
            .count() /
        1e6 * system.simulation_speed;

    for(uint i = 0; i < forces.size(); i++)
    {
        Mass& m = system.masses[i];
        const Eigen::Vector3f velocity =
            (m.position - m.prior_position) * (1.f / c_delta);
        forces[i] += gravity - velocity * system.damping;

        m.position = VerletIntegration(
            m.prior_position,
            m.position,
            !m.fixed * (forces[i] * 1.f / m.mass),
            system.last_time_delta,
            c_delta);
    }
    system.last_time_stamp = c_time;
    system.last_time_delta = c_delta;
}

NECore::RawMeshData SerializeMassSystem(SpringMassSystem& system)
{
    std::vector<float> result;
    for(auto& mass: system.masses)
    {
        result.push_back(mass.position.x());
        result.push_back(mass.position.y());
        result.push_back(mass.position.z());

        result.push_back(0);
        result.push_back(0);

        result.push_back(0);
        result.push_back(1);
        result.push_back(0);
    }

    std::vector<std::byte> buffer(result.size() * sizeof(float));
    memcpy(buffer.data(), result.data(), buffer.size());

    return {{buffer}, system.surface_connectivity};
}
}  // namespace SM

