#pragma once

#include "Animation.hpp"

/** @cond */
#include "Eigen/Dense"
/** @endcond */

#include "Geometry/Math/EigenHelpers.hpp"
#include "Geometry/HalfEdge/MeshConstraints.hpp"
#include "Core/Core.hpp"

// TODO(medium): refactor the concepts in this file to make things more readable.

#define IMPLEMENT_ELEMENT_GETTER(Name, name)                                    \
    auto& Get##Name(uint i) requires MC::Name##ArrayIsAccessible<MeshType>      \
    {                                                                           \
        if constexpr(MC::Implements##Name##sArrayIndexGetter<MeshType>)         \
        {                                                                       \
            Assert(i < GetSize(), "Out of bounds access for " #Name);           \
            return mesh.Name(i);                                                \
        }                                                                       \
        else                                                                    \
        {                                                                       \
            Assert(i < mesh.name##s.size(), "Out of bounds access for " #Name); \
            return mesh.name##s[i];                                             \
        }                                                                       \
    }                                                                           \
    auto& Get##Name(uint i) requires(!MC::Name##ArrayIsAccessible<MeshType>)    \
    {                                                                           \
        Assert(i < name##s.size(), "Out of bounds access for " #Name);          \
        return name##s[i];                                                      \
    }

/**
 * @brief Macro to create templated functions based on how the member of a function is
 * accessible.
 *
 * Bascally either it can be accessed directly from the underlying type or as an array
 * in the wrapping structure and a different funciton is needed for each case.
 *
 * Example usage: IMPLEMENT_ELEMENT_GETTER(Normal, normal), guarantees the existence of the getter
 * GetNormal(uint i)
 *
 */

namespace Animation
{
// clang-format off
template<typename T, bool> struct OptionalField;
template<typename T> struct OptionalField<T, true> {T value;};
template<typename T> struct OptionalField<T, false>{};
// clang-format on
template<typename T, bool B> using OptionalVector = OptionalField<std::vector<T>, B>;

template<typename T>
concept HasNecessaryAccessors = MC::SizeIsAccessible<T> &&
    MC::PositionArrayIsAccessible<T> && MC::IndicesArrayIsAccessible<T>;

template<typename MeshType>
requires(HasNecessaryAccessors<MeshType>)  //
    class AnimationMesh
{
    std::string name;
    MeshType mesh;

    Skin skin;                       // (Optional) skin information.
    RigidAnimation rigid_animation;  // (Optional) Affects the whole object.

    // Each column is actually a matrix representing the morph targets, in which
    // every 3 floats is one offset vector.
    Eigen::Matrix4f model_matrix = Eigen::Matrix4f::Identity();

    std::vector<float> morph_weights;            // (Optional)
    std::vector<Eigen::Vector4f> morph_targets;  // (Optional)

    OptionalVector<Eigen::Vector3f, !MC::NormalArrayIsAccessible<MeshType>> normals;
    OptionalVector<Eigen::Vector2f, !MC::UVArrayIsAccessible<MeshType>> uvs;
    OptionalVector<Eigen::Vector4f, !MC::TangentArrayIsAccessible<MeshType>> tangents;
    OptionalVector<Eigen::Vector4i, !MC::JointArrayIsAccessible<MeshType>> joints;
    OptionalVector<Eigen::Vector4f, !MC::WeightArrayIsAccessible<MeshType>> weights;

  public:
    float time = 0;

    explicit AnimationMesh() = default;
    explicit AnimationMesh(

        const MeshType& mesh,
        const Skin& skin,
        const RigidAnimation& animation,
        const Eigen::Matrix4f& model_matrix,
        const std::vector<float>& morph_weights,
        const std::vector<Eigen::Vector4f>& morph_targets)
        : mesh(mesh)
        , skin(skin)
        , rigid_animation(animation)
        , model_matrix(model_matrix)
        , morph_weights(morph_weights)
        , morph_targets(morph_targets)
    {
        if constexpr(!MC::NormalArrayIsAccessible<MeshType>)
            normals.value.resize(GetSize(), {0, 1, 0});
        if constexpr(!MC::UVArrayIsAccessible<MeshType>)
            uvs.value.resize(GetSize(), {0, 0});
        if constexpr(!MC::TangentArrayIsAccessible<MeshType>)
            tangents.value.resize(GetSize(), {0, 0, 0});
        if constexpr(!MC::JointArrayIsAccessible<MeshType>)
            joints.value.resize(GetSize(), {0, 0, 0, 0});
        if constexpr(!MC::WeightArrayIsAccessible<MeshType>)
            weights.value.resize(GetSize(), {1, 0, 0, 0});

        // clang-format off
        Warn(
                MC::NormalArrayIsAccessible<MeshType> &&
                MC::UVArrayIsAccessible<MeshType> &&
                MC::TangentArrayIsAccessible<MeshType> &&
                MC::JointArrayIsAccessible<MeshType> &&
                MC::WeightArrayIsAccessible<MeshType>,
            "At least one of the needed accessors is not defined in the underlying "
            "type.\n"
            "Found Normals: {0}\n"
            "Found UVs: {1}\n"
            "Found Tangets: {2}\n"
            "Found Joints: {3}\n"
            "Found Weights: {4}\n",
            std::to_string(MC::NormalArrayIsAccessible<MeshType>),
            std::to_string(MC::UVArrayIsAccessible<MeshType>),
            std::to_string(MC::TangentArrayIsAccessible<MeshType>),
            std::to_string(MC::JointArrayIsAccessible<MeshType>),
            std::to_string(MC::WeightArrayIsAccessible<MeshType>));
        // clang-format on
    }

    auto& GetPosition(uint i);
    auto& GetIndices();
    size_t GetSize();
    IMPLEMENT_ELEMENT_GETTER(Normal, normal);
    IMPLEMENT_ELEMENT_GETTER(UV, uv);
    IMPLEMENT_ELEMENT_GETTER(Tangent, tangent);
    IMPLEMENT_ELEMENT_GETTER(Joint, joint);
    IMPLEMENT_ELEMENT_GETTER(Weight, weight);

    Eigen::Matrix4f GetRigidAnimationMatrix();

    const Eigen::Matrix4f& GetModelMatrix() const { return model_matrix; };
    const Skin& GetSkin() const { return skin; };
    void SwitchSkinAnimation(uint i)
    {
        skin.active_animation_cycle = i % skin.animations.size();
    };

    const std::vector<Eigen::Vector4i>& GetJoints() requires(
        MC::HasJointsArray<MeshType> || MC::ImplementsJointsArrayDirectGetter<MeshType> ||
        !MC::JointArrayIsAccessible<MeshType>);
    const std::vector<Eigen::Vector4f>& GetWeights() requires(
        MC::HasWeightsArray<MeshType> ||
        MC::ImplementsWeightsArrayDirectGetter<MeshType> ||
        !MC::WeightArrayIsAccessible<MeshType>);

    const std::vector<Eigen::Vector4f>& GetMorphTargets() { return morph_targets; }
    const std::vector<float>& GetMorphWeights() { return morph_weights; }
    uint GetTargetCount()
    {
        return morph_weights.size() / rigid_animation.morph_key_times.size();
    }

    std::pair<float, uint> GetMorphWeightAndIndex(float t)
    {
        const auto& key_times = rigid_animation.morph_key_times;
        t = fmod(t, key_times.back());
        uint index = 0;
        while(!(t >= key_times[index] && t <= key_times[index + 1]) &&
              index < key_times.size() - 1)
        {
            index++;
        }

        const float v =
            (t - key_times[index]) / (key_times[index + 1] - key_times[index]);
        return {v, index};
    }

    static MC::ByteBuffers GetGeometryData(AnimationMesh<MeshType>& mesh);
};

template<typename MeshType>
requires(HasNecessaryAccessors<MeshType>)  //
    auto& AnimationMesh<MeshType>::GetPosition(uint i)
{
    Assert(i < GetSize(), "Out of bounds access for Position");
    if constexpr(MC::ImplementsPositionsArrayIndexGetter<MeshType>)
    {
        return mesh.Position(i);
    }
    else
    {
        return mesh.positions[i];
    }
}

template<typename MeshType>
requires(HasNecessaryAccessors<MeshType>)  //
    auto& AnimationMesh<MeshType>::GetIndices()
{
    if constexpr(MC::ImplementsIndicesArrayIndexGetter<MeshType>)
    {
        return mesh.Indices();
    }
    else
    {
        return mesh.indices;
    }
}

template<typename MeshType>
requires(HasNecessaryAccessors<MeshType>)  //
    size_t AnimationMesh<MeshType>::GetSize()
{
    if constexpr(MC::ImplementsSizeGetter<MeshType>) { return mesh.size(); }
    else { return mesh.size; }
}

template<typename MeshType>
requires(HasNecessaryAccessors<MeshType>)  //
    const std::vector<Eigen::Vector4i>& AnimationMesh<MeshType>::GetJoints()
    requires(
        MC::HasJointsArray<MeshType> || MC::ImplementsJointsArrayDirectGetter<MeshType> ||
        !MC::JointArrayIsAccessible<MeshType>)
{
    if constexpr(MC::HasJointsArray<MeshType>) { return mesh.joints; }
    if constexpr(MC::ImplementsJointsArrayDirectGetter<MeshType>)
    {
        return mesh.Joints();
    }
    if constexpr(!MC::JointArrayIsAccessible<MeshType>) { return joints.value; }
}

template<typename MeshType>
requires(HasNecessaryAccessors<MeshType>)  //
    const std::vector<Eigen::Vector4f>& AnimationMesh<MeshType>::GetWeights() requires(
        MC::HasWeightsArray<MeshType> ||
        MC::ImplementsWeightsArrayDirectGetter<MeshType> ||
        !MC::WeightArrayIsAccessible<MeshType>)
{
    if constexpr(MC::HasWeightsArray<MeshType>) { return mesh.weights; }
    if constexpr(MC::ImplementsWeightsArrayDirectGetter<MeshType>)
    {
        return mesh.Weights();
    }
    if constexpr(!MC::WeightArrayIsAccessible<MeshType>) { return weights.value; }
}

template<typename MeshType>
requires(HasNecessaryAccessors<MeshType>)  //
    Eigen::Matrix4f AnimationMesh<MeshType>::GetRigidAnimationMatrix()
{
    Eigen::Vector3f translation = rigid_animation.SampleTranslation(time);
    Eigen::Quaternionf rotation = rigid_animation.SampleRotation(time);
    Eigen::Vector3f scale = rigid_animation.SampleScaling(time);

    return BuildTRSMatrix(translation, rotation, scale);
}

// Static memebers -----------------------------------------------------------------------
template<typename MeshType> requires(HasNecessaryAccessors<MeshType>)
MC::ByteBuffers AnimationMesh<MeshType>::GetGeometryData(AnimationMesh<MeshType>& mesh)
{
    MC::ByteBuffers attributes = {};
    attributes.first.reserve(mesh.GetSize());
    attributes.first.push_back({});

    Assert(mesh.GetSize() > 0, "An empty size variable suggests an error in the data.");

    for(uint i = 0; i < mesh.GetSize(); i++)
    {
        SerializeMember(attributes.first[0], mesh.GetPosition(i));
        SerializeMember(attributes.first[0], mesh.GetUV(i));
        SerializeMember(attributes.first[0], mesh.GetNormal(i));
    }

    attributes.second = mesh.GetIndices();

    return attributes;
}
}  // namespace Animation

