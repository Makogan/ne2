#pragma once

#include "AnimationMesh.hpp"
#include "GltfLib.hpp"

namespace Animation
{
std::vector<AnimationMesh<GltfLib::GltfModelData>> LoadAnimatedMeshFromGltf(
    const GltfLib::Model& model);

GltfLib::GltfModelData LoadStaticMeshFromGltf(const GltfLib::Model& g_model);

std::vector<CpuImage> LoadImagesFromGltf(const GltfLib::Model& g_model);

MC::ByteBuffers SerializeAttributeMap(const GltfLib::GltfModelData& attributes);
}  //namespace Animation

