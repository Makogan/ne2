#include "Animation.hpp"

using namespace std;

namespace Animation
{
Eigen::Vector3f RigidAnimation::SampleTranslation(float t) const
{
    if(translations.empty()) return Eigen::Vector3f(0, 0, 0);
    t = fmod(t, translation_key_times.back());

    uint index = 0;
    const uint key_time_num = translation_key_times.size();
    float key_time1 = translation_key_times[0];
    float key_time2 = translation_key_times[1];
    while(!(t >= key_time1 && t < key_time2))
    {
        index = (index + 1) % key_time_num;
        key_time1 = translation_key_times[index] >= translation_key_times.back() ?
             0.f : translation_key_times[index];
        key_time2 = translation_key_times[(index + 1) % key_time_num];
    }

    float v = (t - key_time1) / (key_time2 - key_time1);

    return (1.f - v) * translations[index] + v * translations[(index + 1) % key_time_num];
}

Eigen::Quaternionf RigidAnimation::SampleRotation(const float time) const
{
    if(rotations.empty()) return Eigen::Quaternionf::Identity();
    const float t = fmod(time, rotation_key_times.back());

    assert(rotation_key_times.size() > 1);
    assert(rotation_key_times.size() == rotations.size());

    uint index = 0;
    const uint key_time_num = rotation_key_times.size();
    float key_time1 = rotation_key_times[0];
    float key_time2 = rotation_key_times[1];
    while(!(t >= key_time1 && t < key_time2))
    {
        index = (index + 1) % key_time_num;
        key_time1 = rotation_key_times[index] >= rotation_key_times.back() ?
            0.f : rotation_key_times[index];
        key_time2 = rotation_key_times[(index + 1) % key_time_num];
    }

    float v = (t - key_time1) / (key_time2 - key_time1);

    return rotations[index].slerp(v, rotations[(index + 1) % key_time_num]).normalized();
}

Eigen::Vector3f RigidAnimation::SampleScaling(const float time) const
{
    if(scales.empty()) return Eigen::Vector3f(1, 1, 1);
    const float t = fmod(time, scale_key_times.back());

    assert(scale_key_times.size() > 1);
    assert(scale_key_times.size() == scales.size());

    uint index = 0;
    const uint key_time_num = scale_key_times.size();
    float key_time1 = scale_key_times[0];
    float key_time2 = scale_key_times[1];
    while(!(t >= key_time1 && t < key_time2))
    {
        index = (index + 1) % key_time_num;
        key_time1 = scale_key_times[index] >= scale_key_times.back() ?
            0.f : scale_key_times[index];
        key_time2 = scale_key_times[(index + 1) % key_time_num];
    }

    float v = (t - key_time1) / (key_time2 - key_time1);

    return (1.f - v) * scales[index] + v * scales[(index + 1) % key_time_num];
}

std::vector<Eigen::Matrix4f> GetGlobalJointMatrices(
    const std::vector<Eigen::Matrix4f>& animation_matrices,
    const std::vector<int>& skeleton)
{
    std::vector<Eigen::Matrix4f> skeleton_matrices(skeleton.size());
    skeleton_matrices[0] = Eigen::Matrix4f::Identity();

    for(uint child = 0; child < skeleton.size(); child++)
    {
        const int parent = skeleton[child];
        skeleton_matrices[child] = skeleton_matrices[parent] * animation_matrices[child];
    }

    return skeleton_matrices;
}

std::vector<Eigen::Matrix4f> SampleSkinAnimation(const Skin& skin, float t)
{
    if(skin.animations.empty())
    { return GetGlobalJointMatrices(skin.joint_matrices, skin.skeleton); }
    std::vector<Eigen::Matrix4f> mats(skin.skeleton.size());

    uint i = 0;
    for(const auto& joint_animation: skin.animations[skin.active_animation_cycle])
    {
        Eigen::Quaternionf rotation = joint_animation.SampleRotation(t);
        Eigen::Vector3f translation = joint_animation.SampleTranslation(t);
        Eigen::Vector3f scale = joint_animation.SampleScaling(t);

        Eigen::Matrix4f t_mat = skin.joint_matrices[i];

        t_mat.block(0, 0, 3, 3) = (rotation).toRotationMatrix();

        t_mat.row(0).head<3>() *= scale(0);
        t_mat.row(1).head<3>() *= scale(1);
        t_mat.row(2).head<3>() *= scale(2);

        t_mat(0, 3) = translation[0];
        t_mat(1, 3) = translation[1];
        t_mat(2, 3) = translation[2];

        mats[i] = t_mat;
        i++;
    }

    return GetGlobalJointMatrices(mats, skin.skeleton);
}

}  // namespace Animation

