#include "GltfLoaders.hpp"

#include "MiscUtils/VectorManipulation.hpp"

using namespace std;

namespace Animation
{
GltfLib::GltfModelData LoadPrimitives(
    const tinygltf::Model& model, const tinygltf::Node& node)
{
    const uint mesh_id = node.mesh;
    GltfLib::GltfModelData attribute = {};
    uint count = 0;
    vector<uint> per_vertex_id;
    uint total_index_offset = 0;
    for(const auto& primitive: model.meshes[mesh_id].primitives)
    {
        GltfLib::GltfModelData primitive_attributes =
            GltfLib::ExtractPrimitive(model, mesh_id, count++);

        const size_t v_count = primitive_attributes.positions.size();
        Assert(v_count > 0, "");
        if(primitive_attributes.uvs.empty())
            primitive_attributes.uvs.resize(v_count, {0, 0});

        if(primitive_attributes.normals.empty())
            primitive_attributes.normals.resize(v_count, {0, 0, 0});

        // TODO (low): change this and load full materials rather than just the texture.
        const int material = primitive.material;
        int texture = -1;
        if(material > -1)
        {
            texture =
                model.materials[material].pbrMetallicRoughness.baseColorTexture.index;
        }

        per_vertex_id.insert(
            per_vertex_id.end(), primitive_attributes.positions.size(), texture);

        AppendVectors(attribute.positions, primitive_attributes.positions);
        AppendVectors(attribute.uvs, primitive_attributes.uvs);
        AppendVectors(attribute.normals, primitive_attributes.normals);
        AppendVectors(attribute.tangents, primitive_attributes.tangents);
        AppendVectors(attribute.joints, primitive_attributes.joints);
        AppendVectors(attribute.weights, primitive_attributes.weights);

        std::transform(
            primitive_attributes.indices.begin(),
            primitive_attributes.indices.end(),
            primitive_attributes.indices.begin(),
            [total_index_offset](const uint& i) { return i + total_index_offset; });
        AppendVectors(attribute.indices, primitive_attributes.indices);
        total_index_offset = attribute.positions.size();
        attribute.size = total_index_offset;
    }
    attribute.vertex_material_ids = per_vertex_id;

    if(attribute.joints.size() == 0)
        attribute.joints.resize(total_index_offset, {0, 0, 0, 0});
    if(attribute.weights.size() == 0)
        attribute.weights.resize(total_index_offset, {1, 0, 0, 0});

    return attribute;
}

std::vector<Eigen::Vector4f> LoadMorphTargets(
    const tinygltf::Model& model, const tinygltf::Node& node)
{
    const uint mesh_id = node.mesh;
    // TODO (medium): generalize this to handle multiple primitives.
    if(model.meshes[mesh_id].primitives.size() != 1) return {};

    const uint primitive_id = 0;
    return GltfLib::ExtractMorphTargets(model, mesh_id, primitive_id);
}

// TODO(low): This function should not exist.
int GetNodeAnimationID(const tinygltf::Model& model, uint node_id)
{
    int selected_id = -1;
    int anim_id = 0;
    for(const auto& animation: model.animations)
    {
        for(const auto& channel: animation.channels)
        {
            if((uint)channel.target_node == node_id)
            {
                Assert(selected_id == -1 || selected_id == anim_id, "");
                selected_id = anim_id;
                return selected_id;
            }
        }
        anim_id++;
    }
    return selected_id;
}

std::vector<AnimationMesh<GltfLib::GltfModelData>> LoadAnimatedMeshFromGltf(
    const GltfLib::Model& g_model)
{
    using Attributes = GltfLib::GltfModelData;
    using Mesh = AnimationMesh<Attributes>;
    using namespace Eigen;
    using namespace GltfLib;
    using namespace Animation;

    auto& model = g_model.model;

    const vector<int> parent_graph = GetParentGraph(model);

    std::vector<AnimationMesh<GltfModelData>> meshes(model.meshes.size());

    uint node_id = 0;
    for(const auto& node: model.nodes)
    {
        if(node.mesh < 0) continue;
        const int animation_id = GetNodeAnimationID(model, node_id);

        const Attributes merged_primitives = LoadPrimitives(model, node);
        const Skin skin = ExtractSkin(model, node.skin);
        const Matrix4f model_m = GetTransformFromRoot(model, parent_graph, node_id);
        const std::vector<Eigen::Vector4f> morph_targets = LoadMorphTargets(model, node);
        const auto [animation, weights] = ExtractAnimation(model, animation_id, 0);

        meshes[node_id++] =
            Mesh(merged_primitives, skin, animation, model_m, weights, morph_targets);
    }
    return meshes;
}

GltfLib::GltfModelData LoadStaticMeshFromGltf(const GltfLib::Model& model)
{
    Assert(model.model.nodes.size() == 1, "");
    return LoadPrimitives(model.model, model.model.nodes[0]);
}

std::vector<CpuImage> LoadImagesFromGltf(const GltfLib::Model& g_model)
{
    auto& model = g_model.model;
    std::vector<CpuImage> images;
    for(uint i = 0; i < model.images.size(); i++)
    {
        images.push_back(GltfLib::ExtractImage(model, i));
    }
    return images;
}

MC::ByteBuffers SerializeAttributeMap(const GltfLib::GltfModelData& input_attributes)
{
    MC::ByteBuffers attributes = {};
    attributes.first.push_back({});

    for(uint i = 0; i < input_attributes.size; i++)
    {
        SerializeMember(attributes.first[0], input_attributes.positions[i]);
        SerializeMember(attributes.first[0], input_attributes.uvs[i]);
        SerializeMember(attributes.first[0], input_attributes.normals[i]);
    }

    attributes.first.push_back(SerializeVector(input_attributes.vertex_material_ids));
    attributes.second = input_attributes.indices;

    return attributes;
}
}  // namespace Animation

