//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file NumericIntegration.hpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2021-07-05
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#pragma once

/** @cond */
#include <iostream>

#include "Eigen/Dense"
/** @endcond */

Eigen::Vector3f VerletIntegration(
    Eigen::Vector3f& prior_position,
    const Eigen::Vector3f& position,
    const Eigen::Vector3f& acceleration,
    const float prior_time_delta = 0.1,
    const float current_time_delta = 0.1);

