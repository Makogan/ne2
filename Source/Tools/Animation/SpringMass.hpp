#pragma once

/** @cond */
#include <any>
#include <chrono>
#include <vector>

#include "Eigen/Dense"
/** @endcond */

#include "Core/Gallery.hpp"

namespace SM
{
struct Mass
{
    bool fixed = false;
    float mass = 1;
    Eigen::Vector3f position;
    Eigen::Vector3f prior_position = Eigen::Vector3f(0, 0, 0);
};

struct Spring
{
    uint mass_1;
    uint mass_2;

    float stifness = 1.f;
    float rest_length;
    bool compression_only = false;

    static void Connect(Spring& s1, const Spring& s2) { s1.mass_2 = s2.mass_1; }
};

struct SpringMassSystem
{
    std::vector<Mass> masses;
    std::vector<Spring> springs;
    std::vector<uint> surface_connectivity;
    float damping = 0.5f;
    float simulation_speed = 3.f;
    float last_time_delta = 1e-6;
    float stiffness_modifier = 1.f;
    std::chrono::steady_clock::time_point last_time_stamp =
        std::chrono::steady_clock::now();
};

void UpdateSpringMassSystem(SpringMassSystem& system);

NECore::RawMeshData SerializeMassSystem(SpringMassSystem& system);
}  // namespace SM

