#define TINYGLTF_IMPLEMENTATION
#define TINYGLTF_NOEXCEPTION
#include "GltfLib.hpp"

/** @cond */
#include <algorithm>
#include <set>
/** @endcond */

#include "MiscUtils/VectorManipulation.hpp"
#include "Geometry/Math/NumberTheory.hpp"

using namespace std;
using ushort = unsigned short;

namespace GltfLib
{
namespace
{
// clang-format off
template<typename T> int ComponentCode()
{
    Assert(false, "Component not recongized");
    return -1;
}
template<> constexpr int ComponentCode<float>() { return TINYGLTF_COMPONENT_TYPE_FLOAT; }
template<> constexpr int ComponentCode<double>() { return TINYGLTF_COMPONENT_TYPE_DOUBLE; }
template<> constexpr int ComponentCode<char>() { return TINYGLTF_COMPONENT_TYPE_BYTE; }
template<> constexpr int ComponentCode<unsigned char>() { return TINYGLTF_COMPONENT_TYPE_UNSIGNED_BYTE; }
template<> constexpr int ComponentCode<short>() { return TINYGLTF_COMPONENT_TYPE_SHORT; }
template<> constexpr int ComponentCode<unsigned short>() { return TINYGLTF_COMPONENT_TYPE_UNSIGNED_SHORT; }
template<> constexpr int ComponentCode<int>() { return TINYGLTF_COMPONENT_TYPE_INT; }
template<> constexpr int ComponentCode<unsigned int>() { return TINYGLTF_COMPONENT_TYPE_UNSIGNED_INT; }
template<> constexpr int ComponentCode<Eigen::Quaternionf>() { return TINYGLTF_COMPONENT_TYPE_FLOAT; }
template<> constexpr int ComponentCode<Eigen::Vector2f>() { return TINYGLTF_COMPONENT_TYPE_FLOAT; }
template<> constexpr int ComponentCode<Eigen::Vector3f>() { return TINYGLTF_COMPONENT_TYPE_FLOAT; }
template<> constexpr int ComponentCode<Eigen::Vector4f>() { return TINYGLTF_COMPONENT_TYPE_FLOAT; }
template<> constexpr int ComponentCode<Eigen::Vector4s>(){ return TINYGLTF_COMPONENT_TYPE_UNSIGNED_SHORT; }
template<> constexpr int ComponentCode<Eigen::Vector4c>(){ return TINYGLTF_COMPONENT_TYPE_UNSIGNED_BYTE; }
template<> constexpr int ComponentCode<Eigen::Matrix2f>() { return TINYGLTF_COMPONENT_TYPE_FLOAT; }
template<> constexpr int ComponentCode<Eigen::Matrix3f>() { return TINYGLTF_COMPONENT_TYPE_FLOAT; }
template<> constexpr int ComponentCode<Eigen::Matrix4f>() { return TINYGLTF_COMPONENT_TYPE_FLOAT; }
// clang-format on

std::string ComponentCodeToString(int code)
{
    switch(code)
    {
        case TINYGLTF_COMPONENT_TYPE_FLOAT:
			return "TINYGLTF_COMPONENT_TYPE_FLOAT";
        case TINYGLTF_COMPONENT_TYPE_DOUBLE:
			return "TINYGLTF_COMPONENT_TYPE_DOUBLE";
        case TINYGLTF_COMPONENT_TYPE_BYTE:
			return "TINYGLTF_COMPONENT_TYPE_BYTE";
        case TINYGLTF_COMPONENT_TYPE_UNSIGNED_BYTE:
			return "TINYGLTF_COMPONENT_TYPE_UNSIGNED_BYTE";
        case TINYGLTF_COMPONENT_TYPE_SHORT:
			return "TINYGLTF_COMPONENT_TYPE_SHORT";
        case TINYGLTF_COMPONENT_TYPE_UNSIGNED_SHORT:
			return "TINYGLTF_COMPONENT_TYPE_UNSIGNED_SHORT";
        case TINYGLTF_COMPONENT_TYPE_INT:
			return "TINYGLTF_COMPONENT_TYPE_INT";
        case TINYGLTF_COMPONENT_TYPE_UNSIGNED_INT:
			return "TINYGLTF_COMPONENT_TYPE_UNSIGNED_INT";
        default:
            Assert(false, "");
            return "";
    }
}

// TODO (low): add to this list as cases are encountered
template<typename T> int TypeCode()
{
    Log::RecordLogError("Type not recongized");
    return -1;
}
template<> constexpr int TypeCode<unsigned short>() { return TINYGLTF_TYPE_SCALAR; }
template<> constexpr int TypeCode<unsigned int>() { return TINYGLTF_TYPE_SCALAR; }
template<> constexpr int TypeCode<short>() { return TINYGLTF_TYPE_SCALAR; }
template<> constexpr int TypeCode<float>() { return TINYGLTF_TYPE_SCALAR; }
template<> constexpr int TypeCode<Eigen::Vector2f>() { return TINYGLTF_TYPE_VEC2; }
template<> constexpr int TypeCode<Eigen::Vector3f>() { return TINYGLTF_TYPE_VEC3; }
template<> constexpr int TypeCode<Eigen::Vector4f>() { return TINYGLTF_TYPE_VEC4; }
template<> constexpr int TypeCode<Eigen::Matrix2f>() { return TINYGLTF_TYPE_MAT2; }
template<> constexpr int TypeCode<Eigen::Matrix3f>() { return TINYGLTF_TYPE_MAT3; }
template<> constexpr int TypeCode<Eigen::Matrix4f>() { return TINYGLTF_TYPE_MAT4; }
template<> constexpr int TypeCode<Eigen::Vector4s>() { return TINYGLTF_TYPE_VEC4; }
template<> constexpr int TypeCode<Eigen::Vector4c>() { return TINYGLTF_TYPE_VEC4; }
template<> constexpr int TypeCode<Eigen::Quaternionf>() { return TINYGLTF_TYPE_VEC4; }
template<> constexpr int TypeCode<std::vector<float>>() { return TINYGLTF_TYPE_VECTOR; }

void CopySparseBuffer(
    void* dest,
    const void* src,
    const size_t element_count,
    const size_t stride,
    const size_t type_size)
{
    Assert(stride >= type_size, "");
    // Typecast src and dest addresses to (char *)
    unsigned char* csrc = (unsigned char*)src;
    unsigned char* cdest = (unsigned char*)dest;
    // Iterate over the total number of elements to copy
    for(uint i = 0; i < element_count; i++)
        // Copy each byte of the element. Since the stride could be different from the
        // type size (in the case of padding bytes for example) the right access
        // should skip over any interleaved data, that's why we use the stride.
        for(uint j = 0; j < type_size; j++)
            *(cdest + i * type_size + j) = *(csrc + i * stride + j);
}

template<typename T>
std::vector<T> ExtractDataFromAccessor(
    const tinygltf::Model& model, const int accessor_index)
{
    const int buffer_view_index = model.accessors[accessor_index].bufferView;
    const int accessor_offset = model.accessors[accessor_index].byteOffset;
    const int element_num = model.accessors[accessor_index].count;

    const int buffer_index = model.bufferViews[buffer_view_index].buffer;
    const int buffer_offset = model.bufferViews[buffer_view_index].byteOffset;
    const int buffer_stride = model.bufferViews[buffer_view_index].byteStride;

    const std::vector<unsigned char> data = model.buffers[buffer_index].data;

    Assert(
        model.accessors[accessor_index].componentType == ComponentCode<T>(),
        "Extracting type {0}. With component code {1}, gltf file has {2}.",
        Log::Demangle(typeid(T).name()),
        ComponentCodeToString(ComponentCode<T>()),
        ComponentCodeToString(model.accessors[accessor_index].componentType));
    Assert(model.accessors[accessor_index].type == TypeCode<T>(), "");
    // Size in bytes of a single element (e.g. 12 for a vec3 of floats)
    const int type_size = sizeof(T);
    Assert(
        buffer_stride == 0 || (long unsigned int)buffer_stride >= sizeof(T),
        "It doesn't make sense for a positive buffer "
        "stride to be less than the type size");
    Assert(
        (long unsigned int)element_num * type_size <= model.bufferViews[buffer_view_index].byteLength, "");
    const size_t stride = std::max(buffer_stride, type_size);

    std::vector<T> holder(element_num);
    CopySparseBuffer(
        holder.data(),
        data.data() + buffer_offset + accessor_offset,
        element_num,
        stride,
        type_size);

    return holder;
}

int AccessorElmentCount(const tinygltf::Model& model, const int accessor_index)
{
    return model.accessors[accessor_index].count;
}

int MeshPositionAccessorIndex(const tinygltf::Model& model, const int mesh_index)
{
    Assert(model.meshes[mesh_index].primitives.size() == 1, "");
    return model.meshes[mesh_index].primitives[0].attributes.at("POSITION");
}

}  // namespace

GltfLib::Model LoadModel(const std::string& path)
{
    tinygltf::Model model;
    tinygltf::TinyGLTF loader;
    std::string error;
    std::string warning;

    bool is_fine = loader.LoadASCIIFromFile(&model, &error, &warning, path);

    Warn(warning.empty(), warning);
    Assert(error.empty(), error);
    Assert(is_fine, "Failed to parse glTF");

    return {model};
}

GltfModelData ExtractPrimitive(
    const tinygltf::Model& model, uint mesh_id, uint primitive_id)
{
    const tinygltf::Mesh& mesh = model.meshes[mesh_id];
    GltfModelData attribute_map = {};
    // Extract all the attributes;
    for(auto& [attribute, index]: mesh.primitives[primitive_id].attributes)
    {
        if(attribute == "POSITION")
        {
            attribute_map.positions =
                ExtractDataFromAccessor<Eigen::Vector3f>(model, index);
        }
        if(attribute == "TEXCOORD_0")
        {
            attribute_map.uvs =  //
                ExtractDataFromAccessor<Eigen::Vector2f>(model, index);
        }
        if(attribute == "NORMAL")
        {
            attribute_map.normals =
                ExtractDataFromAccessor<Eigen::Vector3f>(model, index);
        }
        if(attribute == "TANGENT")
        {
            attribute_map.tangents =
                ExtractDataFromAccessor<Eigen::Vector4f>(model, index);
        }
        if(attribute == "JOINTS_0")
        {
            vector<Eigen::Vector4s> short_vecs;
            if(model.accessors[index].componentType == ComponentCode<unsigned short>())
            {
                short_vecs = ExtractDataFromAccessor<Eigen::Vector4s>(model, index);
            }
            else if(model.accessors[index].componentType == ComponentCode<unsigned char>())
            {
                auto byte_vecs =
                    ExtractDataFromAccessor<Eigen::Vector4c>(model, index);
                short_vecs.resize(byte_vecs.size());

                for(uint i=0; i < byte_vecs.size(); i++)
                {
                    const auto& v = byte_vecs[i];
                    short_vecs[i] = {
                        ushort(v[0]), ushort(v[1]), ushort(v[2]), ushort(v[3]) };
                }
            }
            else
            {
                Assert(
                    false,
                    "Joint data type is {0}, which is not supported.",
                    ComponentCodeToString(model.accessors[index].componentType));
            }

            for(const auto& v: short_vecs)
                attribute_map.joints.push_back({
                    v[0],
                    v[1],
                    v[2],
                    v[3],
                });
        }
        if(attribute == "WEIGHTS_0")
        {
            attribute_map.weights =
                ExtractDataFromAccessor<Eigen::Vector4f>(model, index);
        }
    }
    Assert(!attribute_map.positions.empty(), "");
    attribute_map.size = attribute_map.positions.size();

    // Create the index connectivity array.
    const int indices_index = mesh.primitives[primitive_id].indices;
    if(indices_index < 0)
    {
        attribute_map.indices.resize(attribute_map.positions.size());
        std::iota(attribute_map.indices.begin(), attribute_map.indices.end(), 0);
        return attribute_map;
    }

    // The interface should be uniform, so we need to assert that values are stored as
    // integers and never as shorts.
    if(model.accessors[indices_index].componentType == ComponentCode<unsigned short>())
    {
        vector<unsigned short> shorts =
            ExtractDataFromAccessor<unsigned short>(model, indices_index);

        attribute_map.indices.resize(shorts.size());
        std::transform(
            attribute_map.indices.begin(),
            attribute_map.indices.end(),
            shorts.begin(),
            attribute_map.indices.begin(),
            [](const uint& i, const unsigned short& s) { return uint(s); });
    }
    else
    {
        attribute_map.indices =
            ExtractDataFromAccessor<unsigned int>(model, indices_index);
    }

    return attribute_map;
}

std::vector<Eigen::Vector4f> ExtractMorphTargets(
    const tinygltf::Model& model, uint mesh_id, uint primitive_id)
{
    Assert(model.meshes[mesh_id].primitives.size() == 1, "");

    const tinygltf::Primitive& primitive = model.meshes[mesh_id].primitives[primitive_id];
    const int total_position_count =
        AccessorElmentCount(model, MeshPositionAccessorIndex(model, primitive_id));
    const int target_count = primitive.targets.size();
    std::vector<Eigen::Vector4f> position_targets(total_position_count * target_count);

    uint count = 0;
    for(auto& morph_target_map: primitive.targets)
    {
        int index = morph_target_map.at("POSITION");

        vector<Eigen::Vector3f> offsets =
            ExtractDataFromAccessor<Eigen::Vector3f>(model, index);

        const int target_offset = count * total_position_count;
        Assert(
            target_offset + offsets.size() <= position_targets.size(),
            "{0}, {1}",
            to_string(target_offset + offsets.size()),
            to_string(position_targets.size()));
        for(uint i = 0; i < offsets.size(); i++)
        {
            const auto& offset = offsets[i];
            position_targets[target_offset + i] = {offset.x(), offset.y(), offset.z(), 0};
        }

        count++;
    }

    return position_targets;
}

void LoadAnimation(
    const tinygltf::Model& model,
    const int animation_id,
    const int channel_id,
    Animation::RigidAnimation& animation)
{
    if(animation_id < 0) return;
    Assert(model.animations.size() >= 1, "");
    Assert(model.animations[animation_id].samplers.size() >= 1, "");
    const tinygltf::Animation& raw_animation = model.animations[animation_id];

    const uint sampler_id = raw_animation.channels[channel_id].sampler;
    const string target_name = raw_animation.channels[channel_id].target_path;

    const int input = raw_animation.samplers[sampler_id].input;
    const int output = raw_animation.samplers[sampler_id].output;

    // Buffer is quaternion.
    if(target_name == "rotation")
    {
        animation.rotations =
            ExtractDataFromAccessor<Eigen::Quaternionf>(model, output);

        animation.rotation_key_times = ExtractDataFromAccessor<float>(model, input);
    }

    // Buffer is vec3.
    if(target_name == "translation")
    {
        animation.translations =
            ExtractDataFromAccessor<Eigen::Vector3f>(model, output);

        animation.translation_key_times = ExtractDataFromAccessor<float>(model, input);
    }

    // Buffer is vec3.
    if(target_name == "scale")
    {
        animation.scales = ExtractDataFromAccessor<Eigen::Vector3f>(model, output);
        animation.scale_key_times = ExtractDataFromAccessor<float>(model, input);
    }
}

[[deprecated]]
std::pair<Animation::RigidAnimation, std::vector<float>> ExtractAnimation(
    const tinygltf::Model& model, const int animation_id, const int sampler_id)
{
    if(animation_id < 0) return {};
    Assert(model.animations.size() >= 1, "");
    Assert(model.animations[animation_id].samplers.size() >= 1, "");
    const tinygltf::Animation& raw_animation = model.animations[animation_id];
    Animation::RigidAnimation animation = {};
    animation.translation_key_times = {0.f, 1.f};
    animation.rotation_key_times = {0.f, 1.f};
    animation.scale_key_times = {0.f, 1.f};

    std::vector<float> weights;

    const uint target_node = raw_animation.channels[sampler_id].target_node;
    for(uint anim_index = 0; anim_index < raw_animation.channels.size(); anim_index++)
    {
        // Look only at animation channels that target the same node
        if((uint)raw_animation.channels[anim_index].target_node != target_node) continue;

        const int input = raw_animation.samplers[anim_index].input;
        const int output = raw_animation.samplers[anim_index].output;
        const string target_name = raw_animation.channels[anim_index].target_path;

        // Buffer is quaternion.
        if(target_name == "rotation")
        {
            animation.rotations =
                ExtractDataFromAccessor<Eigen::Quaternionf>(model, output);

            animation.rotation_key_times = ExtractDataFromAccessor<float>(model, input);
        }

        // Buffer is vec3.
        if(target_name == "translation")
        {
            animation.translations =
                ExtractDataFromAccessor<Eigen::Vector3f>(model, output);

            animation.translation_key_times = ExtractDataFromAccessor<float>(model, input);
        }

        // Buffer is vec3.
        if(target_name == "scale")
        {
            animation.scales = ExtractDataFromAccessor<Eigen::Vector3f>(model, output);
            animation.scale_key_times = ExtractDataFromAccessor<float>(model, input);
        }

        // Buffer is weights vector (these are the morph target weights)
        if(target_name == "weights")
        {
            weights = ExtractDataFromAccessor<float>(model, output);
            animation.morph_key_times = ExtractDataFromAccessor<float>(model, input);
        }
    }

    return {animation, weights};
}

CpuImage ExtractImage(const tinygltf::Model& model, uint id)
{
    Assert(id < model.textures.size(), "");
    int image_id = model.textures[id].source;

    Assert(model.images[image_id].bits == 8, "");
    Assert(
        model.images[image_id].pixel_type == TINYGLTF_COMPONENT_TYPE_UNSIGNED_BYTE ||
            model.images[image_id].pixel_type == TINYGLTF_COMPONENT_TYPE_BYTE,

        "");

    return CpuImage(
        model.images[image_id].image.data(),
        model.images[image_id].width,
        model.images[image_id].height,
        model.images[image_id].component);
}

std::vector<int> GetSkeleton(const tinygltf::Model& model, const int skin_id)
{
    std::map<int, int> joint_index_map;
    for(uint i=0; i < model.skins[skin_id].joints.size(); i++)
    {
        joint_index_map.insert({model.skins[skin_id].joints[i], i});
    }

    std::vector<int> skeleton(model.skins[skin_id].joints.size());
    skeleton[0] = 0;

    const int start_joint = model.skins[skin_id].skeleton == -1
        ? model.skins[skin_id].joints[0]
        : model.skins[skin_id].skeleton;

    vector<int> stack = {start_joint};
    while(!stack.empty())
    {
        const auto joint_index = stack.back();
        stack.pop_back();

        for(auto child: model.nodes[joint_index].children)
        {
            if(joint_index_map.count(child) == 0) continue;
            // Set the parent of the child to the current joint
            skeleton[joint_index_map.at(child)] = joint_index_map.at(joint_index);
            stack.push_back(child);
        }
    }
    return skeleton;
}

std::vector<Eigen::Matrix4f> GetLocalJointMatrices(
    const tinygltf::Model& model, const int skin_id)
{
    std::vector<Eigen::Matrix4f> joint_matrices(model.skins[skin_id].joints.size());

    std::map<int, int> joint_index_map;
    for(uint j = 0; j < model.skins[skin_id].joints.size(); j++)
    {
        joint_index_map.insert({model.skins[skin_id].joints[j], j});
    }

    for(int joint: model.skins[skin_id].joints)
    {
        assert((uint)joint_index_map.at(joint) < joint_matrices.size());
        joint_matrices[joint_index_map.at(joint)] = GetNodeTransform(model, joint);
    }

    return joint_matrices;
}

Animation::Skin ExtractSkin(const tinygltf::Model& model, const int skin_id)
{
    if(skin_id < 0) return {};

    Animation::Skin skin = {};
    // Get the value id of the first/root joint in the skin's skeleton.
    skin.joint_matrices = GetLocalJointMatrices(model, skin_id);
    skin.skeleton = GetSkeleton(model, skin_id);
    std::map<int, int> joint_index_map;

    // Find the index permutations (e.g. joint 3 is stored at index 7).
    for(uint j = 0; j < model.skins[skin_id].joints.size(); j++)
    {
        joint_index_map.insert({model.skins[skin_id].joints[j], j});
    }

    skin.inverse_bind_matrices = ExtractDataFromAccessor<Eigen::Matrix4f>(
        model, model.skins[skin_id].inverseBindMatrices);

    Assert(model.skins[skin_id].joints.size() == skin.inverse_bind_matrices.size(), "");

    auto [start_joint, end_joint] = FindMinMax(model.skins[skin_id].joints);
    const uint joint_count = model.skins[skin_id].joints.size();

    Assert(skin.inverse_bind_matrices.size() == joint_count, "");

    for(uint anim_id = 0; anim_id < model.animations.size(); anim_id++)
    {
        vector<Animation::RigidAnimation> local_animations;
        local_animations.resize(joint_count);

        uint channel_id = 0;
        for(const auto& channel: model.animations[anim_id].channels)
        {
            // GLTF uses its own nodes to represent the skeleton, so we have to verify
            // that the animation targets any node in the skeleton (which are the joints).
            const int joint = channel.target_node;
            if(joint >= start_joint && joint <= end_joint)
            {
                LoadAnimation(
                    model,
                    anim_id,
                    channel_id,
                    local_animations[joint_index_map.at(joint)]);
            }

            channel_id++;
        }

        for(uint joint = start_joint; joint <= (uint)end_joint; joint++)
        {
            if(joint_index_map.count(joint) == 0)
            {
                continue;
            }

            const uint translation_size = local_animations[joint_index_map.at(joint)].translation_key_times.size();
            const uint rotation_size = local_animations[joint_index_map.at(joint)].rotation_key_times.size();
            const uint scale_size = local_animations[joint_index_map.at(joint)].scale_key_times.size();

            auto [translation, rotation, scale] =
                TRSMatToComponents(skin.joint_matrices[joint_index_map.at(joint)]);

            local_animations[joint_index_map.at(joint)].rotations.resize(rotation_size, rotation);
            local_animations[joint_index_map.at(joint)].translations.resize(translation_size, translation);
            local_animations[joint_index_map.at(joint)].scales.resize(scale_size, scale);
        }

        skin.animations.push_back(local_animations);
    }

    return skin;
}

Eigen::Matrix4f GetNodeTransform(const tinygltf::Model& model, const uint node)
{
    // This exists only to avoid passing a null pointer into memcpy, which is
    // technically undefined behaviour.
    const double safety_dummy = 0;

    Eigen::Quaterniond rotation = Eigen::Quaterniond::Identity();
    {
    const double* data_ptr = model.nodes[node].rotation.data();
    memcpy(
        rotation.coeffs().data(),
        data_ptr != nullptr? data_ptr : &safety_dummy,
        model.nodes[node].rotation.size() * sizeof(double));
    }
    Eigen::Vector3d scaling = {1, 1, 1};
    {
    const double* data_ptr = model.nodes[node].scale.data();
    memcpy(
        scaling.data(),
        data_ptr != nullptr? data_ptr : &safety_dummy,
        model.nodes[node].scale.size() * sizeof(double));
    }

    Eigen::Vector3d translation = {0, 0, 0};
    {
    const double* data_ptr = model.nodes[node].translation.data();
    memcpy(
        translation.data(),
        data_ptr != nullptr? data_ptr : &safety_dummy,
        model.nodes[node].translation.size() * sizeof(double));
    }

    Eigen::Matrix4d matrix = Eigen::Matrix4d::Identity();
    {
    const double* data_ptr = model.nodes[node].matrix.data();
    memcpy(
        matrix.data(),
        data_ptr != nullptr? data_ptr : &safety_dummy,
        model.nodes[node].matrix.size() * sizeof(double));
    }

    matrix(0, 0) *= scaling[0];
    matrix(1, 1) *= scaling[1];
    matrix(2, 2) *= scaling[2];

    matrix = CastTo4D(rotation.toRotationMatrix()) * matrix;

    matrix(0, 3) += translation[0];
    matrix(1, 3) += translation[1];
    matrix(2, 3) += translation[2];

    return matrix.cast<float>();
}

vector<int> GetParentGraph(const tinygltf::Model& model)
{
    vector<int> graph(model.nodes.size(), 0);

    for(uint node = 0; node < model.nodes.size(); node++)
    {
        for(int child: model.nodes[node].children)
        {
            graph[child] = node;
        }
    }

    return graph;
}

Eigen::Matrix4f GetTransformFromRoot(
    const tinygltf::Model& model, const vector<int>& graph, uint node)
{
    Assert(graph.size() == model.nodes.size(), "");

    int current_node = node;
    Eigen::Matrix4f transform = Eigen::Matrix4f::Identity();

    int safety_count = model.nodes.size();
    do
    {
        transform = GetNodeTransform(model, current_node) * transform;
        current_node = graph[current_node];
        safety_count--;
    } while(current_node != 0 && safety_count);

    Assert(safety_count > 0 || current_node == 0, "");
    return transform;
}

}  // namespace GltfLib

