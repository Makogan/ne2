#pragma once

#include "Animation.hpp"

/** @cond */
#include "Eigen/Dense"
#include "tiny_gltf.h"
/** @endcond */

#include "Geometry/Math/EigenHelpers.hpp"
#include "ImageManipulation/CpuImage.hpp"

namespace Eigen
{
using Vector4s = Matrix<unsigned short, 4, 1>;
using Vector4c = Matrix<unsigned char, 4, 1>;
}  // namespace Eigen

namespace GltfLib
{
struct GltfModelData;

using MeshInfo = std::pair<GltfModelData, std::vector<uint>>;
/**
 * @brief Wrapper around tinygltf model.
 *
 * A native parser might be implemented in the future, but for now this is a solution.
 *
 */
struct Model
{
    tinygltf::Model model;
};
/**
 * @brief Container of all the data of a gltf mesh.
 *
 */
struct GltfModelData
{
    std::vector<Eigen::Vector3f> positions = {};
    std::vector<Eigen::Vector2f> uvs = {};
    std::vector<Eigen::Vector3f> normals = {};
    std::vector<Eigen::Vector4f> tangents = {};
    std::vector<Eigen::Vector4i> joints = {};
    std::vector<Eigen::Vector4f> weights = {};

    std::vector<uint> indices = {};
    std::vector<uint> vertex_material_ids = {};
    size_t size;
};

/**
 * @brief Load a gltf model.
 *
 * It only parses the gltf file, it does not extract any of the raw byte values. You
 * need to call the `Extract*` methods for each property you want.
 *
 * @param path Path to the glTF file.
 * @return GltfLib::Model Tinygltf wrapper.
 */
GltfLib::Model LoadModel(const std::string& path);

GltfModelData ExtractPrimitive(
    const tinygltf::Model& model, uint mesh_id, uint primitive_id);

std::pair<Animation::RigidAnimation, std::vector<float>> ExtractAnimation(
    const tinygltf::Model& model, const int animation_id, const int sampler_id);

std::vector<Eigen::Vector4f> ExtractMorphTargets(
    const tinygltf::Model& model, uint mesh_id, uint primitive_id);

Animation::Skin ExtractSkin(const tinygltf::Model& model, const int skin_id);
// TODO (low): make this return the gltf image instead and make a generic method
// to initialize CpuImages.
CpuImage ExtractImage(const tinygltf::Model& model, uint material_id);

std::vector<int> GetParentGraph(const tinygltf::Model& model);

Eigen::Matrix4f GetTransformFromRoot(
    const tinygltf::Model& model, const std::vector<int>& graph, uint node);

Eigen::Matrix4f GetNodeTransform(const tinygltf::Model& model, const uint node);

}  // namespace GltfLib

