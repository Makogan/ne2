#pragma once

/** @cond */
#include "Eigen/Dense"
/** @endcond */

using uint = unsigned int;

namespace Animation
{
struct RigidAnimation
{
    std::vector<Eigen::Quaternionf> rotations;
    std::vector<float> rotation_key_times = {0, 1};
    std::vector<Eigen::Vector3f> translations;
    std::vector<float> translation_key_times = {0, 1};
    std::vector<Eigen::Vector3f> scales;
    std::vector<float> scale_key_times = {0, 1};

    std::vector<float> morph_key_times = {0, 1};

    float time = 0;
    float time_step = 0.01;

    void Tick()
    {
        // WARNING(medium): This is assuming that the translation times can be used
        // as canonical time, this is not guaranteed.
        if(translation_key_times.empty()) return;
        time = fmod(time + time_step, translation_key_times.back());
    }

    Eigen::Vector3f SampleTranslation() const { return SampleTranslation(time); }
    Eigen::Vector3f SampleTranslation(float t) const;

    Eigen::Quaternionf SampleRotation() const { return SampleRotation(time); }
    Eigen::Quaternionf SampleRotation(const float time) const;

    Eigen::Vector3f SampleScaling() const { return SampleScaling(time); }
    Eigen::Vector3f SampleScaling(const float time) const;
};

struct Skin
{
    std::vector<int> skeleton = {0};
    std::vector<Eigen::Matrix4f> inverse_bind_matrices = {Eigen::Matrix4f::Identity()};
    std::vector<Eigen::Matrix4f> joint_matrices = {Eigen::Matrix4f::Identity()};
    std::vector<std::vector<RigidAnimation>> animations = {};
    uint active_animation_cycle = 0;
};

std::vector<Eigen::Matrix4f> SampleSkinAnimation(const Skin& skin, float t);

}  // namespace Animation

