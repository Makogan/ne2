#include "Camera/Camera.hpp"

#include "Geometry/Math/EigenHelpers.hpp"

using namespace std;

namespace
{
Eigen::Vector3f ScreenToArcSurface(Eigen::Vector2f pos)
{
    const float radius = 1.0f;  // Controls the speed
    if(pos.x() * pos.x() + pos.y() * pos.y() >= (radius * radius) / 2.f - 0.00001)
    {
        // This is equal to (r^2 / 2) / (sqrt(x^2 + y^2)) since the magnitude of the
        // vector is sqrt(x^2 + y^2)
        return {pos.x(), pos.y(), -(radius * radius / 2.f) / (pos.norm())};
    }

    return {
        pos.x(),
        pos.y(),
        -sqrt(radius * radius - (pos.x() * pos.x() + pos.y() * pos.y()))};
}

}  // namespace

namespace NECamera
{
// Camera --------------------------------------------------------------------------------
Camera::Camera(float width, float height)
    : screen_width(width)
    , screen_height(height)
{
}

Eigen::Matrix4f Camera::GetViewMatrix() const
{
    return LookAt(position, look_at_point, up);
}

Eigen::Matrix4f Camera::GetProjectionMatrix() const
{
    return Perspective(Radians(fov), screen_width / screen_height, z_near, z_far);
}

// Spherical Camera ----------------------------------------------------------------------
void SphericalCamera::UpdateCameraAngles(
    void* ptr,
    const float position_x,
    const float position_y,
    const float offset_x,
    const float offset_y)
{
    const Eigen::Vector2f position(position_x, position_y);
    const Eigen::Vector2f offset(offset_x, offset_y);

    auto camera = reinterpret_cast<SphericalCamera*>(ptr);

    const float speed = 150;
    camera->SetTheta(camera->GetTheta() - offset.x() * speed);
    camera->SetPhi(camera->GetPhi() - offset.y() * speed);

    Eigen::Vector3f up = camera->up;
    const Eigen::Vector3f forward = camera->default_forward;
    Eigen::Vector3f side = up.cross(forward).normalized();
    float phi = camera->phi * M_PI / 180.f;
    float theta = camera->theta * M_PI / 180.f;
    auto theta_rotation = RotateQuaternion(theta, up);
    auto phi_rotation = RotateQuaternion(phi, side);

    const Eigen::Vector3f rotated =
        theta_rotation * phi_rotation * Eigen::Vector3f(forward * camera->radius);
    camera->position = camera->look_at_point + rotated;
}

void SphericalCamera::UpdateCameraPosition(
    void* ptr,
    const float position_x,
    const float position_y,
    const float offset_x,
    const float offset_y)
{
    const Eigen::Vector2f position(position_x, position_y);
    const Eigen::Vector2f offset(offset_x, offset_y);

    auto camera = reinterpret_cast<SphericalCamera*>(ptr);

    const float speed = 3.5;

    Eigen::Vector3f up = camera->up;
    Eigen::Vector3f forward = (camera->look_at_point - camera->position).normalized();
    Eigen::Vector3f side = up.cross(forward).normalized();
    up = forward.cross(side).normalized();

    Eigen::Vector3f offset_3d = (up * offset.y() + side * offset.x()) * speed;
    camera->position += offset_3d;
    camera->look_at_point += offset_3d;
}

void SphericalCamera::UpdateCameraZoom(
    void* ptr, const float offset_x, const float offset_y)
{
    const Eigen::Vector2f offset(offset_x, offset_y);

    auto camera = reinterpret_cast<SphericalCamera*>(ptr);

    const float speed = 0.5;
    camera->radius -= speed * offset.y();
    camera->radius = std::max(0.0001f, camera->radius);

    const auto rotated = camera->look_at_point +
        (camera->position - camera->look_at_point).normalized() * camera->radius;
    camera->position = rotated;
}

// Arcball Camera ------------------------------------------------------------------------
void ArcballCamera::UpdateCameraAngles(
    void* ptr,
    const float position_x,
    const float position_y,
    const float offset_x,
    const float offset_y)
{
    const Eigen::Vector2f position(position_x, position_y);
    const Eigen::Vector2f offset(offset_x, offset_y);

    auto camera = reinterpret_cast<ArcballCamera*>(ptr);

    Eigen::Vector3f vb = ScreenToArcSurface({position.x(), -position.y()}).normalized();
    auto centered_pos = position - offset;
    Eigen::Vector3f va =
        ScreenToArcSurface({centered_pos.x(), -centered_pos.y()}).normalized();

    const float speed = 5000.f;
    float angle = acos(std::min(1.f, vb.dot(va))) * speed;
    angle = std::min(angle, Radians(60.f));
    Eigen::Vector3f axis = vb.cross(va);

    camera->rotation *= Eigen::Quaternionf(Eigen::AngleAxis(angle, axis)).normalized();

    const Eigen::Quaternion rot = camera->rotation;
    camera->position = camera->look_at_point + rot * (camera->forward * camera->radius);
}

void ArcballCamera::UpdateCameraPosition(
    void* ptr,
    const float position_x,
    const float position_y,
    const float offset_x,
    const float offset_y)
{
    const Eigen::Vector2f position(position_x, -position_y);
    const Eigen::Vector2f offset(offset_x, -offset_y);

    auto camera = reinterpret_cast<ArcballCamera*>(ptr);

    Eigen::Vector3f up = (camera->rotation * camera->up).normalized();
    Eigen::Vector3f side =
        (camera->rotation * (camera->forward.cross(camera->up))).normalized();

    Eigen::Vector3f offset_3d = (up * -offset.y() + side * offset.x()) * camera->speed * 0.1;
    camera->look_at_point += offset_3d;
    const Eigen::Quaternion rot = camera->rotation;
    camera->position = camera->look_at_point + rot * (camera->forward * camera->radius);
}

void ArcballCamera::UpdateCameraZoom(
    void* ptr, const float offset_x, const float offset_y)
{
    const Eigen::Vector2f offset(offset_x, offset_y);

    auto camera = reinterpret_cast<ArcballCamera*>(ptr);

    const float speed = 0.005 * camera->speed;
    camera->radius -= speed * offset.y();
    camera->radius = std::max(0.0001f, camera->radius);

    camera->position = camera->look_at_point +
        (camera->position - camera->look_at_point).normalized() * camera->radius;
}

Eigen::Matrix4f ArcballCamera::GetViewMatrix() const
{
    return LookAt(position, look_at_point, rotation * up);
}

void ArcballCamera::SetRadius(const float radius)
{
    this->radius = radius;
    const auto dir = (position - look_at_point).normalized();
    position = look_at_point + dir * radius;
}

void ArcballCamera::SetPosition(const Eigen::Vector3f& new_position)
{
    Eigen::Vector3f offset = new_position - position;
    position = new_position;
    look_at_point += offset;
}

// Fly Camera ----------------------------------------------------------------------------
void FlyCamera::UpdateCameraAngles(
    void* ptr,
    const float position_x,
    const float position_y,
    const float offset_x,
    const float offset_y)
{
    const Eigen::Vector2f position(position_x, position_y);
    const Eigen::Vector2f offset(offset_x, offset_y);

    auto camera = reinterpret_cast<FlyCamera*>(ptr);

    Eigen::Vector3f look_dir = camera->GetLookDirection();
    Eigen::Vector3f pivot = camera->up.cross(look_dir).normalized();

    Eigen::Matrix3f turn_y = Eigen::Matrix3f(Eigen::AngleAxisf(-offset.x(), camera->up));
    Eigen::Matrix3f turn_x = Eigen::Matrix3f(Eigen::AngleAxisf(-offset.y(), pivot));

    look_dir = turn_y * turn_x * look_dir;

    camera->look_at_point = camera->position + look_dir;
}

// Utilities -----------------------------------------------------------------------------
template Eigen::Matrix<float, 3, 1> Unproject(
    const Eigen::Matrix<float, 3, 1>& screen,
    const Eigen::Matrix<float, 4, 4>& view_proj);

template Eigen::Matrix<double, 3, 1> Unproject(
    const Eigen::Matrix<double, 3, 1>& screen,
    const Eigen::Matrix<double, 4, 4>& view_proj);

Eigen::Vector3f ScreenWorldDirection(
    const Camera& camera, const Eigen::Vector2f& screen_coords)
{
    const Eigen::Matrix4f mp = camera.GetProjectionMatrix();
    const Eigen::Matrix4f mv = camera.GetViewMatrix();

    const Eigen::Matrix4f view_proj = mp * mv;

    const Eigen::Vector3f unprojected =
        Unproject({screen_coords.x(), screen_coords.y(), 0}, view_proj);

    const Eigen::Vector3f camera_position = camera.GetPosition();

    return (unprojected - camera_position).normalized();
}

 } // namespace NECamera