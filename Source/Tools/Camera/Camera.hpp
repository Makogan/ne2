#pragma once

/** @cond */
#include <math.h>

#include "Eigen/Dense"
/** @endcond */

using uint = unsigned int;

namespace NECamera
{
// Camera --------------------------------------------------------------------------------
/**
 * @brief General representation of a camera state.
 *
 * Used to store and update the view and projection matrices. Also
 * defines useful static methods for camera manipualtion.
 *
 */
class Camera
{
  public:
    Eigen::Vector3f up = Eigen::Vector3f(0, -1, 0);
    Eigen::Vector3f position = Eigen::Vector3f(0, 0, -5);
    Eigen::Vector3f look_at_point = Eigen::Vector3f(0, 0, 0);
    float screen_width = 512;
    float screen_height = 512;
    float z_near = 0.1;
    float z_far = 200000.0;
    float fov = 45.f;

  public:
    Camera(){};

    Camera(float width, float height);
    virtual ~Camera() = default;

    virtual Eigen::Matrix4f GetViewMatrix() const;

    virtual Eigen::Matrix4f GetProjectionMatrix() const;

    virtual void SetPosition(const Eigen::Vector3f& direction)
    {
        position = direction;
    }

    Eigen::Vector3f GetPosition() const { return position; }

    Eigen::Vector3f GetUp() const { return up; }

    Eigen::Vector3f GetLookDirection() const { return look_at_point - position; }

    Eigen::Vector3f GetSide() const { return GetLookDirection().normalized().cross(up); }

    void Offset(const Eigen::Vector3f& offset)
    {
        position += offset;
        look_at_point += offset;
    }

    void SetLookDirection(const Eigen::Vector3f& direction)
    {
        look_at_point = position + direction;
    }

    void SetLookAtPoint(const Eigen::Vector3f& point)
    {
        look_at_point = point;
    }

    void SetUp(const Eigen::Vector3f& direction) { up = direction; }

    void UpdateScreenDimensions(uint w, uint h)
    {
        screen_width = w;
        screen_height = h;
    }

    float GetNear() const { return z_near; }

    float GetFar() const { return z_far; }

    void SetCreenDimensions(const uint width, const uint height)
    { screen_width = width; screen_height = height; }
};
/**
 * @brief Specialized camera object, defines a camera that always focuses
 *  a specific point.
 *
 * The up direction of the camera remains unchanged as it rotates and moves.
 * around the world.
 *
 */
class SphericalCamera : public Camera
{
  public:
    static void UpdateCameraAngles(
        void* ptr,
        const float position_x,
        const float position_y,
        const float offset_x,
        const float offset_y);

    static void UpdateCameraZoom(
        void* ptr,
        const float offset_x,
        const float offset_y);

    static void UpdateCameraPosition(
        void* ptr,
        const float position_x,
        const float position_y,
        const float offset_x,
        const float offset_y);

  protected:
    float theta = 0;
    float phi = 0;
    float radius = 5;

    Eigen::Vector3f default_forward = Eigen::Vector3f(0, 0, -1);

  public:
    SphericalCamera() {}
    SphericalCamera(float width, float height)
        : Camera(width, height)
        , theta(0)
        , phi(0)
        , radius(5)
    {
    }

    float GetTheta() { return theta; }

    float GetPhi() { return phi; }

    float GetRadius() { return radius; }

    void SetTheta(float t) { theta = t; }

    void SetPhi(float t)
    {
        phi = t;
        phi = std::clamp(phi, -89.99f, 89.99f);
    }

    void SetRadius(float t) { radius = t; }
};

/**
 * @brief Implementation of an arcball camera, which is a camera where the rotation
 * matrix at any time is infered from vector projections into a hemisphere.
 *
 * The theory can be found [here](https://www.khronos.org/opengl/wiki/Object_Mouse_Trackball).
 * Be careful since OGL's and Vulkan's coordinate systems have different handedness. In
 * our case we have to flip the z coordinate with respect to the linked reference.
 * (Above might no longer be true as the current implementation of the camera matrix
 * might be correcting the issue by using a different handedness).
 *
 */
class ArcballCamera : public Camera
{
  public:
    static void UpdateCameraAngles(
        void* ptr,
        const float position_x,
        const float position_y,
        const float offset_x,
        const float offset_y);

    static void UpdateCameraZoom(
        void* ptr,
        const float offset_x,
        const float offset_y);

    static void UpdateCameraPosition(
        void* ptr,
        const float position_x,
        const float position_y,
        const float offset_x,
        const float offset_y);

  public:
    float radius = 1;
    float speed = 10.f;

    Eigen::Vector3f forward = Eigen::Vector3f(0, 0, -1);

    Eigen::Quaternionf rotation = Eigen::Quaternionf(1, 0, 0, 0);

  public:
    ArcballCamera() {}
    /**
     * @brief Construct a new Arcball Camera object
     *
     * @param width Width of the screen in number of pixels.
     * @param height Height of the screen in number of pixels.
     * @param radius Distance from the focal point to the camera. Default is 5.
     */
    ArcballCamera(float width, float height, float radius = 5)
        : Camera(width, height)
        , radius(radius)
    {
        position = Eigen::Vector3f(0, 0, -radius);
    }

    virtual Eigen::Matrix4f GetViewMatrix() const override;

    virtual void SetPosition(const Eigen::Vector3f& position) override;

    void SetLookAtPoint(const Eigen::Vector3f& focus);

    void SetRotation(const Eigen::Quaternionf& rotation) { this->rotation = rotation; }

    void SetRadius(const float radius);

    void SetSpeed(const float speed) { this->speed = speed; }
};
// Fly Camera ----------------------------------------------------------------------------
class FlyCamera : public Camera
{
  public:
    static void UpdateCameraAngles(
        void* ptr,
        const float position_x,
        const float position_y,
        const float offset_x,
        const float offset_y);

  public:
    FlyCamera() {}
    /**
     * @brief Construct a new Arcball Camera object
     *
     * @param width Width of the screen in number of pixels.
     * @param height Height of the screen in number of pixels.
     * @param radius Distance from the focal point to the camera. Default is 5.
     */
    FlyCamera(float width, float height, const Eigen::Vector3f& position)
        : Camera(width, height)
    {
        this->position = position;
    }
};
// Utilities -----------------------------------------------------------------------------
/**
 * @brief Unproject a screen value onto world coordinates. For values that should be on
 * the screen plane in 3D, the z coordinate should be equal to the near plane of the
 * camera.
 *
 * @tparam T Scalar type.
 * @param screen Point expressed in screen coordinates (including depth).
 * @param view_proj View Projection matrix.
 * @return Eigen::Matrix<S, 3, 1> Unprojected point.
 */
template<typename S>
Eigen::Matrix<S, 3, 1> Unproject(
    const Eigen::Matrix<S, 3, 1>& screen, const Eigen::Matrix<S, 4, 4>& view_proj)
{
    Eigen::Matrix<S, 4, 1> v = view_proj.inverse() *
        Eigen::Matrix<S, 4, 1>(screen[0], screen[1], screen[2], static_cast<S>(1));

    return Eigen::Matrix<S, 3, 1>{v[0], v[1], v[2]} * (static_cast<S>(1) / v[3]);
}

extern template Eigen::Matrix<float, 3, 1> Unproject(
    const Eigen::Matrix<float, 3, 1>& screen,
    const Eigen::Matrix<float, 4, 4>& view_proj);

extern template Eigen::Matrix<double, 3, 1> Unproject(
    const Eigen::Matrix<double, 3, 1>& screen,
    const Eigen::Matrix<double, 4, 4>& view_proj);

// Utilities -----------------------------------------------------------------------------
/**
 * @brief Calculates the ray direction from the camera position to the screen coordinate
 * in world space.
 *
 * @param camera Active camera from which the ray is to be unprojected.
 * @param screen_coords Screen coordinates.
 * @return Eigen::Vector3f Normalized ray direction in world space.
 */
Eigen::Vector3f ScreenWorldDirection(
    const Camera& camera, const Eigen::Vector2f& screen_coords);

} // NECamera
