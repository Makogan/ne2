//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file Noise.hpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2021-07-05
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#pragma once

/** @cond */
#include <iostream>

#include "Eigen/Dense"
/** @endcond */

// Theory: http://eastfarthing.com/blog/2015-04-21-noise/

namespace details
{
template<typename T> T Fract(T n) { return n - std::floor(n); }

template<typename T> T Rand(T n) { return Fract(sin(n) * 43758.5453123); }

template<typename T, int N> Eigen::Vector<T, 2> Rand2(const Eigen::Vector<T, N>& n)
{
    using Vec = const Eigen::Vector<T, 2>;
    return {
        Fract(sin(n.dot(Vec(12.9898, 4.1414))) * 43758.5453123),
        Fract(cos(n.dot(Vec(2.716579086, -21.7494750439))) * M_PI * 100.f)};
}

template<typename T> T Fade(T t) { return t * t * t * (t * (t * 6.0 - 15.0) + 10.0); }

}  // namespace details

template<typename T, int N> T Perlin(const Eigen::Vector<T, N>& input)
{
    using Vec = Eigen::Vector<T, N>;
    std::array<T, N> fract_parts;
    std::array<int, N> int_parts;
    // Extract integer and decimal portions of the coordinates.
    for(uint i = 0; i < N; i++)
    {
        fract_parts[i] = details::Fract(input[i]);
        int_parts[i] = int(std::round(input[i] - fract_parts[i]));
    }

    constexpr int corner_total = 1 << N;
    std::array<Vec, corner_total> corner_vectors;
    // Generate a random vector at each corner of the cell.
    for(uint corner = 0; corner < corner_total; corner++)
    {
        // The bit pattern of the index also determines the relative coordinates of the
        // corner. For example in 2D, index 0 = 0b00 is cell (0,0) and index 2 = 0b10
        // is cell (1, 0).
        for(uint bit_index = 0; bit_index < N; bit_index++)
        {
            T offset = (corner >> (N - 1 - bit_index)) & 1;
            corner_vectors[corner][bit_index] =
                details::Rand(T(int_parts[bit_index] + offset));
        }
    }

    // Create scalar weights based on the dot with the random vectors
    std::array<T, corner_total> weights;
    Vec center;
    for(uint i = 0; i < N; i++)
        center[i] = fract_parts[i];

    for(uint corner = 0; corner < corner_total; corner++)
    {
        Vec offset;
        for(uint bit_index = 0; bit_index < N; bit_index++)
            offset[bit_index] = (corner >> (N - 1 - bit_index)) & 1;

        weights[corner] = corner_vectors[corner].dot(center - offset);
    }

    // Do N-linear-interpolation of all the weights.
    for(int dimension = N; dimension > 0; dimension--)
    {
        // Do linear interpolation across one dimension. This creates a N-1 hyper-quad.
        // When the dimension of the quad becomes 0 we have our perlin weight.
        const int n = (dimension - 1);
        for(int i = 0; i < (1 << n); i++)
        {
            // Each pair of interpolated points will have the same msb value. For example
            // when the dimension is 2 this generates (0,0)--(0,1) and (1,0)--(1,1).
            // This means that the values are changing along the highest dimension
            // (y in the example, z in the 3D case, etc) so that's the fractional portion
            // from which we need to get the weight.
            weights[i] = GLA::mix(
                weights[i << n], weights[i << n | 1], details::Fade(fract_parts[n]));
        }
    }

    return weights[0];
}

