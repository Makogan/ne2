#pragma once

/** @cond */
#include <iterator>
#include <set>
#include <string>
#include <vector>
/** @endcond */

using uint = unsigned int;

template<typename T>
std::vector<T> Flatten(const std::vector<std::vector<T>> &orig)
{
    std::vector<T> ret;
    for(const auto &v: orig)
        ret.insert(ret.end(), v.begin(), v.end());
    return ret;
}

template<typename T>
void AppendVectors(std::vector<T>& container, const std::vector<T>& data)
{
    container.insert(container.end(), data.begin(), data.end());
}

template<typename T> std::vector<T> SetToVector(std::set<T>& container)
{
    std::vector<T> v;
    v.reserve(container.size());
    std::copy(container.begin(), container.end(), std::back_inserter(v));
    return v;
}

template<typename T> std::vector<T> DuplicateEntries(std::vector<T>& original, uint n)
{
    const uint v_size = original.size() * n;
    std::vector<T> new_vector(v_size);

    for(uint i = 0; i < v_size / n; i++)
    {
        for(uint j = 0; j < n; j++)
        {
            new_vector[i * n + j] = original[i];
        }
    }
    return new_vector;
}

template<typename T>
std::vector<std::byte> SerializeVector(const std::vector<T>& original)
{
    std::vector<std::byte> buffer(original.size() * sizeof(T));
    memcpy(buffer.data(), original.data(), buffer.size());

    return buffer;
}

template<typename T> std::pair<T, T> FindMinMax(const std::vector<T>& original)
{
    Assert(!original.empty(), "");
    T min = std::numeric_limits<T>::max();
    T max = std::numeric_limits<T>::lowest();

    for(const auto e: original)
    {
        min = std::min(min, e);
        max = std::max(max, e);
    }

    return {min, max};
}

template<typename T> std::string to_string(const std::vector<T>& original)
{
    std::string final_str = "";
    for(auto& v: original)
        final_str += std::to_string(v) + " ";

    return final_str;
}

template<typename Field>
void SerializeMember(std::vector<std::byte>& buffer, const Field& field)
{
    const size_t size = sizeof(Field);
    const std::byte* data = (std::byte*)&field;

    for(uint i = 0; i < size; i++)
    {
        buffer.push_back((std::byte) * (data + i));
    }
}

