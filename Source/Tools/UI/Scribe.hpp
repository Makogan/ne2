#pragma once

/** @cond */
#include <any>
#include <string>
#include <unordered_map>
#include <memory>

#include "Eigen/Core"
#include "ft2build.h"
#include FT_FREETYPE_H
/** @endcond */

#include "Core/TextManipulation.hpp"
#include "Core/Log.hpp"

struct ScGlyphData
{
    long int width;
    long int height;
    long int bearing_x;
    long int bearing_y;
    long int advance;
    Eigen::Vector2f lt_uv;
    Eigen::Vector2f rb_uv;
};

typedef std::pair<std::vector<float>, std::vector<uint>> ScGeometry;
typedef std::unordered_map<uint, ScGlyphData> ScCharacterMap;

struct ScGlyphTexture
{
    uint width;
    uint height;
    std::vector<char> buffer;
};

struct ScGeomMetaInfo
{
    float v_offset;  // vertical offset of the current line
    float h_offset;  // horizontal offset of the current line
    float char_len;  // side length of a character quad
    uint c_num = 0;  // number of written characters
    long max_len;    // largest character length in FT coordinates
    float v_delta;   // vertical distance from baseline to baseline
};
/**
 * @brief Class to encapsulate text rendering.
 *
 * This includes the creation of a texture atlas for the text sprites,
 * wrapping the text around the screen, managing ligatures, character spacing, ...
 *
 */
class Scribe
{
  private:
    std::unique_ptr<FT_Library> ft_library;
    std::unique_ptr<FT_Face> ft_face;

    float vertical_spacing = 0.6;
    float horizontal_spacing = 0.01;
    float aspect_ratio = 1;  // (width / height)
    float character_size = 0.05;
    long font_max_length = 0;

    uint atlas_width;
    uint atlas_height;
    std::vector<std::byte> atlas_buffer;
    std::unordered_map<uint, ScGlyphData> glyph_map;

    void ExtractGlyphs(std::vector<ScGlyphTexture>& glyph_textures);
    void CreateFontAtlas(const std::string& font_path);
    void CreateCharacterGeometry(const char c, ScGeomMetaInfo& info, ScGeometry& geometry);

  public:
    /**
     * @brief Dummy constructor, to allow declaration without initialization.
     *
     */
    Scribe(){};
    /**
     * @brief Construct a new Scribe object using a specific font.
     *
     * @param font_path Path to the font (e.g .ttf) file.
     */
    Scribe(const std::string& font_path);
    /**
     * @brief Set the Aspect Ratio of the monitor, to gnerate quads with apropriate scaling.
     *
     * @param ar Aspect ratio of the monitor.
     */
    void SetAspectRatio(float ar) { aspect_ratio = ar; }
    /**
     * @brief Set the Character Size.
     *
     * @param size New size of the characters that will be generated.
     */
    void SetCharacterSize(float size) { character_size = size; }
    /**
     * @brief Get a raw image containing all the glyphs in the font as bitmaps.
     *
     * @return const std::vector<char>& Raw bytes of the atlas.
     */
    const std::vector<std::byte>& GetAtlasBuffer() const { return atlas_buffer; }
    /**
     * @brief Get the Atlas Width.
     *
     * @return uint Width of the atlas.
     */
    uint GetAtlasWidth() const { return atlas_width; }
    /**
     * @brief Get the Atlas Height.
     *
     * @return uint Height of the atlas.
     */
    uint GetAtlasHeight() const { return atlas_height; }
    /**
     * @brief Get the maximum length the current font can occupy in normalized space.
     *
     * @return long
     */
    long GetFontMaxLength() { return font_max_length; }

    ScGeometry CreateTextGeometry(const std::string& text);
};

