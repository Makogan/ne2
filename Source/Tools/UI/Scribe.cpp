#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "Scribe.hpp"

/** @cond */
#include <codecvt>
#include <iostream>
#include <locale>
#include <string>

#include "stb_image_write.h"
/** @endcond */

using namespace std;

// Function declarations
// -----------------------------------------------------------------
namespace
{
const int SIDE_CELL_NUM = 10;

void WriteGlyphsToAtlas(
    const vector<ScGlyphTexture>& glyphs, vector<std::byte>& img_buffer, uint side_l);
}  // namespace

// Function definitions ------------------------------------------------------------------
Scribe::Scribe(const std::string& font_path)
    : ft_library(make_unique<FT_Library>())
    , ft_face(make_unique<FT_Face>())
{
    // Initialize freetype
    {
        DEBUG_VAR(auto error =) FT_Init_FreeType(ft_library.get());
        Assert(!error, "FreeType failed to initialize");
    }

    {
        DEBUG_VAR(auto error =)
        FT_New_Face(*ft_library, font_path.c_str(), 0, ft_face.get());
        Assert(error != FT_Err_Unknown_File_Format, "Unkown font file format");
        Assert(!error, "Couldn't open font file: " + font_path);
    }
    {
        DEBUG_VAR(auto error =) FT_Set_Char_Size(*ft_face, 0, 16 * 64, 300, 300);
        Assert(!error, "Couldn't set character size");
    }

    CreateFontAtlas(font_path);
}

ScGeometry Scribe::CreateTextGeometry(const string& o_text)
{
    auto text = o_text;
    // Add a final newline character for proper formatting
    if(text.back() != '\n') text += "\n";
    // Set the metadata information of the font
    ScGeomMetaInfo g_info = {
        .v_offset = -1,
        .h_offset = -1,
        .char_len = character_size,
        .c_num = 0,
        .max_len = font_max_length,
    };
    g_info.v_delta = g_info.char_len * float((*ft_face)->height) / float(g_info.max_len);
    g_info.v_delta += g_info.char_len * vertical_spacing;
    // Create the geometry of each character and append it to the geometry buffers
    ScGeometry geometry;
    for(auto& c: text)
    {
        auto advance = glyph_map[FT_Get_Char_Index(*ft_face, c)].advance;
        float n_advance = float(advance) / float(g_info.max_len);
        // If the line is too long, or if a new line character is found, got to new
        // line
        if(g_info.h_offset + g_info.char_len * n_advance >= 1.f || c == '\n')
        {
            g_info.h_offset = -1;  // Reset left margin
            g_info.v_offset +=
                g_info.v_delta * aspect_ratio;  // Update vertical offset to new line
        }
        // Append quad geometry for current line
        CreateCharacterGeometry(c, g_info, geometry);
    }
    return geometry;
}

// Private Methods -----------------------------------------------------------------------
void Scribe::CreateCharacterGeometry(
    const char c, ScGeomMetaInfo& info, ScGeometry& geometry)
{
    float char_l = info.char_len;
    float v_anchor = info.v_offset;

    auto& vertices = geometry.first;
    auto& indices = geometry.second;

    auto glyph_index = FT_Get_Char_Index(*ft_face.get(), c);
    FT_Load_Glyph(*ft_face.get(), glyph_index, FT_LOAD_NO_HINTING);
    // Calculate correction, the vertical offset from the baseline of this char
    auto metrics = glyph_map[glyph_index];
    // TODO (low): figure out why this number fixes the font rendering
    float correction = float(metrics.height - metrics.bearing_y) / float(info.max_len);
    correction *= char_l;

    // Calculate offset from cursor to the current character
    float bearingX = char_l * float(metrics.bearing_x) / float(info.max_len);

    float& h_offset = info.h_offset;
    // Insert the vertex positions and uvs into the returned geometry
    float h_coords[] = {h_offset + bearingX, h_offset + bearingX + char_l};
    float v_coords[] = {
        v_anchor + correction * aspect_ratio,
        v_anchor + (correction + char_l) * aspect_ratio};

    auto glyph_data = glyph_map[glyph_index];
    float tex_h_coords[] = {glyph_data.lt_uv.x(), glyph_data.rb_uv.x()};
    float tex_v_coords[] = {glyph_data.lt_uv.y(), glyph_data.rb_uv.y()};
    for(int x = 0; x < 2; x++)
    {
        for(int y = 0; y < 2; y++)
        {
            vertices.insert(end(vertices), {h_coords[x], v_coords[y], 0});
            vertices.insert(end(vertices), {tex_h_coords[x], tex_v_coords[y]});
        }
    }

    // Setup the indices of the current quad
    // There's 4 vertices in a quad, so we offset each index by (quad_size *
    // num_quads)
    uint delta = 4 * info.c_num++;
    indices.insert(
        end(indices), {0 + delta, 3 + delta, 1 + delta, 0 + delta, 3 + delta, 2 + delta});
    h_offset += char_l * metrics.advance / info.max_len + char_l * horizontal_spacing;
}
// TODO (low): if the file exists load it from disk instead of doing the
// processing.
void Scribe::CreateFontAtlas(const string& font_path)
{
    // Obtain bitmaps for all the desired characters.
    vector<ScGlyphTexture> glyph_textures;
    ExtractGlyphs(glyph_textures);

    // Find the largest dimensions vertically and horizontally for a character BB.
    uint m_width = 0;
    uint m_height = 0;
    for(auto& glyph: glyph_textures)
    {
        m_width = max(glyph.width, m_width);
        m_height = max(glyph.height, m_height);
    }
    font_max_length = 0;
    for(auto& glyph: glyph_map)
    {
        font_max_length = max(font_max_length, glyph.second.width);
        font_max_length = max(font_max_length, glyph.second.height);
    }
    uint side_l = max(m_width, m_height);  // Pick the largest dimension
    side_l += side_l % 2;                  // Align to the closest multiple of 2

    // Create the texture atlas bitmap.
    WriteGlyphsToAtlas(glyph_textures, atlas_buffer, side_l);

    atlas_width = side_l * SIDE_CELL_NUM;
    atlas_height = side_l * SIDE_CELL_NUM;
    // Store atlas to disk.
    stbi_write_png(
        "char.png",
        atlas_width,
        atlas_height,
        1,
        atlas_buffer.data(),
        side_l * SIDE_CELL_NUM);
}

// Create the font atlas of a face
void Scribe::ExtractGlyphs(vector<ScGlyphTexture>& glyph_textures)
{
    // For each ascii character render a bitmap
    for(uint i = ' '; i <= '~'; i++)
    {
        ScGlyphTexture texture;
        // Create the current glyph code
        auto glyph_index = FT_Get_Char_Index(*ft_face, (char16_t)i);
        auto error = FT_Load_Glyph(*ft_face, glyph_index, FT_LOAD_NO_HINTING);
        Assert(!error, "Couldn't load glyph: " + to_string(i));
        // Create the bitmap
        error = FT_Render_Glyph((*ft_face)->glyph, ft_render_mode_normal);
        Assert(!error, "Couldn't render glyph: " + to_string(i));

        auto bitmap = (*ft_face)->glyph->bitmap;
        texture.width = bitmap.width;
        texture.height = bitmap.rows;
        // Copy bitmap into the glyph structure and save it on the array
        auto size = texture.width * texture.height;
        texture.buffer.resize(texture.width * texture.height);
        if(texture.buffer.data() != nullptr)
            memcpy(texture.buffer.data(), bitmap.buffer, size);

        auto metrics = (*ft_face)->glyph->metrics;
        ScGlyphData glyph_data = {
            .width = metrics.width,
            .height = metrics.height,
            .bearing_x = metrics.horiBearingX,
            .bearing_y = metrics.horiBearingY,
            .advance = metrics.horiAdvance,
        };
        glyph_data.lt_uv = {0, float(i - ' ') / float('~' - ' ' + 1)};
        glyph_data.rb_uv = {1, float(i + 1 - ' ') / float('~' - ' ' + 1)};

        glyph_map[glyph_index] = glyph_data;
        glyph_textures.push_back(texture);
    }
}

// Internal functions
// --------------------------------------------------------------------
namespace
{
void WriteGlyphsToAtlas(
    const vector<ScGlyphTexture>& glyphs, vector<std::byte>& img_buffer, uint side_l)
{
    const int character_size = side_l * side_l;
    img_buffer.resize(SIDE_CELL_NUM * SIDE_CELL_NUM * character_size, (byte)127);
    uint character_index = 0;
    for(auto& glyph: glyphs)
    {
        const uint char_x = character_index % SIDE_CELL_NUM;
        const uint char_y = character_index / SIDE_CELL_NUM;
        const uint first_texel =
            char_x * side_l + char_y * character_size * SIDE_CELL_NUM;
        // Calculate the offsets needed to place the character on a quad
        // Add a 8 pixel offset to prevent sampling artifacts
        uint delta_h = side_l - glyph.height - 8;
        uint delta_w = 8;
        for(uint y = 0; y < side_l; y++)
        {
            for(uint x = 0; x < side_l; x++)
            {
                // If the current coordinates are in the portion of the quad where the
                // character should be written to, copy the bitmap data
                bool in_range = (delta_h <= y) && (y < glyph.height + delta_h);
                in_range = in_range && (delta_w <= x) && (x < glyph.width + delta_w);
                char value = in_range
                    ? glyph.buffer[(y - delta_h) * glyph.width + (x - delta_w)]
                    : 0;
                img_buffer[first_texel + x + y * side_l * SIDE_CELL_NUM] =
                    (std::byte)(value);
            }
        }
        character_index++;
    }
}

}  // namespace

