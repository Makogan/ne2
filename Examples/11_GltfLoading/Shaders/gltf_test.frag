#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) out vec4 color_out;

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 tex_coord;
layout(location = 2) in vec3 normal;

layout(binding = 1) uniform gltfUBO
{
    vec3 camera_position;
    float time_weight;
    int time_index;
    int target_count;
    int key_times_count;
    int vertex_count;
};

layout(binding = 2) uniform sampler2D text;

#include <phong_lighting.glsl>

void main()
{
    color_out = BlinnPhong(
        position,
        normal,
        camera_position,
        texture(text, tex_coord).rgb,
        vec3(100),
        normalize(vec3(1, 1, 1)) * 0.3,
        100);
}