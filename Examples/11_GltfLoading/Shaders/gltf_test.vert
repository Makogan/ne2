#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_EXT_scalar_block_layout : enable

/**
 * Input Description:
 *
 * @depth_test: true
 * @topology: triangle list
 * @line_width: 1
 * @polygon_mode: fill
*/

layout(location = 0) in vec3 in_position;
layout(location = 1) in vec2 in_tex_coord;
layout(location = 2) in vec3 in_normal;

layout(location = 0) out vec3 position;
layout(location = 1) out vec2 tex_coord;
layout(location = 2) out vec3 normal;

layout(binding = 0) uniform MVPOnlyUbo
{
    mat4 model;
    mat4 view;
    mat4 proj;
} ubo;

layout(binding = 1) uniform gltfUBO
{
    vec3 camera_position;
    float time_weight;
    int time_index;
    int target_count;
    int key_times_count;
    int vertex_count;
};

layout(scalar, binding = 3) buffer ib_matrix_buffer
{
   mat4 inverse_bind_matrices[];
};

layout(scalar, binding = 4) buffer anim_matrix_buffer
{
   mat4 animation_matrices[];
};

layout(scalar, binding = 5) buffer weight_buffer
{
   vec4 weights[];
};

layout(scalar, binding = 6) buffer joint_buffer
{
   ivec4 joints[];
};

layout(scalar, binding = 7) buffer morph_buffer
{
   vec4 morph_targets[];
};

layout(std430, binding = 8) buffer morph_weight_buffer
{
   float morph_weights[];
};

vec3 GetMorphOffset()
{
    vec3 offset = vec3(0);
    for(int target_index=0; target_index < target_count; target_index++)
    {
        float w1 = morph_weights[time_index * target_count + target_index];
        float w2 = morph_weights[(time_index + 1 % target_count) * target_count + target_index];
        float w = (1.f - time_weight) * w1 + w2 * time_weight;
        offset += w * morph_targets[target_index * vertex_count + gl_VertexIndex].xyz;
    }
   return offset;
}

void main()
{
    ivec4 v_joints = joints[gl_VertexIndex];
    vec4 v_weights = weights[gl_VertexIndex];

    mat4 skin_matrix =
          v_weights[0] * animation_matrices[v_joints[0]] * inverse_bind_matrices[v_joints[0]]
        + v_weights[1] * animation_matrices[v_joints[1]] * inverse_bind_matrices[v_joints[1]]
        + v_weights[2] * animation_matrices[v_joints[2]] * inverse_bind_matrices[v_joints[2]]
        + v_weights[3] * animation_matrices[v_joints[3]] * inverse_bind_matrices[v_joints[3]];

    mat4 transform = ubo.view * ubo.model * skin_matrix;
    gl_Position =
          ubo.proj * transform
        * vec4(in_position + GetMorphOffset(), 1.0);

    position = (transform * vec4(in_position, 1.0)).xyz;
    normal = (transform * vec4(in_normal, 0)).xyz;
    tex_coord = in_tex_coord;
}