/** @cond */
#include <algorithm>
#include <array>
#include <chrono>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <map>
#include <set>
#include <string.h>
#include <string>
#include <vector>
/** @endcond */

#include "Core/TextManipulation.hpp"
#include "Core/Gallery.hpp"
#include "Animation/GltfLib.hpp"
#include "Animation/GltfLoaders.hpp"
#include "Animation/AnimationMesh.hpp"
#include "ModuleStorage/Imgui/imgui_bridge.hpp"

using namespace std;
using namespace Animation;
using namespace GltfLib;

struct MVPOnlyUbo
{
    Eigen::Matrix4f model;
    Eigen::Matrix4f view;
    Eigen::Matrix4f proj;
};

std::vector<std::string> model_names = {};
std::vector<std::string> mesh_texture_names;
uint selected_model = 4;

float time_step = 0.01;
float total_time = 0;

ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
bool hovering_imgui = false;
int show_point = false;
void Demo_GUI()
{
    // Start the Dear ImGui frame
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();

    hovering_imgui = ImGui::GetIO().WantCaptureMouse;

    ImGui::Begin(
        "Sponza Data");  // Create a window called "Hello, world!" and append into it.

    ImGui::InputInt("Show Debug Point", (int*)&show_point);

    ImGui::Text(
        "Application average %.3f ms/frame (%.1f FPS)",
        1000.0f / ImGui::GetIO().Framerate,
        ImGui::GetIO().Framerate);

    ImGui::End();

    // Rendering
    ImGui::Render();
}

struct gltfUBO
{
    Eigen::Vector3f camera_position;
    float time_weight;
    int time_index;
    int target_count;
    int key_times_count;
    int vertex_count;
};

void DrawLoop(
    ModuleStorage::ModuleStorage& modules,
    NECore::Gallery& gallery,
    NECamera::ArcballCamera& camera,
    NECore::ShaderHandle gltf_shader)
{
    total_time += time_step;

    AnimationMesh<GltfLib::GltfModelData>& animated_mesh =
        gallery.GetCpuMeshData<AnimationMesh<GltfLib::GltfModelData>>(
            model_names[selected_model]);

    animated_mesh.time = total_time;

    gallery.StoreBuffer(
        SampleSkinAnimation(animated_mesh.GetSkin(), total_time),
        "animation_matrices_test" + model_names[selected_model]);

    MVPOnlyUbo mvp = {};
    mvp.model = animated_mesh.GetModelMatrix() * animated_mesh.GetRigidAnimationMatrix();
    mvp.view = camera.GetViewMatrix();
    mvp.proj = camera.GetProjectionMatrix();

    const auto [weight, index] = animated_mesh.GetMorphWeightAndIndex(total_time);
    gltfUBO gltf_info = {};
    gltf_info.camera_position = camera.GetPosition();
    gltf_info.time_weight = weight;
    gltf_info.time_index = index;
    gltf_info.target_count = animated_mesh.GetTargetCount();
    gltf_info.key_times_count = animated_mesh.GetKeyTimeCount();
    gltf_info.vertex_count = animated_mesh.GetSize();

    std::vector<NECore::GpuDataDescriptor> ssbos;
    ssbos.push_back(
        {gallery.GetGpuBufferData("skin_matrices_test" + model_names[selected_model]),
         3});
    ssbos.push_back(
        {gallery.GetGpuBufferData("animation_matrices_test" + model_names[selected_model]),
         4});
    ssbos.push_back(
        {gallery.GetGpuBufferData("weight_vectors_test" + model_names[selected_model]),
         5});
    ssbos.push_back(
        {gallery.GetGpuBufferData("joint_vectors_test" + model_names[selected_model]),
         6});

    if(animated_mesh.GetMorphTargets().size())
    {
        ssbos.push_back(
            {gallery.GetGpuBufferData("morph_test" + model_names[selected_model]), 7});
        ssbos.push_back(
            {gallery.GetGpuBufferData("morph_weight_test" + model_names[selected_model]),
             8});
    }
    else
    {
        ssbos.push_back(
            {gallery.GetGpuBufferData("empty_buffer"), 7});
        ssbos.push_back(
            {gallery.GetGpuBufferData("empty_buffer"), 8});
    }

    std::vector<NECore::GpuDataDescriptor> textures = {};
    const std::string tex_name = model_names[selected_model] + "_image_0";

    if(
        std::find(mesh_texture_names.begin(), mesh_texture_names.end(), tex_name) !=
        mesh_texture_names.end())
    {
        textures.push_back({gallery.GetGpuImageData(tex_name), 2});
    }
    else
    {
        textures.push_back({gallery.GetGpuImageData("empty_image"), 2});
    }

    modules.Draw(
        {
            gltf_shader,
            {gallery.GetGpuMeshData(model_names[selected_model])},
            textures,
            ssbos
        },
        mvp, 0,
        gltf_info, 1);

    Demo_GUI();
    RenderImgui(modules, gallery);
}

uint active_animation = 0;
void KeyEvent(void* ptr, const NECore::KeyStateMap& key_map)
{
    using namespace NECore;
    if(key_map[KEY_INDEX(GLFW_KEY_LEFT)] == NECore::KeyActionState::PRESS)
    { selected_model = (selected_model + model_names.size() - 1) % model_names.size(); }
    if(key_map[KEY_INDEX(GLFW_KEY_RIGHT)] == NECore::KeyActionState::PRESS)
    { selected_model = (selected_model + 1) % model_names.size(); }
    if(key_map[KEY_INDEX(GLFW_KEY_UP)] == NECore::KeyActionState::PRESS)
    {
        NECore::Gallery& gallery = *reinterpret_cast<NECore::Gallery*>(ptr);
        gallery
            .GetCpuMeshData<AnimationMesh<GltfLib::GltfModelData>>(
                model_names[selected_model])
            .SwitchSkinAnimation(++active_animation);
    }
}

const std::vector<std::string> paths = {
    "glTF-Sample-Models/2.0/AnimatedMorphCube/glTF/AnimatedMorphCube.gltf",
    "glTF-Sample-Models/2.0/AnimatedMorphSphere/glTF/"
    "AnimatedMorphSphere.gltf",
    "glTF-Sample-Models/2.0/SimpleSkin/glTF/SimpleSkin.gltf",
    "glTF-Sample-Models/2.0/RiggedSimple/glTF/RiggedSimple.gltf",
    "glTF-Sample-Models/2.0/CesiumMan/glTF/CesiumMan.gltf",
    "glTF-Sample-Models/2.0/RiggedFigure/glTF/RiggedFigure.gltf",
    "glTF-Sample-Models/2.0/Fox/glTF/Fox.gltf"};

std::vector<std::byte> SerializeMatrix4Vector(std::vector<Eigen::Matrix4f>& vec)
{
    std::vector<std::byte> buffer(vec.size() * sizeof(vec[0]));
    memcpy(buffer.data(), vec.data(), buffer.size());
    return buffer;
}

void StoreGltfMeshes(NECore::Gallery& gallery)
{
    std::vector<std::byte> ibuffer = {255_byte};
    CpuImage image(ibuffer.data(), 1, 1, 1);
    gallery.StoreImage<CpuImage::GetImageData>(
        image, "empty_image", NECore::ImageFormat::R8_UNORM);

    std::vector<std::byte> buffer = {0_byte};
    gallery.StoreBuffer(buffer, "empty_buffer");

    for(const auto& path: paths)
    {
        GltfLib::Model model = GltfLib::LoadModel(path);
        AnimationMesh<GltfLib::GltfModelData> mesh =
            Animation::LoadAnimatedMeshFromGltf(model)[0];

        const string path_name = Core::BaseNameWithoutExtension(path);
        model_names.push_back(path_name);

        gallery.StoreMesh(mesh, path_name);
        gallery.StoreBuffer<SerializeMatrix4Vector>(
            mesh.GetSkin().inverse_bind_matrices, "skin_matrices_test" + path_name);
        gallery.StoreBuffer(
            SampleSkinAnimation(mesh.GetSkin(), 0),
            "animation_matrices_test" + path_name);
        gallery.StoreBuffer(mesh.GetWeights(), "weight_vectors_test" + path_name);
        gallery.StoreBuffer(mesh.GetJoints(), "joint_vectors_test" + path_name);
        if(mesh.GetMorphTargets().size())
        {
            gallery.StoreBuffer(mesh.GetMorphTargets(), "morph_test" + path_name);
            gallery.StoreBuffer(
                mesh.GetMorphWeights(), "morph_weight_test" + path_name);
        }

        std::vector<CpuImage> images = Animation::LoadImagesFromGltf(model);

        const string name = path_name + "_image";
        for(uint i = 0; i < images.size(); i++)
        {
            CpuImage image = images[i];
            gallery.StoreImage<CpuImage::GetImageData>(image, name + "_" + std::to_string(i));
            mesh_texture_names.push_back(name + "_" + std::to_string(i));
        }
    }
}


void UpdateCameraAngles(
    void* ptr, float position_x, float position_y, float offset_x, float offset_y)
{
    using namespace NECamera;
    if(!hovering_imgui)
        ArcballCamera::UpdateCameraAngles(
            ptr,
            position_x,
            position_y,
            offset_x,
            offset_y);
}

int main(int argc, const char ** argv)
{
    auto modules = ModuleStorage::ModuleStorage(argc, argv);

    const NECore::ShaderHandle gltf_shader = modules.AddShader(
        {SHADER_PATH "Shaders/gltf_test.vert",
         SHADER_PATH "Shaders/gltf_test.frag"});

    auto[width, height] = modules.GetWindow().GetDimensions();
    auto camera = NECamera::ArcballCamera(width, height);
    camera.SetRadius(6);

    auto gallery = NECore::Gallery(
        ModuleStorage::GpuAllocateBuffer,
        ModuleStorage::GpuDestroyBuffer,
        ModuleStorage::GpuAllocateImage);

    NECore::InputHandler& input_handler = modules.GetInputHandler();
    input_handler.AddEvent(
        NECore::MouseInputState::LEFT_DRAG, UpdateCameraAngles, &camera);
    input_handler.AddEvent(
        NECore::MouseInputState::RIGHT_DRAG,
        NECamera::ArcballCamera::UpdateCameraPosition,
        &camera);
    input_handler.AddEvent(
        NECore::ScrollInputState::SCROLL,
        NECamera::ArcballCamera::UpdateCameraZoom,
        &camera);
    input_handler.AddEvent(KeyEvent, &gallery);

    StoreGltfMeshes(gallery);

    ImGui::SetCurrentContext(InitImgui(modules, gallery));
    while(modules.FrameUpdate())
    {
        if(modules.GetWindow().CheckSizeChanged())
        {
            auto[width, height] = modules.GetWindow().GetDimensions();
            camera.SetCreenDimensions(width, height);
        }

        DrawLoop(modules, gallery, camera, gltf_shader);


        modules.EndFrame();
        FrameMark
    }

    return EXIT_SUCCESS;
}