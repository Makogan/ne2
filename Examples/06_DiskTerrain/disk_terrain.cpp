#include <iostream>
#include <chrono>
#include <iostream>

#include "Eigen/Core"

#include "ModuleStorage/ModuleStorage.hpp"
#include "Profiler/profiler.hpp"
#include "Core/Gallery.hpp"
#include "Geometry/HalfEdge/HMesh.hpp"
#include "ImageManipulation/CpuImage.hpp"
#include "Camera/Camera.hpp"
#include "MiscUtils/VectorManipulation.hpp"
#include "MiscUtils/Noise.hpp"
#include "Geometry/Parametrics/Parametrics.hpp"
#include "ModuleStorage/Imgui/imgui_bridge.hpp"

using namespace std;
using namespace NECamera;

ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
bool hovering_imgui = false;
bool show_point = false;

float g_frequency = 1200;
float g_amplitude = 4000;
int g_frequency_decay = 5;
int g_amplitude_decay = 3;

void Demo_GUI()
{
    // Start the Dear ImGui frame.
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();

    hovering_imgui = ImGui::GetIO().WantCaptureMouse;

    ImGui::Begin("Sponza Data");

    ImGui::DragFloat("g_frequency", &g_frequency, 1.0f);
    ImGui::DragFloat("g_amplitude", &g_amplitude, 1.0f);
    ImGui::DragInt("g_frequency_decay", &g_frequency_decay, 1.0f);
    ImGui::DragInt("g_amplitude_decay", &g_amplitude_decay, 1.0f);

    ImGui::Text(
        "Application average %.3f ms/frame (%.1f FPS)",
        1000.0f / ImGui::GetIO().Framerate,
        ImGui::GetIO().Framerate);

    ImGui::End();

    // Rendering
    ImGui::Render();
}

std::pair<vector<Eigen::Vector3f>, vector<uint>> GenerateDiskTerrain(
    const Eigen::Vector3f& center = {0, 0, 0}, const Eigen::Vector3f& dir = {0, 0, 1});

struct TerrainData
{
    vector<Eigen::Vector3f> positions;
    vector<Eigen::Vector3f> normals;
    vector<uint> indices;
};

NECore::RawMeshData SerializeTerrainData(TerrainData& terrain_data)
{
    return {{
        SerializeVector(terrain_data.positions),
        SerializeVector(terrain_data.normals)},
        {terrain_data.indices}
    };
}

std::mutex geometry_lock;
void WorkerThread(
    const FlyCamera::Camera& camera,
    NECore::Gallery& gallery,
    NECore::NEWindow& window)
{
    bool flip = 0;
    while(window.IsOpen())
    {
        auto& terrain = gallery.GetCpuMeshData<TerrainData>("radial_terrain1");

        auto [positions, indices] =
            GenerateDiskTerrain(camera.GetPosition(), camera.GetLookDirection());
        terrain.positions = positions;
        terrain.normals = GenerateSmoothNormals(positions, indices);
        terrain.indices = indices;

        flip = !flip;

        std::lock_guard<std::mutex> guard = std::lock_guard<std::mutex>(geometry_lock);
        gallery.UpdateMeshData("radial_terrain1");
    }
}

float HeightFunction(
    const Eigen::Vector2f& coords,
    const float frequency = 1200,
    const float amplitude = 4000,
    const int frequency_decay = 5,
    const int amplitude_decay = 3)
{
    float height = 0;
    for(uint i = 0; i < 16; i++)
    {
        const float f = float(i * frequency_decay + 1) / frequency;
        const float a = pow(2.f, -float(i * amplitude_decay));
        height += Perlin(Eigen::Vector2f(coords * f)) * a;
    }

    return height * amplitude;
}

std::pair<vector<Eigen::Vector3f>, vector<uint>> GenerateDiskTerrain(
    const Eigen::Vector3f& position, const Eigen::Vector3f& dir)
{
    const Eigen::Vector3f center = position - dir.normalized() * 100.f;

    vector<Eigen::Vector3f> disk;
    const uint u_divisions = 300;
    const uint v_divisions = 300;
    const float theta = 120.f * M_PI / 180.f;

    const Eigen::Vector3f& start =
        Eigen::Vector3f{
            dir.x() * cos(-theta / 2.f) - dir.y() * sin(-theta / 2.f),
            dir.x() * sin(-theta / 2.f) + dir.y() * cos(-theta / 2.f),
            0}
            .normalized();

    for(uint i = 0; i < u_divisions; i++)
    {
        const float u = (float(i) / float(u_divisions - 1)) * theta;

        const Eigen::Vector3f& current_axis =
            Eigen::Vector3f{
                start.x() * cos(u) - start.y() * sin(u),
                start.x() * sin(u) + start.y() * cos(u),
                0}
                .normalized();

        for(uint j = 0; j < v_divisions; j++)
        {
            float v = (float(j) / float(v_divisions - 1));
            v = exp(2.24 * v) * v / exp(1) * 2'000.f + 0.1;

            const Eigen::Vector2f planar_coord = {
                current_axis.x() * v + center.x(), current_axis.y() * v + center.y()};
            disk.push_back(Eigen::Vector3f(
                planar_coord.x(),
                planar_coord.y(),
                HeightFunction(
                    planar_coord,
                    g_frequency,
                    g_amplitude,
                    g_frequency_decay,
                    g_amplitude_decay)));
        }
    }

    vector<uint> connections;
    for(uint i = 0; i < u_divisions - 1; i++)
    {
        for(uint j = 0; j < v_divisions - 1; j++)
        {
            connections.push_back(i * v_divisions + j);
            connections.push_back(((i + 1) % u_divisions) * v_divisions + j);
            connections.push_back(i * v_divisions + ((j + 1) % v_divisions));

            connections.push_back(((i + 1) % u_divisions) * v_divisions + j);
            connections.push_back(
                ((i + 1) % u_divisions) * v_divisions + ((j + 1) % v_divisions));
            connections.push_back(i * v_divisions + ((j + 1) % v_divisions));
        }
    }

    return {disk, connections};
}

void AddMeshes(NECore::Gallery& gallery, FlyCamera& camera)
{
    auto [data, indices] = GenerateDiskTerrain({0, 0, 0}, {0, 1, 0});
    TerrainData dbuffer = {data, GenerateSmoothNormals(data, indices), indices};

    gallery.StoreMesh<SerializeTerrainData>(dbuffer, "radial_terrain1");
}

void KeyEvent(void* ptr, const NECore::KeyStateMap& key_state_map)
{
    using namespace NECore;
    auto camera = reinterpret_cast<FlyCamera*>(ptr);

    Eigen::Vector3f forward = camera->GetLookDirection();
    const Eigen::Vector3f up = camera->GetUp();
    forward -= forward.dot(up) * up;
    forward.normalized();

    Eigen::Vector3f side = camera->GetSide();
    side -= side.dot(up) * up;
    side.normalized();

    const KeyActionState w_action = key_state_map[KEY_INDEX(GLFW_KEY_W)];
    const bool w_pressed =
        (w_action == KeyActionState::HELD || w_action == KeyActionState::PRESS);
    const KeyActionState a_action = key_state_map[KEY_INDEX(GLFW_KEY_A)];
    const bool a_pressed =
        (a_action == KeyActionState::HELD || a_action == KeyActionState::PRESS);
    const KeyActionState s_action = key_state_map[KEY_INDEX(GLFW_KEY_S)];
    const bool s_pressed =
        (s_action == KeyActionState::HELD || s_action == KeyActionState::PRESS);
    const KeyActionState d_action = key_state_map[KEY_INDEX(GLFW_KEY_D)];
    const bool d_pressed =
        (d_action == KeyActionState::HELD || d_action == KeyActionState::PRESS);
    const KeyActionState space_action = key_state_map[KEY_INDEX(GLFW_KEY_SPACE)];
    const bool space_pressed =
        (space_action == KeyActionState::HELD || space_action == KeyActionState::PRESS);
    const KeyActionState control_action = key_state_map[KEY_INDEX(GLFW_KEY_LEFT_CONTROL)];
    const bool control_pressed =
        (control_action == KeyActionState::HELD || control_action == KeyActionState::PRESS);

    const float speed = 4.0f;
    const Eigen::Vector3f offset(
        -a_pressed * side + d_pressed * side - s_pressed * forward + w_pressed * forward +
        -up * space_pressed + control_pressed * up);

    camera->Offset(offset * speed);
}

struct MVPOnlyUbo
{
    Eigen::Matrix4f model;
    Eigen::Matrix4f view;
    Eigen::Matrix4f proj;
};

int main(int argc, const char ** argv)
{
        auto modules = ModuleStorage::ModuleStorage(argc, argv);
    const NECore::ShaderHandle terrain_shader = modules.AddShader(
        {SHADER_PATH "Shaders/lines.vert",
         SHADER_PATH "Shaders/lines.frag"});

    auto gallery = NECore::Gallery(
        ModuleStorage::GpuAllocateBuffer,
        ModuleStorage::GpuDestroyBuffer,
        ModuleStorage::GpuAllocateImage);

    auto[width, height] = modules.GetWindow().GetDimensions();
    auto camera = FlyCamera(width, height, Eigen::Vector3f(0, 1, 0));
    camera.SetPosition({0, 5, -1800});
    camera.SetLookDirection({0, 1, 0.5});
    camera.SetUp({0, 0, 1});
    AddMeshes(gallery, camera);
    std::thread worker(WorkerThread, std::ref(camera), std::ref(gallery), std::ref(modules.GetWindow()));

    NECore::InputHandler& input_handler = modules.GetInputHandler();
    input_handler.AddEvent(
        NECore::MouseInputState::LEFT_DRAG, FlyCamera::UpdateCameraAngles, &camera);
    input_handler.AddEvent(KeyEvent, &camera);

    ImGui::SetCurrentContext(InitImgui(modules, gallery));
    while(modules.FrameUpdate())
    {
        if(modules.GetWindow().CheckSizeChanged())
        {
            auto[width, height] = modules.GetWindow().GetDimensions();
            camera.SetCreenDimensions(width, height);
        }

        MVPOnlyUbo mvp = {};
        mvp.model = Eigen::Matrix4f::Identity();
        mvp.view = camera.GetViewMatrix();
        mvp.proj = camera.GetProjectionMatrix();

        std::lock_guard<std::mutex> guard = std::lock_guard<std::mutex>(geometry_lock);
        modules.Draw({
                terrain_shader,
                {gallery.GetGpuMeshData("radial_terrain1")},
                {}
            },
            mvp, 0,
            60.f, 1);

        Demo_GUI();
        RenderImgui(modules, gallery);
        modules.EndFrame();
        FrameMark
    }

    worker.join();
    return 0;
}