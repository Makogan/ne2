from pyx import *
from pyx import text
from pyx.metapost.path import beginknot, endknot, smoothknot, tensioncurve
import numpy as np
import math

unit.set(wscale=1)
text.set(text.LatexEngine, texenc="utf-8")
text.preamble(r"\usepackage[utf8]{inputenc}")
text.preamble(r"\usepackage{amssymb}")
text.preamble(r"\usepackage{amsmath}")
text.preamble(r"\usepackage{amsfonts}")

DOTTED_LINE = style.linestyle(style.linecap.round, style.dash([0, 6]))
DASHED_LINE = style.linestyle.dashed
SOLID_LINE = style.linestyle.solid

c = canvas.canvas().layer("Canvas")

light_blue = (0, 0.8, 1)
lighter_blue = (0.7, 0.95, 1)
dark_blue = (0, 0.3, 0.9)

light_green = (0.8, 0.9, 0.5)
dark_green = (0.1, 0.4, 0.1)

light_red = (1, 0.3, 0.3)
lighter_red = (1, 0.8, 0.8)
pale = (0.9, 0.9, 1)


def DrawHalfArrow(in_p1, in_p2, tcolor=(0, 0, 0)):
    p1 = np.array(in_p1)
    p2 = np.array(in_p2)

    curve = path.line(p1[0], p1[1], p2[0], p2[1])

    c.stroke(curve, [style.linecap.round, color.rgb(tcolor[0], tcolor[1], tcolor[2])])

    p = 0.95 * p1 + 0.05 * p2
    c.stroke(
        path.line(p1[0], p1[1], p[0], p[1]),
        [
            style.linecap.round,
            trafo.rotate(-30, x=p1[0], y=p1[1]),
            color.rgb(tcolor[0], tcolor[1], tcolor[2]),
        ],
    )

    return path


def DrawArrow(in_p1, in_p2, tcolor=(0, 0, 0), cap_size=0.05, width=0.02):
    p1 = np.array(in_p1)
    p2 = np.array(in_p2)

    curve = path.line(p1[0], p1[1], p2[0], p2[1])

    c.stroke(curve, [
        style.linecap.round,
        color.rgb(tcolor[0], tcolor[1], tcolor[2]),
        style.linewidth(width)
        ])

    p = (1.0 - cap_size) * p2 + (cap_size) * p1
    c.stroke(
        path.line(p2[0], p2[1], p[0], p[1]),
        [
            style.linecap.round,
            trafo.rotate(-30, x=p2[0], y=p2[1]),
            color.rgb(tcolor[0], tcolor[1], tcolor[2]),
            style.linewidth(width)
        ],
    )

    c.stroke(
        path.line(p2[0], p2[1], p[0], p[1]),
        [
            style.linecap.round,
            trafo.rotate(30, x=p2[0], y=p2[1]),
            color.rgb(tcolor[0], tcolor[1], tcolor[2]),
            style.linewidth(width)
        ],
    )

    return curve


def DrawDoubleArrow(p1, p2, tcolor=(0, 0, 0)):
    DrawArrow(p1, p2, tcolor)
    return DrawArrow(p2, p1, tcolor)


def DrawHalfEdges(p1, p2, color1=(0, 0, 0), color2=(0, 0, 0)):
    dir = p2 - p1
    dir = dir / np.linalg.norm(dir)

    c_p1 = p1 + 0.55 * dir
    c_p2 = p2 - 0.55 * dir

    orthogonal_dir = np.array([dir[1], -dir[0]])
    epsilon = 0.23
    curve1 = DrawArrow(
        c_p1 + orthogonal_dir * epsilon, c_p2 + orthogonal_dir * epsilon, color1
    )
    epsilon *= -1
    curve2 = DrawArrow(
        c_p2 + orthogonal_dir * epsilon, c_p1 + orthogonal_dir * epsilon, color2
    )

    return (curve1, curve2)


def DrawSquare(corner, width, fill_color=(1, 1, 1), outline_color=(0, 0, 0), alpha=0, outline=True):
    points = [
        (corner[0], corner[1]),
        (corner[0] + width, corner[1]),
        (corner[0] + width, corner[1] + width),
        (corner[0], corner[1] + width),
    ]
    DrawPolygon(points, fill_color, outline_color, alpha, outline)


def DrawPolygon(
    points, fill_color=(1, 1, 1), outline_color=(0, 0, 0), alpha=1, outline=True
):
    point = points[0]
    poly_line = [path.moveto(point[0], point[1])]

    for p in points[1:]:
        poly_line += [path.lineto(p[0], p[1])]

    poly_line.append(path.closepath())

    polygon = path.path(*poly_line)

    style_params = [
        style.linejoin.round,
        color.rgb(outline_color[0], outline_color[1], outline_color[2]),
        deco.filled(
            [
                color.rgb(fill_color[0], fill_color[1], fill_color[2]),
                color.transparency(alpha),
            ]
        ),
    ]

    if not outline:
        style_params.append(style.linewidth(0))

    c.stroke(polygon, style_params)

    return poly_line


def DrawTriangle(
    p1, p2, p3, fill_color=(1, 1, 1), outline_color=(0, 0, 0), alpha=0, outline=True, width=0.02):

    curve = path.path(
        path.moveto(p1[0], p1[1]),
        path.lineto(p2[0], p2[1]),
        path.lineto(p3[0], p3[1]),
        path.closepath(),
    )

    style_params = [
        style.linejoin.round,
        color.rgb(outline_color[0], outline_color[1], outline_color[2]),
    ]

    if not outline:
        style_params.append(style.linewidth(0))
    else:
        style_params.append(style.linewidth(width))

    c.stroke(
        curve,
        [
            color.rgb.red,
            deco.filled(
                [
                    color.rgb(fill_color[0], fill_color[1], fill_color[2]),
                    color.transparency(alpha),
                ]
            ),
            deco.stroked(style_params),
            style.linecap.round,
        ],
    )

    return curve


def DrawFace(p1, p2, p3, fill_color=(1, 1, 1), outline_color=(0, 0, 0)):
    curve = path.path(
        path.moveto(p1[0], p1[1]),
        path.lineto(p2[0], p2[1]),
        path.lineto(p3[0], p3[1]),
        path.closepath(),
    )

    c.stroke(
        curve,
        [
            color.rgb.red,
            deco.filled([color.rgb(fill_color[0], fill_color[1], fill_color[2])]),
            deco.stroked(
                [
                    style.linejoin.round,
                    color.rgb(outline_color[0], outline_color[1], outline_color[2]),
                ]
            ),
            style.linecap.round,
        ],
    )

    return curve


def DrawLine(p1, p2, tcolor=(0, 0, 0), width=0.05, stroke=style.linestyle.dashed):

    curve = path.line(p1[0], p1[1], p2[0], p2[1])

    c.stroke(
        curve,
        [
            stroke,
            style.linecap.round,
            style.linewidth(width),
            color.rgb(tcolor[0], tcolor[1], tcolor[2]),
        ],
    )

    return curve


def DrawCircle(p1, radius, tcolor=(0, 0, 0, 0)):
    c.fill(
        path.circle(p1[0], p1[1], radius),
        [color.rgb(tcolor[0], tcolor[1], tcolor[2]), color.transparency(tcolor[3])],
    )


def DrawPoint(p1, radius, tcolor=(0, 0, 0), bcolor=(0, 0, 0)):
    c.fill(
        path.circle(p1[0], p1[1], radius), [color.rgb(tcolor[0], tcolor[1], tcolor[2])]
    )
    c.stroke(
        path.circle(p1[0], p1[1], radius), [color.rgb(bcolor[0], bcolor[1], bcolor[2]), style.linewidth(radius * 0.35)]
    )


def DrawCurve(points, tcolor=(0, 0, 0), width=1):
    array_start = [beginknot(*points[0]), tensioncurve()]

    array_body = []
    for point in points[0:-1]:
        array_body.append(smoothknot(*point))
        array_body.append(tensioncurve())

    array = array_start + array_body
    array.append(endknot(*points[-1]))

    openpath = metapost.path.path(array)
    c.stroke(openpath, [color.rgb(tcolor[0], tcolor[1], tcolor[2]), style.linewidth(width)])

    return openpath


def DrawPartialCircle(center, start_point, angle, tcolor=(0, 0, 0)):
    line = [np.array(center), np.array(start_point)]
    relative_point = line[1] - line[0]

    angle = angle * math.pi / 180.0
    # fmt: off
    rotation = np.matrix([
        [math.cos(angle / 2.0), -math.sin(angle / 2.0)],
        [math.sin(angle / 2.0), +math.cos(angle / 2.0)]
    ])
    # fmt: on

    mid_point = np.array(rotation @ relative_point)[0]
    final_point = np.array(rotation @ mid_point)[0]

    return DrawCurve(
        [line[1], mid_point + line[0], final_point + line[0]], tcolor=tcolor
    )


def DrawText(position, latex_string, tcolor=(0, 0, 0), scale=1):
    c.text(
        position[0],
        position[1],
        latex_string,
        [text.halign.boxcenter, color.rgb(tcolor[0], tcolor[1], tcolor[2]), trafo.scale(scale)],
    )


def Normalize(p):
    return p / np.linalg.norm(p)


def GetIntersectionPoints(p1, p2):

    (a1, a2), (b1, b2) = p1.intersect(p2)

    x1, y1 = p1.at(a1)
    x2, y2 = p1.at(a2)

    return ((x1, y1), (x2, y2))


def DrawBox(points, styles=[DOTTED_LINE] * 12, tcolor=(0, 0, 0)):
    thick_width = 0.02
    thin_width = 0.005

    width = thin_width if styles[0] == DOTTED_LINE else thick_width
    DrawLine(points[0], points[1], width=width, stroke=styles[0], tcolor=tcolor)

    width = thin_width if styles[1] == DOTTED_LINE else thick_width
    DrawLine(points[0], points[2], width=width, stroke=styles[1], tcolor=tcolor)

    width = thin_width if styles[2] == DOTTED_LINE else thick_width
    DrawLine(points[1], points[3], width=width, stroke=styles[2], tcolor=tcolor)

    width = thin_width if styles[3] == DOTTED_LINE else thick_width
    DrawLine(points[2], points[3], width=width, stroke=styles[3], tcolor=tcolor)
    # ===
    offset = np.array([-0.3, 0.3])
    bpoints = points + offset

    width = thin_width if styles[4] == DOTTED_LINE else thick_width
    DrawLine(bpoints[0], bpoints[1], width=width, stroke=styles[4], tcolor=tcolor)

    width = thin_width if styles[5] == DOTTED_LINE else thick_width
    DrawLine(bpoints[0], bpoints[2], width=width, stroke=styles[5], tcolor=tcolor)

    width = thin_width if styles[6] == DOTTED_LINE else thick_width
    DrawLine(bpoints[1], bpoints[3], width=width, stroke=styles[6], tcolor=tcolor)

    width = thin_width if styles[7] == DOTTED_LINE else thick_width
    DrawLine(bpoints[2], bpoints[3], width=width, stroke=styles[7], tcolor=tcolor)

    # ===
    width = thin_width if styles[8] == DOTTED_LINE else thick_width
    DrawLine(points[0], bpoints[0], width=width, stroke=styles[8], tcolor=tcolor)

    width = thin_width if styles[9] == DOTTED_LINE else thick_width
    DrawLine(points[1], bpoints[1], width=width, stroke=styles[9], tcolor=tcolor)

    width = thin_width if styles[10] == DOTTED_LINE else thick_width
    DrawLine(points[2], bpoints[2], width=width, stroke=styles[10], tcolor=tcolor)

    width = thin_width if styles[11] == DOTTED_LINE else thick_width
    DrawLine(points[3], bpoints[3], width=width, stroke=styles[11], tcolor=tcolor)


def DrawFilledBox(coords, tcolor=light_blue):
    color = np.array(tcolor)

    i, j, k = coords
    points = [(i, j), (i, j + 1), (i + 1, j), (i + 1, j + 1)]

    offset = np.array([0.3, -0.3])
    bpoints = points + offset

    offset = np.array([0.3, -0.3])
    points = [offset * (k-1) + np.array([p[0], p[1]]) for p in points]
    bpoints = [offset * (k-1) + np.array([p[0], p[1]]) for p in bpoints]
    DrawPolygon(
        [bpoints[0], bpoints[1], bpoints[3], bpoints[2]],
        fill_color=color,
        outline=False,
        alpha=0.4,
    )

    DrawPolygon(
        [points[1], points[3], bpoints[3], bpoints[1]],
        fill_color=color,
        outline=False,
        alpha=0.6,
    )

    DrawPolygon(
        [points[0], points[1], bpoints[1], bpoints[0]],
        fill_color=color,
        outline=False,
        alpha=0.8,
    )


def DrawGridBox(
    coords, dimensions, highlight_coords=(0, 0, 0), highlight=False, tcolor=(0, 0, 0)
):
    i, j, k = coords
    points = [(i, j), (i, j + 1), (i + 1, j), (i + 1, j + 1)]
    styles = [DOTTED_LINE] * 12
    if i == 0:
        styles[4] = SOLID_LINE
        styles[9] = SOLID_LINE
        styles[8] = SOLID_LINE

    if j == dimensions[1] - 1:
        styles[6] = SOLID_LINE
        styles[11] = SOLID_LINE

    if k == dimensions[2] - 1:
        styles[0] = SOLID_LINE
        styles[1] = SOLID_LINE
        styles[2] = SOLID_LINE
        styles[3] = SOLID_LINE

    offset = np.array([0.3, -0.3]) * k
    bpoints = points + offset

    DrawBox(bpoints, styles=styles, tcolor=tcolor)


def Draw3DGrid(
    dimensions,
    highlight_coords=(0, 0, 0),
    highlight=False,
    draw_at_layer=lambda layer: None,
):
    for k in range(dimensions[0]):
        for j in range(dimensions[1]):
            for i in range(dimensions[2]):
                DrawGridBox((i, j, k), dimensions, highlight_coords, highlight)
                draw_at_layer(k)
                if highlight and (i, j, k) == highlight_coords:
                    DrawFilledBox(highlight_coords)
                    DrawGridBox(
                        highlight_coords,
                        dimensions,
                        highlight_coords,
                        highlight,
                        light_blue,
                    )
