/**
 * Input Description:
 *
 * @depth_test: true
 * @topology: point list
 * @line_width: 1
 * @polygon_mode: fill
*/
#version 450
#extension GL_ARB_separate_shader_objects : enable

// Binding 0 {
layout(location = 0) in vec3 in_position;
// }

layout(location = 0) out vec3 out_position;

layout(binding = 0) uniform MVPOnlyUbo
{
    mat4 model;
    mat4 view;
    mat4 proj;
};

layout(binding = 1) uniform DottedLineUbo
{
    vec4 color;
    float size;
};

void main()
{
    out_position = in_position;
    gl_PointSize = size;
    vec3 model_position = (model * vec4(in_position, 1.0)).xyz;
    gl_Position = proj * view * vec4(model_position, 1.0);
}