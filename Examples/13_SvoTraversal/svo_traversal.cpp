const float EPSILON = 0.001;

/** @cond */
#include <array>
#include <chrono>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <map>
#include <set>
#include <string.h>
#include <string>
#include <vector>
/** @endcond */

#include "Eigen/Core"
#include "Eigen/Core"
#include "Geometry/Parametrics/Parametrics.hpp"

using namespace std;
using vec3 = Eigen::Vector3f;
using ivec3 = Eigen::Vector3i;
// namespace fs = std::filesystem;

const float CUBE_SIZE = 10;

vec3 ray_direction = {0, -1, 0};
vec3 ray_position = {-2.5, 20, -2.5};

// Check whether the position is inside of the specified box.
bool InBoxBounds(vec3 corner, float size, vec3 position)
{
    bool inside = true;
    // Put the position in the coordinate frame of the box.
    position -= corner;
    // The point is inside only if all of its components are inside.
    for(int i = 0; i < 3; i++)
    {
        inside = inside && (position[i] >= -EPSILON);
        inside = inside && (position[i] <= size + EPSILON);
    }

    return inside;
}

// Calculate the distance to the intersection to a box, or inifnity if the box cannot be hit.
float BoxIntersection(const vec3 origin, const vec3 dir, const vec3 corner0, const float size)
{
    // Calculate opposite corner.
    const vec3 corner1 = corner0 + vec3(size, size, size);

    // Set the ray plane intersections.
    float coeffs[6];
    coeffs[0] = (corner0[0] - origin[0]) / (dir[0]);
    coeffs[1] = (corner0[1] - origin[1]) / (dir[1]);
    coeffs[2] = (corner0[2] - origin[2]) / (dir[2]);
    coeffs[3] = (corner1[0] - origin[0]) / (dir[0]);
    coeffs[4] = (corner1[1] - origin[1]) / (dir[1]);
    coeffs[5] = (corner1[2] - origin[2]) / (dir[2]);

    float t = 1000000.f;//std::numeric_limits<float>::max();
    // Check for the smallest valid intersection distance.
    for(uint i = 0; i < 6; i++)
    {
        t = (coeffs[i] >= 0.00001) && InBoxBounds(corner0, size, origin + dir * (coeffs[i])) ?
            min(coeffs[i], t) : t;
    }
    return t;
}

vec3 FindBoxCoord(vec3 point, vec3 corner, float size)
{
    const vec3 scaled = (point - corner) * 2.f / float(size);
    const ivec3 truncated =
        {(int)floor(scaled[0]), (int)floor(scaled[1]), (int)floor(scaled[2])};
    return vec3(truncated[0], truncated[1], truncated[2]) * size / 2.f + corner;
}

struct CollisionPoint
{
    vec3 position;
    vec3 corner;
    int level;
};

std::array<CollisionPoint, 1000> svo_collisions;
uint stack_index = 0;
std::vector<Eigen::Vector3f> intersections;
// https://www.scratchapixel.com/lessons/advanced-rendering/introduction-acceleration-structure/grid
void IterateGrid(vec3 origin, vec3 dir, vec3 child_corner, float size, vec3 parent_corner, std::vector<CollisionPoint>& collisions, uint* collision_count_ptr)
{
    uint& collision_count = *collision_count_ptr;
    float deltas[3];
    deltas[0] = (size) / abs(dir[0]);
    deltas[1] = (size) / abs(dir[1]);
    deltas[2] = (size) / abs(dir[2]);

    const vec3 relative_origin = origin - child_corner;
    float distances[3];
    distances[0] = dir[0] < 0? relative_origin[0] / -dir[0] : (size - relative_origin[0]) / dir[0];
    distances[1] = dir[1] < 0? relative_origin[1] / -dir[1] : (size - relative_origin[1]) / dir[1];
    distances[2] = dir[2] < 0? relative_origin[2] / -dir[2] : (size - relative_origin[2]) / dir[2];

    ivec3 indices = ivec3(0, 0, 0);
    ivec3 index_steps = ivec3(GLA::sign(dir[0]), GLA::sign(dir[1]), GLA::sign(dir[2]));

    float t = 0;
    collision_count = 0;
    while(collision_count < 4)
    {
        const vec3 position = origin + t * dir;
        collisions.push_back({position, child_corner + vec3(indices[0], indices[1], indices[2]) * size, 0});
        intersections.push_back({position});

        if(
            (position.x() > parent_corner.x() + size * 2 ) || (position.x() < parent_corner.x() ) ||
            (position.y() > parent_corner.y() + size * 2 ) || (position.y() < parent_corner.y() ) ||
            (position.z() > parent_corner.z() + size * 2 ) || (position.z() < parent_corner.z() ) )
        {
            break;
        }

        float t_next = distances[0];
        uint distance_index = 0;
        for(uint i=0; i < 3; i++)
        {
            if(distances[i] < t_next && !isinf(distances[i]))
            {
                distance_index = i;
                t_next = distances[i];
            }
        }

        indices[distance_index] += index_steps[distance_index];
        t = t_next;
        distances[distance_index] += deltas[distance_index];

        collision_count++;
    }
}

NECore::RawMeshData SerializeFloatData(const std::pair<std::vector<float>, std::vector<uint>>& verts)
{
    std::vector<std::byte> data(verts.first.size() * sizeof(verts.first[0]));
    memcpy(data.data(), verts.first.data(), data.size());

    return {{data}, verts.second};
}

NECore::RawMeshData SerializeEigenVectorPair(
    std::pair<std::vector<Eigen::Vector3f>, std::vector<uint>>& vec)
{
    std::vector<std::byte> buffer(vec.first.size() * sizeof(vec.first[0]));
    memcpy(buffer.data(), vec.first.data(), buffer.size());

    return {{buffer}, vec.second};
}

NECore::RawMeshData SerializeEigenVector(std::vector<Eigen::Vector3f>& vec)
{
    std::vector<std::byte> buffer(vec.size() * sizeof(vec[0]));
    memcpy(buffer.data(), vec.data(), buffer.size());

    return {{buffer}, {(uint)vec.size()}};
}

std::vector<CollisionPoint> order;
void CollideGrid(NECore::Gallery& gallery)
{
    order.clear();
    intersections.clear();
    const vec3 cube_origin = {-CUBE_SIZE/2, -CUBE_SIZE/2, -CUBE_SIZE/2};
    bool starts_inside = InBoxBounds(cube_origin, CUBE_SIZE, ray_position);
    float t = starts_inside ? 0 : BoxIntersection(ray_position, ray_direction, cube_origin, CUBE_SIZE);
    vec3 start = ray_position + t * ray_direction;

    svo_collisions[stack_index++] =
    {
        start,
        cube_origin,
        0
    };

    intersections.push_back({start});
    // order.push_back({
    //     start,
    //     FindBoxCoord(start + 0.1 * ray_direction, cube_origin, CUBE_SIZE),
    //     CUBE_SIZE/2.f
    // });

    uint tmp = 0;
    while(tmp++ < 400 && stack_index > 0)
    {
        stack_index--;
        vec3 current_corner = svo_collisions[stack_index].corner;
        vec3 position = svo_collisions[stack_index].position;
        const int level = svo_collisions[stack_index].level;
        float size = CUBE_SIZE / float(1 << level);

        order.push_back(
        {
            position,
            current_corner,
            level
        });

        if(level == 2) continue;

        std::vector<CollisionPoint> collisions;
        uint collision_count = 0;

        vec3 child_corner = FindBoxCoord(position + 0.01 * ray_direction, current_corner, size);
        IterateGrid(position + 0.01 * ray_direction, ray_direction, child_corner, size / 2, current_corner, collisions, &collision_count);

        // for(uint i = 0; i < 4; i++)
        // {
        //     vec3 next_corner = FindBoxCoord(position + ray_direction * 0.01, current_corner, size);
        //     collisions[collision_count].position = position;
        //     collisions[collision_count].corner = next_corner;
        //     collisions[collision_count].level = level + 1;
        //     collision_count++;

        //     t = BoxIntersection(position, ray_direction, next_corner, size / 2);
        //     position = position + t * ray_direction * 1.001;

        //     if(!InBoxBounds(current_corner, size, position))
        //         break;

        //     intersections.push_back({position});
        // }

        for(int i = collision_count-1; i >=0; i--)
        {
            svo_collisions[stack_index++] =
            {
                collisions[i].position,
                collisions[i].corner,
                level + 1
            };
        }
    }

    std::vector<Eigen::Vector3f> boxes;
    for(auto& collision : order)
    {
        float size = CUBE_SIZE / float(1 << (collision.level));
        boxes.push_back(collision.corner);
        boxes.push_back(collision.corner + vec3(size, 0, 0));

        boxes.push_back(collision.corner + vec3(0, size, 0));
        boxes.push_back(collision.corner + vec3(size, size, 0));

        boxes.push_back(collision.corner + vec3(0, 0, size));
        boxes.push_back(collision.corner + vec3(size, 0, size));

        boxes.push_back(collision.corner + vec3(0, size, size));
        boxes.push_back(collision.corner + vec3(size, size, size));

        boxes.push_back(collision.corner);
        boxes.push_back(collision.corner + vec3(0, size, 0));

        boxes.push_back(collision.corner + vec3(size, 0, 0));
        boxes.push_back(collision.corner + vec3(size, size, 0));

        boxes.push_back(collision.corner + vec3(0, 0, size));
        boxes.push_back(collision.corner + vec3(0, size, size));

        boxes.push_back(collision.corner + vec3(size, 0, size));
        boxes.push_back(collision.corner + vec3(size, size, size));

        boxes.push_back(collision.corner);
        boxes.push_back(collision.corner + vec3(0, 0, size));

        boxes.push_back(collision.corner + vec3(size, 0, 0));
        boxes.push_back(collision.corner + vec3(size, 0, size));

        boxes.push_back(collision.corner + vec3(0, size, 0));
        boxes.push_back(collision.corner + vec3(0, size, size));

        boxes.push_back(collision.corner + vec3(size, size, 0));
        boxes.push_back(collision.corner + vec3(size, size, size));

    }
    gallery.StoreMesh<SerializeEigenVector>(intersections, "intersections");
    gallery.StoreMesh<SerializeEigenVector>(boxes, "boxes");
}

void AddMeshes(NECore::Gallery& gallery)
{
    const uint grid_res = 2;
    const float grid_size = CUBE_SIZE;
    std::vector<Eigen::Vector3f> grid(grid_res * grid_res * grid_res);
    for(uint i=0; i<grid_res; i++)
    {
        const float x = grid_size * (float(i) / float(grid_res - 1) - 0.5);
        for(uint j=0; j<grid_res; j++)
        {
            const float y = grid_size * (float(j) / float(grid_res - 1) - 0.5);
            for(uint k=0; k<grid_res; k++)
            {
                const float z = grid_size * (float(k) / float(grid_res - 1) - 0.5);
                grid[i * grid_res * grid_res + j * grid_res + k] =
                    {x, y, z};
            }
        }
    }

    std::vector<uint> connections;
    connections.reserve(grid_res * grid_res * grid_res);
    for(uint i=0; i<grid_res; i++)
    {
        for(uint j=0; j<grid_res; j++)
        {
            for(uint k=0; k<grid_res; k++)
            {
                uint p000 = i * grid_res * grid_res + j * grid_res + k;
                uint p001 = (i + 1) * grid_res * grid_res + j * grid_res + k;
                uint p010 = i * grid_res * grid_res + (j + 1) * grid_res + k;
                // uint p011 = (i+1) * grid_res * grid_res + (j+1) * grid_res + k;
                uint p100 = i * grid_res * grid_res + j * grid_res + (k + 1);
                // uint p101 = (i+1) * grid_res * grid_res + j * grid_res + (k+1);
                // uint p110 = i * grid_res * grid_res + (j+1) * grid_res + (k+1);
                // uint p111 = (i+1) * grid_res * grid_res + (j+1) * grid_res + (k+1);

                if(i + 1 < grid_res)
                {
                    connections.push_back(p000);
                    connections.push_back(p001);
                }

                if(j + 1 < grid_res)
                {
                    connections.push_back(p000);
                    connections.push_back(p010);
                }

                if(k + 1 < grid_res)
                {
                    connections.push_back(p000);
                    connections.push_back(p100);
                }
            }
        }
    }
    auto data = std::make_pair(grid, connections);
    gallery.StoreMesh<SerializeEigenVectorPair>(data, "grid");
    // Arrow
    {
    auto [vertices, indices] = Generate3DArrow(0.5, 4, 10, 10);
    auto normals = GenerateSharpNormals(vertices, indices);
    vector<float> arrow_vertices;
    uint i = 0;
    for(auto& v: vertices)
    {
        arrow_vertices.push_back(v.x());
        arrow_vertices.push_back(v.y());
        arrow_vertices.push_back(v.z());
        arrow_vertices.push_back(0);
        arrow_vertices.push_back(0);
        auto normal = normals[i++];
        arrow_vertices.push_back(normal.x());
        arrow_vertices.push_back(normal.y());
        arrow_vertices.push_back(normal.z());
    }

    std::pair<std::vector<float>, std::vector<uint>> arrow_data = {arrow_vertices, indices};
    gallery.StoreMesh<SerializeFloatData>(arrow_data, "arrow");
    }

    std::vector<Eigen::Vector3f> dots;
    const uint divisions = 100;
    for(uint i=0; i < divisions; i++)
    {
        dots.push_back({0, 0, float(i)* 50/divisions});
    }
    gallery.StoreMesh<SerializeEigenVector>(dots, "dots");

    CollideGrid(gallery);
}

struct MVPOnlyUbo
{
    Eigen::Matrix4f model;
    Eigen::Matrix4f view;
    Eigen::Matrix4f proj;
};

struct DottedLineUbo
{
    std::array<float, 4> color;
    float size;
};

void Loopy(
    ModuleStorage::ModuleStorage& modules,
    NECore::Gallery& gallery,
    NECamera::FlyCamera& camera,
    NECore::ShaderHandle line_shader,
    NECore::ShaderHandle dotted_line_shader,
    NECore::ShaderHandle phong_shader)
{
    MVPOnlyUbo mvp = {};
    mvp.model = Eigen::Matrix4f::Identity();
    mvp.view = camera.GetViewMatrix();
    mvp.proj = camera.GetProjectionMatrix();

    modules.Draw(
        {
            line_shader,
            {gallery.GetGpuMeshData("grid")},
            {},
            {},
            true,
            {0, 0, 0, 1}
        },
        mvp, 0);

    modules.Draw(
        {
            line_shader,
            {gallery.GetGpuMeshData("boxes")},
            {},
            {},
            false
        },
        mvp, 0);

    DottedLineUbo line_ubo = {};
    line_ubo.color = {1,0,0,1};
    line_ubo.size = 10;

    modules.Draw(
        {
            dotted_line_shader,
            {gallery.GetGpuMeshData("intersections")},
            {},
            {},
            false
        },
        mvp, 0,
        line_ubo, 1);

    mvp.model = LookAt(ray_position, Eigen::Vector3f(ray_position - ray_direction), Eigen::Vector3f{0,0,1}).inverse();
    modules.Draw(
        {
            phong_shader,
            {gallery.GetGpuMeshData("arrow")},
            {},
            {},
            false
        },
        mvp, 0);

    line_ubo.color = {1,1,1,1};
    line_ubo.size = 5;

    modules.Draw(
        {
            dotted_line_shader,
            {gallery.GetGpuMeshData("dots")},
            {},
            {},
            false
        },
        mvp, 0,
        line_ubo, 1);
}

struct KeyEventData
{
    NECamera::FlyCamera* camera;
    NECore::Gallery* gallery;
};

void KeyEvent(void* ptr, const NECore::KeyStateMap& key_state_map)
{
    using namespace NECore;
    auto event_data = reinterpret_cast<KeyEventData*>(ptr);

    auto camera = event_data->camera;
    auto gallery = event_data->gallery;

    Eigen::Vector3f forward = camera->GetLookDirection();
    const Eigen::Vector3f up = camera->GetUp();
    forward -= forward.dot(up) * up;
    forward.normalized();

    Eigen::Vector3f side = camera->GetSide();
    side -= side.dot(up) * up;
    side.normalized();

    const KeyActionState w_action = key_state_map[KEY_INDEX(GLFW_KEY_W)];
    const bool w_pressed =
        (w_action == KeyActionState::HELD || w_action == KeyActionState::PRESS);
    const KeyActionState a_action = key_state_map[KEY_INDEX(GLFW_KEY_A)];
    const bool a_pressed =
        (a_action == KeyActionState::HELD || a_action == KeyActionState::PRESS);
    const KeyActionState s_action = key_state_map[KEY_INDEX(GLFW_KEY_S)];
    const bool s_pressed =
        (s_action == KeyActionState::HELD || s_action == KeyActionState::PRESS);
    const KeyActionState d_action = key_state_map[KEY_INDEX(GLFW_KEY_D)];
    const bool d_pressed =
        (d_action == KeyActionState::HELD || d_action == KeyActionState::PRESS);
    const KeyActionState space_action = key_state_map[KEY_INDEX(GLFW_KEY_SPACE)];
    const bool space_pressed =
        (space_action == KeyActionState::HELD || space_action == KeyActionState::PRESS);
    const KeyActionState control_action = key_state_map[KEY_INDEX(GLFW_KEY_LEFT_CONTROL)];
    const bool control_pressed =
        (control_action == KeyActionState::HELD || control_action == KeyActionState::PRESS);

    const float speed = 0.05f;
    const Eigen::Vector3f offset(
        -a_pressed * side + d_pressed * side - s_pressed * forward + w_pressed * forward +
        -up * space_pressed + control_pressed * up);

    camera->Offset(offset.normalized() * speed);

    if(key_state_map[KEY_INDEX(GLFW_KEY_R)] == KeyActionState::PRESS)
    {
        ray_direction = camera->GetLookDirection().normalized();
        ray_position = camera->GetPosition();
        CollideGrid(*gallery);
    }
}

int main(int argc, const char ** argv)
{
    using namespace NECamera;
        auto modules = ModuleStorage::ModuleStorage(argc, argv);

    const NECore::ShaderHandle dotted_line_shader = modules.AddShader(
        {SHADER_PATH "Shaders/dotted_line.vert",
         SHADER_PATH "Shaders/dotted_line.frag"});

    const NECore::ShaderHandle line_shader = modules.AddShader(
        {SHADER_PATH "Shaders/lines.vert",
         SHADER_PATH "Shaders/lines.frag"});

    const NECore::ShaderHandle phong_shader = modules.AddShader(
        {SHADER_PATH "Shaders/phong.vert",
         SHADER_PATH "Shaders/phong.frag"});

    auto gallery = NECore::Gallery(
        ModuleStorage::GpuAllocateBuffer,
        ModuleStorage::GpuDestroyBuffer,
        ModuleStorage::GpuAllocateImage);

    AddMeshes(gallery);

    auto[width, height] = modules.GetWindow().GetDimensions();
    auto camera = FlyCamera(width, height, Eigen::Vector3f(0, -10, 0));
    camera.SetLookDirection({0, 1, 0});
    camera.SetUp({0, 0, 1});

    NECore::InputHandler& input_handler = modules.GetInputHandler();
    input_handler.AddEvent(
        NECore::MouseInputState::LEFT_DRAG, FlyCamera::UpdateCameraAngles, &camera);
    KeyEventData data = {&camera, &gallery};
    input_handler.AddEvent(KeyEvent, &data);

    while(modules.FrameUpdate())
    {
        if(modules.GetWindow().CheckSizeChanged())
        {
            auto[width, height] = modules.GetWindow().GetDimensions();
            camera.SetCreenDimensions(width, height);
        }

        Loopy(modules, gallery, camera, line_shader, dotted_line_shader, phong_shader);

        modules.EndFrame();
        FrameMark
    }

    return EXIT_SUCCESS;
}