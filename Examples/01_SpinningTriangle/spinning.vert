/**
 * Input Description:
 *
 * @depth_test: false
*/

#version 450
#extension GL_EXT_scalar_block_layout : enable

layout(location = 0) in vec2 inPosition;
layout(location = 1) in vec3 inColor;

layout(location = 0) out vec3 fragColor;

layout(binding = 0, scalar) uniform MatUBO
{
    mat2 transform;
};

void main() {
    gl_Position = vec4(transform * inPosition, 0.5, 1.0);
    fragColor = inColor;
}
