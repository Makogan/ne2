#include <iostream>
#include <chrono>
#include <iostream>

#include "Eigen/Core"

#include "ModuleStorage/ModuleStorage.hpp"
#include "Profiler/profiler.hpp"
#include "Core/Gallery.hpp"

using namespace std;

struct Vertex
{
    Eigen::Vector2f position;
    Eigen::Vector3f color;
};

const std::vector<Vertex> vertices = {
    {{0.0f, -0.5f}, {1.0f, 0.0f, 0.0f}},
    {{0.5f, 0.5f}, {0.0f, 1.0f, 0.0f}},
    {{-0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}}
};

NECore::RawMeshData SerializeColorVertex(const std::vector<Vertex>& verts)
{
    std::vector<std::byte> data(verts.size() * sizeof(verts[0]));
    memcpy(data.data(), verts.data(), data.size());

    return {{data}, {3}};
}

int main(int argc, const char ** argv)
{
        auto modules = ModuleStorage::ModuleStorage(argc, argv);
    NECore::ShaderHandle shader = modules.AddShader({SHADER_PATH "spinning.vert", SHADER_PATH "spinning.frag"});
    auto gallery = NECore::Gallery(
        ModuleStorage::GpuAllocateBuffer,
        ModuleStorage::GpuDestroyBuffer,
        ModuleStorage::GpuAllocateImage);

    gallery.StoreMesh<SerializeColorVertex>(vertices, "verts");

    while(modules.FrameUpdate())
    {
                static auto startTime = std::chrono::high_resolution_clock::now();

        auto currentTime = std::chrono::high_resolution_clock::now();
        float time = std::chrono::duration<float, std::chrono::seconds::period>(currentTime - startTime).count();

        Eigen::Matrix2f mat;
        mat << cos(time), -sin(time), sin(time), cos(time);

        modules.Draw({shader, {gallery.GetGpuMeshData("verts")}}, mat, 0);
        modules.EndFrame();
        FrameMark
    }
    return 0;
}