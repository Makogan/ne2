#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) out vec4 color_out;

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;

layout(binding = 0) uniform MVPOnlyUbo
{
    mat4 model;
    mat4 view;
    mat4 proj;
};

#include <phong_lighting.glsl>

void main()
{
    vec3 camera_position = vec3(view[0][3], view[1][3], view[2][3]);
    color_out = BlinnPhong(
        position,
        normal,
        camera_position,
        vec3(1),
        vec3(100),
        normalize(vec3(1, 1, 1)) * 0.3,
        100.f);
}