/** @cond */
#include <array>
#include <chrono>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <map>
#include <math.h>
#include <set>
#include <string.h>
#include <string>
#include <vector>
/** @endcond */

#include "Geometry/Parametrics/Parametrics.hpp"
#include "Animation/GltfLib.hpp"
#include "Animation/GltfLoaders.hpp"
#include "ModuleStorage/Imgui/imgui_bridge.hpp"

using namespace std;
namespace fs = std::filesystem;

namespace
{
const float DEBUG_SPHERE_RADIUS = 0.2;

uint selected = 0;

uint voxel_resolution = 256;
float voxel_map_scale = 300;

}  // namespace

NECore::RawMeshData SerializeFloatData(const std::pair<std::vector<float>, std::vector<uint>>& verts)
{
    std::vector<std::byte> data(verts.first.size() * sizeof(verts.first[0]));
    memcpy(data.data(), verts.first.data(), data.size());

    return {{data}, verts.second};
}

struct VolumeImage
{
    uint width, height, depth, channel_num;
    std::vector<Eigen::Vector4f> data;
};

NECore::RawImageData SerializeVolumeImage(VolumeImage& image)
{
    return {image.width, image.height, image.depth, image.channel_num, (void*)image.data.data()};
}

std::vector<std::string> mesh_texture_names;
void AddMeshes(NECore::Gallery& gallery)
{
    auto model = GltfLib::LoadModel(
        "glTF-Sample-Models/2.0/Sponza/glTF/Sponza.gltf");

    GltfLib::GltfModelData gltf_mesh = Animation::LoadStaticMeshFromGltf(model);
    std::vector<CpuImage> images = Animation::LoadImagesFromGltf(model);

    gallery.StoreMesh<Animation::SerializeAttributeMap>(gltf_mesh, "sponza");

    const string name = "Image";
    for(uint i = 0; i < images.size(); i++)
    {
        CpuImage image = images[i];
        gallery.StoreImage<CpuImage::GetImageData>(image, name + "_" + std::to_string(i));
        mesh_texture_names.push_back(name + "_" + std::to_string(i));
    }

    // sphere
    auto [vertices, indices] = GenerateSphere(DEBUG_SPHERE_RADIUS, 15, 15);
    auto normals = GenerateSharpNormals(vertices, indices);
    vector<float> sphere_vertices;
    uint i = 0;
    for(auto& v: vertices)
    {
        sphere_vertices.push_back(v.x());
        sphere_vertices.push_back(v.y());
        sphere_vertices.push_back(v.z());
        sphere_vertices.push_back(0);
        sphere_vertices.push_back(0);
        auto normal = normals[i++];
        sphere_vertices.push_back(normal.x());
        sphere_vertices.push_back(normal.y());
        sphere_vertices.push_back(normal.z());
    }

    std::pair<std::vector<float>, std::vector<uint>> data = {sphere_vertices, indices};
    gallery.StoreMesh<SerializeFloatData>(data, "debug_sphere");

    std::vector<Eigen::Vector3f> positions = {{0, 0, 0}};
    gallery.StoreBuffer(positions, "debug_sphere_positions");
}

VolumeImage voxel_image = {};
void AddImages(NECore::Gallery& gallery, ModuleStorage::ModuleStorage& modules)
{
    {
        voxel_image.width = voxel_resolution;
        voxel_image.height = voxel_resolution;
        voxel_image.depth = voxel_resolution;
        voxel_image.channel_num = 4;
        voxel_image.data = std::vector<Eigen::Vector4f>(
            voxel_image.width * voxel_image.height * voxel_image.depth,
            Eigen::Vector4f(0, 0, 0, 0));

        gallery.StoreImage<SerializeVolumeImage>(voxel_image, "VoxelAlbedo");
    }

    {
        auto[width, height] = modules.GetWindow().GetDimensions();

        CpuImage image(nullptr, width, height, 4);
        gallery.StoreImage<CpuImage::GetImageData>(image, "VoxelCanvas", NECore::ImageFormat::B8G8R8A8_UNORM);
    }

    {
        CpuImage image(nullptr, 1, 1, 1);
        gallery.StoreImage<CpuImage::GetImageData>(image, "empty_image", NECore::ImageFormat::R8_UNORM);
    }
}

ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
bool hovering_imgui = false;
void Demo_GUI(NECamera::FlyCamera& camera)
{
    // Start the Dear ImGui frame
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();

    hovering_imgui = ImGui::GetIO().WantCaptureMouse;

    ImGui::Begin(
        "Sponza Data");

    ImGui::InputInt("Voxel resolution", (int*)&voxel_resolution);
    ImGui::InputFloat("Voxel scale", (float*)&voxel_map_scale);

    ImGui::Text(
        "Application average %.3f ms/frame (%.1f FPS)",
        1000.0f / ImGui::GetIO().Framerate,
        ImGui::GetIO().Framerate);

    Eigen::Vector3f cam = camera.GetPosition();
    float x=cam.x(), y=cam.y(), z=cam.z();
    ImGui::InputFloat("x", (float*)&x);
    ImGui::InputFloat("y", (float*)&y);
    ImGui::InputFloat("z", (float*)&z);

    ImGui::End();

    // Rendering
    ImGui::Render();
}

struct MVPOnlyUbo
{
    Eigen::Matrix4f model;
    Eigen::Matrix4f view;
    Eigen::Matrix4f proj;
};

struct Node
{
    Eigen::Vector4i key;
    Eigen::Vector4f value;
    std::array<int, 8> children;
};

std::vector<std::byte> ssbo_data;
void CreateVoxelSSBO(NECore::Gallery& gallery)
{
    const size_t metadata_size = sizeof(int) * 4;
    const int total_voxel_memory = pow(0.2 * voxel_resolution, 3) * sizeof(Node);
    ssbo_data = std::vector<std::byte>(metadata_size + total_voxel_memory, (std::byte)0);
    const int depth = round(log2(voxel_resolution));
    const int span_size = total_voxel_memory / sizeof(Node);
    memcpy(ssbo_data.data(), &depth, sizeof(int));
    memcpy(ssbo_data.data() + sizeof(int), &depth, sizeof(int));
    memcpy(ssbo_data.data() + sizeof(int) * 2, &span_size, sizeof(int));

    gallery.StoreBuffer(ssbo_data, "voxel_ssbo");

    std::vector<int> occupancy_data(int(pow(voxel_resolution, 3)), 0);
    gallery.StoreBuffer(occupancy_data, "occupancy_ssbo");
}

struct VoxelizationUbo
{
    Eigen::Vector3f locus;
    int resolution;
    float voxel_scale;
};

struct VoxelData
{
    int resolution;
    uint display_width;
    uint display_height;
    float voxel_scale;
    Eigen::Vector3f locus;
};

uint voxelize = 0;
void Loopy(
    ModuleStorage::ModuleStorage& modules,
    NECore::Gallery& gallery,
    NECamera::FlyCamera& camera,
    NECore::ShaderHandle color_shader,
    NECore::ShaderHandle voxel_marcher_shader)
{
    MVPOnlyUbo mvp = {};
    mvp.model = Eigen::Matrix4f::Identity();
    mvp.view = camera.GetViewMatrix();
    mvp.proj = camera.GetProjectionMatrix();

    std::vector<NECore::GpuDataDescriptor> textures = {};
    if(voxelize == 0)
    {
        std::cout << "check" << std::endl;
        CreateVoxelSSBO(gallery);

        NECore::BufferHandle voxel_ssbo = gallery.GetGpuBufferData("voxel_ssbo");
        NECore::BufferHandle occupancy_ssbo = gallery.GetGpuBufferData("occupancy_ssbo");

        for(size_t i = 0; i < mesh_texture_names.size(); i++)
            textures.push_back({gallery.GetGpuImageData(mesh_texture_names[i]), 1, i});

        gallery.StoreImage<SerializeVolumeImage>(voxel_image, "VoxelAlbedo");
        textures.push_back({gallery.GetGpuImageData("VoxelAlbedo"), 2});

        const uint resolution = voxel_resolution;
        VoxelizationUbo voxelization = {};
        voxelization.resolution = resolution;
        voxelization.voxel_scale = voxel_map_scale;
        voxelization.locus = camera.GetPosition();

        NECore::RenderRequest request =
        {
            color_shader,
            {gallery.GetGpuMeshData("sponza")},
            textures,
            {{voxel_ssbo, 4}, {occupancy_ssbo, 5}},

        };
        request.image_outputs = {{gallery.GetGpuImageData("VoxelCanvas"), 0}};
        modules.Draw(
            request,
            mvp, 0,
            voxelization, 3);
    }

    voxelize = (voxelize + 1);

    NECore::BufferHandle voxel_ssbo = gallery.GetGpuBufferData("voxel_ssbo");
    NECore::BufferHandle occupancy_ssbo = gallery.GetGpuBufferData("occupancy_ssbo");

    auto[width, height] = modules.GetWindow().GetDimensions();

    VoxelData voxel_data = {};
    voxel_data.resolution = voxel_resolution;
    voxel_data.display_width = width;
    voxel_data.display_height = height;
    voxel_data.voxel_scale = voxel_map_scale;
    voxel_data.locus = camera.GetPosition();
    modules.Compute({
            voxel_marcher_shader,
            {
                {gallery.GetGpuImageData("VoxelAlbedo"), 1},
                {gallery.GetGpuImageData("VoxelCanvas"), 2}
            },
            {{voxel_ssbo, 4}, {occupancy_ssbo, 5}},
            {uint(ceil(width / 8.f)), uint(ceil(height / 8.f)), 1}},
            mvp, 0,
            voxel_data, 3
        );

    Demo_GUI(camera);
    RenderImgui(modules, gallery);
}

struct KeyEventData
{
    NECamera::FlyCamera* camera;
    NECore::Gallery* gallery;
};

void KeyEvent(void* ptr, const NECore::KeyStateMap& key_state_map)
{
    using namespace NECore;
    using namespace NECamera;
    auto camera = reinterpret_cast<FlyCamera*>(ptr);

    Eigen::Vector3f forward = camera->GetLookDirection();
    const Eigen::Vector3f up = camera->GetUp();
    forward -= forward.dot(up) * up;
    forward.normalized();

    Eigen::Vector3f side = camera->GetSide();
    side -= side.dot(up) * up;
    side.normalized();

    const KeyActionState w_action = key_state_map[KEY_INDEX(GLFW_KEY_W)];
    const bool w_pressed =
        (w_action == KeyActionState::HELD || w_action == KeyActionState::PRESS);
    const KeyActionState a_action = key_state_map[KEY_INDEX(GLFW_KEY_A)];
    const bool a_pressed =
        (a_action == KeyActionState::HELD || a_action == KeyActionState::PRESS);
    const KeyActionState s_action = key_state_map[KEY_INDEX(GLFW_KEY_S)];
    const bool s_pressed =
        (s_action == KeyActionState::HELD || s_action == KeyActionState::PRESS);
    const KeyActionState d_action = key_state_map[KEY_INDEX(GLFW_KEY_D)];
    const bool d_pressed =
        (d_action == KeyActionState::HELD || d_action == KeyActionState::PRESS);
    const KeyActionState space_action = key_state_map[KEY_INDEX(GLFW_KEY_SPACE)];
    const bool space_pressed =
        (space_action == KeyActionState::HELD || space_action == KeyActionState::PRESS);
    const KeyActionState control_action = key_state_map[KEY_INDEX(GLFW_KEY_LEFT_CONTROL)];
    const bool control_pressed =
        (control_action == KeyActionState::HELD || control_action == KeyActionState::PRESS);

    const float speed = 4.0f;
    const Eigen::Vector3f offset(
        -a_pressed * side + d_pressed * side - s_pressed * forward + w_pressed * forward +
        -up * space_pressed + control_pressed * up);

    camera->Offset(offset * speed);
}

int main(int argc, const char ** argv)
{
    using namespace NECamera;
        auto modules = ModuleStorage::ModuleStorage(argc, argv);

    const NECore::ShaderHandle color_shader = modules.AddShader(
        {SHADER_PATH "Shaders/color.vert",
         SHADER_PATH "Shaders/color.frag"});

    const NECore::ShaderHandle voxel_shader = modules.AddShader(
        {SHADER_PATH "Shaders/VoxelMarcher.comp"});

    auto gallery = NECore::Gallery(
        ModuleStorage::GpuAllocateBuffer,
        ModuleStorage::GpuDestroyBuffer,
        ModuleStorage::GpuAllocateImage);

    auto[width, height] = modules.GetWindow().GetDimensions();
    auto camera = FlyCamera(width, height, Eigen::Vector3f(0, -10, 0));
    camera.SetLookDirection({0, 1, 0});
    camera.SetUp({0, 0, 1});

    NECore::InputHandler& input_handler = modules.GetInputHandler();
    input_handler.AddEvent(
        NECore::MouseInputState::LEFT_DRAG, FlyCamera::UpdateCameraAngles, &camera);
    KeyEventData data = {&camera, &gallery};
    input_handler.AddEvent(KeyEvent, &data);

    AddMeshes(gallery);
    CreateVoxelSSBO(gallery);
    AddImages(gallery, modules);
    ImGui::SetCurrentContext(InitImgui(modules, gallery));
    while(modules.FrameUpdate())
    {
        if(modules.GetWindow().CheckSizeChanged())
        {
            auto[width, height] = modules.GetWindow().GetDimensions();
            camera.SetCreenDimensions(width, height);
        }

        Loopy(modules, gallery, camera, color_shader, voxel_shader);

        modules.EndFrame();
        FrameMark
    }

    return EXIT_SUCCESS;
}
