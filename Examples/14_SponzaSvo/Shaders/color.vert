#version 450
#extension GL_ARB_separate_shader_objects : enable

/**
 * Input Description:
 *
 * @depth_test: false
 * @topology: triangle list
 * @line_width: 10
 * @polygon_mode: fill
 * @extent: 256, 256
 * @cull_mode: none
*/
// Binding 0 {
layout(location = 0) in vec3 in_position;
layout(location = 1) in vec2 in_tex_coord;
layout(location = 2) in vec3 in_normal;
// }

// Binding 1 {
layout(location = 3) in int in_texture_id;
// }

layout(location = 0) out vec3 position;
layout(location = 1) out vec2 tex_coord;
layout(location = 2) out vec3 normal;
layout(location = 3) out flat int tex_id;

layout(binding = 0) uniform MVPOnlyUbo {
    mat4 model;
    mat4 view;
    mat4 proj;
} ubo;

layout(binding = 3) uniform VoxelizationUbo
{
    vec3 locus;
    int resolution;
    float voxel_scale;
};

void main()
{
    position = (ubo.model * vec4(in_position - locus, 1.0)).xyz / voxel_scale;

    if((abs(in_normal.x) >= abs(in_normal.y)) && (abs(in_normal.x) >= abs(in_normal.z)))
    {
        gl_Position = vec4(position.yz, 0.5, 1);
    }
    else if((abs(in_normal.y) >= abs(in_normal.x)) && (abs(in_normal.y) >= abs(in_normal.z)))
    {
        gl_Position = vec4(position.xz, 0.5, 1);
    }
    else
    {
        gl_Position = vec4(position.xy, 0.5, 1);
    }

    tex_coord = in_tex_coord;
    normal = (ubo.model * vec4(in_normal, 0)).xyz;
    tex_id = in_texture_id;
}