#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_EXT_scalar_block_layout : enable

layout(location = 0) out vec4 color_out;

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 tex_coord;
layout(location = 2) in vec3 normal;
layout(location = 3) in flat int tex_id;

layout(binding = 1) uniform sampler2D textures[69];
layout(binding = 2, rgba16f) uniform image3D volume_texture;
layout(binding = 3) uniform VoxelizationUbo
{
    vec3 locus;
    int resolution;
    float voxel_scale;
};

struct Node
{
    ivec4 key;
    vec4 value;
    int children[8];
};

layout(scalar, binding = 4) buffer hash_tree_ssbo
{
    int depth;
    int depth_decay;
    int span_size;
    int pad2;
    Node hash_tree[];
};

layout(scalar, binding = 5) buffer VoxelOccupancyData
{
    uint voxel_occupancy[];
};

#include "hash_function.glsl"

void main()
{
    color_out = texture(textures[tex_id], tex_coord);

    for(int i=0; i <= depth; i++)
    {
        ivec3 final = NormalizedPositionToVoxelCoords(position, i);
        StoreValue(color_out, ivec3(final), i);
        SetOccupancy(final, i);
    }

    // SetOccupancy(ivec3(0), 8);
    // SetOccupancy(ivec3(0), 7);
    // SetOccupancy(ivec3(1), 7);

    ivec3 final = NormalizedPositionToVoxelCoords(position, 0);
    imageStore(volume_texture, ivec3(final), color_out);
}
