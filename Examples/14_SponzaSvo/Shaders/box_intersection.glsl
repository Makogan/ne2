
#define EPSILON 0.001
// Check whether the position is inside of the specified box.
bool InBoxBounds(vec3 corner, float size, vec3 position)
{
    bool inside = true;
    // Put the position in the coordinate frame of the box.
    position -= corner;
    // The point is inside only if all of its components are inside.
    for(int i = 0; i < 3; i++)
    {
        inside = inside && (position[i] >= -EPSILON);
        inside = inside && (position[i] <= size + EPSILON);
    }

    return inside;
}

// Calculate the distance to the intersection to a box, or inifnity if the box cannot be hit.
float BoxIntersection(const vec3 origin, const vec3 dir, const vec3 corner0, const float size)
{
    // Calculate opposite corner.
    const vec3 corner1 = corner0 + vec3(size, size, size);

    // Set the ray plane intersections.
    float coeffs[6];
    coeffs[0] = (corner0[0] - origin[0]) / (dir[0]);
    coeffs[1] = (corner0[1] - origin[1]) / (dir[1]);
    coeffs[2] = (corner0[2] - origin[2]) / (dir[2]);
    coeffs[3] = (corner1[0] - origin[0]) / (dir[0]);
    coeffs[4] = (corner1[1] - origin[1]) / (dir[1]);
    coeffs[5] = (corner1[2] - origin[2]) / (dir[2]);

    float t = 1.f / 0.f;
    // Check for the smallest valid intersection distance.
    for(uint i = 0; i < 6; i++)
    {
        t = (coeffs[i] >= 0.00001) && InBoxBounds(corner0, size, origin + dir * (coeffs[i])) ?
            min(coeffs[i], t) : t;
    }
    return t;
}

vec3 FindBoxCoord(vec3 point, vec3 corner, float size)
{
    const vec3 scaled = (point - corner) * 2.f / float(size);
    const ivec3 truncated = ivec3(floor(scaled[0]), floor(scaled[1]), floor(scaled[2]));
    return vec3(truncated[0], truncated[1], truncated[2]) * size / 2.f + corner;
}