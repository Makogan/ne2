uint CalculateOffset(ivec3 coord, int lod)
{
    const uint pow_2 = 2 << (3 - 3 * lod);
    return resolution + resolution * ((1 - pow_2) / 7);
    // const int max_voxels = resolution * resolution * resolution;
    // const float reduction_rate = (1.f / 2.f) * (1.f / 2.f) * (1.f / 2.f);
    // const float geometric_series = (1.f - pow(reduction_rate, lod)) / (1.f - reduction_rate);
    // return int(round(max_voxels * geometric_series));
}

void SetOccupancy(ivec3 coord, int lod)
{
    const uint offset = CalculateOffset(coord, lod);

    const uint c_resolution = resolution >> lod;
    uint voxel_index =
        coord.x * c_resolution * c_resolution + coord.y * c_resolution + coord.z + offset;

    voxel_occupancy[voxel_index / 32] =
        voxel_occupancy[voxel_index / 32] | (1 << (voxel_index % 32));
}

bool CheckOccupancy(ivec3 coord, int lod)
{
    const uint offset = CalculateOffset(coord, lod);

    const uint c_resolution = resolution >> lod;
    uint voxel_index =
        coord.x * c_resolution * c_resolution + coord.y * c_resolution + coord.z + offset;

    return (voxel_occupancy[voxel_index / 32] & (1 << (voxel_index % 32))) != 0;
}

int HashFunction(ivec3 coord, int m)
{
    const int p1 = 7979;
    const int p2 = 3229;
    const int p3 = 7877;

    return abs(coord.x * p3 + coord.y * p2 + coord.z * p1) % m;
}

vec4 ReadValue(ivec3 coord, int lod)
{
    const float pn = pow(0.5, lod);
    const float basis = span_size / 2;
    const int offset = int((1.f - pn) * basis / (1.f - 0.5));

    const int local_span_size = int(span_size >> (lod + 1));
    int hash = HashFunction(coord, local_span_size);
    vec4 read_hash = hash_tree[hash + offset].value;

    uint safety = 0;
    while(read_hash.a != 0.f && hash_tree[hash + offset].key.xyz != coord)
    {
        hash = (hash * 7) % local_span_size;
        read_hash = hash_tree[hash + offset].value;
        if(safety++ > 10) return vec4(0);
    }

    if(hash_tree[hash + offset].key.xyz == coord)
        return hash_tree[hash + offset].value;

    return vec4(0);
}

int TableIndex(ivec3 coord, int lod, out int offset, out int local_span_size)
{
    const float pn = pow(0.5, lod);
    const float basis = span_size / 2;
    offset = int((1.f - pn) * basis / (1.f - 0.5));

    local_span_size = int(span_size >> (lod + 1));
    return HashFunction(coord, local_span_size);
}

void StoreValue(vec4 val, ivec3 coord, int lod)
{
    int offset;
    int local_span_size;
    int hash = TableIndex(coord, lod, offset, local_span_size);

    uint safety = 0;
    while(
           atomicCompSwap(hash_tree[hash + offset].key.w, 0, 1) != 0
        && (hash_tree[hash + offset].key.xyz != coord))
    {
        hash = (hash * 7) % local_span_size;
        if(safety++ > 10) return;
    }

    hash_tree[hash + offset].value = val;
    hash_tree[hash + offset].key = ivec4(coord, 1);

    // The 0 node has no parent.
    if(lod == 0) return;
    int parent_offset;
    int parent_hash = TableIndex(coord / 2, lod - 1, parent_offset, local_span_size);
    int m_index = (coord / 2).x & 4 + (coord / 2).y & 2 + (coord / 2).z & 1;
    hash_tree[parent_hash + parent_offset].children[m_index] = hash + offset;
}

ivec3 WorldPositionToVoxelCoords(vec3 position, int lod)
{
    const uint lod_resolution = resolution >> lod;
    const vec3 relative_pos = position - locus;
    ivec3 texel = ivec3( (vec3(0.5) + relative_pos / (2.f * voxel_scale) ) * lod_resolution );
    texel.y = int(lod_resolution) - texel.y;

    return texel;
}

ivec3 NormalizedPositionToVoxelCoords(vec3 position, int lod)
{
    const uint lod_resolution = resolution >> lod;
    vec3 final = (position + vec3(1)) * (lod_resolution / 2.f);
    final.y = lod_resolution - final.y;

    return ivec3(final);
}