/**
 * Input Description:
 *
 * @depth_test: true
 * @topology: point list
 * @line_width: 5
*/
#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec4 in_position;
layout(location = 1) in vec4 in_prior_position;

layout(location = 0) out vec3 position;
layout(location = 1) out float lifetime;

layout(binding = 0) uniform MVPOnlyUbo {
    mat4 model;
    mat4 view;
    mat4 proj;
};

void main()
{
    gl_PointSize = 15.f;
    vec3 modelPosition = (model * vec4(in_position.xyz, 1.0)).xyz;
    gl_Position = proj * view * vec4(modelPosition, 1.0);

    position = modelPosition;
    lifetime = in_position.w;
}