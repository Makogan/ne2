#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_GOOGLE_include_directive : require

#include <phong_lighting.glsl>

layout(location = 0) out vec4 color_out;

layout(location = 0) in vec3 position;
layout(location = 1) in float lifetime;

layout(binding = 1) uniform CameraInfo {
    vec3 camera_position;
};

void main()
{

    float relative_time = lifetime / 3.f;
    color_out = mix(vec4(1,0,0,1), vec4(1,1,0,1), relative_time);
    // color_out.a = smoothstep(1, 0.8, relative_time);
    color_out = vec4(color_out.xyz * color_out.a, color_out.a);
}