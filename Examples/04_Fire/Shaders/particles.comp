#version 450
#extension GL_ARB_separate_shader_objects : enable

struct Particle
{
	vec4 pos;
    vec4 prior_pos;
};

layout(std140, binding = 0) buffer Pos
{
   Particle particles[];
};

layout (local_size_x = 1) in;


layout(binding = 1) uniform ParticleMetaData {
    float time;
};

float rand(vec2 co){
    return fract(sin(dot(co, vec2(12.9898, 78.233))) * 43758.5453);
}

void main()
{
    // Current SSBO index
    uint index = gl_GlobalInvocationID.x;
    vec3 c_pos = particles[index].pos.xyz;
    vec3 p_pos = particles[index].prior_pos.xyz;
    float p_time = particles[index].pos.w / 3.f;

    vec3 offset = vec3(
        rand(vec2(time + c_pos.y, c_pos.x * c_pos.y * index * 13)),
        rand(vec2(time + c_pos.z, c_pos.z * index * 3)),
        rand(vec2(time + c_pos.x, c_pos.y * c_pos.z * index * -51)));

    offset = (offset * 2 - vec3(1));
    offset.y = p_time < 0.005? abs(offset.y) : offset.y;
    offset.y = c_pos.y < 0? abs(offset.y) : offset.y;
    offset.y = offset.y + abs(offset.y) * p_time;

    float delta_t = 1.f / 60.f;
    vec3 velocity = (c_pos - p_pos) / delta_t;

    float magnitude = length(abs(c_pos.xz) + vec2(0.0001));
    vec3 centrifugal = -vec3(c_pos.x, 0, c_pos.z);
    vec3 force = vec3(offset) * 0.1 + 0.1 * centrifugal * pow(0.1 + p_time, 4) * magnitude;
    force -= vec3(0, 0.05f / (0.1 + p_time) * 0.1 * velocity.y, 0);
    vec3 new_pos = 2 * c_pos - p_pos + force * delta_t;

    particles[index].prior_pos = particles[index].pos;
    particles[index].pos = vec4(new_pos, delta_t + particles[index].pos.w);
    if(particles[index].pos.w > 3 )
    {
        particles[index].pos = vec4(vec3(0), 0);
        particles[index].prior_pos = vec4(vec3(0,-0.01 , 0), 0);
    }
}
