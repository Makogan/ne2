#include <chrono>

#include "Eigen/Core"

#include "ModuleStorage/ModuleStorage.hpp"
#include "Profiler/profiler.hpp"
#include "Core/Gallery.hpp"
#include "Geometry/HalfEdge/HMesh.hpp"
#include "ImageManipulation/CpuImage.hpp"
#include "Camera/Camera.hpp"
#include "Geometry/Parametrics/Parametrics.hpp"

using namespace std;
using namespace NECamera;

struct MVPOnlyUbo
{
    Eigen::Matrix4f model;
    Eigen::Matrix4f view;
    Eigen::Matrix4f proj;
};

using uint = unsigned int;

struct Particle
{
    Eigen::Vector4f pos;
    Eigen::Vector4f prior_pos;
};

struct ParticleEmitter
{
    uint particle_count = 0;
    uint particle_spawn_rate = 40;
    uint max_particle_count = particle_spawn_rate * 120 * 3 ;
    std::vector<Particle> particles;
};
ParticleEmitter g_emitter;

std::vector<std::byte> SerializeParticles(std::vector<Particle>& particles)
{
    std::vector<std::byte> buffer(particles.size() * sizeof(Particle));
    memcpy(buffer.data(), particles.data(), buffer.size());
    return buffer;
}

void InitializeEmitter(ParticleEmitter& emitter, NECore::Gallery& gallery)
{
    emitter.particles = std::vector<Particle>(emitter.max_particle_count, {{0,0,0,0}, {0,0,0,0}});
    gallery.StoreBuffer<SerializeParticles>(emitter.particles, "fire_particles");
}

void UpdateEmitter(ParticleEmitter& emitter, NECore::Gallery& gallery)
{
    emitter.particle_count += emitter.particle_spawn_rate;
    emitter.particle_count = min(emitter.particle_count, emitter.max_particle_count);
}

int main(int argc, const char ** argv)
{
        auto modules = ModuleStorage::ModuleStorage(argc, argv);
    const NECore::ShaderHandle phong_shader = modules.AddShader(
        {SHADER_PATH "Shaders/phong_shader.vert",
         SHADER_PATH "Shaders/phong_shader.frag"});
    NECore::ShaderHandle compute_shader = modules.AddShader(
        {SHADER_PATH "Shaders/particles.comp"});

    auto gallery = NECore::Gallery(
        ModuleStorage::GpuAllocateBuffer,
        ModuleStorage::GpuDestroyBuffer,
        ModuleStorage::GpuAllocateImage);

    InitializeEmitter(g_emitter, gallery);

    auto[width, height] = modules.GetWindow().GetDimensions();
    auto camera = ArcballCamera(width, height);
    camera.SetRadius(6);
    camera.SetPosition({0, 3, -10});

    NECore::InputHandler& input_handler = modules.GetInputHandler();
    input_handler.AddEvent(
        NECore::MouseInputState::LEFT_DRAG, ArcballCamera::UpdateCameraAngles, &camera);
    input_handler.AddEvent(
        NECore::MouseInputState::RIGHT_DRAG, ArcballCamera::UpdateCameraPosition, &camera);
    input_handler.AddEvent(
        NECore::ScrollInputState::SCROLL, ArcballCamera::UpdateCameraZoom, &camera);

    float sim_time=0;
    while(modules.FrameUpdate())
    {
        if(modules.GetWindow().CheckSizeChanged())
        {
            auto[width, height] = modules.GetWindow().GetDimensions();
            camera.SetCreenDimensions(width, height);
        }

        UpdateEmitter(g_emitter, gallery);

        NECore::BufferHandle particles = gallery.GetGpuBufferData("fire_particles");

        uint c_particle_count = g_emitter.particle_count;
        modules.Compute({
            compute_shader,
            {},
            {{particles, 0}},
            {c_particle_count, 1, 1}},
            sim_time, 1
        );

        MVPOnlyUbo mvp = {};
        mvp.model = Eigen::Matrix4f::Identity();
        mvp.view = camera.GetViewMatrix();
        mvp.proj = camera.GetProjectionMatrix();

        NECore::GraphicsInputDescription graphics_input = {};
        graphics_input.vertex_inputs.buffers = {particles};
        graphics_input.element_count = c_particle_count;

        modules.Draw({
            phong_shader,
            graphics_input,    // Mesh inputs
            },
            mvp, 0,
            camera.GetPosition(), 1);
        modules.EndFrame();
        FrameMark
    }
    return 0;
}