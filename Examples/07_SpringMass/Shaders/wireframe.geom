#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(triangles) in;
layout(triangle_strip, max_vertices = 6) out;

layout(location = 0) in vec3 position[];
layout(location = 1) in vec2 tex_coord[];
layout(location = 2) in vec3 normal[];

layout(location = 0) out vec3 in_position;
layout(location = 1) out vec2 in_tex_coord;
layout(location = 2) out vec3 in_normal;

void main()
{
    vec3 p1 = position[0];
    vec3 p2 = position[1];
    vec3 p3 = position[2];

    vec3 t_normal = normalize(cross(p2 - p1, p3 - p1));

    in_normal = t_normal;
    in_position = position[0];
    gl_Position = gl_in[0].gl_Position;
    EmitVertex();
    in_normal = t_normal;
    in_position = position[1];
    gl_Position = gl_in[1].gl_Position;
    EmitVertex();
    in_normal = t_normal;
    in_position = position[2];
    gl_Position = gl_in[2].gl_Position;
    EmitVertex();
    EndPrimitive();
}