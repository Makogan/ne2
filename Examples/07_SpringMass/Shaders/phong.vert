/**
 * Input Description:
 *
 * @depth_test: true
 * @topology: triangle list
 * @line_width: 5
*/
#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 in_position;
layout(location = 1) in vec2 in_tex_coord;
layout(location = 2) in vec3 in_normal;

layout(location = 3) in vec3 instance_position;

layout(location = 0) out vec3 position;
layout(location = 1) out vec2 tex_coord;
layout(location = 2) out vec3 normal;

layout(binding = 0) uniform MVPOnlyUbo {
    mat4 model;
    mat4 view;
    mat4 proj;
} ubo;

layout(binding = 1) uniform CameraUbo {
    vec3 camera_position;
};

void main()
{
    position = (ubo.view * ubo.model * vec4(in_position + instance_position, 1.0)).xyz;

    gl_Position = ubo.proj * vec4(position, 1.0);

    tex_coord = in_tex_coord;
    normal = (ubo.view * ubo.model * vec4(in_normal, 0)).xyz;
}