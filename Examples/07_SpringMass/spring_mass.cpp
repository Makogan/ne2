#include <iostream>
#include <chrono>
#include <iostream>

#include "Eigen/Core"

#include "ModuleStorage/ModuleStorage.hpp"
#include "Profiler/profiler.hpp"
#include "Core/Gallery.hpp"
#include "Geometry/HalfEdge/HMesh.hpp"
#include "ImageManipulation/CpuImage.hpp"
#include "Camera/Camera.hpp"
#include "Animation/SpringMass.hpp"
#include "Geometry/Parametrics/Parametrics.hpp"
#include "ModuleStorage/Imgui/imgui_bridge.hpp"

namespace
{
const std::string MODEL_PATH = "Assets/bunny/bunny.obj";
const std::string TEXTURE_PATH = "Assets/statue.png";

const float DEBUG_SPHERE_RADIUS = 0.05;

int selected_vertex = -1;
float depth = 0.0;
bool toggle = true;

}  // namespace

using namespace std;
using namespace NECamera;

NECore::RawMeshData SerializeSphere(const std::pair<std::vector<float>, std::vector<uint>>& verts)
{
    std::vector<std::byte> data(verts.first.size() * sizeof(verts.first[0]));
    memcpy(data.data(), verts.first.data(), data.size());

    return {{data}, verts.second};
}

struct MVPOnlyUbo
{
    Eigen::Matrix4f model;
    Eigen::Matrix4f view;
    Eigen::Matrix4f proj;
};

std::vector<std::byte> SerializeFloats(std::vector<Eigen::Vector3f>& vec)
{
    std::vector<std::byte> buffer(vec.size() * sizeof(vec[0]));
    memcpy(buffer.data(), vec.data(), buffer.size());
    return buffer;
}

ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
bool hovering_imgui = false;
int show_point = false;
void Demo_GUI(NECore::Gallery& gallery)
{
    // Start the Dear ImGui frame
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();

    hovering_imgui = ImGui::GetIO().WantCaptureMouse;

    ImGui::Begin(
        "Jiggly Data");  // Create a window called "Hello, world!" and append into it.

    ImGui::InputInt("Show Debug Point", (int*)&show_point);
    SM::SpringMassSystem& mesh =
        gallery.GetCpuMeshData<SM::SpringMassSystem>("loaded_model");

    ImGui::InputFloat("Damping", &mesh.damping);
    ImGui::InputFloat("Sim Speed", &mesh.simulation_speed);
    ImGui::InputFloat("Stiffness", &mesh.stiffness_modifier);

    ImGui::Text(
        "Application average %.3f ms/frame (%.1f FPS)",
        1000.0f / ImGui::GetIO().Framerate,
        ImGui::GetIO().Framerate);

    ImGui::End();

    // Rendering
    ImGui::Render();
}

void AddMeshes(NECore::Gallery& gallery)
{
    const uint divisions = 6;

    std::vector<Eigen::Vector3f> helper;
    for(uint i = 0; i < divisions; i++)
    {
        const float x = float(i) / float(divisions - 1);
        for(uint j = 0; j < divisions; j++)
        {
            const float y = float(j) / float(divisions - 1);
            for(uint k = 0; k < divisions; k++)
            {
                const float z = float(k) / float(divisions - 1);
                helper.push_back(Eigen::Vector3f(x, y, z));
            }
        }
    }

    SM::SpringMassSystem system;
    for(auto& p: helper)
    {
        SM::Mass mass = {};
        mass.position = p;
        mass.prior_position = p;
        system.masses.push_back(mass);
    }

    for(uint i = 0; i < system.masses.size(); i++)
    {
        for(uint j = i + 1; j < system.masses.size(); j++)
        {
            SM::Spring spring = {};
            spring.mass_1 = i;
            spring.mass_2 = j;
            spring.stifness = 0.5;

            spring.rest_length =
                (system.masses[i].position - system.masses[j].position).norm();

            system.springs.push_back(spring);
        }
    }

    for(uint k = 0; k < divisions; k += divisions - 1)
    {
        for(uint i = 0; i < divisions - 1; i++)
        {
            for(uint j = 0; j < divisions - 1; j++)
            {
                system.surface_connectivity.push_back(
                    i * divisions * divisions + j * divisions + k);
                system.surface_connectivity.push_back(
                    (i + 1) * divisions * divisions + (j + 1) * divisions + k);
                system.surface_connectivity.push_back(
                    (i + 1) * divisions * divisions + j * divisions + k);

                system.surface_connectivity.push_back(
                    i * divisions * divisions + j * divisions + k);
                system.surface_connectivity.push_back(
                    i * divisions * divisions + (j + 1) * divisions + k);
                system.surface_connectivity.push_back(
                    (i + 1) * divisions * divisions + (j + 1) * divisions + k);

                if(k == divisions - 1)
                {
                    std::swap(
                        system.surface_connectivity.back(),
                        system.surface_connectivity
                            [system.surface_connectivity.size() - 2]);

                    std::swap(
                        system
                            .surface_connectivity[system.surface_connectivity.size() - 4],
                        system.surface_connectivity
                            [system.surface_connectivity.size() - 5]);
                }
            }
        }
    }

    for(uint i = 0; i < divisions; i += divisions - 1)
    {
        for(uint j = 0; j < divisions - 1; j++)
        {
            for(uint k = 0; k < divisions - 1; k++)
            {
                system.surface_connectivity.push_back(
                    i * divisions * divisions + j * divisions + k);
                system.surface_connectivity.push_back(
                    i * divisions * divisions + (j + 1) * divisions + (k + 1));
                system.surface_connectivity.push_back(
                    i * divisions * divisions + (j + 1) * divisions + k);

                system.surface_connectivity.push_back(
                    i * divisions * divisions + j * divisions + k);
                system.surface_connectivity.push_back(
                    i * divisions * divisions + j * divisions + (k + 1));
                system.surface_connectivity.push_back(
                    i * divisions * divisions + (j + 1) * divisions + (k + 1));

                if(i == divisions - 1)
                {
                    std::swap(
                        system.surface_connectivity.back(),
                        system.surface_connectivity
                            [system.surface_connectivity.size() - 2]);

                    std::swap(
                        system
                            .surface_connectivity[system.surface_connectivity.size() - 4],
                        system.surface_connectivity
                            [system.surface_connectivity.size() - 5]);
                }
            }
        }
    }

    for(uint j = 0; j < divisions; j += divisions - 1)
    {
        for(uint i = 0; i < divisions - 1; i++)
        {
            for(uint k = 0; k < divisions - 1; k++)
            {
                system.surface_connectivity.push_back(
                    i * divisions * divisions + j * divisions + k);
                system.surface_connectivity.push_back(
                    (i + 1) * divisions * divisions + j * divisions + k);
                system.surface_connectivity.push_back(
                    (i + 1) * divisions * divisions + j * divisions + (k + 1));

                system.surface_connectivity.push_back(
                    i * divisions * divisions + j * divisions + k);
                system.surface_connectivity.push_back(
                    (i + 1) * divisions * divisions + j * divisions + (k + 1));
                system.surface_connectivity.push_back(
                    i * divisions * divisions + j * divisions + (k + 1));

                if(j == divisions - 1)
                {
                    std::swap(
                        system.surface_connectivity.back(),
                        system.surface_connectivity
                            [system.surface_connectivity.size() - 2]);

                    std::swap(
                        system
                            .surface_connectivity[system.surface_connectivity.size() - 4],
                        system.surface_connectivity
                            [system.surface_connectivity.size() - 5]);
                }
            }
        }
    }

    system.last_time_stamp = std::chrono::steady_clock::now();
    gallery.StoreMesh<SM::SerializeMassSystem>(system, "loaded_model");

    // sphere
    auto [vertices, indices] = GenerateSphere(DEBUG_SPHERE_RADIUS, 15, 15);
    auto normals = GenerateSharpNormals(vertices, indices);
    vector<float> sphere_vertices;
    uint i = 0;
    for(auto& v: vertices)
    {
        sphere_vertices.push_back(v.x());
        sphere_vertices.push_back(v.y());
        sphere_vertices.push_back(v.z());
        sphere_vertices.push_back(0);
        sphere_vertices.push_back(0);
        auto normal = normals[i++];
        sphere_vertices.push_back(normal.x());
        sphere_vertices.push_back(normal.y());
        sphere_vertices.push_back(normal.z());
    }

    std::pair<std::vector<float>, std::vector<uint>> data = {sphere_vertices, indices};
    gallery.StoreMesh<SerializeSphere>(data, "debug_sphere");

    std::vector<Eigen::Vector3f> positions = {{0, 0, 0}};
    gallery.StoreBuffer<SerializeFloats>(positions, "debug_sphere_position");
}

struct ClickData
{
    NECore::Gallery& gallery;
    NECamera::ArcballCamera& camera;
};

void LeftClick(
    void* ptr,
    const float position_x,
    const float position_y,
    const float offset_x,
    const float offset_y)
{
    const Eigen::Vector2f position(position_x, position_y);
    const Eigen::Vector2f offset(offset_x, offset_y);

    ClickData& click_data = *reinterpret_cast<ClickData*>(ptr);
    auto& gallery = click_data.gallery;
    auto& camera = click_data.camera;

    auto& mesh = gallery.GetCpuMeshData<SM::SpringMassSystem>("loaded_model");

    const Eigen::Vector3f dir = ScreenWorldDirection(camera, position);

    uint count = 0;
    float closest_t = std::numeric_limits<float>::max();
    for(const auto& point: mesh.masses)
    {
        const float t = LineSphereIntersection(
            camera.GetPosition(), dir, point.position, DEBUG_SPHERE_RADIUS * 2);

        if(t < closest_t && t >= 0)
        {
            closest_t = t;
            selected_vertex = count;
        }
        count++;
    }
    if(selected_vertex != -1)
    {
        Eigen::Matrix4f view_proj = camera.GetProjectionMatrix() * camera.GetViewMatrix();
        const vector<Eigen::Vector3f> vertex = {mesh.masses[selected_vertex].position};
        gallery.StoreBuffer<SerializeFloats>(vertex, "debug_sphere");

        Eigen::Vector3f v = mesh.masses[selected_vertex].position;
        Eigen::Vector4f p = (view_proj * Eigen::Vector4f(v.x(), v.y(), v.z(), 1.f));
        p = p * 1.f / (p.w());

        depth = p.z();
    }
}

void LeftDrag(
    void* ptr,
    const float position_x,
    const float position_y,
    const float offset_x,
    const float offset_y)
{
    const Eigen::Vector2f position(position_x, position_y);
    const Eigen::Vector2f offset(offset_x, offset_y);

    ClickData& click_data = *reinterpret_cast<ClickData*>(ptr);
    auto& gallery = click_data.gallery;
    auto& camera = click_data.camera;

    if(toggle)
    {
        if(hovering_imgui) return;
        ArcballCamera::UpdateCameraAngles(
            &camera, position.x(), position.y(), offset.x(), offset.y());
    }
    else
    {
        if(selected_vertex == -1) return;
        auto& mesh = gallery.GetCpuMeshData<SM::SpringMassSystem>("loaded_model");

        const Eigen::Matrix4f mp = camera.GetProjectionMatrix();
        const Eigen::Matrix4f mv = camera.GetViewMatrix();

        const Eigen::Matrix4f view_proj = mp * mv;

        const Eigen::Vector3f unprojected =
            Unproject({position.x(), position.y(), depth}, view_proj);

        mesh.masses[selected_vertex].position = unprojected;
        gallery.UpdateMeshData("loaded_model");
        const vector<Eigen::Vector3f> vertex = {unprojected};
        gallery.StoreBuffer<SerializeFloats>(vertex, "debug_sphere");
    }
}

void KeyEvent(void* ptr, const NECore::KeyStateMap& key_state_map)
{
    using namespace NECore;
    if(key_state_map[KEY_INDEX(GLFW_KEY_SPACE)] == KeyActionState::PRESS)
    {
        toggle = !toggle;
    }
}

void Loopy(
    ModuleStorage::ModuleStorage& modules,
    NECore::Gallery& gallery,
    NECamera::ArcballCamera& camera,
    const NECore::ShaderHandle wireframe_shader,
    const NECore::ShaderHandle phong_shader)
{
    auto& mesh = gallery.GetCpuMeshData<SM::SpringMassSystem>("loaded_model");
    SM::UpdateSpringMassSystem(mesh);

    if(selected_vertex != -1)
    {
        const vector<Eigen::Vector3f> vertex = {mesh.masses[selected_vertex].position};
        gallery.StoreBuffer<SerializeFloats>(vertex, "debug_sphere_position");
    }

    gallery.UpdateMeshData("loaded_model");

    MVPOnlyUbo mvp = {};
    mvp.model = Eigen::Matrix4f::Identity();
    mvp.view = camera.GetViewMatrix();
    mvp.proj = camera.GetProjectionMatrix();

    modules.Draw({
            wireframe_shader,
            {gallery.GetGpuMeshData("loaded_model")},
            {}
        },
        mvp, 0);

    modules.Draw({
            phong_shader,
            {
                gallery.GetGpuMeshData("debug_sphere"),            // Mesh
                gallery.GetGpuBufferData("debug_sphere_position"), // Instance
                1
            },
            {},
            {},
            false
        },
        mvp, 0,
        camera.GetPosition(), 1);
}

int main(int argc, const char ** argv)
{
        auto modules = ModuleStorage::ModuleStorage(argc, argv);
    const NECore::ShaderHandle phong_shader = modules.AddShader(
        {SHADER_PATH "Shaders/phong.vert",
         SHADER_PATH "Shaders/phong.frag"});
    const NECore::ShaderHandle wireframe_shader = modules.AddShader(
        {SHADER_PATH "Shaders/wireframe.vert",
         SHADER_PATH "Shaders/wireframe.geom",
         SHADER_PATH "Shaders/wireframe.frag"});

    auto gallery = NECore::Gallery(
        ModuleStorage::GpuAllocateBuffer,
        ModuleStorage::GpuDestroyBuffer,
        ModuleStorage::GpuAllocateImage);

    HMesh<VertexData> mesh(MODEL_PATH);
    CpuImage image(TEXTURE_PATH);
    gallery.StoreMesh<HMesh<VertexData>::GetGeometryData>(mesh, "dragon");
    gallery.StoreImage<CpuImage::GetImageData>(image, "statue");

    auto[width, height] = modules.GetWindow().GetDimensions();
    auto camera = ArcballCamera(width, height);
    camera.SetRadius(6);
    ArcballCamera::UpdateCameraAngles((void*)&camera, -0.8, -0.1, 0.5, -0.7);

    ClickData click_data = {gallery, camera};

    NECore::InputHandler& input_handler = modules.GetInputHandler();
    input_handler.AddEvent(
        NECore::MouseInputState::LEFT_DRAG, LeftDrag, &click_data);
    input_handler.AddEvent(
        NECore::MouseInputState::LEFT_DOWN, LeftClick, &click_data);
    input_handler.AddEvent(
        NECore::MouseInputState::RIGHT_DRAG, ArcballCamera::UpdateCameraPosition, &camera);
    input_handler.AddEvent(
        NECore::ScrollInputState::SCROLL, ArcballCamera::UpdateCameraZoom, &camera);
    input_handler.AddEvent(KeyEvent, &camera);

    AddMeshes(gallery);
    ImGui::SetCurrentContext(InitImgui(modules, gallery));
    while(modules.FrameUpdate())
    {
        if(modules.GetWindow().CheckSizeChanged())
        {
            auto[width, height] = modules.GetWindow().GetDimensions();
            camera.SetCreenDimensions(width, height);
        }

        Loopy(modules, gallery, camera, wireframe_shader, phong_shader);

        Demo_GUI(gallery);
        RenderImgui(modules, gallery);
        modules.EndFrame();
        FrameMark
    }
    return 0;
}