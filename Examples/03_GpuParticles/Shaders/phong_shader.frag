#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_GOOGLE_include_directive : require

#include <phong_lighting.glsl>

layout(location = 0) out vec4 color_out;

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 tex_coord;
layout(location = 2) in vec3 normal;

layout(binding = 1) uniform CameraInfo {
    vec3 camera_position;
};

void main()
{
    color_out = BlinnPhong(
        position,
        normal,
        camera_position,
        vec3(0, 1, 1),
        vec3(1),
        normalize(vec3(1, 1, 1)) * 0.3,
        100);
}