#include <chrono>

#include "Eigen/Core"

#include "ModuleStorage/ModuleStorage.hpp"
#include "Profiler/profiler.hpp"
#include "Core/Gallery.hpp"
#include "Geometry/HalfEdge/HMesh.hpp"
#include "ImageManipulation/CpuImage.hpp"
#include "Camera/Camera.hpp"
#include "Geometry/Parametrics/Parametrics.hpp"

using namespace std;
using namespace NECamera;

struct MVPOnlyUbo
{
    Eigen::Matrix4f model;
    Eigen::Matrix4f view;
    Eigen::Matrix4f proj;
};

struct Particle
{
    Eigen::Vector4f pos;
};

struct Vertex
{
    Eigen::Vector3f position;
    Eigen::Vector2f uv;
    Eigen::Vector3f normal;
};

NECore::RawMeshData SerializeVertex(const std::pair<std::vector<Vertex>, std::vector<uint>>& verts)
{
    std::vector<std::byte> data(verts.first.size() * sizeof(verts.first[0]));
    memcpy(data.data(), verts.first.data(), data.size());

    return {{data}, verts.second};
}

std::vector<std::byte> SerializeParticles(std::vector<Particle>& vec)
{
    std::vector<std::byte> buffer(vec.size() * sizeof(vec[0]));
    memcpy(buffer.data(), vec.data(), buffer.size());
    return buffer;
}

int main(int argc, const char ** argv)
{
        auto modules = ModuleStorage::ModuleStorage(argc, argv);
    const NECore::ShaderHandle phong_shader = modules.AddShader(
        {SHADER_PATH "Shaders/phong_shader.vert",
         SHADER_PATH "Shaders/phong_shader.frag"});
    NECore::ShaderHandle compute_shader = modules.AddShader(
        {SHADER_PATH "Shaders/particles.comp"});

    auto gallery = NECore::Gallery(
        ModuleStorage::GpuAllocateBuffer,
        ModuleStorage::GpuDestroyBuffer,
        ModuleStorage::GpuAllocateImage);

    // Sphere
    auto [vertices, indices] = GenerateSphere(0.1, 50, 50);
    auto normals = GenerateSharpNormals(vertices, indices);
    vector<Vertex> sphere_vertices;
    uint i = 0;
    for(auto& v: vertices)
    {
        sphere_vertices.push_back({
            {v.x(), v.y(), v.z()},
            {0, 0},
            {normals[i].x(), normals[i].y(), normals[i].z()}
        });
        i++;
    }
    auto pair = std::make_pair(sphere_vertices, indices);
    gallery.StoreMesh<SerializeVertex>(pair, "sphere");

    std::vector<Particle> particles(100);
    i = 0;
    for(auto& particle: particles)
    {
        uint q = i / 10;
        uint r = i % 10;
        particle.pos = Eigen::Vector4f(q / 10.f, r / 10.f, 0, 0);
        i++;
    }
    gallery.StoreBuffer<SerializeParticles>(particles, "particles");

    auto[width, height] = modules.GetWindow().GetDimensions();
    auto camera = ArcballCamera(width, height);
    camera.SetRadius(6);

    NECore::InputHandler& input_handler = modules.GetInputHandler();
    input_handler.AddEvent(
        NECore::MouseInputState::LEFT_DRAG, ArcballCamera::UpdateCameraAngles, &camera);
    input_handler.AddEvent(
        NECore::MouseInputState::RIGHT_DRAG, ArcballCamera::UpdateCameraPosition, &camera);
    input_handler.AddEvent(
        NECore::ScrollInputState::SCROLL, ArcballCamera::UpdateCameraZoom, &camera);

    while(modules.FrameUpdate())
    {
        if(modules.GetWindow().CheckSizeChanged())
        {
            auto[width, height] = modules.GetWindow().GetDimensions();
            camera.SetCreenDimensions(width, height);
        }

        modules.Compute({
            compute_shader,
            {},
            {{gallery.GetGpuBufferData("particles"), 0}},
            {100, 1, 1}}
        );

        MVPOnlyUbo mvp = {};
        mvp.model = Eigen::Matrix4f::Identity();
        mvp.view = camera.GetViewMatrix();
        mvp.proj = camera.GetProjectionMatrix();

        modules.Draw({
            phong_shader,
            {gallery.GetGpuMeshData("sphere"),          // Mesh inputs
             gallery.GetGpuBufferData("particles"),     // Instance buffer
             100                                        // Instance count
            }},
            mvp, 0,
            camera.GetPosition(), 1);

        modules.EndFrame();
        FrameMark
    }
    return 0;
}