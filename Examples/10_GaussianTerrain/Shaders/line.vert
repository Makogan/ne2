#version 450
#extension GL_ARB_separate_shader_objects : enable

/**
 * Input Description:
 *
 * @depth_test: true
 * @topology: triangle list
 * @line_width: 1
 * @polygon_mode: line
*/
layout(location = 0) in vec3 in_position;

layout(location = 0) out vec3 position;

layout(binding = 0) uniform MVPOnlyUbo {
    mat4 model;
    mat4 view;
    mat4 proj;
} ubo;

void main()
{
    position = (ubo.view * ubo.model * vec4(in_position, 1.0)).xyz;

    gl_Position = ubo.proj * vec4(position, 1.0);
    gl_PointSize = 5.f;
}