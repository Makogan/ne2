#version 450
#extension GL_ARB_separate_shader_objects : enable

#define PI 3.14159265359

layout(location = 0) in vec2 fragTexCoord;

layout(location = 0) out vec4 outColor;

layout(binding = 0) uniform sampler2D texSampler;

layout(binding = 1) uniform GaussianDilationUbo {
    int radius;
    float sigma;
    float intensity;
    float x_frequency;
    float y_frequency;
    float distortion_strength;
};

float Gaussian(float x, float sigma)
{
    return 1.f / (sqrt(2.f * PI) * sigma) * exp(-(x * x) / (2.f * sigma * sigma));
}

void main()
{
    ivec2 texture_size = textureSize(texSampler, 0).xy;
    ivec2 texel_coord = ivec2(fragTexCoord.x * texture_size.x, fragTexCoord.y * texture_size.y);
    vec2 texel_offset =
        (vec2(cos(texel_coord.y * x_frequency) / texture_size.y, sin(texel_coord.x * y_frequency)
            / texture_size.y)) * distortion_strength;

    vec4 value = vec4(0);
    for(int i=-radius; i<radius; i++)
    {
        for(int j=-radius; j<radius; j++)
        {
            vec2 new_coord = texel_coord + ivec2(i, j);
            new_coord.x /= texture_size.x;
            new_coord.y /= texture_size.y;
            float d = float(i * i + j * j);
            float g1 = Gaussian(i, sigma);
            float g2 = Gaussian(j, sigma);
            value += texture(texSampler, new_coord + texel_offset)
                * (g1 * g2 * g1 * g2) * intensity;
        }
    }

    outColor = normalize(value);
}