/** @cond */
#include <algorithm>
#include <array>
#include <chrono>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <map>
#include <random>
#include <set>
#include <string.h>
#include <string>
#include <vector>
/** @endcond */

#include "Geometry/HalfEdge/Subdivision.hpp"
#include "Core/Gallery.hpp"
#include "MiscUtils/Noise.hpp"
#include "Geometry/CSG/DualContouring.hpp"
#include "MiscUtils/VectorManipulation.hpp"
#include "UI/Scribe.hpp"
#include "Geometry/Parametrics/Parametrics.hpp"
#include "ImageManipulation/CpuImage.hpp"
#include "ModuleStorage/Imgui/imgui_bridge.hpp"
#include "ModuleStorage/ModuleStorage.hpp"
#include "Geometry/HalfEdge/HMeshUtils.hpp"
#include "Camera/Camera.hpp"

using namespace std;
namespace fs = std::filesystem;

namespace
{
const std::string MODEL_PATH = "Assets/cube.obj";
const std::string TEXTURE_PATH = "Assets/statue.png";

const float RADIUS = 3;

std::vector<HyperVertex> original_vertices;

vector<float> HyperVerticesToGPUData()
{
    vector<float> data;
    for(auto& v: original_vertices)
    {
        auto [mu, sigma_inv] = DualToComponents(v.position);

        data.push_back(mu[0]);
        data.push_back(mu[1]);
        data.push_back(mu[2]);

        for(int i = 0; i < sigma_inv.size(); i++)
        {
            data.push_back(sigma_inv.data()[i]);
        }
    }

    return data;
}

std::vector<Eigen::VectorXf> ExtractPositions(HMesh<HyperVertex>& mesh)
{
    const uint size = mesh.size();

    std::vector<Eigen::VectorXf> positions(size);
    for(uint i = 0; i < size; i++)
    {
        positions[i] = mesh.VertexData()[i].position;
    }

    return positions;
}

std::vector<Eigen::Vector3f> ExtractNormals(HMesh<HyperVertex>& mesh)
{
    const uint size = mesh.size();

    std::vector<Eigen::Vector3f> normals(size);
    for(uint i = 0; i < size; i++)
    {
        normals[i] = mesh.VertexData()[i].normal;
    }

    return normals;
}

NECore::RawMeshData ExtractVertexDebugInfo(HMesh<HyperVertex>*& mesh)
{
    auto v_positions = ExtractPositions(*mesh);
    auto normals = ExtractNormals(*mesh);
    auto indices = Flatten(mesh->Indices());

    std::vector<Eigen::Vector3f> positions;
    positions.reserve(mesh->size());
    for(const auto& p: v_positions)
    {
        positions.push_back({p[0], p[1], p[2]});
    }

    std::vector<std::byte> serialized_positions(positions.size() * sizeof(positions[0]));
    memcpy(serialized_positions.data(), positions.data(), serialized_positions.size());

    std::vector<std::byte> serialized_normals(normals.size() * sizeof(normals[0]));
    memcpy(serialized_normals.data(), normals.data(), serialized_normals.size());

    return {{serialized_positions, serialized_normals}, indices};
}

}  // namespace

// Example -------------------------------------------------------------------------------

void InitGaussianSigmas(HMesh<HyperVertex>& mesh)
{
    using namespace Eigen;
    auto& vertices = mesh.VertexDataD();
    for(auto& vertex: vertices)
    {
        Eigen::VectorXf& position = vertex.position;
        position.conservativeResize(9, 1);
        MatrixXf sigma_inv = MatrixXf::Identity(3, 3);

        position << position[0], position[1], position[2], sigma_inv(0, 0),
            sigma_inv(1, 0), sigma_inv(1, 1), sigma_inv(2, 0), sigma_inv(2, 1),
            sigma_inv(2, 2);
    }
}

void SetCovarianceOfVertex(HMesh<HyperVertex>& mesh, uint i, const Eigen::MatrixXf& cov)
{
    using namespace Eigen;
    auto& vertex = mesh.VertexDataD()[i].position;
    MatrixXf position(1, 3);
    position << vertex[0], vertex[1], vertex[2];
    MatrixXf sigma_inv = cov.inverse();

    vertex << position(0, 0), position(0, 1), position(0, 2), sigma_inv(0, 0),
        sigma_inv(1, 0), sigma_inv(1, 1), sigma_inv(2, 0), sigma_inv(2, 1),
        sigma_inv(2, 2);
}

vector<Eigen::Vector3f> UniformPoints(uint size)
{
    vector<Eigen::Vector3f> return_val;
    std::default_random_engine generator(glfwGetTime());
    std::normal_distribution<float> distributionx(0.0, 1);
    std::normal_distribution<float> distributiony(0.0, 1);
    std::normal_distribution<float> distributionz(0.0, 1);

    for(uint i = 0; i < size; i++)
    {
        float x, y, z;
        x = distributionx(generator);
        y = distributiony(generator);
        z = distributionz(generator);

        const auto p = Eigen::Vector3f(x, y, z);
        assert(p.norm() != 0);
        return_val.push_back(p.normalized());
    }

    return return_val;
}

vector<Eigen::MatrixXf> RandomGaussians(const int num)
{
    std::default_random_engine generator;
    std::normal_distribution<float> scale_distribution(1, 0.5);
    auto scale_selector = std::bind(scale_distribution, generator);
    std::uniform_real_distribution<float> angle_distribution(0, M_PI);
    auto angle_selector = std::bind(angle_distribution, generator);
    std::uniform_int_distribution<int> sign_distribution(-1, 1);
    auto sign_selector = std::bind(sign_distribution, generator);

    auto points = UniformPoints(num);

    vector<Eigen::MatrixXf> gaussians;
    for(int i = 0; i < num; i++)
    {
        const float angle = angle_selector();
        auto axis = points[i];
        const auto x = abs(scale_selector());
        const auto y = abs(scale_selector());
        const auto z = abs(scale_selector());

        if(axis.norm() <= 0.000001)
            axis = axis +
                Eigen::Vector3f{
                    float(sign_selector()), float(sign_selector()), float(num)};
        axis.normalize();

        Eigen::MatrixXf scale = Eigen::MatrixXf::Identity(3, 3);
        scale.diagonal() = Eigen::Vector3f(x, y, z);

        Eigen::Matrix3f rotation(Eigen::AngleAxisf(angle, axis));

        gaussians.push_back(rotation.transpose() * scale * rotation);
    }
    return gaussians;
}

void GenerateTerrain(NECore::Gallery& gallery)
{
    using Vec2 = Eigen::Vector2f;
    // Floor
    const float side_length = 30.f;
    const uint divisions = 10;
    std::vector<HyperVertex> terrain(divisions * divisions);
    for(uint i = 0; i < divisions; i++)
    {
        const float u = float(i) / float(divisions - 1);
        const float x = u * side_length;
        for(uint j = 0; j < divisions; j++)
        {
            const float v = float(j) / float(divisions - 1);
            const float y = v * side_length;
            const float z = Perlin(Vec2(Vec2(x, y) * 1.f / 4.f)) * 10.f;

            terrain[divisions * i + j].position.resize(3);
            terrain[divisions * i + j].position << x - 0.5 * side_length,
                y - 0.5 * side_length, z;
        }
    }

    std::vector<uint> connectivity;
    for(uint i = 0; i < divisions - 1; i++)
    {
        for(uint j = 0; j < divisions - 1; j++)
        {
            // Grab the 4 points in a quad face in the sphere
            uint p00 = (i * (divisions) + j);
            uint p01 = (i * (divisions) + ((j + 1) % divisions));
            uint p10 = ((i + 1) * divisions + j);
            uint p11 = ((i + 1) * divisions + ((j + 1) % divisions));

            // Add the first triangle
            connectivity.push_back(p00);
            connectivity.push_back(p01);
            connectivity.push_back(p11);

            // Add the second triangle
            connectivity.push_back(p00);
            connectivity.push_back(p11);
            connectivity.push_back(p10);
        }
    }

    HMesh<HyperVertex> terrain_mesh(terrain, connectivity);
    InitGaussianSigmas(terrain_mesh);

    auto gaussians = RandomGaussians(terrain_mesh.VertexData().size());
    for(uint i = 0; i < terrain_mesh.VertexData().size(); i++)
    {
        SetCovarianceOfVertex(terrain_mesh, i, gaussians[i]);
    }

    gallery.StoreMesh<HMesh<HyperVertex>::GetGeometryData>(terrain_mesh, "terrain");

    // cube
    auto F = [](const Eigen::Vector3f& v) {
        Eigen::Vector3f q = v.array().abs().matrix() - Eigen::Vector3f(1, 1, 1);
        return (q.array().max(0.f)).matrix().norm() +
            std::min(std::max(q.x(), std::max(q.y(), q.z())), 0.f) - 0.4f;
    };

    auto data = DualContouring<Eigen::Vector3f>(
        F,
        5.f,
        30
    );

    // Torus
    /*auto data = DualContouring<Eigen::Vector3f, float>([](const Eigen::Vector3f& v) {
        Eigen::Vector2f q =
            Eigen::Vector2f(Eigen::Vector2f(v.x(), v.z()).norm() - 2.1f, v.y());
        return q.norm() - 1.3f;
    });*/

    // Cool
    // auto data = DualContouring<Eigen::Vector3f, float>(
    //     [](const Eigen::Vector3f& v)
    //     {
    //         const float x = v.x();
    //         const float y = v.y();
    //         const float z = v.z();

    //         const float a1 = 3 * (x - 1) * x * x * (x + 1) + 2 * y * y;
    //         const float b1 = z * z - 0.85;

    //         const float a2 = 3 * (y - 1) * y * y * (y + 1) + 2 * x * x;
    //         const float b2 = x * x - 0.85;

    //         const float a3 = 3 * (z - 1) * z * z * (z + 1) + 2 * x * x;
    //         const float b3 = y * y - 0.85;

    //         return a1 * a1 + b1 * b1 * a2 * a2 + b2 * b2 * a3 * a3 + b3 * b3 * -0.12f;
    //     },
    //     5.f,
    //     120);

    // TODO (low): figure out why this clashes with the half edge.
    // Plane
    /*auto data = DualContouring<Eigen::Vector3f, float>(
        [](const Eigen::Vector3f& v) {
            return (v - Eigen::Vector3f(0, 0, 0)).dot(Eigen::Vector3f(0, 1, 0));
        },
        10.f,
        4);*/

    std::vector<HyperVertex> implicit_verts(data.first.size());

    for(uint i = 0; i < data.first.size(); i++)
    {
        implicit_verts[i].normal = {0, 1, 0};
        implicit_verts[i].position.resize(3);
        implicit_verts[i].position << data.first[i].x(), data.first[i].y(),
            data.first[i].z();
    }

    HMesh<HyperVertex> i_mesh(implicit_verts, data.second);

    InitGaussianSigmas(i_mesh);

    auto i_gaussians = RandomGaussians(i_mesh.VertexData().size());
    for(uint i = 0; i < i_mesh.VertexData().size(); i++)
    {
        SetCovarianceOfVertex(i_mesh, i, i_gaussians[i]);
    }

    Smoothen(i_mesh);
    i_mesh.CalculateNormals();
    gallery.StoreMesh<HMesh<HyperVertex>::GetGeometryData>(i_mesh, "contour");

    auto& mesh = gallery.GetCpuMeshData<HMesh<HyperVertex>>("contour");
    auto ptr = &mesh;
    gallery.StoreMesh<ExtractVertexDebugInfo>(ptr, "contour_debug");
}

NECore::RawMeshData SerializeFloatData(const std::pair<std::vector<float>, std::vector<uint>>& verts)
{
    std::vector<std::byte> data(verts.first.size() * sizeof(verts.first[0]));
    memcpy(data.data(), verts.first.data(), data.size());

    return {{data}, verts.second};
}

void AddMeshes(NECore::Gallery& gallery)
{
    CpuImage image(TEXTURE_PATH);
    gallery.StoreImage<CpuImage::GetImageData>(image, "statue");

    Scribe scribe("Assets/Fonts/dejavu-serif/DejaVuSerif.ttf");

    CpuImage font_image(
        scribe.GetAtlasBuffer().data(), scribe.GetAtlasWidth(), scribe.GetAtlasHeight(), 1);

    gallery.StoreImage<CpuImage::GetImageData>(
        font_image, "font_atlas", NECore::ImageFormat::R8_UNORM);

    GenerateTerrain(gallery);
    // Model
    HMesh<HyperVertex> mesh_tmp(MODEL_PATH);
    uint count = 0;
    for(auto& edge: mesh_tmp.EdgesD())
    {
        edge.Sharpness(4);
        if(count++ > 0) break;
    }
    InitGaussianSigmas(mesh_tmp);

    original_vertices = mesh_tmp.VertexDataD();

    gallery.StoreMesh<HMesh<HyperVertex>::GetGeometryData>(
        mesh_tmp, "loaded_model_mod");
    gallery.StoreMesh<HMesh<HyperVertex>::GetGeometryData>(
        mesh_tmp, "loaded_model_original");

    // Sphere
    auto [vertices, indices] = GenerateSphere(RADIUS, 50, 50);
    auto normals = GenerateSharpNormals(vertices, indices);
    vector<float> sphere_vertices;
    uint i = 0;
    for(auto& v: vertices)
    {
        sphere_vertices.push_back(v.x());
        sphere_vertices.push_back(v.y());
        sphere_vertices.push_back(v.z());
        sphere_vertices.push_back(0);
        sphere_vertices.push_back(0);
        auto normal = normals[i++];
        sphere_vertices.push_back(normal.x());
        sphere_vertices.push_back(normal.y());
        sphere_vertices.push_back(normal.z());
    }

    std::pair<std::vector<float>, std::vector<uint>> data = {sphere_vertices, indices};

    gallery.StoreMesh<SerializeFloatData>(data, "sphere");
}

void SetDualVectorSigmaCoeffs(Eigen::VectorXf& position, const Eigen::Matrix3f& sigma_inv)
{
    for(uint k = 0; k < 6; k++)
    {
        uint x = (sqrt(1.0 + 8.0 * double(k)) - 1.0) / 2.0;
        uint y = k - x * (x + 1) / 2;

        position(3 + k) = sigma_inv(x, y);
    }
}

uint subdivision_counter = 0;
float persistence = 0.9;
void KeyEvent(void* ptr, const NECore::KeyStateMap& key_map)
{
    using namespace NECore;
    if(key_map[KEY_INDEX(GLFW_KEY_SPACE)] == NECore::KeyActionState::PRESS)
    {
        auto& gallery = *reinterpret_cast<NECore::Gallery*>(ptr);
        auto& mesh = gallery.GetCpuMeshData<HMesh<HyperVertex>>("contour");

        GaussianSubdivideMesh(mesh);

        auto gaussians = RandomGaussians(mesh.VertexData().size());
        float blend = std::pow(persistence, subdivision_counter + 1);
        for(uint i = 0; i < mesh.VertexData().size(); i++)
        {
            auto [mu, sigma] = DualToComponents(mesh.VertexData()[i].position);

            auto new_sigma = (1.f - blend) * sigma + blend * gaussians[i];
            SetCovarianceOfVertex(mesh, i, new_sigma);
        }

        mesh.CalculateNormals();
        gallery.UpdateMeshData("contour");
        subdivision_counter++;
    }
}

// Our state
bool show_another_window = false;
ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
int selected_vertex = 0;
bool hovering_imgui = false;
void Demo_GUI()
{
    // Start the Dear ImGui frame
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();

    hovering_imgui = ImGui::GetIO().WantCaptureMouse;

    ImGui::Begin(
        "Mesh Data");  // Create a window called "Hello, world!" and append into it.

    ImGui::Text(
        "Application average %.3f ms/frame (%.1f FPS)",
        1000.0f / ImGui::GetIO().Framerate,
        ImGui::GetIO().Framerate);

    ImGui::End();

    // Rendering
    ImGui::Render();
}

void UpdateCameraAngles(
    void* ptr, float position_x, float position_y, float offset_x, float offset_y)
{
    using namespace NECamera;
    if(!hovering_imgui)
        ArcballCamera::UpdateCameraAngles(
            ptr,
            position_x,
            position_y,
            offset_x,
            offset_y);
}

struct MVPOnlyUbo
{
    Eigen::Matrix4f model;
    Eigen::Matrix4f view;
    Eigen::Matrix4f proj;
};

void Loopy(
    ModuleStorage::ModuleStorage& modules,
    NECore::Gallery& gallery,
    NECamera::ArcballCamera& camera,
    NECore::ShaderHandle wire_frame_shader)
{
    MVPOnlyUbo mvp = {};
    mvp.model = Eigen::Matrix4f::Identity();
    mvp.view = camera.GetViewMatrix();
    mvp.proj = camera.GetProjectionMatrix();

    modules.Draw({
                wire_frame_shader,
                {gallery.GetGpuMeshData("contour")},
                {}
            },
            mvp, 0);

    Demo_GUI();
    RenderImgui(modules, gallery);
}

int main(int argc, const char ** argv)
{
        auto modules = ModuleStorage::ModuleStorage(argc, argv);
    const NECore::ShaderHandle wireframe_shader = modules.AddShader(
        {SHADER_PATH "Shaders/wireframe.vert",
         SHADER_PATH "Shaders/wireframe.frag"});

    auto[width, height] = modules.GetWindow().GetDimensions();
    auto camera = NECamera::ArcballCamera(width, height);
    camera.SetRadius(6);

    auto gallery = NECore::Gallery(
        ModuleStorage::GpuAllocateBuffer,
        ModuleStorage::GpuDestroyBuffer,
        ModuleStorage::GpuAllocateImage);

    NECore::InputHandler& input_handler = modules.GetInputHandler();
    input_handler.AddEvent(
        NECore::MouseInputState::LEFT_DRAG, UpdateCameraAngles, &camera);
    input_handler.AddEvent(
        NECore::MouseInputState::RIGHT_DRAG,
        NECamera::ArcballCamera::UpdateCameraPosition,
        &camera);
    input_handler.AddEvent(
        NECore::ScrollInputState::SCROLL,
        NECamera::ArcballCamera::UpdateCameraZoom,
        &camera);
    input_handler.AddEvent(KeyEvent, &gallery);

    AddMeshes(gallery);
    ImGui::SetCurrentContext(InitImgui(modules, gallery));

    while(modules.FrameUpdate())
    {
        if(modules.GetWindow().CheckSizeChanged())
        {
            auto[width, height] = modules.GetWindow().GetDimensions();
            camera.SetCreenDimensions(width, height);
        }

        Loopy(modules, gallery, camera, wireframe_shader);

        modules.EndFrame();
        FrameMark
    }

    return EXIT_SUCCESS;
}