#include <iostream>
#include <chrono>
#include <iostream>

#include "Eigen/Core"

#include "ModuleStorage/ModuleStorage.hpp"
#include "Profiler/profiler.hpp"
#include "Core/Gallery.hpp"
#include "Geometry/HalfEdge/HMesh.hpp"
#include "ImageManipulation/CpuImage.hpp"
#include "Camera/Camera.hpp"
#include "Animation/SpringMass.hpp"
#include "Geometry/Parametrics/Parametrics.hpp"
#include "Geometry/ShapeAnalysis/HeatGeodesics.hpp"
#include "ModuleStorage/Imgui/imgui_bridge.hpp"

namespace
{
const std::string MODEL_PATH = "Assets/bunny/low_poly_bunny.obj";

}  // namespace

using namespace std;
using namespace NECamera;

Eigen::VectorXf distances;
NECore::RawMeshData GetDistanceMeshInfo(HMesh<VertexData>& mesh)
{
    std::vector<Eigen::Vector3f> positions;
    positions.reserve(mesh.size());
    for(uint i = 0; i < mesh.size(); i++)
    {
        positions.push_back(mesh.Positions(i));
    }
    auto indices = mesh.Indices();

    Eigen::VectorXf sources(positions.size());
    sources.setZero();
    sources[1] = 1.f;

    distances = HeatGeodesics(mesh, sources);

    std::vector<float> data;
    uint count = 0;
    for(auto& v: positions)
    {
        data.push_back(v.x());
        data.push_back(v.y());
        data.push_back(v.z());
        data.push_back(distances[count++]);
    }

    std::vector<std::byte> serialized_data(data.size() * sizeof(data[0]));
    memcpy(serialized_data.data(), data.data(), serialized_data.size());

    return {{serialized_data}, Flatten(indices)};
}


NECore::RawMeshData SerializeSphere(const std::pair<std::vector<float>, std::vector<uint>>& verts)
{
    std::vector<std::byte> data(verts.first.size() * sizeof(verts.first[0]));
    memcpy(data.data(), verts.first.data(), data.size());

    return {{data}, verts.second};
}

struct MVPOnlyUbo
{
    Eigen::Matrix4f model;
    Eigen::Matrix4f view;
    Eigen::Matrix4f proj;
};

std::vector<std::byte> SerializeFloats(std::vector<Eigen::Vector3f>& vec)
{
    std::vector<std::byte> buffer(vec.size() * sizeof(vec[0]));
    memcpy(buffer.data(), vec.data(), buffer.size());
    return buffer;
}

ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
bool hovering_imgui = false;
int show_point = false;
void Demo_GUI(NECore::Gallery& gallery)
{
    // Start the Dear ImGui frame
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();

    hovering_imgui = ImGui::GetIO().WantCaptureMouse;

    ImGui::Begin(
        "Jiggly Data");  // Create a window called "Hello, world!" and append into it.

    ImGui::InputInt("Show Debug Point", (int*)&show_point);
    SM::SpringMassSystem& mesh =
        gallery.GetCpuMeshData<SM::SpringMassSystem>("loaded_model");

    ImGui::InputFloat("Damping", &mesh.damping);
    ImGui::InputFloat("Sim Speed", &mesh.simulation_speed);
    ImGui::InputFloat("Stiffness", &mesh.stiffness_modifier);

    ImGui::Text(
        "Application average %.3f ms/frame (%.1f FPS)",
        1000.0f / ImGui::GetIO().Framerate,
        ImGui::GetIO().Framerate);

    ImGui::End();

    // Rendering
    ImGui::Render();
}

void AddMeshes(NECore::Gallery& gallery)
{
    // Model
    HMesh<VertexData> mesh(MODEL_PATH);
    gallery.StoreMesh<GetDistanceMeshInfo>(mesh, "loaded_model");
}

void Loopy(
    ModuleStorage::ModuleStorage& modules,
    NECore::Gallery& gallery,
    NECamera::ArcballCamera& camera,
    const NECore::ShaderHandle heat_shader)
{

    MVPOnlyUbo mvp = {};
    mvp.model = Eigen::Matrix4f::Identity();
    mvp.view = camera.GetViewMatrix();
    mvp.proj = camera.GetProjectionMatrix();

    modules.Draw({
            heat_shader,
            {gallery.GetGpuMeshData("loaded_model")},
            {}
        },
        mvp, 0);
}

int main(int argc, const char ** argv)
{
        auto modules = ModuleStorage::ModuleStorage(argc, argv);
    const NECore::ShaderHandle heat_shader = modules.AddShader(
        {SHADER_PATH "Shaders/heat_vis.vert",
         SHADER_PATH "Shaders/heat_vis.frag"});
    auto gallery = NECore::Gallery(
        ModuleStorage::GpuAllocateBuffer,
        ModuleStorage::GpuDestroyBuffer,
        ModuleStorage::GpuAllocateImage);

    auto[width, height] = modules.GetWindow().GetDimensions();
    auto camera = ArcballCamera(width, height);
    camera.SetRadius(6);
    camera.SetPosition({0, 1, -7});

    NECore::InputHandler& input_handler = modules.GetInputHandler();
    input_handler.AddEvent(
        NECore::MouseInputState::LEFT_DRAG, ArcballCamera::UpdateCameraAngles, &camera);
    input_handler.AddEvent(
        NECore::MouseInputState::RIGHT_DRAG, ArcballCamera::UpdateCameraPosition, &camera);
    input_handler.AddEvent(
        NECore::ScrollInputState::SCROLL, ArcballCamera::UpdateCameraZoom, &camera);

    AddMeshes(gallery);
    while(modules.FrameUpdate())
    {
        if(modules.GetWindow().CheckSizeChanged())
        {
            auto[width, height] = modules.GetWindow().GetDimensions();
            camera.SetCreenDimensions(width, height);
        }

        Loopy(modules, gallery, camera, heat_shader);

        modules.EndFrame();
        FrameMark
    }
    return 0;
}