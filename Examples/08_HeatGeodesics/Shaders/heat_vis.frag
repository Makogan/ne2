#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) out vec4 color_out;

layout(location = 0) in float distance;

void main()
{
    color_out = vec4(vec3(distance / 100.f), 1);
}