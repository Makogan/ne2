#include <iostream>
#include <chrono>
#include <iostream>

#include "Eigen/Core"

#include "ModuleStorage/ModuleStorage.hpp"
#include "Profiler/profiler.hpp"
#include "Core/Gallery.hpp"
#include "Geometry/HalfEdge/HMesh.hpp"
#include "ImageManipulation/CpuImage.hpp"
#include "Camera/Camera.hpp"

namespace
{
const std::string MODEL_PATH = "Assets/bunny/bunny.obj";
const std::string TEXTURE_PATH = "Assets/statue.png";
}  // namespace

using namespace std;
using namespace NECamera;

struct MVPOnlyUbo
{
    Eigen::Matrix4f model;
    Eigen::Matrix4f view;
    Eigen::Matrix4f proj;
};

int main(int argc, const char ** argv)
{
        auto modules = ModuleStorage::ModuleStorage(argc, argv);
    const NECore::ShaderHandle phong_shader = modules.AddShader(
        {SHADER_PATH "../../Source/CommonShaders/phong_shader.vert",
         SHADER_PATH "../../Source/CommonShaders/phong_shader.frag"});

    auto gallery = NECore::Gallery(
        ModuleStorage::GpuAllocateBuffer,
        ModuleStorage::GpuDestroyBuffer,
        ModuleStorage::GpuAllocateImage);

    HMesh<VertexData> mesh(MODEL_PATH);
    CpuImage image(TEXTURE_PATH);
    gallery.StoreMesh<HMesh<VertexData>::GetGeometryData>(mesh, "dragon");
    gallery.StoreImage<CpuImage::GetImageData>(image, "statue");

    auto[width, height] = modules.GetWindow().GetDimensions();
    auto camera = ArcballCamera(width, height);
    camera.SetRadius(6);
    camera.SetPosition({0, 5, -20});

    NECore::InputHandler& input_handler = modules.GetInputHandler();
    input_handler.AddEvent(
        NECore::MouseInputState::LEFT_DRAG, ArcballCamera::UpdateCameraAngles, &camera);
    input_handler.AddEvent(
        NECore::MouseInputState::RIGHT_DRAG, ArcballCamera::UpdateCameraPosition, &camera);
    input_handler.AddEvent(
        NECore::ScrollInputState::SCROLL, ArcballCamera::UpdateCameraZoom, &camera);

    while(modules.FrameUpdate())
    {
        if(modules.GetWindow().CheckSizeChanged())
        {
            auto[width, height] = modules.GetWindow().GetDimensions();
            camera.SetCreenDimensions(width, height);
        }

        MVPOnlyUbo mvp = {};
        mvp.model = Eigen::Matrix4f::Identity();
        mvp.view = camera.GetViewMatrix();
        mvp.proj = camera.GetProjectionMatrix();

        modules.Draw({
                phong_shader,
                {gallery.GetGpuMeshData("dragon")},
                {{gallery.GetGpuImageData("statue"), 2}}
            },
            mvp, 0,
            camera.GetPosition(), 1);
        modules.EndFrame();
        FrameMark
    }
    return 0;
}