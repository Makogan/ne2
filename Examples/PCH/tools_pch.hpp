#include <vector>
#include <string>
#include <iostream>

#include "Eigen/Core"
#include "Eigen/Dense"

#include "Camera/Camera.hpp"
#include "Core/Gallery.hpp"
#include "ImageManipulation/CpuImage.hpp"
#include "Geometry/HalfEdge/HMesh.hpp"
#include "Geometry/HalfEdge/HMesh.hpp"
#include "Geometry/HalfEdge/Subdivision.hpp"
#include "Geometry/HalfEdge/Subdivision.hpp"
#include "Profiler/profiler.hpp"
#include "ModuleStorage/ModuleStorage.hpp"