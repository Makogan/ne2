#version 450
#extension GL_ARB_separate_shader_objects : enable

#define PI 3.14159265359

layout(location = 0) in vec2 fragTexCoord;

layout(location = 0) out vec4 outColor;

layout(binding = 0) uniform sampler2D albedo;
layout(binding = 1) uniform sampler2D dilated_mask;
layout(binding = 2) uniform sampler2D dot_view;
layout(binding = 3) uniform sampler2D sobel_edges;
layout(binding = 4) uniform sampler2D gaussian_edges;

const int radius = 20;

void main()
{
    outColor = vec4(0.2,0,0.2,1) / texture(dilated_mask, fragTexCoord).r;
    vec4 color = texture(albedo, fragTexCoord);
    float alpha = color.a;
    float darkening_factor = round((texture(dot_view, fragTexCoord).r) * 3.0);
    darkening_factor = 0.3 + darkening_factor / 3.0;
    color *= darkening_factor;
    outColor = mix(outColor, color, alpha);

    outColor = outColor + (round(texture(sobel_edges, fragTexCoord)));
    outColor.a = 1;
    // outColor *= 1.0 - texture(gaussian_edges, fragTexCoord) * 0.5;
}