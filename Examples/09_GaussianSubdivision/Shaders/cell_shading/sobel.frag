#version 450
#extension GL_ARB_separate_shader_objects : enable

#define PI 3.14159265359

layout(location = 0) in vec2 fragTexCoord;

layout(location = 0) out vec4 outColor;

layout(binding = 0) uniform sampler2D image;

void main()
{
    ivec2 texture_size = textureSize(image, 0).xy;
    vec2 offset = 1.0 / vec2(float(texture_size.x), float(texture_size.y));
    int sobel_coeffs[3] = int[](1, 2, 1);

    vec4 x_gradient = vec4(0);
    // Horizontal edge detection
    for(int i = -1; i <= 1; i++)
    {
        vec2 new_coord = fragTexCoord + vec2(i * offset.x, offset.y);
        x_gradient += texture(image, new_coord) * sobel_coeffs[i+1];
    }
    for(int i = -1; i <= 1; i++)
    {
        vec2 new_coord = fragTexCoord + vec2(i * offset.x, -offset.y);
        x_gradient += texture(image, new_coord) * -sobel_coeffs[i+1];
    }

    vec4 y_gradient = vec4(0);
    // Vertical edge detection
    for(int i = -1; i <= 1; i++)
    {
        vec2 new_coord = fragTexCoord + vec2(offset.x, i * offset.y);
        y_gradient += texture(image, new_coord) * sobel_coeffs[i+1];
    }
    for(int i = -1; i <= 1; i++)
    {
        vec2 new_coord = fragTexCoord + vec2(-offset.x, i * offset.y);
        y_gradient += texture(image, new_coord) * -sobel_coeffs[i+1];
    }

    float magnitude = sqrt(dot(x_gradient, x_gradient) + dot(y_gradient, y_gradient));
    outColor = vec4(1) *  float(magnitude > 0.5);
}
