#version 450
#extension GL_ARB_separate_shader_objects : enable

#define PI 3.14159265359

layout(location = 0) in vec2 fragTexCoord;

layout(location = 0) out vec4 outColor;

layout(binding = 0) uniform sampler2D texSampler;

const int radius = 12;

void main()
{
    ivec2 texture_size = textureSize(texSampler, 0).xy;
    ivec2 texel_coord = ivec2(fragTexCoord.x * texture_size.x, fragTexCoord.y * texture_size.y);

    float value = 0;
    for(int i=-radius; i<radius; i++)
    {
        for(int j=-radius; j<radius; j++)
        {
            vec2 new_coord = texel_coord + ivec2(i, j);
            new_coord.x /= texture_size.x;
            new_coord.y /= texture_size.y;
            value = max(texture(texSampler, new_coord).a, value);
        }
    }

    outColor = vec4(1) * value;
}