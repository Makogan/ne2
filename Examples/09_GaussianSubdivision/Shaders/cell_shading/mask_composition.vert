/**
* Input Description:
* @depth_test: true
* @topology: triangle list
* @depth_write: false
*
*/
#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 inPosition;

layout(location = 0) out vec2 fragTexCoord;

void main() {
    gl_Position = vec4(inPosition, 1.0);
    fragTexCoord = (gl_Position.xy + vec2(1)) / 2.f;
}