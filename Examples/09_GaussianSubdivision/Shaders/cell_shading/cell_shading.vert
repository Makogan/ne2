/**
* Input Description:
* @depth_test: true
* @topology: triangle list
*
*/

#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec2 inTexCoord;
layout(location = 2) in vec3 inNormal;

layout(location = 0) out vec3 modelPosition;
layout(location = 1) out vec2 fragTexCoord;
layout(location = 2) out vec3 modelNormal;

layout(binding = 0) uniform CellShadingInfo {
    float near_plane;
    float far_plane;
    mat4 model;
    mat4 view;
    mat4 proj;
    vec4 view_dir;
} ubo;

void main() {
    vec3 new_position = inPosition;
    modelPosition = (ubo.model * vec4(new_position, 1.0)).xyz;
    fragTexCoord = inTexCoord;
    modelNormal = (ubo.model * vec4(inNormal, 1.0)).xyz;

    gl_Position = ubo.proj * ubo.view * vec4(modelPosition, 1.0);
}