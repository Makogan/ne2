#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 modelPosition;
layout(location = 1) in vec2 fragTexCoord;
layout(location = 2) in vec3 modelNormal;

layout(location = 0) out vec4 albedo_out;
layout(location = 1) out vec4 normal_out;
layout(location = 2) out vec4 stencil_out;
layout(location = 3) out vec4 depth_out;
layout(location = 4) out vec4 view_dot_out;

layout(binding = 1) uniform sampler2D texSampler;

layout(binding = 0) uniform CellShadingInfo {
    float near_plane;
    float far_plane;
    mat4 model;
    mat4 view;
    mat4 proj;
    vec4 view_dir;
} ubo;

void main() {
    vec3 screen_position = (ubo.view * vec4(modelPosition, 1.0)).xyz;

    albedo_out = texture(texSampler, fragTexCoord);
    normal_out = vec4(abs(modelNormal), 1.0);
    stencil_out = vec4(1);
    float depth = -(screen_position.z - ubo.near_plane) / (ubo.far_plane - ubo.near_plane);
    depth = (depth - 0.5) * 2 * 0.5 + 0.5;
    depth_out = vec4(vec3(depth), 1);
    view_dot_out = vec4(vec3(abs(dot(modelNormal, normalize(ubo.view_dir.xyz)))), 1);
}
