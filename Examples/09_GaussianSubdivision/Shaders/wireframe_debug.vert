#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 in_position;
layout(location = 1) in vec2 in_tex_coord;
layout(location = 2) in vec3 in_normal;

layout(location = 3) in vec3 instance_position;
layout(location = 4) in vec3 instance_normal;
layout(location = 5) in vec3 instance_color;

layout(location = 0) out vec3 position;
layout(location = 1) out vec2 tex_coord;
layout(location = 2) out vec3 normal;
layout(location = 3) out vec3 in_color;

layout(binding = 0) uniform MVPOnlyUbo {
    mat4 model;
    mat4 view;
    mat4 proj;
} ubo;

layout(binding = 1) uniform WireframeDebugInfo {
    vec3 camera_position;
    int treat_normal_as_point;
};

mat4 RotationMatrix(vec3 axis, float angle)
{
    axis = normalize(axis);
    float s = sin(angle);
    float c = cos(angle);
    float oc = 1.0 - c;

    return mat4(oc * axis.x * axis.x + c,           oc * axis.x * axis.y - axis.z * s,  oc * axis.z * axis.x + axis.y * s,  0.0,
                oc * axis.x * axis.y + axis.z * s,  oc * axis.y * axis.y + c,           oc * axis.y * axis.z - axis.x * s,  0.0,
                oc * axis.z * axis.x - axis.y * s,  oc * axis.y * axis.z + axis.x * s,  oc * axis.z * axis.z + c,           0.0,
                0.0,                                0.0,                                0.0,                                1.0);
}

void main()
{
    vec3 dir_normal = instance_normal;
    vec3 modified_position = in_position;

    if(bool(treat_normal_as_point) == true)
    {
        dir_normal = (instance_normal - instance_position);
        modified_position.z *=  length(dir_normal);
        dir_normal = normalize(dir_normal);
    }

    vec3 axis = normalize(cross(dir_normal, vec3(0,0,1)));
    float angle = acos(dot(dir_normal, vec3(0,0,1)));
    if(abs(angle) < 0.0001)
    {
        angle = 0;
        axis = vec3(0,0,1);
    }

    if(abs(dot(dir_normal, vec3(0,0,1)) + 1.0) < 0.0001)
    {
        axis = vec3(0,1,0);
        angle = acos(-1);
    }

    mat4 orientation = RotationMatrix(axis, angle);
    orientation[3][0] = instance_position.x;
    orientation[3][1] = instance_position.y;
    orientation[3][2] = instance_position.z;

    gl_Position = ubo.proj * ubo.view * ubo.model * orientation *
        vec4(modified_position, 1.0);

    position = (ubo.view * ubo.model * orientation * vec4(modified_position, 1.0)).xyz;
    tex_coord = in_tex_coord;
    normal = (ubo.view * ubo.model * orientation * vec4(in_normal, 0)).xyz;
    in_color = instance_color;
}