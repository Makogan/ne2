#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 in_position;
layout(location = 1) in vec2 in_tex_coord;
layout(location = 2) in vec3 in_normal;

layout(location = 3) in vec3 instance_position;
layout(location = 4) in vec3 instance_gaussian_row1;
layout(location = 5) in vec3 instance_gaussian_row2;
layout(location = 6) in vec3 instance_gaussian_row3;

layout(location = 0) out vec3 position;
layout(location = 1) out vec2 tex_coord;
layout(location = 2) out vec3 normal;
layout(location = 3) out vec3 in_color;

layout(binding = 0) uniform MVPOnlyUbo {
    mat4 model;
    mat4 view;
    mat4 proj;
} ubo;

layout(binding = 1) uniform CameraUbo {
    vec3 camera_position;
};

mat3 sigma_inv = mat3(instance_gaussian_row1, instance_gaussian_row2, instance_gaussian_row3);

void main()
{
    position = (ubo.view * ubo.model * vec4((sigma_inv * in_position) + instance_position, 1.0)).xyz;

    gl_Position = ubo.proj * vec4(position, 1.0);

    tex_coord = in_tex_coord;
    normal = (ubo.view * ubo.model * vec4(in_normal, 0)).xyz;
    in_color = vec3(1,0,1);
}