#include <chrono>
#include <iostream>
#include <random>

#include "Eigen/Core"

#include "ModuleStorage/ModuleStorage.hpp"
#include "Profiler/profiler.hpp"
#include "Core/Gallery.hpp"
#include "Geometry/HalfEdge/HMesh.hpp"
#include "Geometry/HalfEdge/Subdivision.hpp"
#include "Geometry/Parametrics/Parametrics.hpp"
#include "ImageManipulation/CpuImage.hpp"
#include "Camera/Camera.hpp"
#include "ModuleStorage/Imgui/imgui_bridge.hpp"
#include "Effects/CelShading.hpp"

namespace
{
const std::string MODEL_PATH = "Assets/cube.obj";
const std::string TEXTURE_PATH = "Assets/CheckerBoards/checker_1k_orange.png";

std::vector<HyperVertex> original_vertices;

struct WireframeDebugInfo
{
    Eigen::Vector3f camera_position;
    int treat_normal_as_point;
} ubo;

NECore::RawMeshData SerializeFloatData(const std::pair<std::vector<float>, std::vector<uint>>& verts)
{
    std::vector<std::byte> data(verts.first.size() * sizeof(verts.first[0]));
    memcpy(data.data(), verts.first.data(), data.size());

    return {{data}, verts.second};
}

std::vector<std::byte> SerializeFloatBuffer(std::vector<float>& vec)
{
    std::vector<std::byte> buffer(vec.size() * sizeof(vec[0]));
    memcpy(buffer.data(), vec.data(), buffer.size());
    return buffer;
}

std::vector<std::byte> SerializeEigenVerts(std::vector<Eigen::Vector3f>& vec)
{
    std::vector<std::byte> buffer(vec.size() * sizeof(vec[0]));
    memcpy(buffer.data(), vec.data(), buffer.size());
    return buffer;
}

void SetDebugMesh(NECore::Gallery& gallery, float size = 1.f)
{
    auto& mesh = gallery.GetCpuMeshData<HMesh<HyperVertex>>("loaded_model_mod");
    const auto& faces = mesh.Faces();
    std::vector<float> m_i_d;
    for(const auto& face: faces)
    {
        auto vertices = face.Vertices();
        for(uint i = 0; i < 3; i++)
        {
            m_i_d.push_back(vertices[i]->Data<HyperVertex>().position[0]);
            m_i_d.push_back(vertices[i]->Data<HyperVertex>().position[1]);
            m_i_d.push_back(vertices[i]->Data<HyperVertex>().position[2]);

            m_i_d.push_back(vertices[i]->Data<HyperVertex>().normal[0]);
            m_i_d.push_back(vertices[i]->Data<HyperVertex>().normal[1]);
            m_i_d.push_back(vertices[i]->Data<HyperVertex>().normal[2]);

            m_i_d.push_back(0.3);
            m_i_d.push_back(0);
            m_i_d.push_back(0.3);
        }
    }

    auto model_instance_data = m_i_d;

    {
        // arrow
        auto [vertices, indices] = Generate3DArrow(0.04 * size, 0.2 * size, 10, 10);
        auto normals = GenerateSharpNormals(vertices, indices);
        std::vector<float> arrow_vertices;
        uint i = 0;
        for(auto& v: vertices)
        {
            arrow_vertices.push_back(v.x());
            arrow_vertices.push_back(v.y());
            arrow_vertices.push_back(v.z());
            arrow_vertices.push_back(0);
            arrow_vertices.push_back(0);
            auto normal = normals[i++];
            arrow_vertices.push_back(normal.x());
            arrow_vertices.push_back(normal.y());
            arrow_vertices.push_back(normal.z());
        }

        std::pair<std::vector<float>, std::vector<uint>> data = {arrow_vertices, indices};

        gallery.StoreMesh<SerializeFloatData>(data, "arrow");

        gallery.StoreBuffer<SerializeFloatBuffer>(model_instance_data, "arrow_positions");
    }

    {
        // sphere
        auto [vertices, indices] = GenerateSphere(0.02 * size, 15, 15);
        auto normals = GenerateSharpNormals(vertices, indices);
        std::vector<float> sphere_vertices;
        uint i = 0;
        for(auto& v: vertices)
        {
            sphere_vertices.push_back(v.x());
            sphere_vertices.push_back(v.y());
            sphere_vertices.push_back(v.z());
            sphere_vertices.push_back(0);
            sphere_vertices.push_back(0);
            auto normal = normals[i++];
            sphere_vertices.push_back(normal.x());
            sphere_vertices.push_back(normal.y());
            sphere_vertices.push_back(normal.z());
        }

        std::pair<std::vector<float>, std::vector<uint>> data = {
            sphere_vertices, indices};
        gallery.StoreMesh<SerializeFloatData>(
            data, "debug_sphere");
        gallery.StoreBuffer<SerializeFloatBuffer>(
            model_instance_data, "debug_sphere_positions");
    }

    {
        // cylinder
        auto edges =
            gallery.GetCpuMeshData<HMesh<HyperVertex>>("loaded_model_mod").EdgeVertices();

        auto [vertices, indices] = GenerateCylinder(0.01 * size, 1.f, 15, 15);
        auto normals = GenerateSharpNormals(vertices, indices);
        std::vector<float> cylinder_vertices;
        uint i = 0;
        for(auto& v: vertices)
        {
            cylinder_vertices.push_back(v.x());
            cylinder_vertices.push_back(v.y());
            cylinder_vertices.push_back(v.z());
            cylinder_vertices.push_back(0);
            cylinder_vertices.push_back(0);
            auto normal = normals[i++];
            cylinder_vertices.push_back(normal.x());
            cylinder_vertices.push_back(normal.y());
            cylinder_vertices.push_back(normal.z());
        }

        std::pair<std::vector<float>, std::vector<uint>> data = {
            cylinder_vertices, indices};
        gallery.StoreMesh<SerializeFloatData>(data, "cylinder");

        std::vector<Eigen::Vector3f> edge_instance_data;
        edge_instance_data.reserve(model_instance_data.size());
        for(auto& [n1, n2]: edges)
        {
            auto& p1 = n1->template Data<HyperVertex>().position;
            auto& p2 = n2->template Data<HyperVertex>().position;
            const auto v1 = Eigen::Vector3d{p1[0], p1[1], p1[2]};
            const auto v2 = Eigen::Vector3d{p2[0], p2[1], p2[2]};
            edge_instance_data.push_back(v1.cast<float>());
            edge_instance_data.push_back(v2.cast<float>());
            edge_instance_data.push_back(Eigen::Vector3f(0, 0.2, 0.6));
        }
        gallery.StoreBuffer<SerializeEigenVerts>(edge_instance_data, "edge_positions");
    }

    {
        // sphere
        auto [vertices, indices] = GenerateSphere(0.02 * size, 15, 15);
        auto normals = GenerateSharpNormals(vertices, indices);
        std::vector<float> sphere_vertices;
        uint i = 0;
        for(auto& v: vertices)
        {
            sphere_vertices.push_back(v.x());
            sphere_vertices.push_back(v.y());
            sphere_vertices.push_back(v.z());
            sphere_vertices.push_back(0);
            sphere_vertices.push_back(0);
            auto normal = normals[i++];
            sphere_vertices.push_back(normal.x());
            sphere_vertices.push_back(normal.y());
            sphere_vertices.push_back(normal.z());
        }

        std::pair<std::vector<float>, std::vector<uint>> data = {
            sphere_vertices, indices};
        gallery.StoreMesh<SerializeFloatData>(
            data, "gaussian_sphere");
    }
}

void InitGaussianSigmas(HMesh<HyperVertex>& mesh)
{
    auto& vertices = mesh.VertexDataD();
    for(auto& vertex: vertices)
    {
        Eigen::VectorXf& position = vertex.position;
        position.conservativeResize(9, 1);
        Eigen::MatrixXf sigma_inv = Eigen::MatrixXf::Identity(3, 3);

        position << position[0], position[1], position[2], sigma_inv(0, 0),
            sigma_inv(1, 0), sigma_inv(1, 1), sigma_inv(2, 0), sigma_inv(2, 1),
            sigma_inv(2, 2);
    }
}

void SetCovarianceOfVertex(HMesh<HyperVertex>& mesh, uint i, const Eigen::MatrixXf& cov)
{
    auto& vertex = mesh.VertexDataD()[i].position;
    Eigen::MatrixXf position(1, 3);
    position << vertex[0], vertex[1], vertex[2];
    Eigen::MatrixXf sigma_inv = cov.inverse();

    vertex << position(0, 0), position(0, 1), position(0, 2), sigma_inv(0, 0),
        sigma_inv(1, 0), sigma_inv(1, 1), sigma_inv(2, 0), sigma_inv(2, 1),
        sigma_inv(2, 2);
}

void AddMeshes(NECore::Gallery& gallery)
{
    CpuImage image(TEXTURE_PATH);
    gallery.StoreImage<CpuImage::GetImageData>(image, "statue");

    // Model
    HMesh<HyperVertex> mesh_tmp(MODEL_PATH);
    InitGaussianSigmas(mesh_tmp);
    std::default_random_engine generator;
    std::uniform_int_distribution<int> distribution(0, mesh_tmp.VertexData().size() - 1);
    auto index_selector = std::bind(distribution, generator);

    std::normal_distribution<double> fdistribution(10, 1005.2);
    for(uint i = 0; i < 100; i++)
    {
        uint selected = index_selector();

        Eigen::MatrixXf covariance = Eigen::MatrixXf::Identity(3, 3);
        SetCovarianceOfVertex(mesh_tmp, selected, covariance);
    }
    original_vertices = mesh_tmp.VertexDataD();

    gallery.StoreMesh<HMesh<HyperVertex>::GetGeometryData>(mesh_tmp, "loaded_model_mod");
    gallery.StoreMesh<HMesh<HyperVertex>::GetGeometryData>(mesh_tmp, "loaded_model_original");

    // Sphere
    auto [vertices, indices] = GenerateSphere(0.98, 50, 50);
    auto normals = GenerateSharpNormals(vertices, indices);
    std::vector<float> sphere_vertices;
    uint i = 0;
    for(auto& v: vertices)
    {
        sphere_vertices.push_back(v.x());
        sphere_vertices.push_back(v.y());
        sphere_vertices.push_back(v.z());
        sphere_vertices.push_back(0);
        sphere_vertices.push_back(0);
        auto normal = normals[i++];
        sphere_vertices.push_back(normal.x());
        sphere_vertices.push_back(normal.y());
        sphere_vertices.push_back(normal.z());
    }

    std::pair<std::vector<float>, std::vector<uint>> data = {sphere_vertices, indices};

    gallery.StoreMesh<SerializeFloatData>(data, "sphere");

    SetDebugMesh(gallery);

    std::pair<std::vector<float>, std::vector<uint>> screen_triangle =
    {
        {
            -5, -5, 0.5,
            -5, 5, 0.5,
            5, 0, 0.5
        },
        {2, 1, 0}
    };

    gallery.StoreMesh<SerializeFloatData>(screen_triangle, "screen_triangle");
}

void AddImages(NECore::Gallery& gallery, ModuleStorage::ModuleStorage& modules)
{
    auto[width, height] = modules.GetWindow().GetDimensions();

    CpuImage image(nullptr, width, height, 4);
    gallery.StoreImage<CpuImage::GetImageData>(
        image, "albedo", NECore::ImageFormat::B8G8R8A8_UNORM);
    gallery.StoreImage<CpuImage::GetImageData>(
        image, "normal", NECore::ImageFormat::B8G8R8A8_UNORM);
    gallery.StoreImage<CpuImage::GetImageData>(
        image, "stencil", NECore::ImageFormat::B8G8R8A8_UNORM);
    gallery.StoreImage<CpuImage::GetImageData>(
        image, "depth", NECore::ImageFormat::B8G8R8A8_UNORM);
    gallery.StoreImage<CpuImage::GetImageData>(
        image, "view_dot", NECore::ImageFormat::B8G8R8A8_UNORM);
    gallery.StoreImage<CpuImage::GetImageData>(
        image, "composition", NECore::ImageFormat::B8G8R8A8_UNORM);
    gallery.StoreImage<CpuImage::GetImageData>(
        image, "mid_canvas", NECore::ImageFormat::B8G8R8A8_UNORM);
    gallery.StoreImage<CpuImage::GetImageData>(
        image, "depth_attachment", NECore::ImageFormat::D32_SFLOAT_S8_UINT);
}

void SetDualVectorSigmaCoeffs(Eigen::VectorXf& position, const Eigen::Matrix3f& sigma_inv)
{
    for(uint k = 0; k < 6; k++)
    {
        uint x = (sqrt(1.0 + 8.0 * double(k)) - 1.0) / 2.0;
        uint y = k - x * (x + 1) / 2;

        position(3 + k) = sigma_inv(x, y);
    }
}

uint subdivision_counter = 0;
void KeyEvent(void* ptr, const NECore::KeyStateMap& key_map)
{
    using namespace NECore;
    if(key_map[KEY_INDEX(GLFW_KEY_SPACE)] == NECore::KeyActionState::PRESS)
    {
        auto& gallery = *reinterpret_cast<NECore::Gallery*>(ptr);
        auto& mesh = gallery.GetCpuMeshData<HMesh<HyperVertex>>("loaded_model_mod");
        GaussianSubdivideMesh(mesh);

        gallery.UpdateMeshData("loaded_model_mod");
        subdivision_counter++;
    }
}

// Our state
int selected_vertex = 0;
bool hovering_imgui = false;
void Demo_GUI(NECore::Gallery& gallery)
{
    // Start the Dear ImGui frame
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();

    hovering_imgui = ImGui::GetIO().WantCaptureMouse;

    HMesh<HyperVertex>& mesh =
        gallery.GetCpuMeshData<HMesh<HyperVertex>>("loaded_model_mod");

    ImGui::Begin(
        "Mesh Data");  // Create a window called "Hello, world!" and append into it.

    ImGui::Text(
        "Application average %.3f ms/frame (%.1f FPS)",
        1000.0f / ImGui::GetIO().Framerate,
        ImGui::GetIO().Framerate);

    const uint v_num = original_vertices.size();
    const std::string vertex_info_str = "Vertices in mesh: " + std::to_string(v_num);
    ImGui::Text("%s", vertex_info_str.c_str());

    const std::string selected_str = "Selected vertex:";
    ImGui::Text("%s", selected_str.c_str());
    ImGui::InputInt("selected vertex", &selected_vertex);
    selected_vertex = (selected_vertex + v_num) % v_num;

    auto [position, sigma_inv] =
        DualToComponents(original_vertices[selected_vertex].position);

    std::stringstream ss;
    ss << "Position:\n" << position.transpose() << "\n\nMatrix:\n" << sigma_inv.inverse();

    const std::string vertex_data_str = "The data of the vertex is:\n" + ss.str();
    ImGui::Text("%s", vertex_data_str.c_str());

    ImGui::Text("positon");
    ImGui::PushItemWidth(170);
    ImGui::DragFloat3("", &position[0], 0.05f);

    ImGui::Text("Sigma inv");
    ImGui::PushItemWidth(200);
    Eigen::Matrix3f input_mat = sigma_inv;
    Eigen::Vector3f v1 = input_mat.row(0);
    Eigen::Vector3f v2 = input_mat.row(1);
    Eigen::Vector3f v3 = input_mat.row(2);
    ImGui::DragFloat3("##one", v1.data(), 0.05f);
    ImGui::DragFloat3("##two", v2.data(), 0.05f);
    ImGui::DragFloat3("##tre", v3.data(), 0.05f);

    input_mat << v1, v2, v3;

    ImGui::End();

    // Rendering
    ImGui::Render();

    if(!input_mat.isApprox(sigma_inv))
    {
        mesh = gallery.GetCpuMeshData<HMesh<HyperVertex>>("loaded_model_original");
        SetDualVectorSigmaCoeffs(original_vertices[selected_vertex].position, input_mat);
        mesh.VertexDataD() = original_vertices;

        for(uint i = 0; i < subdivision_counter; i++)
            GaussianSubdivideMesh(mesh);

        gallery.UpdateMeshData("loaded_model_mod");
        mesh = gallery.GetCpuMeshData<HMesh<HyperVertex>>("loaded_model_mod");
    }
}

void UpdateCameraAngles(
    void* ptr, float position_x, float position_y, float offset_x, float offset_y)
{
    using namespace NECamera;
    if(!hovering_imgui)
        ArcballCamera::UpdateCameraAngles(
            ptr,
            position_x,
            position_y,
            offset_x,
            offset_y);
}

}  // namespace

using namespace std;
using namespace NECamera;

struct MVPOnlyUbo
{
    Eigen::Matrix4f model;
    Eigen::Matrix4f view;
    Eigen::Matrix4f proj;
};

int main(int argc, const char ** argv)
{
        auto modules = ModuleStorage::ModuleStorage(argc, argv);
    const NECore::ShaderHandle phong_shader = modules.AddShader(
        {SHADER_PATH "../../Source/CommonShaders/phong_shader.vert",
         SHADER_PATH "../../Source/CommonShaders/phong_shader.frag"});

    const NECore::ShaderHandle mesh_shader = modules.AddShader(
        {SHADER_PATH "Shaders/cell_shading/wire_frame.vert",
         SHADER_PATH "Shaders/cell_shading/wire_frame.frag"});

    const NECore::ShaderHandle gaussian_shader = modules.AddShader(
        {SHADER_PATH "Shaders/cell_shading/gaussian.vert",
         SHADER_PATH "Shaders/cell_shading/gaussian.frag"});

    const NECore::ShaderHandle sobel_shader = modules.AddShader(
        {SHADER_PATH "Shaders/cell_shading/sobel.vert",
         SHADER_PATH "Shaders/cell_shading/sobel.frag"});

    const NECore::ShaderHandle gaussian_difference_shader = modules.AddShader(
        {SHADER_PATH "Shaders/cell_shading/gaussian_difference.vert",
         SHADER_PATH "Shaders/cell_shading/gaussian_difference.frag"});

    const NECore::ShaderHandle dilation_shader = modules.AddShader(
        {SHADER_PATH "Shaders/cell_shading/dilation.vert",
         SHADER_PATH "Shaders/cell_shading/dilation.frag"});

    const NECore::ShaderHandle wireframe_shader = modules.AddShader(
        {SHADER_PATH "Shaders/wireframe_debug.vert",
         SHADER_PATH "Shaders/wireframe_debug.frag"});

    const NECore::ShaderHandle cel_shading_shader = modules.AddShader(
        {SHADER_PATH "Shaders/cell_shading/cell_shading.vert",
         SHADER_PATH "Shaders/cell_shading/cell_shading.frag"});

    const NECore::ShaderHandle mask_shader = modules.AddShader(
        {SHADER_PATH "Shaders/cell_shading/mask_composition.vert",
         SHADER_PATH "Shaders/cell_shading/mask_composition.frag"});

    auto gallery = NECore::Gallery(
        ModuleStorage::GpuAllocateBuffer,
        ModuleStorage::GpuDestroyBuffer,
        ModuleStorage::GpuAllocateImage);

    AddMeshes(gallery);
    AddImages(gallery, modules);

    auto[width, height] = modules.GetWindow().GetDimensions();
    auto camera = ArcballCamera(width, height);
    camera.SetRadius(6);
    // camera.SetPosition({0, 5, -20});

    NECore::InputHandler& input_handler = modules.GetInputHandler();
    input_handler.AddEvent(
        NECore::MouseInputState::LEFT_DRAG, UpdateCameraAngles, &camera);
    input_handler.AddEvent(
        NECore::MouseInputState::RIGHT_DRAG, ArcballCamera::UpdateCameraPosition, &camera);
    input_handler.AddEvent(
        NECore::ScrollInputState::SCROLL, ArcballCamera::UpdateCameraZoom, &camera);
    input_handler.AddEvent(KeyEvent, &gallery);

    ImGui::SetCurrentContext(InitImgui(modules, gallery));
    while(modules.FrameUpdate())
    {
        if(modules.GetWindow().CheckSizeChanged())
        {
            auto[width, height] = modules.GetWindow().GetDimensions();
            camera.SetCreenDimensions(width, height);
        }

        MVPOnlyUbo mvp = {};
        mvp.model = Eigen::Matrix4f::Identity();
        mvp.view = camera.GetViewMatrix();
        mvp.proj = camera.GetProjectionMatrix();

        // This is only to se tthe depth buffer
        modules.Draw({
                phong_shader,
                {gallery.GetGpuMeshData("loaded_model_mod")},
                {{gallery.GetGpuImageData("statue"), 2}}
            },
            mvp, 0,
            camera.GetPosition(), 1);

        CelShading(
            modules,
            gallery,
            camera,
            cel_shading_shader,
            dilation_shader,
            gaussian_shader,
            gaussian_difference_shader,
            sobel_shader,
            mask_shader,
            gallery.GetGpuImageData("albedo"),
            gallery.GetGpuImageData("normal"),
            gallery.GetGpuImageData("stencil"),
            gallery.GetGpuImageData("depth"),
            gallery.GetGpuImageData("view_dot"),
            gallery.GetGpuImageData("composition"));

            modules.Draw({
                mesh_shader,
                {gallery.GetGpuMeshData("loaded_model_mod")},
                {},
                {},
                false
            },
            mvp, 0);

        WireframeDebugInfo wire_info = {};
        wire_info.camera_position = ubo.camera_position;
        wire_info.treat_normal_as_point = false;

        modules.Draw({
                wireframe_shader,
                {
                    gallery.GetGpuMeshData("debug_sphere"),
                    gallery.GetGpuBufferData("debug_sphere_positions"),
                    (uint)gallery.GetCpuBufferData<std::vector<float>>("debug_sphere_positions").size() / (9)
                },
                {},
                {},
                false
            },
            mvp, 0,
            wire_info, 1);

        wire_info.treat_normal_as_point = true;
        modules.Draw({
                wireframe_shader,
                {
                    gallery.GetGpuMeshData("cylinder"),
                    gallery.GetGpuBufferData("edge_positions"),
                    (uint)gallery.GetCpuBufferData<std::vector<Eigen::Vector3f>>("edge_positions").size() / (3)
                },
                {},
                {},
                false
            },
            mvp, 0,
            wire_info, 1);

        Demo_GUI(gallery);
        RenderImgui(modules, gallery);
        modules.EndFrame();
        FrameMark
    }
    return 0;
}