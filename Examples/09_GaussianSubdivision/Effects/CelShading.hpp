#pragma once

void CelShading(
    ModuleStorage::ModuleStorage& modules,
    NECore::Gallery& gallery,
    NECamera::Camera& camera,
    NECore::ShaderHandle cel_shading,
    NECore::ShaderHandle dilation_shader,
    NECore::ShaderHandle gaussian_shader,
    NECore::ShaderHandle gaussian_difference_shader,
    NECore::ShaderHandle sobel_shader,
    NECore::ShaderHandle mask_composition_shader,
    NECore::ImageHandle albedo_out,
    NECore::ImageHandle normal_out,
    NECore::ImageHandle stencil_out,
    NECore::ImageHandle depth_out,
    NECore::ImageHandle view_dot_out,
    NECore::ImageHandle composition);