#include "CelShading.hpp"

#include "Eigen/Core"

#include "Core/Gallery.hpp"
#include "Camera/Camera.hpp"
#include "ModuleStorage/ModuleStorage.hpp"

struct MVPOnlyUbo
{
    Eigen::Matrix4f model;
    Eigen::Matrix4f view;
    Eigen::Matrix4f proj;
};

struct CellShadingInfo
{
    float near_plane;
    float far_plane;
    Eigen::Matrix4f model;
    Eigen::Matrix4f view;
    Eigen::Matrix4f proj;
    Eigen::Vector4f view_dir;
};

struct GaussianProperties
{
    int radius;
    uint vertical;
};

void BlurRender(
    NECore::Gallery& gallery,
    ModuleStorage::ModuleStorage& modules,
    NECore::ShaderHandle gaussian,
    NECore::ImageHandle input_image,
    NECore::ImageHandle tmp_image,
    NECore::ImageHandle output_image,
    int radius)
{
    NECore::RenderRequest request = {};
    request.shader = gaussian;
    request.mesh_input.vertex_inputs = gallery.GetGpuMeshData("screen_triangle");
    request.image_inputs = {{input_image, 1}};
    request.image_outputs = {
        {tmp_image, 0}
    };
    request.should_clear = true;
    request.mesh_input.element_count = request.mesh_input.vertex_inputs.index_count;

    GaussianProperties properties = {};
    properties.radius = radius;
    properties.vertical = false;
    modules.Draw(request, properties, 0);

    properties.vertical = true;
    request.image_inputs = {{tmp_image, 1}};
    request.image_outputs = {
        {output_image, 0}
    };
    request.should_clear = false;
    modules.Draw(request, properties, 0);
}

void CelShading(
    ModuleStorage::ModuleStorage& modules,
    NECore::Gallery& gallery,
    NECamera::Camera& camera,
    NECore::ShaderHandle cel_shading,
    NECore::ShaderHandle dilation_shader,
    NECore::ShaderHandle gaussian_shader,
    NECore::ShaderHandle gaussian_difference_shader,
    NECore::ShaderHandle sobel_shader,
    NECore::ShaderHandle mask_composition_shader,
    NECore::ImageHandle albedo_out,
    NECore::ImageHandle normal_out,
    NECore::ImageHandle stencil_out,
    NECore::ImageHandle depth_out,
    NECore::ImageHandle view_dot_out,
    NECore::ImageHandle composition)
{
    NECore::RenderRequest request = {};
    request.shader = cel_shading;
    request.mesh_input.vertex_inputs = gallery.GetGpuMeshData("loaded_model_mod");
    request.image_inputs = {{gallery.GetGpuImageData("statue"), 1}};
    request.image_outputs = {
        {albedo_out, 0},
        {normal_out, 1},
        {stencil_out, 2},
        {depth_out, 3},
        {view_dot_out, 4},
        {gallery.GetGpuImageData("depth_attachment"), -1}
    };
    request.should_clear = true;
    request.mesh_input.element_count = request.mesh_input.vertex_inputs.index_count;

    CellShadingInfo info = {};
    info.model = Eigen::Matrix4f::Identity();
    info.view = camera.GetViewMatrix();
    info.proj = camera.GetProjectionMatrix();
    auto look_dir = camera.GetLookDirection();
    info.view_dir = {look_dir.x(), look_dir.y(), look_dir.z(), 0};
    info.near_plane = 0.01;
    info.far_plane = 20;

    modules.Draw(request, info, 0);

    auto mid_canvas = gallery.GetGpuImageData("mid_canvas");
    BlurRender(gallery, modules, gaussian_shader, normal_out, mid_canvas, composition, 1);
    BlurRender(gallery, modules, gaussian_shader, normal_out, mid_canvas, normal_out, 2);

    request.shader = gaussian_difference_shader;
    request.mesh_input.vertex_inputs = gallery.GetGpuMeshData("screen_triangle");
    request.mesh_input.element_count = request.mesh_input.vertex_inputs.index_count;
    request.image_inputs = {{normal_out, 0}, {composition, 1}};
    request.image_outputs = {{mid_canvas, 0}};
    request.should_clear = true;
    modules.Draw(request);

    request.shader = dilation_shader;
    request.image_inputs = {{stencil_out, 0}};
    request.image_outputs = {{normal_out, 0}};
    modules.Draw(request);

    request.shader = sobel_shader;
    request.image_inputs = {{depth_out, 0}};
    request.image_outputs = {{stencil_out, 0}};
    modules.Draw(request);

    request.shader = mask_composition_shader;
    request.image_inputs = {{albedo_out, 0}, {normal_out, 1}, {view_dot_out, 2}, {stencil_out, 3}, {mid_canvas, 4}};
    request.image_outputs.clear();
    request.should_clear = false;
    modules.Draw(request);

}