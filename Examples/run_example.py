from ast import Return
from http.server import executable
import os
import pathlib
import subprocess

def ScreenShotExample(path, capture_frame=20):
    source_path = pathlib.Path(path)

    exec_name = source_path.parts[-1:][0][3:]
    exec_path = pathlib.Path("./").joinpath("/".join(list(source_path.parts[-2:]) + [exec_name]))

    pathlib.Path("Documentation").mkdir(parents=True, exist_ok=True)

    path_to_docs = os.path.abspath("Documentation")
    command = [
        exec_path,
        "-c",
        "-f", str(capture_frame),
        "-p", os.path.join(path_to_docs, exec_name + ".png")
    ]
    subprocess.call(command, cwd="../../build-clang/", stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

    return exec_name