#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) out vec4 color_out;

layout(location = 0) in vec3 position;

void main()
{
    float distance = sqrt(dot(position, position));
    distance = mod(distance, 1.f);
    color_out = vec4(vec3(distance / 10.f), 1);
}