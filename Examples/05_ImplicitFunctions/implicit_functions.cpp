#include <iostream>
#include <chrono>
#include <iostream>

#include "Eigen/Core"

#include "ModuleStorage/ModuleStorage.hpp"
#include "ModuleStorage/Imgui/imgui_bridge.hpp"
#include "Profiler/profiler.hpp"
#include "Core/Gallery.hpp"
#include "Geometry/HalfEdge/HMesh.hpp"
#include "Geometry/HalfEdge/HMeshUtils.hpp"
#include "Geometry/CSG/DualContouring.hpp"
#include "Camera/Camera.hpp"
#include "ImageManipulation/CpuImage.hpp"

using namespace std;
using namespace NECamera;

struct MVPOnlyUbo
{
    Eigen::Matrix4f model;
    Eigen::Matrix4f view;
    Eigen::Matrix4f proj;
};


NECore::RawMeshData SerializeEigenPair(
    const std::pair<std::vector<Eigen::Vector3f>, std::vector<uint>>& input)
{
    auto [data, indices] = input;

    std::vector<std::byte> result(data.size() * sizeof(data[0]));
    memcpy(result.data(), data.data(), result.size());

    return {{result}, indices};
}

void AddMeshes(NECore::Gallery& gallery)
{
    using Vec3 = Eigen::Vector3f;

    // sphere
    auto F = [](const Vec3& v) { return v.dot(v) - 2.f; };
    auto data = DualContouring<Vec3>(F, 5.f, 40);

    // // cube
    // auto data = DualContouring<Vec3, float>([](const Eigen::Vector3f& v) {
    //     Eigen::Vector3f q = v.array().abs().matrix() - Eigen::Vector3f(1, 1, 1);
    //     return (q.array().max(0.f)).matrix().norm() +
    //         std::min(std::max(q.x(), std::max(q.y(), q.z())), 0.f) - 0.4f;
    // }, 5.f, 40);

    // Torus
    /*auto data = DualContouring<Eigen::Vector3f, float>([](const Eigen::Vector3f& v) {
        Eigen::Vector2f q =
            Eigen::Vector2f(Eigen::Vector2f(v.x(), v.z()).norm() - 2.1f, v.y());
        return q.norm() - 1.3f;
    });*/

    // Cool
    // auto F = [](const Eigen::Vector3f& v) {
    //         const float x = v.x();
    //         const float y = v.y();
    //         const float z = v.z();

    //         const float a1 = 3 * (x - 1) * x * x * (x + 1) + 2 * y * y;
    //         const float b1 = z * z - 0.85;

    //         const float a2 = 3 * (y - 1) * y * y * (y + 1) + 2 * x * x;
    //         const float b2 = x * x - 0.85;

    //         const float a3 = 3 * (z - 1) * z * z * (z + 1) + 2 * x * x;
    //         const float b3 = y * y - 0.85;

    //         return a1 * a1 + b1 * b1 * a2 * a2 + b2 * b2 * a3 * a3 + b3 * b3 * -0.12f;
    //     };

    // auto data = DualContouring<Eigen::Vector3f, float>(
    //     F,
    //     5.f,
    //     120);

    using V = Eigen::Vector3f;
    auto Gradient = [&](const V& v)
    {
        using S = float;
        const float epsilon = 1e-3;
        return V(
            (F(v + V(epsilon, 0, 0)) - F(v - V(epsilon, 0, 0))) /
                (S(2) * epsilon),
            (F(v + V(0, epsilon, 0)) - F(v - V(0, epsilon, 0))) /
                (S(2) * epsilon),
            (F(v + V(0, 0, epsilon)) - F(v - V(0, 0, epsilon))) /
                (S(2) * epsilon));
    };

    std::vector<VertexData> v_data(data.first.size());
    for(uint i=0; i < data.first.size(); i++)
    {
        v_data[i].position = data.first[i];
    }

    HMesh<VertexData> i_mesh(v_data, data.second);
    Smoothen(i_mesh);
    SetNormals(i_mesh, Gradient);

    gallery.StoreMesh<HMesh<VertexData>::GetGeometryData>(i_mesh, "contour");

    std::vector<Eigen::Vector3f> debug_plane = {
        {-1, -1, 0}, {-1, 1, 0}, {1, -1, 0}, {1, 1, 0}};
    std::vector<uint> indices = {0, 2, 1, 2, 3, 1};

    auto tmp = std::make_pair(debug_plane, indices);
    gallery.StoreMesh<SerializeEigenPair>(tmp, "debug_plane");
}

// Our state
bool show_demo_window = true;
bool show_another_window = false;
ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
int selected_vertex = 0;
bool hovering_imgui = false;
void Demo_GUI()
{
    // Start the Dear ImGui frame
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();

    hovering_imgui = ImGui::GetIO().WantCaptureMouse;

    ImGui::Begin(
        "Mesh Data");  // Create a window called "Hello, world!" and append into it.

    ImGui::Text(
        "Application average %.3f ms/frame (%.1f FPS)",
        1000.0f / ImGui::GetIO().Framerate,
        ImGui::GetIO().Framerate);

    ImGui::End();

    // Rendering
    ImGui::Render();
}

int main(int argc, const char ** argv)
{
        auto modules = ModuleStorage::ModuleStorage(argc, argv);
    const NECore::ShaderHandle wireframe_shader = modules.AddShader(
        {SHADER_PATH "Shaders/wireframe.vert",
         SHADER_PATH "Shaders/wireframe.frag"});
    const NECore::ShaderHandle debug_plane_shader = modules.AddShader(
        {SHADER_PATH "Shaders/implicit_debug_plane.vert",
         SHADER_PATH "Shaders/implicit_debug_plane.frag"});

    auto gallery = NECore::Gallery(
        ModuleStorage::GpuAllocateBuffer,
        ModuleStorage::GpuDestroyBuffer,
        ModuleStorage::GpuAllocateImage);

    auto[width, height] = modules.GetWindow().GetDimensions();
    auto camera = ArcballCamera(width, height);
    camera.SetRadius(13);
    ArcballCamera::UpdateCameraAngles((void*)&camera, -0.1, -0.1, 0, -0.7);

    NECore::InputHandler& input_handler = modules.GetInputHandler();
    input_handler.AddEvent(
        NECore::MouseInputState::LEFT_DRAG, ArcballCamera::UpdateCameraAngles, &camera);
    input_handler.AddEvent(
        NECore::MouseInputState::RIGHT_DRAG, ArcballCamera::UpdateCameraPosition, &camera);
    input_handler.AddEvent(
        NECore::ScrollInputState::SCROLL, ArcballCamera::UpdateCameraZoom, &camera);

    AddMeshes(gallery);
    ImGui::SetCurrentContext(InitImgui(modules, gallery));
    while(modules.FrameUpdate())
    {
        if(modules.GetWindow().CheckSizeChanged())
        {
            auto[width, height] = modules.GetWindow().GetDimensions();
            camera.SetCreenDimensions(width, height);
        }

        MVPOnlyUbo mvp = {};
        mvp.model = Eigen::Matrix4f::Identity();
        mvp.view = camera.GetViewMatrix();
        mvp.proj = camera.GetProjectionMatrix();

        modules.Draw({
                wireframe_shader,
                {gallery.GetGpuMeshData("contour")},
                {},
                {}
            },
            mvp, 0);

        modules.Draw({
                debug_plane_shader,
                {gallery.GetGpuMeshData("debug_plane")},
                {},
                {},
                false
            },
            mvp, 0);

        Demo_GUI();
        RenderImgui(modules, gallery);
        modules.EndFrame();
        FrameMark
    }
    return 0;
}