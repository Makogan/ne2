import argparse
import nbformat as nbf
import notebook_generator
import os


def MakeParser():
    parser = argparse.ArgumentParser(description='Parse a special jupyter notebook into a '
                                                 'test file.')
    parser.add_argument('path',
                        help='Path to the notebook file.')
    parser.add_argument('out_path',
                    help='Path where the gtest file will be written to.')

    return parser.parse_args()


def ExportNotebookToGtest(path):
    nb = nbf.read(path, as_version=4)

    tests = ''
    suite_name = os.path.splitext(path)[0].replace("/", "_").replace("..", "up")
    print(suite_name)
    for cell in nb['cells']:
        if cell['cell_type'] == 'raw':
            if notebook_generator.PLACE_HOLDER_MESSAGE in cell['source']:
                continue
            # Strip non relevant markdown information from the name
            documented_object = (' '.join(cell['source'].split('\n')[0].split()[2:]).replace('`', '').replace('*', 'ptr'))

            test_name = documented_object
            invalid_tokens = "_ ()<>&:,"
            for char in invalid_tokens:
                test_name = test_name.replace(char, "_")

            code = '\t'.join([''] + cell['source'].splitlines(True))
            test_function = \
                f"TEST({suite_name}, {test_name})\n{{\n{code}\n}}\n"
            tests += test_function  + "\n"

    hpp_path = path.replace('ipynb', 'hpp')
    test_file_content = \
        f'/** @cond */\n' \
        f'#include "gtest/gtest.h"\n' \
        f'/** @endcond */\n\n' \
        f'#include "{hpp_path}"\n\n' \
        f'{tests}' \

    return test_file_content



def main():
    args = MakeParser()

    file_contents = ExportNotebookToGtest(args.path)

    with open(os.path.join(args.out_path, os.path.basename(args.path).replace('.ipynb', '_test.cpp')), 'w') as f:
        f.write(file_contents)

if __name__ == "__main__":
    main()