import argparse
import os
from sre_constants import SUCCESS
import notebook_generator
import nbformat as nbf
import tempfile
import clang.cindex

def MakeParser():
    parser = argparse.ArgumentParser(description='Check if a jupyter notebook is synced with a header file.')
    parser.add_argument('header_path',
                        help='Path to the header file.')
    parser.add_argument('notebook_path',
                    help='Path where the notebook is.')
    parser.add_argument('build_path',
                    help='Path where the compilation_database.json is.')

    return parser.parse_args()


def ParseMarkdownBlock(block):
    headers = [line for line in block.split('\n') if line.startswith('#')]
    return "\n\n".join(headers)


def GetCellTitle(cell):
    return cell['source'].split('\n')[0]


def CheckCellEquivalency(cell1, cell2):
    cell1 = ''.join(cell1.split())
    cell2 = ''.join(cell2.split())

    return cell1 == cell2


def VerifyNotebook(notebook_path, header_path, build_path):
    nb = nbf.read(notebook_path, as_version=4)

    dummy_cells = []
    for cell in nb['cells']:
        if cell['cell_type'] == 'markdown':
            dummy_cells.append(ParseMarkdownBlock(cell['source']))

    nb = nbf.v4.new_notebook()
    nb['cells'] = dummy_cells

    dirpath = tempfile.mkdtemp()
    notebook_generator.CreateNotebookFromHeader(header_path, dirpath, build_path)

    nb_dummy = nbf.read(os.path.join(dirpath, notebook_generator.GetNameFromHeader(header_path)), as_version=4)

    markdown_cells = {}
    for cell in nb_dummy['cells']:
        if cell['cell_type'] == 'markdown':
            markdown_cells[GetCellTitle(cell)] = cell['source']

    for cell in nb['cells']:
        actual_tile = cell.split('\n')[0]

        if actual_tile in markdown_cells:
            cell_prototype = markdown_cells[actual_tile]
        else:
            print(f"Cell {actual_tile} does not exist in header file [✗]")
            break
        if not CheckCellEquivalency(cell, cell_prototype):
            header1 = actual_tile
            print(f"Cell {header1} does not match [✗]")

            print("Is prototype\n```\n" + cell + '\n```')
            print("But the header would export prototype\n```" + cell_prototype + "\n```")
            break

        print(f"Cell {actual_tile} matches [✔]")


def main():
    args = MakeParser()

    clang.cindex.Config.set_library_file('/usr/lib/x86_64-linux-gnu/libclang-11.so.1')
    VerifyNotebook(args.notebook_path, args.header_path)



if __name__ == "__main__":
    main()