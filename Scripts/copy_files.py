import argparse
from pathlib import Path
import shutil

def MakeParser():
    parser = argparse.ArgumentParser(description='Parse a C++ header file and sync to a special'
                                                 ' jupyter notebook.')
    parser.add_argument('in_path',
                        help='Root from which to start copying.')
    parser.add_argument('out_path',
                    help='Path where the copies will be written to.')
    parser.add_argument('pattern',
                    help='Pattern to match files against.')

    return parser.parse_args()


def main():
    args = MakeParser()

    Path(args.out_path).mkdir(parents=True, exist_ok=True)
    for path in Path(args.in_path).rglob(args.pattern):
        shutil.copy(path, args.out_path)

if __name__ == "__main__":
    main()