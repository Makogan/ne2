import argparse
import os
from sre_constants import SUCCESS
import notebook_generator
import notebook_verifier
import nbformat as nbf
import tempfile
import difflib
import clang.cindex


def MakeParser():
    parser = argparse.ArgumentParser(description='Sync a notebook and a header file.')
    parser.add_argument('header_path',
                        help='Path to the header file.')
    parser.add_argument('notebook_path',
                    help='Path where the notebook is.')
    parser.add_argument('build_path',
                    help='Path where the compilation_database.json is.')

    return parser.parse_args()


def ParseMarkdownBlock(block):
    headers = [line for line in block.split('\n') if line.startswith('#')]
    return "\n\n".join(headers)


def GetDifferences(str1, str2):
    return [li for li in difflib.ndiff(str1, str2) if li[0] != ' ']


def main():
    args = MakeParser()

    clang.cindex.Config.set_library_file('/usr/lib/x86_64-linux-gnu/libclang-11.so.1')
    notebook_verifier.VerifyNotebook(args.notebook_path, args.header_path, args.build_path)

    hpp_cells = notebook_generator.GetHeaderCellPrototypes(args.header_path, args.build_path)
    nb = nbf.read(args.notebook_path, as_version=4)

    notebook_cells = {}
    for cell in nb['cells']:
        if cell['cell_type'] == 'markdown':
            notebook_cells[notebook_verifier.GetCellTitle(cell)] = cell['source']

    for i, cell in enumerate(hpp_cells):
        if not cell['cell_type'] == 'markdown':
            continue
        header = notebook_verifier.GetCellTitle(cell)
        if header in notebook_cells:
            hpp_cells[i]['source'] = notebook_cells[header]

    nb['cells'] = hpp_cells
    with open(args.notebook_path, 'w') as f:
        nbf.write(nb, f)



if __name__ == "__main__":
    main()