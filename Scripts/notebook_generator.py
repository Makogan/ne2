import argparse
import nbformat as nbf
import clang.cindex
import os
import re
from pathlib import Path

PLACE_HOLDER_MESSAGE = "Write C++ tests/examples here"


relevant_tokens = [
    clang.cindex.CursorKind.FUNCTION_DECL,
    clang.cindex.CursorKind.CXX_METHOD,
    clang.cindex.CursorKind.CONSTRUCTOR,
    clang.cindex.CursorKind.DESTRUCTOR,
    clang.cindex.CursorKind.STRUCT_DECL,
    clang.cindex.CursorKind.CLASS_DECL,
    clang.cindex.CursorKind.FIELD_DECL,
    clang.cindex.CursorKind.FUNCTION_TEMPLATE,
    clang.cindex.CursorKind.CLASS_TEMPLATE,
]

PyClangToEnglish = {
    clang.cindex.CursorKind.FUNCTION_DECL : "Function",
    clang.cindex.CursorKind.CXX_METHOD : "Method",
    clang.cindex.CursorKind.CONSTRUCTOR : "Constructor",
    clang.cindex.CursorKind.DESTRUCTOR : "Destructor",
    clang.cindex.CursorKind.STRUCT_DECL : "Structure",
    clang.cindex.CursorKind.CLASS_DECL : "Class",
    clang.cindex.CursorKind.FIELD_DECL : "Field",
    clang.cindex.CursorKind.FUNCTION_TEMPLATE: "Function Template",
    clang.cindex.CursorKind.CLASS_TEMPLATE: "Class Template"
}

def MakeParser():
    parser = argparse.ArgumentParser(description='Parse a C++ header file and sync to a special'
                                                 ' jupyter notebook.')
    parser.add_argument('path',
                        help='Path to the header file.')
    parser.add_argument('out_path',
                    help='Path where the notebook will be written to.')
    parser.add_argument('build_path',
                    help='Path where the compilation_database.json is.')

    return parser.parse_args()


def ExtractTokensOfInterest(path, build_path):
    """
        Parse a single header file and extract all potentially relevant tokens
        (e.g. function and clas decls)
    """
    index = clang.cindex.Index.create()
    compdb = clang.cindex.CompilationDatabase.fromDirectory(build_path)
    commands = compdb.getCompileCommands(path)

    file_args = []
    for command in commands:
        for argument in command.arguments:
            file_args.append(argument)
    file_args = file_args[:-1]

    translation_unit = index.parse(path, file_args)
    token_dict = {}
    for token in translation_unit.get_tokens(extent=translation_unit.cursor.extent):
        if token.kind == clang.cindex.TokenKind.IDENTIFIER and token.cursor.kind in relevant_tokens:
            token_dict[token.cursor.displayname] = token

    return token_dict


def FunctionOrMethodToString(token):
    token_kind = PyClangToEnglish[token.cursor.kind]

    arg_tokens = token.cursor.get_arguments()
    arg_list = [""]
    for arg in arg_tokens:
        arg_list.append("Param: " + arg.type.spelling + " " + arg.displayname)

    args = "\n\n#### ".join(arg_list)
    message = \
        f'## {token_kind} {token.cursor.displayname}\n\n'\
        f'#### Brief :\n\n'\
        f'#### Description : \n\n'\
        f'{args}\n\n'\
        f'#### return {token.cursor.result_type.spelling}'

    return message


def StructOrclassToString(token):
    token_kind = PyClangToEnglish[token.cursor.kind]

    field_list = [""]
    children = token.cursor.get_children()
    for child in children:
        if child.displayname == "" or child.kind != clang.cindex.CursorKind.FIELD_DECL:
            continue
        field_list.append("Field: " + child.type.spelling + " " + child.displayname)

    message = \
        f'## {token_kind} {token.cursor.displayname}\n\n'\
        f'#### Brief :\n\n'\
        f'#### Description : \n\n'\

    return message


def FieldToString(token):
    token_kind = PyClangToEnglish[token.cursor.kind]

    field_list = [""]
    children = token.cursor.get_children()
    for child in children:
        if child.displayname == "" or child.kind != clang.cindex.CursorKind.FIELD_DECL:
            continue
        field_list.append("Field: " + child.type.spelling + " " + child.displayname)

    message = \
        f'### {token_kind} {token.cursor.displayname} ({token.cursor.type.spelling})\n\n'\
        f'#### Description : \n\n'\
        f'###### Field of {token.cursor.lexical_parent.displayname}\n\n'

    return message


token_kind_parser_mapping ={
    clang.cindex.CursorKind.FUNCTION_DECL : FunctionOrMethodToString,
    clang.cindex.CursorKind.CXX_METHOD : FunctionOrMethodToString,
    clang.cindex.CursorKind.CONSTRUCTOR : FunctionOrMethodToString,
    clang.cindex.CursorKind.DESTRUCTOR : FunctionOrMethodToString,
    clang.cindex.CursorKind.STRUCT_DECL : StructOrclassToString,
    clang.cindex.CursorKind.CLASS_DECL : StructOrclassToString,
    clang.cindex.CursorKind.FIELD_DECL : FieldToString,
    clang.cindex.CursorKind.FUNCTION_TEMPLATE : FunctionOrMethodToString,
    clang.cindex.CursorKind.CLASS_TEMPLATE : StructOrclassToString,
}


def StringifyTokensOfInterest(token_dict):
    """ Convert each token to a representative string template """
    strings = []
    for token_name in token_dict:
        token = token_dict[token_name]

        string = token_kind_parser_mapping[token.cursor.kind](token)
        string = string.replace(r'<', r'`<')
        string = string.replace(r'>', r">`")
        strings.append(string)
    return strings


def GetNameFromHeader(path):
    return Path(path).stem + ".ipynb"


def GetHeaderCellPrototypes(path, build_path):
    tokens = ExtractTokensOfInterest(path, build_path)
    strings = StringifyTokensOfInterest(tokens)

    cells = [nbf.v4.new_markdown_cell('# ' + os.path.basename(path))]
    for cell in strings:
        cells.append(nbf.v4.new_markdown_cell(cell))
        documented_object_name = cell.split('\n')[0].replace('#', '').strip()
        cells.append(nbf.v4.new_raw_cell(f"// {documented_object_name}\n\n{PLACE_HOLDER_MESSAGE}"))

    return cells


def CreateNotebookFromHeader(path, out_path, build_path):
    if '.hpp' not in path and '.h' not in path:
        print("Only works with header files.")
        return

    nb = nbf.v4.new_notebook()
    nb['cells'] = GetHeaderCellPrototypes(path, build_path)

    notebook_name = GetNameFromHeader(path)
    with open(os.path.join(out_path, notebook_name), 'w') as f:
        nbf.write(nb, f)


def main():
    args = MakeParser()

    clang.cindex.Config.set_library_file('/usr/lib/x86_64-linux-gnu/libclang-11.so.1')
    CreateNotebookFromHeader(args.path, args.out_path, args.build_path)

if __name__ == "__main__":
    main()