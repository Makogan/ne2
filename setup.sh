#!/bin/bash

sudo apt install --assume-yes python-is-python3
sudo apt install --assume-yes python3-pip
sudo apt install --assume-yes g++-11
sudo apt install --assume-yes clang-11
sudo apt install --assume-yes cmake


sudo update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-11 20
sudo update-alternatives --install /usr/bin/clang++ clang++ /usr/bin/clang++-11 20

curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash
sudo apt-get install git-lfs

git-lfs pull

sudo pip install -r Scripts/python-requirements

conan profile update settings.compiler.libcxx=libstdc++11 default
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys AA8452080E383F7E
conan install . -if build-debug -s build_type=Debug --build=missing
conan imports . -if build-debug

conan source . -if build-debug
conan build . -if build-debug
