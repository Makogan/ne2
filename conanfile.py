from conans import ConanFile, tools, Meson
from conans.tools import OSInfo
from conans.tools import os_info, SystemPackageTool
import subprocess

class ConanFileNeverEngine(ConanFile):
    name = "NeverEngine"
    version = "0.0.1"
    generators = "PkgConfigDeps"
    requires = [
        "eigen/3.4.0",
        # "libffi/3.4.2", # Not sure why but without this wayland causes a conflict.
        "glfw/3.3.7",
        "shaderc/2021.1",
        "freetype/2.10.4",
        # "harfbuzz/2.8.2",
        "vulkan-memory-allocator/3.0.1",
        # "gtest/1.10.0",
        # "benchmark/1.6.0",
        "tinygltf/2.5.0",
        "stb/20200203",
        "vulkan-headers/1.3.204.0",
        # "vulkan-validationlayers/1.3.211.0",
        "cereal/1.3.0",
        "spirv-cross/cci.20211113",
        "imgui/1.87"
    ]

    settings = {
        "os": None,
        "compiler" : None,
        "cppstd": None,
        "build_type" : None}

    keep_imports = True


    def layout(self):
        self.folders.build = "build-{}".format(str(self.settings.build_type).lower())
        self.folders.generators = "libraries/conan_packages"
        self.folders.imports = "."
        self.folders.source = "."


    def requirements(self):
        if os_info.is_linux:
            self.requires("wayland/1.19.0")
        self.UbuntuSetup()


    def imports(self):
        self.copy("*.cpp", dst=self.folders.source + "/Source/Modules/ModuleStorage/Imgui/ext/imgui", src=self.deps_cpp_info["imgui"].resdirs[0])
        self.copy("*.hpp", dst=self.folders.source + "/Source/Modules/ModuleStorage/Imgui/ext/imgui", src=self.deps_cpp_info["imgui"].resdirs[0])
        self.copy("*.h", dst=self.folders.source + "/Source/Modules/ModuleStorage/Imgui/ext/imgui", src=self.deps_cpp_info["imgui"].resdirs[0])


    def source(self):
        git = tools.Git(folder=self.folders.source)
        git.clone("https://gitlab.com/Makogan/ne2.git", "master")

    #     git = tools.Git(folder=f"{self.folders.build}/glTF-Sample-models")
    #     git.clone(
    #         "https://github.com/KhronosGroup/glTF-Sample-Models.git", "master")

        git = tools.Git(folder=f"./subprojects/SPIRV-Cross")
        git.clone(
            "https://github.com/KhronosGroup/SPIRV-Cross", "master")

        git = tools.Git(folder=f"Source/Modules/Profiler/tracy")
        git.clone(
            "https://github.com/wolfpld/tracy.git")



    def system_requirements(self):
        os_info = OSInfo()
        installer = SystemPackageTool()
        if os_info.is_linux:
            if os_info.linux_distro == 'ubuntu':
                installer.install('cmake')
                installer.install('clang-format')
                installer.install('libglfw3-dev')
                installer.install('ninja-build')
                installer.install('cppcheck')
                installer.install('doxygen')
                installer.install('gitlab-cli')
                installer.install('bear')
                installer.install('graphviz')
                installer.install('libvulkan1')
                installer.install('vulkan-tools')
                installer.install('libvulkan-dev')
                installer.install('git-lfs')
                installer.install('clang-format')
                installer.install('libclang-dev')


    def build(self):
        os_info = OSInfo()

        if self.settings.os != "Linux" and self.settings.os != "Windows":
            self.output.warn("NeverEngine only officially supports Linux and Windows.")
        if os_info.is_linux and os_info.linux_distro not in ['ubuntu']:
            self.output.warn("NeverEngine only officially supports Ubuntu Linux.")
        elif os_info.is_windows:
            self.output.info(f"Running on Windows {os_info.os_version}")
            self.WindowsSetup()
        else:
            message = f"We are on {self.settings.os} and the version/distro is {os_info.os_version}."
            self.output.success(message)

        meson = Meson(self)
        meson.configure(
            pkg_config_paths=[self.folders.generators],
            source_folder=self.folders.source,
            build_folder='.'
        )
        meson.build(args=['-j2'])


    def package(self):
        meson = Meson(self)
        meson.install()

        self.copy(pattern="*.dll", dst="bin", keep_path=False)
        self.copy(pattern="*.dylib", dst="lib", keep_path=False)
        self.copy(pattern="*.so*", dst="lib", keep_path=False)


    def package_info(self):
        self.cpp_info.libs = [
            "NeverEngine",
            "ne_core",
            "ne_loader",
            "ne_imgui",
            "ne_profiler",
            "animation",
            "camera",
            "ne_filesystem",
            "geometry",
            "image",
            "misc_utils",
            "ui_utils",
        ]


    def UbuntuSetup(self):
        subprocess.run("wget -qO - https://packages.lunarg.com/lunarg-signing-key-pub.asc | sudo apt-key add -".split())
        subprocess.run("sudo wget -qO /etc/apt/sources.list.d/lunarg-vulkan-1.3.211-focal.list https://packages.lunarg.com/vulkan/1.3.211/lunarg-vulkan-1.3.211-focal.list".split())
        subprocess.run("sudo apt update".split())
        subprocess.run("sudo apt install vulkan-sdk".split())


    def WindowsSetup(self):
        return
